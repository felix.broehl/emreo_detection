%
% Analyze saccade timings saved in .mat file
%

close all
clearvars

% get ExpList
USECOMP = 3;
ExpList
defdirs;

S = 2;
Experiment = 3;

% create save name for saccade parameter
sname = sprintf('FB03_S%02d_SaccParam.mat',S);

%% parameters

ARG_event.exp = Experiment;

ppd = 23;    % pixels per degree


%% --------------------------------------------------------------------------
% Load Event data
[EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath_log,ARG_event);

SaccadeTimings = EventLog{S}.SaccTime;
SaccPos = EventLog{S}.SaccPos;
ntrial = length(SaccadeTimings);


%%
% analyze timings here
% timings matrix [t_fix, t_ssa, t_tag, t_sea]
Sequence = EventLog{S}.EventLog(:,1);

% covert to angular distance
Sequence(Sequence>=6) = 8;
Sequence(Sequence>1 & Sequence<6) = 4;
Sequence(Sequence==1) = 0;
% label trial distance
Sequence = Sequence ./ 4;

% get timing
Sstart_latency = diff(SaccadeTimings(:,[1,2]),[],2);
S_duration = diff(SaccadeTimings(:,[2,4]),[],2);



%%
% plot timing data

% first, get the good saccades, defined by distance
min_distance = 2; % below 2 degrees
max_distance = 16; % above 18 degrees
StartPos = SaccPos(:,[1,2]);
EndPos = SaccPos(:,[3,4]);

% augment gaze vectors if any of the positions are not valid
validfix = [StartPos(:,1) == 0];
StartPos(validfix,:) = repmat([960, 540],sum(validfix),1);
validtag = [EndPos(:,1) == 0];
EndPos(validtag,:) = repmat([960, 540],sum(validtag),1);

gazeVectors = (EndPos - StartPos);
gazeVmag = sqrt(sum(gazeVectors.^2,2)) ./ ppd;

% reject faulty trials
bad_dist = (gazeVmag < min_distance | gazeVmag > max_distance);
bad_lat = (Sstart_latency > 0.4 | Sstart_latency < 0);
bad_dur = (S_duration > 0.15 | S_duration < 0);
goodSacc = true(ntrial,1);
goodSacc(bad_dist) = false;
goodSacc(bad_lat) = false;
goodSacc(bad_dur) = false;

TrlSequence = Sequence(goodSacc);


% plot distance sizes
figure
subplot(211);
histogram(gazeVmag,100);
title('raw saccade distances');

subplot(212);
histogram(gazeVmag(goodSacc),100);
title('cleaned saccade distances');

ckfiguretitle(['gaze vector magnitude' newline 'assert magnitude threshold']);



% saccade onset latency with respect to fixation dot presentation
all_latencies = Sstart_latency(goodSacc);
S_latency_small = all_latencies(TrlSequence==1);
S_latency_big =   all_latencies(TrlSequence==2);
S_latency_out =   all_latencies(TrlSequence>0);
S_latency_in =    all_latencies(TrlSequence==0);
% all latencies, between angular distance, between outward and inward
Latencies = [all_latencies; S_latency_small; S_latency_big; ...
    S_latency_out; S_latency_in];
g_label = [ones(size(all_latencies)); ones(size(S_latency_small))*2; ...
    ones(size(S_latency_big))*3; ones(size(S_latency_out))*4; ...
    ones(size(S_latency_in))*5];

figure
% plot data
subplot(211);
boxplot(Latencies, g_label);

title('saccade onset latencies');



% saccade duration, not considering angular distance 
all_duration = S_duration(goodSacc);
S_duration_small = all_duration(TrlSequence==1);
S_duration_big =   all_duration(TrlSequence==2);
S_duration_out =   all_duration(TrlSequence>=1);
S_duration_in =    all_duration(TrlSequence==0);
% all durations, between angular distance, between outward and inward
Durations = [all_duration; S_duration_small; S_duration_big; ...
    S_duration_out; S_duration_in];
g_label = [ones(size(all_duration)); ones(size(S_duration_small))*2; ...
    ones(size(S_duration_big))*3; ones(size(S_duration_out))*4; ...
    ones(size(S_duration_in))*5];

% plot data
subplot(212);
boxplot(Durations, g_label);

title('saccade durations');



% distributions ------------------------------------------------
legendtxt = {'all','inwards','outwards'};
figure
subplot(211);
h = histogram(all_latencies,150);
hold on
histogram(S_latency_in,h.BinEdges,'FaceColor','r');
histogram(S_latency_out,h.BinEdges,'FaceColor','g');

title('latency distribution');
legend(legendtxt,'Location','northwest');


subplot(212);
h = histogram(all_duration,150);
hold on
histogram(S_duration_in,h.BinEdges,'FaceColor','r');
histogram(S_duration_out,h.BinEdges,'FaceColor','g');

title('duration distribution');
legend(legendtxt,'Location','northwest');


fprintf(' - all saccades - \n');
fprintf('median latency:    %.3f s \n',median(S_latency_out));
fprintf('median duration:   %.3f s \n',median(S_duration_out));



%%
% plot gaze position on screen

figure

% get only the outward directed trials
StartPos = SaccPos(goodSacc,[1,2]);
EndPos = SaccPos(goodSacc,[3,4]);
StartPos = StartPos(TrlSequence>=1,:);
EndPos = EndPos(TrlSequence>=1,:);

subplot(211);
plot(StartPos(:,1),StartPos(:,2),'r+');
hold on
plot(EndPos(:,1),EndPos(:,2),'bo');

xlim([0,1920]); ylim([0,1080]);

title('gaze positions');


subplot(212);
gazeVectors = (EndPos - StartPos)';
% plot relative gaze vectors
plotv(gazeVectors);

%xlim([0,1920]); ylim([0,1080]);

title('gaze vectors');



return


%%
% take saccade parameters and save to file for subject

% create a structure for each participants
Parameters.SaccadeLatency = median(S_latency_out);
Parameters.SaccadeDuration = median(S_duration_out);

if exist(sname,2)
    % overwrite only if desired
    rep = input('Found file with same name. Do you want to overwrite? (y/n)','s');
    if isempty(rep)
        rep = 'n';
    end
    
    if strcmp(rep,'y')
        save(sname,'Parameters');
    else
        fprintf('\n did not overwrite! \n');
    end
else
    save(sname,'Parameters');
end




