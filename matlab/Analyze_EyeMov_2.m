function Analyze_EyeMov_2(S,Experiment)
%
% analyze second part of experiment
%
% the condition labels in the log files describe the following conditions:
%   1) 6�  right
%   2) 6�  left
%   3) 12� right
%   4) 12� left
%
% description of the analyses that we perform here:
%

close all

% get ExpList
USECOMP = 3
ExpList;
defdirs;


Subname = SubList(S).name

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% structure in which we save summary metrics from individual analyses
sumname = sprintf([datapath_res 'SumMetrics_FB03_S%02d_%d.mat'],S,Experiment);
SumMetrics = [];
SumMetrics.date = date;


%% ----------------------------------------------------------------------------
% generel paramters
ARG.resamplefs = globalP.ResampleFs;
ARG.zscore = false; % if whole data matrix should be zscored

% parameters for filtering and data inclusion - We expect EMREO around 35 Hz
ARG_ear.zscore = false;
ARG_ear.hpfilter   = 'no';  
ARG_ear.hpfreq     = 15;
ARG_ear.lpfilter   = 'no';  
ARG_ear.lpfreq     = 50;
ARG_ear.prestim    = -1.5;
ARG_ear.poststim   =  2.0;
ARG_ear.exp = Experiment;
ARG_ear.Ntrials = 144;

ARG_eog.hpfilter   = 'no';  
ARG_eog.hpfreq     = 15;
ARG_eog.lpfilter   = 'no';  
ARG_eog.lpfreq     = 80;
ARG_eog.exp = Experiment;


ARG_eye.Window = [-1.5,2.0]; % seconds
ARG_eye.Align = 'onset';
ARG_eye.ppd = globalP.ppd;
ARG_eye.exp = Experiment;
ARG_eye.resamplefs = ARG.resamplefs;

ARG_event.exp = Experiment;

Midpoint = globalP.centerpoint ./ globalP.ppd; % screen center


% save data to struct
SumMetrics.samplerate = ARG.resamplefs;


%% --------------------------------------------------------------------------
% Load Ear data
Ear_data = load_eardata_blocks(S,SubList,datapath_ear,ARG_ear);


% resample to new sample rate
cfg = [];
cfg.resamplefs = ARG.resamplefs;
Ear_data = ft_resampledata(cfg, Ear_data);

    

%% --------------------------------------------------------------------------
% Load EOG data - only if they were recorded
if SubList(S).eog
    EOG_data = load_eogdata_blocks(S,SubList,datapath_ear,ARG_eog);

    % resample to new sample rate
    cfg = [];
    cfg.resamplefs = ARG.resamplefs;
    EOG_data = ft_resampledata(cfg, EOG_data);
end

%% --------------------------------------------------------------------------
% Load Event data
[EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath_log,ARG_event);


%% --------------------------------------------------------------------------
% Load Eye data
[Sacc,TimingLog] = load_eyedata_blocks(S,SubList,datapath_eye,ARG_eye);


% put into one structure to clean trials
Eye_data.Window = ARG_eye.Window;
Eye_data.Sacc = Sacc;
Eye_data.TimingLog = TimingLog;



%% ---------------------------------------------------------------
% now we extract the Ear data locked to saccade onset / offset
% convert to matrix

Analysis_log = EventLog;
Analysis_log.TimingLog = TimingLog;
Analysis_log.Condition = EventLog.EventSequence(:,1);

% clean trials here
cfg = [];
cfg.fsample = ARG.resamplefs;
cfg.window = [-0.15 0.2]; % window in seconds to cut around saccade onset
cfg.TFR = true; % returns TRF Ear data 
cfg.TFR_window = [-0.6 0.8]; % in seconds - for TFR analysis
[Analysis_log,Ear_data_clean,Eye_data_clean,TrialStats] = clean_trialsFB03(cfg,Analysis_log,Ear_data,Eye_data);


% unpack eye data again
Sacc = Eye_data_clean.Sacc;
TimingLog = Eye_data_clean.TimingLog;
% find column based on label
VelColumn = strcmp(Sacc.label,'EYE_V');
AccColumn = strcmp(Sacc.label,'EYE_Acc');
Velocity = cellfun(@(x) x(VelColumn,:), Sacc.trial,'UniformOutput',false);
Velocity = cat(1,Velocity{:});
Acceleration = cellfun(@(x) x(AccColumn,:), Sacc.trial,'UniformOutput',false);
Acceleration = cat(1,Acceleration{:});

% unpack ear data
ALL_EAR = Ear_data_clean.ALL_EAR_ONSET;
tax_ear = Ear_data_clean.tax_ear;
ALL_EAR_TFR = Ear_data_clean.ALL_EAR_TFR;
tax_tfr = Ear_data_clean.tax_tfr;

% zscore if desired
if ARG.zscore
    ALL_EAR = (ALL_EAR - mean(ALL_EAR(:))) / std(ALL_EAR(:));
    ALL_EAR_TFR = (ALL_EAR_TFR - mean(ALL_EAR_TFR(:))) / std(ALL_EAR_TFR(:));
    emreounit = 'SD';
else
    % convert from uV to mV
    ALL_EAR = ALL_EAR ./ 1000; 
    ALL_EAR_TFR = ALL_EAR_TFR ./ 1000;
    emreounit = 'mV';
end

% unpack stats
Ngood_trials = TrialStats.Ngood_trials;


% save to metrics
SumMetrics.Trials.good_trials = TrialStats.good_trials;
SumMetrics.Trials.Ngood_trials = Ngood_trials;
SumMetrics.Trials.ntrial = TrialStats.ntrial;



% get sound onset parameters
sysdelay = ARG_Matlab.sysdelay; % sound card delay (only subtract with this subjects data)
try
    OnsetLatency = ARG_Matlab.OnsetLatency;
catch
    % compute framing time points
    presacc = ARG_Matlab.SaccDelay - (1/2)*ARG_Matlab.SaccDurat;
    dursacc = ARG_Matlab.SaccDelay + (1/2)*ARG_Matlab.SaccDurat;
    possacc = ARG_Matlab.SaccDelay + (3/2)*ARG_Matlab.SaccDurat;
    OnsetLatency = [presacc dursacc possacc] + sysdelay; 
    warning('OnsetLatency not saved: recreating from loaded parameters');
end

% add reaction time to Analysis_log
EventPoint = Analysis_log.EventSequence(:,4);
ontime = OnsetLatency(EventPoint)';
RT = Analysis_log.EventTiming(:,[3,end]) + [ontime zeros(size(ontime))];
Analysis_log.RT = diff(RT,[],2) .* 1000; % convert to ms



% save to metrics
SumMetrics.Trials.good_trials = TrialStats.good_trials;
SumMetrics.Trials.Ngood_trials = Ngood_trials;
SumMetrics.Trials.ntrial = TrialStats.ntrial;

fprintf('\n');
fprintf('Retaining %d of %d trials (%.2f %%) \n\n',Ngood_trials,TrialStats.ntrial,Ngood_trials/TrialStats.ntrial);


%% Start Analysis here

% plot saccade start and end positions
ntrial = size(Sacc.trial,2);

figure
hold on
iter = 1;
for itrial = 1:ntrial
    if isnan(TimingLog(itrial,1))
        continue
    end
    % get start and end position
    Pos0(itrial,:) = sq(Sacc.trial{itrial}([2,3],TimingLog(itrial,1)))' - Midpoint;
    Pos1(itrial,:) = sq(Sacc.trial{itrial}([2,3],TimingLog(itrial,2)))' - Midpoint;
    
    iter = iter + 1;
end
% plot positions
plot(Pos0(:,1),Pos0(:,2),'kx');
plot(Pos1(:,1),Pos1(:,2),'ro');
% title, axes
title('Fixations (x) and saccadic (o) end points')
axis([-16 16 -16 16]) % in degrees
xlabel('degrees');
ylabel('degrees');
header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fprintf('\n');
fprintf('Found good saccades in %d of %d trials\n\n',iter,ntrial);

fname = sprintf([figurepath Subname 'E%d' '_Saccade.png'],Experiment);
print('-dpng',fname);



%% -----------------------------------------------------------------
% display mean EAR trace per condition


figure('Position',[642 130 922 419]); 

% left and right
k=[1:2:12]; % all odd condition numbers
curr_cond = find(any(Analysis_log.Condition==k,2));
nright = length(curr_cond);
tmp_right = ALL_EAR((curr_cond),:);
rightmean = mean(tmp_right);
fb_errorshade(tax_ear,mean(tmp_right),sem(tmp_right),'b');
hold on

k=[0:2:12]; % all even condition numbers
curr_cond = find(any(Analysis_log.Condition==k,2));
nleft = length(curr_cond);
tmp_left = ALL_EAR((curr_cond),:);
leftmean = mean(tmp_left);
fb_errorshade(tax_ear,mean(tmp_left),sem(tmp_left),'r');
xlabel('time (ms)');
header = sprintf('Ear: ipsilateral(b, n=%d)/contralateral(r, n=%d)',nright,nleft);
title(header);
grid on
header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);


fname = sprintf([figurepath Subname 'E%d' '_EMREO.png'],Experiment);
print('-dpng',fname);


% save data to structure
SumMetrics.EMREO.label = {'ipsilateral','contralateral'};
SumMetrics.EMREO.time = tax_ear;
SumMetrics.EMREO.samplerate = ARG.resamplefs;
SumMetrics.EMREO.mean = expand_dims([mean(tmp_right); mean(tmp_left)], 1); % add singleton dim first
% this data is needed for thesting whether a spatial cue by itself shape
% the EMREO
SumMetrics.EMREO.data = {ALL_EAR};
SumMetrics.EMREO.condition = {Analysis_log.Condition};


%% Power spectrum Ear data (includes full time window)
% analyze power spectrum of ear data

cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'pow';
cfg.taper = 'hanning';
cfg.pad = 'nextpow2';
cfg.foilim = [2 128];
cfg.toi = 0;

% freq analysis
freq = ft_freqanalysis(cfg,Ear_data);

% plot results
figure('Position',[170 586 1070 392]);
subplot(121);
plot(freq.freq,freq.powspctrm);
xlim([0,45]);
xlabel('Frequency (Hz)');
ylabel('Power');
subplot(122);
plot(freq.freq,freq.powspctrm);
header = sprintf('%s_%d EMREO Ear data spectrum',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_EMREO_spectrum.png'],Experiment);
print('-dpng',fname);


%% TFR analysis EMREO

% time-freq windoe to compute average power
temp_win = globalP.EMREOwindow ./ 1000; % convert from ms to s
freq_win = globalP.EMREOfreqwindow;

% define trials for left and right saccades
curr_cond = [];
% left
k=[0:2:12]; % all even condition numbers
curr_cond{1} = find(any(Analysis_log.Condition==k,2));

% right
k=[1:2:12]; % all uneven condition numbers
curr_cond{2} = find(any(Analysis_log.Condition==k,2));

% left right condition label
condlabel = {'contralateral','ipsilateral'};

figure('Position',[321 542 1204 371]);
for icondx = 1:length(curr_cond)
    Ear_TFR = Ear_data;
    Ear_TFR.trialinfo = 1:length(curr_cond{icondx});
    Ear_TFR.trial = mat2cell(ALL_EAR_TFR(curr_cond{icondx},:),ones(1,length(curr_cond{icondx})),length(tax_tfr))';
    Ear_TFR.time = repmat({tax_tfr ./ 1000}, [length(curr_cond{icondx}),1])';

    % set TFR parameters
    cfg = [];
    cfg.output = 'pow';
    cfg.method = 'mtmconvol';
    cfg.taper = 'hanning';
    cfg.foi = [16:1:44];
    cfg.t_ftimwin = 7./cfg.foi;
    cfg.toi = [tax_tfr(1)/1000 : 0.01 : tax_tfr(end)/1000];
    
    % analysis
    freq_ear = ft_freqanalysis(cfg,Ear_TFR);
    % average power
    t_win = temp_win(1) < freq_ear.time & temp_win(2) > freq_ear.time;
    f_win = freq_win(1) < freq_ear.freq & freq_win(2) > freq_ear.freq;
    temp_spec_density = mean(flatten(freq_ear.powspctrm(1,f_win,t_win)));
    
    % plot 
    p_cfg = [];
    p_cfg.baseline = [-0.20 -0.10];
    p_cfg.channel = 'Erg1';
    p_cfg.xlim = [tax_ear(1) tax_ear(end)]./1000;
    cfg.title = condlabel{icondx};
    subplot(1,2,icondx);
    ft_singleplotTFR(p_cfg,freq_ear);
end

header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_EMREO_TRF.png'],Experiment);
print('-dpng',fname);



%% Sound onset relative to saccade 
% find the sound onset relative to the saccade
% plot the distribution of all three onset points


% get index where sounds where expected to be played
EventPoint = Analysis_log.EventSequence(:,4);
ontime = OnsetLatency(EventPoint)' .* 1000;

% get saccade onset time and subtract that from sound time
sacconset = Analysis_log.TimingLog(:,3) * (1000 / ARG.resamplefs); % <- this is not in ms
saccoffset = Analysis_log.TimingLog(:,4) * (1000 / ARG.resamplefs);
relativeOnTime = ontime - sacconset;
relativeOffTime = ontime - saccoffset;


% get velocity data
tax_eye = [ARG_eye.Window(1):1/ARG.resamplefs:ARG_eye.Window(2)-1/ARG.resamplefs] .* 1000;
tax_window = [-250 250];
for t = 1:size(Velocity,1)
    trial_ind = find([tax_eye>tax_window(1) & tax_eye<tax_window(2)]) + TimingLog(t,2)-TimingLog(t,1);
    vel_off(t,:) = Velocity(t,trial_ind);
end
% convert to pixel per degree
vel = Velocity;
vel_off = vel_off;
meanvel = mean(vel,1,'omitnan') ./ max(mean(vel,1,'omitnan'));


% plot mean EMREO and superimpose saccade velocity
figure('Position',[317 104 998 874]);
subplot(311);
plot(tax_ear,rightmean ./ max(abs(rightmean)),'b-');
hold on
plot(tax_ear,leftmean ./ max(abs(leftmean)),'r-');
plot(tax_eye,meanvel ./ (max(meanvel)*0.9),'k-.');
xlim([-150,200]);
ylim([-1.2, 1.2]);

legend({'right','left','mean velo.'});

% plot all sound onsets colored for aimed before, during or after saccade
subplot(312);
hold on
histogram(relativeOnTime(EventPoint==1),100,'FaceColor','r');
histogram(relativeOnTime(EventPoint==2),100,'FaceColor','g');
histogram(relativeOnTime(EventPoint==3),100,'FaceColor','b');
xlim([-150,200]);

legend({'before','during','after'});
title('sound onset distribution, relative to saccade onset');

% plot all sound onsets relative to saccade offset
subplot(313);
hold on
histogram(relativeOffTime(EventPoint==1),100,'FaceColor','r');
histogram(relativeOffTime(EventPoint==2),100,'FaceColor','g');
histogram(relativeOffTime(EventPoint==3),100,'FaceColor','b');
xlim([-200,150]);

legend({'before','during','after'});
title('sound onset distribution, relative to saccade offset');


header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_SaccTimeDist.png'],Experiment);
print('-dpng',fname);


% save data to structure
SumMetrics.Onsets.VelOn = expand_dims(mean(vel,1,'omitnan'),1);
SumMetrics.Onsets.VelOff = expand_dims(mean(vel_off,1,'omitnan'),1);
SumMetrics.Onsets.tax_eye = expand_dims(tax_eye,1);
SumMetrics.Onsets.tax_window = expand_dims(tax_window,1);
SumMetrics.Onsets.OnTime = {relativeOnTime};
SumMetrics.Onsets.OffTime = {relativeOffTime};
SumMetrics.Onsets.EventPoint = {EventPoint};



%%

% determine where sounds were actually presented
% 0) before 1) during 2) after
sum_frame = sum(ontime > [sacconset saccoffset],2);

% check in what time window they fell
beforeSacc = sum(sum_frame(EventPoint==1) == [0 1 2]);
duringSacc = sum(sum_frame(EventPoint==2) == [0 1 2]);
afterSacc  = sum(sum_frame(EventPoint==3) == [0 1 2]);

OnsetStats = [beforeSacc; duringSacc; afterSacc];
OnsetStats = [sum(OnsetStats,1); OnsetStats];

fprintf('\n%d total trials \n',sum(OnsetStats(:)));
fprintf('scheduled trials: \n before: %d    during: %d    after: %d \n\n',sum(OnsetStats(2:end,:),2));
fprintf('actually accoured trials: \n before: %d    during: %d    after: %d \n',sum(OnsetStats(2:end,:),1));


% consider the sound delay from the PC and if this is already included here

% plot where sound onsets land
figure('Position',[680 200 613 778])
bh = bar(OnsetStats','grouped');
bh(1).FaceColor = 'k';
bh(2).FaceColor = 'r';
bh(3).FaceColor = 'g';
bh(4).FaceColor = 'b';
xticklabels({'before','during','after'});
xlabel('actual sound onset bin');
legendtxt = {'sum','before','during','after'};
legend(legendtxt);
title('sound onsets actually occured');

header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_SoundOnsetRelSacc.png'],Experiment);
print('-dpng',fname);


% save data to struct
SumMetrics.Onsets.OnsetStats   = expand_dims(OnsetStats,1);
SumMetrics.Onsets.OnsetLatency = OnsetLatency;


%% Behavior
%
% for each thing to look at, find the logical vector indexing the trials we
% seek (e.g. all trials where sounds were presented before the saccade
% target) and then get the response and reaction time (RT) in those trials
%
% then we analyze and plot those findings
%

% bar graph presets
cfg = [];
cfg.connect = false;
cfg.jitterrange = 0.2;
cfg.MarkerSize = 5;

% get trialplayed flag, i.e. only trials where sound was actually played
trialplayed = ~Analysis_log.EventSequence(:,6);

% get overall behavior
resp_code = Analysis_log.EventLog(:,4);
Ncorrect = sum(resp_code);

% detection sensitivity d prime
resp = Analysis_log.EventLog(:,3);
resp(~isnan(resp)) = 1;
resp(isnan(resp)) = 0;
HitTrials = resp & trialplayed;
FATrials = resp & ~trialplayed;
HR = sum(resp & trialplayed) / sum(trialplayed);
FAR = sum(resp & ~trialplayed) / sum(~trialplayed);
dprime = ck_stat_signalTheory(HR,FAR);


fprintf('\n\n');
fprintf('%d out of %d trials (%.2f %%) were answered correctly \n\n',Ncorrect,Ngood_trials, Ncorrect/Ngood_trials);



%--------------------------------------------------------
% get behavior for trials divided by (actual) sound onset
corr_before = (sum_frame==0);
corr_during = (sum_frame==1);
corr_after  = (sum_frame==2);
corr_timing = {corr_before, corr_during, corr_after};
% response accuracy
resp_before = sum(resp_code(corr_before)) / sum(corr_before);
resp_during = sum(resp_code(corr_during)) / sum(corr_during);
resp_after = sum(resp_code(corr_after)) / sum(corr_after);
resp_timing = {resp_before, resp_during resp_after};
% response sensitivity (d prime)
HR_before = sum(HitTrials(corr_before))/sum(corr_before);
HR_during = sum(HitTrials(corr_during))/sum(corr_during);
HR_after  = sum(HitTrials(corr_after))/sum(corr_after);
FAT_before = sum(FATrials(corr_before))/sum(corr_before);
FAT_during = sum(FATrials(corr_during))/sum(corr_during);
FAT_after  = sum(FATrials(corr_after))/sum(corr_after);
if FAT_before == 0
    FAT_before = 1/(2 * sum(corr_before));
end
if FAT_during == 0
    FAT_during = 1/(2 * sum(corr_during));
end
if FAT_after == 0
    FAT_after = 1/(2 * sum(corr_after));
end
% augment FAT if participants were very conservative
% signal detection theory
[dprime_before,~,crit_before] = ck_stat_signalTheory(HR_before, FAT_before);
[dprime_during,~,crit_during] = ck_stat_signalTheory(HR_during, FAT_during);
[dprime_after, ~,crit_after ] = ck_stat_signalTheory(HR_after , FAT_after);

figure('Position',[843 379 1017 415]);
cfg.ax = subplot(121);
hold on
for itime = 1:size(resp_timing,2)
    cfg.XAxis = itime;
    % only trials where sound was played and an answer was given
    resp_time = corr_timing{itime};
    trlidx = all([resp_time resp_code trialplayed],2);
    group_RT = Analysis_log.RT(trlidx);
    Dist_RT_median(itime) = median(group_RT);
    barwithdots(cfg, group_RT);
end
box on
cfg.ax.XAxis.TickValues = [1:size(resp_timing,2)];
cfg.ax.XAxis.TickLabels = {'before','during','after'};
xlabel('fixed onset groups');
ylabel('reaction time (ms)');
title('sound onset - RT');

% regression plot of onset time relative to saccade onset (x) against 
% the log reaction time relative to the true sound onset (y)
trlidx = all([resp_code trialplayed],2);
TrialOnTime = relativeOnTime(trlidx); 
TrialReactionTime = Analysis_log.RT(trlidx);
X = TrialOnTime ./ 1000; % convert to seconds
Y = log(TrialReactionTime);
N = length(X);
beta = [ones(size(X)) X]\Y;
yhat = [ones(size(X)) X] * beta;
res  = Y - yhat; % residuals


% log likelihood - LinReg
std_res = std(res);
LL_lin = -N*log(2*pi)/2-N*log(std_res^2)/2-(1/(2*std_res^2))*(yhat-Y)'*(yhat-Y);
n_linfit = length(beta);


% quadratic function handle
quadfunc = @(p,x) p(1).*x.^2+p(2).*x+p(3);
beta_init = [0 0 0];
% curve fit, returns log likelihood
mdl = fitnlm(X,Y,quadfunc,beta_init);
LL_quad = mdl.LogLikelihood;
n_quadfit = size(mdl.Coefficients,1);

% BIC for both models and bayes factor
BIC_l = n_linfit *log(N) - 2*LL_lin;
BIC_q = n_quadfit*log(N) - 2*LL_quad;
BF = exp((BIC_q - BIC_l)/2);
% -> the smaller BIC the better, therefore if BIC_lin < BIC_quad, then diff 
% is positive and we have a higher BF. then we prefer the linear model


% plot trial points and regression line
ax = subplot(122);
plot(X,Y,'ro');
hold on
tx = [-200:250]' ./ 1000;
ylin = sum([ones(size(tx)) tx] * beta, 2);
plot(tx,ylin);
yquad = quadfunc(mdl.Coefficients.Estimate,tx);
plot(tx,yquad,'g-');
betatext = sprintf(['offset = %.3f' newline 'beta = %.4f'],beta);
xmin = ax.XAxis.Limits(1) * 0.90;
xmax = ax.XAxis.Limits(2) * 0.80;
ytop = ax.YAxis.Limits(2) * 0.95;
text(xmin,ytop,betatext);
BFtxt = sprintf(['BIC_{quad} - BIC_{lin}' newline 'delta BF = %.3f'],BF);
text(xmax,ytop,BFtxt,'HorizontalAlignment','right');
xlim([-0.2,0.25]);
xlabel('relative onset time (s)');
ylabel('log[reaction time (ms)]');
title('sound onset_{rel} - RT');

header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_ReactionTimesSoundOnset.png'],Experiment);
print('-dpng',fname);


fprintf('response relative to sound onset group\n');
fprintf('        n=%d            n=%d            n=%d\n',...
    sum(corr_before), sum(corr_during), sum(corr_after));
fprintf('before: %.2f    during: %.2f     after: %.2f\n\n',...
    resp_before, resp_during, resp_after);



%--------------------------------------------------------
% get behavior for trials divided by saccade direction
corr_right = [1:2:12]; % all odd conditions
corr_left  = [2:2:12]; % all even conditions
sacc_right  = any(Analysis_log.Condition==corr_right,2);
sacc_left   = any(Analysis_log.Condition==corr_left,2);
% response accuracy
resp_right = sum(resp_code(sacc_right)) / sum(sacc_right);
resp_left = sum(resp_code(sacc_left)) / sum(sacc_left);
% response sensitivity (d prime)
HR_right = sum(HitTrials(sacc_right))/sum(sacc_right);
HR_left   = sum(HitTrials(sacc_left))/sum(sacc_left);
FAT_right = sum(FATrials(sacc_right))/sum(sacc_right);
FAT_left  = sum(FATrials(sacc_left))/sum(sacc_left);
if FAT_right == 0
    FAT_right = 1/(2 * sum(sacc_right));
end
if FAT_left == 0
    FAT_left = 1/(2 * sum(sacc_left));
end
[dprime_right,~,crit_right] = ck_stat_signalTheory(HR_right,FAT_right);
[dprime_left,~,crit_left]  = ck_stat_signalTheory(HR_left, FAT_left);


% start figure for the following plots here
figure('Position',[255 124 923 833]);

cfg.ax = subplot(221);
% left saccades
cfg.XAxis = 1;
% only trials where sound was played and an answer was given
trlidx = all([sacc_left resp_code trialplayed],2);
RT_left = Analysis_log.RT(trlidx);
barwithdots(cfg, RT_left);
hold on
% right saccades
cfg.XAxis = 2;
% only trials where sound was played and an answer was given
trlidx = all([sacc_right resp_code trialplayed],2);
RT_right = Analysis_log.RT(trlidx);
barwithdots(cfg, RT_right);

box on
cfg.ax.XAxis.TickValues = [1:2];
cfg.ax.XAxis.TickLabels = {'left','right'};
xlabel('saccade target direction');
ylabel('reaction time (ms)');


fprintf('response relative to saccade direction\n');
fprintf('      n=%d            n=%d\n',...
    sum(sacc_left), sum(sacc_right));
fprintf('left: %.2f    right: %.2f\n\n',...
    resp_left, resp_right);



%--------------------------------------------------------
% get behavior for trials divided by absolute saccade amplitude
corr_sixdegrees    = flatten([0 4 8] + [1; 2])';
corr_twelvedegrees = flatten([0 4 8] + [3; 4])';
idx_six            = any(Analysis_log.Condition==corr_sixdegrees,2);
idx_twelve         = any(Analysis_log.Condition==corr_twelvedegrees,2);
% response accuracy
resp_six = sum(resp_code(idx_six)) / sum(idx_six);
resp_twelve = sum(resp_code(idx_twelve)) / sum(idx_twelve);
% response sensitivity (d prime)
HR_six    = sum(HitTrials(idx_six))/sum(idx_six);
HR_twelve = sum(HitTrials(idx_twelve))/sum(idx_twelve);
FAT_six     = sum(FATrials(idx_six))/sum(idx_six);
FAT_twelve  = sum(FATrials(idx_twelve))/sum(idx_twelve);
if FAT_six == 0
    FAT_six = 1/(2 * sum(idx_six));
end
if FAT_twelve == 0
    FAT_twelve = 1/(2 * sum(idx_twelve));
end
[dprime_six,~,crit_six]       = ck_stat_signalTheory(HR_six, FAT_six);
[dprime_twelve,~,crit_twelve] = ck_stat_signalTheory(HR_twelve, FAT_twelve);

% reaction time
trlidx = all([idx_six resp_code trialplayed],2);
RT_six = Analysis_log.RT(trlidx);
trlidx = all([idx_twelve resp_code trialplayed],2);
RT_twelve = Analysis_log.RT(trlidx);

cfg.ax = subplot(222);
% left saccades
cfg.XAxis = 1;
% only trials where sound was played and an answer was given
barwithdots(cfg, RT_six);
hold on
% right saccades
cfg.XAxis = 2;
% only trials where sound was played and an answer was given
barwithdots(cfg, RT_twelve);

box on
cfg.ax.XAxis.TickValues = [1:2];
cfg.ax.XAxis.TickLabels = {'6','12'};
xlabel('saccade target amplitude (�)');
ylabel('reaction time (ms)');

fprintf('response relative to absolute saccade amplitude\n');
fprintf('      n=%d            n=%d\n',...
    sum(idx_six), sum(idx_twelve));
fprintf('six:  %.2f%%    twelve: %.2f%%\n\n',...
    resp_six, resp_twelve);



%--------------------------------------------------------
% get behavior for trials divided by sound location
snd_right = Analysis_log.Sequence(:,5)==2; % based on channel index
snd_left  = Analysis_log.Sequence(:,5)==4;
% response accuracy
resp_sndR = sum(resp_code(snd_right)) / sum(snd_right);
resp_sndL = sum(resp_code(snd_left)) / sum(snd_left);
% response sensitivity (d prime)
HR_sndR = sum(HitTrials(snd_right))/sum(snd_right);
HR_sndL = sum(HitTrials(snd_left))/sum(snd_left);
FAT_sndR     = sum(FATrials(snd_right))/sum(snd_right);
FAT_sndL  = sum(FATrials(snd_left))/sum(snd_left);
if FAT_sndR == 0
    FAT_sndR = 1/(2 * sum(snd_right));
end
if FAT_sndL == 0
    FAT_sndL = 1/(2 * sum(snd_left));
end
[dprime_sndR,~,crit_sndR] = ck_stat_signalTheory(HR_sndR, FAT_sndR);
[dprime_sndL,~,crit_sndL] = ck_stat_signalTheory(HR_sndL, FAT_sndL);
% reaction time
trlidx = all([snd_left resp_code trialplayed],2);
RT_sndL = Analysis_log.RT(trlidx);
trlidx = all([snd_right resp_code trialplayed],2);
RT_sndR = Analysis_log.RT(trlidx);

cfg.ax = subplot(223);
% left saccades
cfg.XAxis = 1;
% only trials where sound was played and an answer was given
barwithdots(cfg, RT_sndL);
hold on
% right saccades
cfg.XAxis = 2;
% only trials where sound was played and an answer was given
barwithdots(cfg, RT_sndR);

box on
cfg.ax.XAxis.TickValues = [1:2];
cfg.ax.XAxis.TickLabels = {'left','right'};
xlabel('sound location');
ylabel('reaction time (ms)');

fprintf('response relative to sound location\n');
fprintf('      n=%d            n=%d\n',...
    sum(snd_left), sum(snd_right));
fprintf('left:  %.2f    right: %.2f\n\n',...
    resp_sndL, resp_sndR);



%--------------------------------------------------------
% get behavior for trials divided by location congruency
idx_congruent = (snd_right & sacc_right) | (snd_left & sacc_left);
idx_incongruent = ~idx_congruent;
% response accuracy
resp_congruent = sum(resp_code(idx_congruent)) / sum(idx_congruent);
resp_incongruent = sum(resp_code(idx_incongruent)) / sum(idx_incongruent);
% response sensitivity (d prime)
HR_congruent = sum(HitTrials(idx_congruent))/sum(idx_congruent);
HR_incongruent = sum(HitTrials(idx_incongruent))/sum(idx_incongruent);
FAT_congruent   = sum(FATrials(idx_congruent))/sum(idx_congruent);
FAT_incongruent = sum(FATrials(idx_incongruent))/sum(idx_incongruent);
if FAT_congruent == 0
    FAT_congruent = 1/(2 * sum(idx_congruent));
end
if FAT_incongruent == 0
    FAT_incongruent = 1/(2 * sum(idx_incongruent));
end
[dprime_congruent,~,crit_congruent] = ck_stat_signalTheory(HR_congruent, FAT_congruent);
[dprime_incongruent,~,crit_incongruent] = ck_stat_signalTheory(HR_incongruent, FAT_incongruent);
% reaction time
trlidx = all([idx_congruent resp_code trialplayed],2);
RT_cong = Analysis_log.RT(trlidx);
trlidx = all([idx_incongruent resp_code trialplayed],2);
RT_inco = Analysis_log.RT(trlidx);

cfg.ax = subplot(224);
% left saccades
cfg.XAxis = 1;
% only trials where sound was played and an answer was given
barwithdots(cfg, RT_cong);
hold on
% right saccades
cfg.XAxis = 2;
% only trials where sound was played and an answer was given
barwithdots(cfg, RT_inco);

box on
cfg.ax.XAxis.TickValues = [1:2];
cfg.ax.XAxis.TickLabels = {'congruent','incongruent'};
xlabel('saccade/sound location congruency');
ylabel('reaction time (ms)');

fprintf('response relative to congruent direction\n');
fprintf('      n=%d            n=%d\n',...
    sum(idx_congruent), sum(idx_incongruent));
fprintf('congruent:  %.2f    incongruent: %.2f\n\n',...
    resp_congruent, resp_incongruent);


% save plot
header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_ReactionTimes.png'],Experiment);
print('-dpng',fname);


% save data to struct
SumMetrics.Behavior.Overall = Ncorrect;
% sound onset groups
SumMetrics.Behavior.SoundOnset.ReactionTime = {TrialReactionTime};
SumMetrics.Behavior.SoundOnset.OnTime = {TrialOnTime};
SumMetrics.Behavior.SoundOnset.Label = {'before','during','after'};
SumMetrics.Behavior.SoundOnset.Resp = [resp_before resp_during resp_after];
SumMetrics.Behavior.SoundOnset.Dprime = [dprime_before dprime_during dprime_after];
SumMetrics.Behavior.SoundOnset.SigDetBias = [crit_before crit_during crit_after];
SumMetrics.Behavior.SoundOnset.RT = Dist_RT_median;
SumMetrics.Behavior.SoundOnset.LinCoeff = beta';
SumMetrics.Behavior.SoundOnset.QuadCoeff = mdl.Coefficients.Estimate';
SumMetrics.Behavior.SoundOnset.BIClin  = BIC_l;
SumMetrics.Behavior.SoundOnset.BICquad = BIC_q;
SumMetrics.Behavior.SoundOnset.BayesFactor = BF;
% saccade direction, left and right, always recorded in the right ear
SumMetrics.Behavior.SaccDir.Name = {'Saccade direction'};
SumMetrics.Behavior.SaccDir.Label = {'contralateral','ipsilateral'};
SumMetrics.Behavior.SaccDir.Resp = [resp_left resp_right];
SumMetrics.Behavior.SaccDir.Dprime = [dprime_left dprime_right];
SumMetrics.Behavior.SaccDir.SigDetBias = [crit_left crit_right];
SumMetrics.Behavior.SaccDir.RT = [median(RT_left) median(RT_right)];
% saccade amplitude
SumMetrics.Behavior.SaccAmp.Name = {'Saccade amplitude'};
SumMetrics.Behavior.SaccAmp.Label = {'six','twelve'};
SumMetrics.Behavior.SaccAmp.Resp = [resp_six resp_twelve];
SumMetrics.Behavior.SaccAmp.Dprime = [dprime_six dprime_twelve];
SumMetrics.Behavior.SaccAmp.SigDetBias = [crit_six crit_twelve];
SumMetrics.Behavior.SaccAmp.RT = [median(RT_six) median(RT_twelve)];
% sound diretion
SumMetrics.Behavior.SndDir.Name = {'Sound location'};
SumMetrics.Behavior.SndDir.Label = {'left','right'};
SumMetrics.Behavior.SndDir.Resp = [resp_sndL resp_sndR];
SumMetrics.Behavior.SndDir.Dprime = [dprime_sndL dprime_sndR];
SumMetrics.Behavior.SndDir.SigDetBias = [crit_sndL crit_sndR];
SumMetrics.Behavior.SndDir.RT = [median(RT_sndL) median(RT_sndR)];
% saccade-sound diretion congruency
SumMetrics.Behavior.SaccSndDir.Name = {'Saccade-sound congruency'};
SumMetrics.Behavior.SaccSndDir.Label = {'congruent','incongruent'};
SumMetrics.Behavior.SaccSndDir.Resp = [resp_congruent resp_incongruent]; 
SumMetrics.Behavior.SaccSndDir.Dprime = [dprime_congruent dprime_incongruent]; 
SumMetrics.Behavior.SaccSndDir.SigDetBias = [crit_congruent crit_incongruent]; 
SumMetrics.Behavior.SaccSndDir.RT = [median(RT_cong) median(RT_inco)];




%% Behavior and EMREO variance
% here we take each condition and compute the mean EMREO, then we leave out
% every EMREO successively and compute their contribution to the variance. 
% We do this the same way for the behavior and correlate within
% participants.
%
% For this we focus on sounds that were played during the EMREO only


% condition label and order
degrees = Analysis_log.Degrees; % index corresponds to the condition labels
SaccPos = Analysis_log.SaccPosCondition;
ncond = max(SaccPos);
[~,sortI] = sort(degrees);

% define temporal bounadries for statistical analysis
stat_bound = globalP.EMREOwindow;
stat_win = (tax_ear > stat_bound(1) & tax_ear < stat_bound(2));

% short first peak boundary
peak_bound = globalP.FirstPeakWin;
peak_win = (tax_ear > peak_bound(1) & tax_ear < peak_bound(2));

% get all trials where sounds were played in that window
trials_during_EMREO = (relativeOnTime > stat_bound(1) & relativeOnTime < stat_bound(2));

% trials where sound was played
trialplayed = ~Analysis_log.EventSequence(:,6);

% participants response vector
resp_code = Analysis_log.EventLog(:,4);
resp = Analysis_log.EventLog(:,3);
resp(~isnan(resp)) = 1;
resp(isnan(resp)) = 0;

% go through conditions from far left to far right
iter = 1;
for icond = sortI
    % we compute EMREO behavior metrics for all trials within a condition
    cond_ind = (SaccPos == icond) & trials_during_EMREO;
    % shuffle indices, then take minimum available trials
    ind_true = fb_shuffle_array(find(cond_ind & resp_code));
    ind_false = fb_shuffle_array(find(cond_ind & ~resp_code));
    Ntrue(iter) = length(ind_true);
    Nfalse(iter) = length(ind_false);
    Nmin = min(length(ind_true),length(ind_false));
    
    % compute EMREO metrics across mean trials
    ALL_EAR_TRUE  = mean(ALL_EAR(ind_true(1:Nmin),:),1);
    ALL_EAR_FALSE = mean(ALL_EAR(ind_false(1:Nmin),:),1);
    % compute hilbert here, more robust on longer window
    HILB_EAR_TRUE  = abs(hilbert(ALL_EAR_TRUE));
    HILB_EAR_FALSE = abs(hilbert(ALL_EAR_FALSE));
    % mean power
    TrueMeanPower(iter)  = mean(HILB_EAR_TRUE(:,stat_win));
    FalseMeanPower(iter) = mean(HILB_EAR_FALSE(:,stat_win));
    
    % save EMREO trajectories to investigate later with all participants
    RAW_EMREO(iter,1,:) = ALL_EAR_TRUE;
    RAW_EMREO(iter,2,:) = ALL_EAR_FALSE;
    HILB_EMREO(iter,1,:) = HILB_EAR_TRUE;
    HILB_EMREO(iter,2,:) = HILB_EAR_FALSE;
    
    iter = iter + 1;
end


% allocate space for variables
LooEMREO = cell(1,ncond);
LooDprime = cell(1,ncond);
LooBias = cell(1,ncond);
CorrCoeff = zeros(1,ncond);
cond_resp_code = cell(1,ncond);

% we compute the single trial contribution for each condition separately
for icond = 1:ncond
    cond_ind = (SaccPos == icond) & trials_during_EMREO;
    Ntrials = sum(cond_ind);
    
    % compute the mean EMREO 
    cm_emreo = mean(ALL_EAR(cond_ind,:),1);
    
    % detection sensitivity
    cond_resp = resp(cond_ind);
    cond_resp_code{icond} = resp_code(cond_ind);
    cond_played = trialplayed(cond_ind);
    HR = sum(cond_resp & cond_played) / sum(cond_played);
    FAR = sum(cond_resp & ~cond_played) / sum(~cond_played);
    [dprime,~,crit] = ck_stat_signalTheory(HR,FAR,Ntrials);
    
    % leave on trial out at a time
    for itrial = 1:sum(cond_ind)
        fcond_ind = find(cond_ind);
        fcond_ind(itrial) = [];
        Ntrials = length(fcond_ind);
        
        % emreo
        loo_emreo = mean(ALL_EAR(fcond_ind,:),1);
        LooEMREO{icond}(itrial,1) = mean(abs(cm_emreo) - abs(loo_emreo));
        
        % detection sensitivity
        fond_resp = resp(fcond_ind);
        fcond_played = trialplayed(fcond_ind);
        HR = sum(fond_resp & fcond_played) / sum(fcond_played);
        FAR = sum(fond_resp & ~fcond_played) / sum(~fcond_played);
        [loo_dprime,~,crit_loo] = ck_stat_signalTheory(HR,FAR,Ntrials);
        LooDprime{icond}(itrial,1) = dprime - loo_dprime;
        LooBias{icond}(itrial,1) = crit - crit_loo;
    end
    
    % correlate both vectors
    CorrCoeff(icond) = corr(LooEMREO{icond}, LooDprime{icond});
end


% save data to structure
% save EMREO trajectories
SumMetrics.EarBehavior.Raw = expand_dims(RAW_EMREO,1);
SumMetrics.EarBehavior.Hilbert = expand_dims(HILB_EMREO,1);
SumMetrics.EarBehavior.time = tax_ear;
% EMREO metrics
SumMetrics.EarBehavior.TrueMeanPower = TrueMeanPower;
SumMetrics.EarBehavior.FalseMeanPower = FalseMeanPower;
% details
SumMetrics.EarBehavior.Ntrue = Ntrue;
SumMetrics.EarBehavior.Nfalse = Nfalse;
SumMetrics.EarBehavior.Resp_code = cond_resp_code;
SumMetrics.EarBehavior.LooEMREO = LooEMREO;
SumMetrics.EarBehavior.LooDprime = LooDprime;
SumMetrics.EarBehavior.LooBias = LooBias;
SumMetrics.EarBehavior.CorrCoeff = CorrCoeff;
SumMetrics.EarBehavior.temp_boundaries = stat_bound;
SumMetrics.EarBehavior.CondLabel = degrees(sortI);




%% Behavior in trials with spatial cue

% do this analysis only if we showed a spatial cue (in exp. 3)
if Experiment == 3

    % first compare response time in congruent cue sound with incongruent
    % trials 
    
    % we have three factors: saccade direction, sound direction and cue
    % direction
    % we are interested whether there is an influence of cue direction
    % relative to the saccade direction and the performance
    %
    % for cue validity (cue and sound) and cue congruency (cue and saccade)
    % we find trials, compute the average EMREO and then the average power
    % over the statisical window defined globally (globalP)
    % 

    % get overall behavior
    resp_code = Analysis_log.EventLog(:,4);
    resp = Analysis_log.EventLog(:,3);
    resp(~isnan(resp)) = 1;
    resp(isnan(resp)) = 0;
    HitTrials = resp & trialplayed;
    FATrials = resp & ~trialplayed;
    
    % get overall Reaction Time
    RT = Analysis_log.RT;
    % trials where sound was played
    trialplayed = ~Analysis_log.EventSequence(:,6);

    % trial indices
    %   1) 6�  right
    %   2) 6�  left
    %   3) 12� right
    %   4) 12� left

    % tranform condition label to degrees as the independent variable
    degrees = Analysis_log.Degrees; % index corresponds to the condition labels
    SaccPos = Analysis_log.SaccPosCondition;
    
    % define temporal bounadries for statistical analysis
    stat_bound = globalP.EMREOwindow;
    stat_win = (tax_ear > stat_bound(1) & tax_ear < stat_bound(2));

    % sound position
    SoundPos = Analysis_log.EventSequence(:,5);
    % cue direction
    CuePos = Analysis_log.EventSequence(:,7);
    
    % vector for left and right saccades
    SaccLeftInd = any(SaccPos == [2,4],2);
    SaccRightInd = any(SaccPos == [1,3],2);
    NLeft = sum(SaccLeftInd);
    NRight = sum(SaccRightInd);
    
    
    % ------------------------------------------------
    % compare trials with informative cue vs uninformative cue
    % sound pos vs cue dir
    
    % Informativeness / Validity of cue and sound position
    % congruent trial vector for 
    Ind_SoundCueCongruent = (SoundPos == CuePos);
    % for both directions
    IndCongruentLeft  = all([Ind_SoundCueCongruent SaccLeftInd ],2);
    IndCongruentRight = all([Ind_SoundCueCongruent SaccRightInd ],2);
    % incongruent and both directions
    IndIncongruentLeft  = all([~Ind_SoundCueCongruent SaccLeftInd ],2);
    IndIncongruentRight = all([~Ind_SoundCueCongruent SaccRightInd ],2);
    
    % response accuracy
    congruentSoundCue = sum(resp_code(Ind_SoundCueCongruent)) / sum(Ind_SoundCueCongruent);
    incongruentSoundCue = sum(resp_code(~Ind_SoundCueCongruent)) / sum(~Ind_SoundCueCongruent);
    % response sensitivity (d prime)
    HR_conSoundCue = sum(HitTrials(Ind_SoundCueCongruent))/sum(Ind_SoundCueCongruent);
    HR_inconSoundCue = sum(HitTrials(~Ind_SoundCueCongruent))/sum(~Ind_SoundCueCongruent);
    FAT_conSoundCue   =  sum(FATrials(Ind_SoundCueCongruent))/sum(Ind_SoundCueCongruent);
    FAT_inconSoundCue = sum(FATrials(~Ind_SoundCueCongruent))/sum(~Ind_SoundCueCongruent);
    [dprime_conSoundCue,~,crit_conSoundCue] = ck_stat_signalTheory(HR_conSoundCue,FAT_conSoundCue, sum(Ind_SoundCueCongruent));
    [dprime_inconSoundCue,~,crit_inconSoundCue] = ck_stat_signalTheory(HR_inconSoundCue, FAT_inconSoundCue, sum(~Ind_SoundCueCongruent));
    % reaction time
    congruentSC_RT = RT(Ind_SoundCueCongruent & resp_code & trialplayed);
    incongruentSC_RT = RT(~Ind_SoundCueCongruent & resp_code & trialplayed);
    % peak eye velocity / acceleration
    VeloSoundCueCongruentL = max(Velocity(IndCongruentLeft,:),[],2);
    VeloSoundCueCongruentR = max(Velocity(IndCongruentRight,:),[],2);
    VeloSoundCueIncongruentL = max(Velocity(IndIncongruentLeft,:),[],2);
    VeloSoundCueIncongruentR = max(Velocity(IndIncongruentRight,:),[],2);
    % label
    caselabelSC = {'valid','invalid'};
    
    % ear data with only valid/invalid trials
    EarLUninformative = ALL_EAR(IndIncongruentLeft,:);
    EarLInformative = ALL_EAR(IndCongruentLeft,:);
    EarRUninformative = ALL_EAR(IndIncongruentRight,:);
    EarRInformative = ALL_EAR(IndCongruentRight,:);
    
    % we take the absolute of the hilbert transform and compute the
    % average power over the EMREO
    % left
    HILB_L_Uninf = abs(hilbert(mean(EarLUninformative,1)));
    HILB_L_Inf   = abs(hilbert(mean(EarLInformative,1)));
    UninfMeanPower_L = mean(HILB_L_Uninf(:,stat_win));
    InfMeanPower_L   = mean(HILB_L_Inf(:,stat_win));
    % right
    HILB_R_Uninf = abs(hilbert(mean(EarRUninformative,1)));
    HILB_R_Inf   = abs(hilbert(mean(EarRInformative,1)));
    UninfMeanPower_R = mean(HILB_R_Uninf(:,stat_win));
    InfMeanPower_R   = mean(HILB_R_Inf(:,stat_win));
    
    
    % plot number of correct trials
    figure('Position',[116 50 1628 937]);
    
    % -----------------------------------
    subplot(4,4,[1,5]);
    bar(gca,[1],congruentSoundCue);
    hold on
    bar(gca,[2],incongruentSoundCue);
    %ylim([0 1]);
    
    ctext = sprintf('n = %d', sum(Ind_SoundCueCongruent));
    text(1,-0.20,ctext,'HorizontalAlignment','center');
    itext = sprintf('n = %d', sum(~Ind_SoundCueCongruent));
    text(2,-0.20,itext,'HorizontalAlignment','center');
    xticks([1 2]);
    xticklabels(caselabelSC);
    ylabel('Sensitivity d''');
    header = 'All Trials';
    title(header);
    
    % plot RT 
    % -----------------------------------
    subplot(4,4,[2,6]);
    cfg = [];
    cfg.connect = false;
    cfg.jitterrange = 0.2;
    cfg.XAxis = [1];
    
    barwithdots(cfg,congruentSC_RT);
    hold on
    cfg.XAxis = [2];
    barwithdots(cfg,incongruentSC_RT);
    box on
    ylim([0 2000]);
    
    ctext = sprintf('n = %d', length(congruentSC_RT));
    text(1,-150,ctext,'HorizontalAlignment','center');
    itext = sprintf('n = %d', length(incongruentSC_RT));
    text(2,-150,itext,'HorizontalAlignment','center');
    
    xticks([1 2]);
    xticklabels(caselabelSC);
    ylabel('Reaction Time (ms)');
    header = ['Correct Trials and Sound played'];
    title(header);
    
    % -----------------------------------
    % plot EMREOs in congruent and incongruent trials
    % congruent and both directions
    
    % plot congruent trial EMREOs
    ax = subplot(4,4,[3,4]);
    hold on
    [~,hp1] = fb_errorshade(tax_ear, mean(EarLUninformative), sem(EarLUninformative), [0.5 0.1 0]);
    [~,hp2] = fb_errorshade(tax_ear, mean(EarLInformative), sem(EarLInformative), [0.8 0 0]);
    grid on
    
    % reset y axis
    ylimits = repmat(max(ax.YLim),[1,2]) .* [-1 1];
    ax.YLim = ylimits * 1.2;
    
    % gray out not relevant area
    fill([tax_ear(1) stat_bound(1) stat_bound(1) tax_ear(1)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    fill([stat_bound(2) tax_ear(end) tax_ear(end) stat_bound(2)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    
    axpos = ax.Position;
    legend([hp2 hp1],caselabelSC,'Location','northeastoutside');
    set(ax,'Position',axpos);
    

    header = sprintf(['EMREO in left trials (n=%d)' newline '%d informative, %d uninformative'],NLeft,sum(IndCongruentLeft),sum(IndIncongruentLeft));
    title(header);
    
    % plot inclose allcongruent trial EMREOs
    ax = subplot(4,4,[7,8]);
    hold on
    [~,hp1] = fb_errorshade(tax_ear, mean(EarRUninformative), sem(EarRUninformative), [0 0.1 0.5]);
    [~,hp2] = fb_errorshade(tax_ear, mean(EarRInformative), sem(EarRInformative), [0 0 0.8]);
    grid on
    
    % reset y axis
    ylimits = repmat(max(ax.YLim),[1,2]) .* [-1 1];
    ax.YLim = ylimits * 1.2;
    
    % gray out not relevant area
    fill([tax_ear(1) stat_bound(1) stat_bound(1) tax_ear(1)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    fill([stat_bound(2) tax_ear(end) tax_ear(end) stat_bound(2)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    
    axpos = ax.Position;
    legend([hp2 hp1],caselabelSC,'Location','northeastoutside');
    set(ax,'Position',axpos);
    header = sprintf(['EMREO in right trials (n=%d)' newline '%d informative, %d uninformative'],NRight,sum(IndCongruentRight),sum(IndIncongruentRight));
    title(header);
    
    
    
    
    % ------------------------------------------------
    % second part: compare trials with same direction cue to saccade vs not
    % same direction
    % saccade dir vs cue dir
    warning('Because sound position and sacc amplitude are not independent, we must dicard some trials!');
    L_SaccCueCongruent = all([SaccLeftInd  (CuePos == 4)],2);
    R_SaccCueCongruent = all([SaccRightInd (CuePos == 2)],2);
    L_SaccCueIncongruent = all([SaccLeftInd  (CuePos == 2)],2);
    R_SaccCueIncongruent = all([SaccRightInd  (CuePos == 4)],2);
    Both_SaccCueCongruent = any([L_SaccCueCongruent R_SaccCueCongruent],2);
    
    % response accuracy
    congruentSaccCue = sum(resp_code(Both_SaccCueCongruent)) / sum(Both_SaccCueCongruent);
    incongruentSaccCue = sum(resp_code(~Both_SaccCueCongruent)) / sum(~Both_SaccCueCongruent);
    % response sensitivity (d prime)
    HR_conSaccCue = sum(HitTrials(Both_SaccCueCongruent))/sum(Both_SaccCueCongruent);
    HR_inconSaccCue = sum(HitTrials(~Both_SaccCueCongruent))/sum(~Both_SaccCueCongruent);
    FAT_conSaccCue   =  sum(FATrials(Both_SaccCueCongruent))/sum(Both_SaccCueCongruent);
    FAT_inconSaccCue = sum(FATrials(~Both_SaccCueCongruent))/sum(~Both_SaccCueCongruent);
    [dprime_conSaccCue,~,crit_conSaccCue] = ck_stat_signalTheory(HR_conSaccCue, FAT_conSaccCue, sum(Both_SaccCueCongruent));
    [dprime_inconSaccCue,~,crit_inconSaccCue] = ck_stat_signalTheory(HR_inconSaccCue, FAT_inconSaccCue, sum(~Both_SaccCueCongruent));
    % reaction time
    congruentSaccC_RT = RT(Both_SaccCueCongruent & resp_code & trialplayed);
    incongruentSaccC_RT = RT(~Both_SaccCueCongruent & resp_code & trialplayed);
    % peak eye velocity / acceleration
    VeloSaccCueCongruentL = max(Velocity(L_SaccCueCongruent,:),[],2);
    VeloSaccCueCongruentR = max(Velocity(R_SaccCueCongruent,:),[],2);
    VeloSaccCueIncongruentL = max(Velocity(L_SaccCueIncongruent,:),[],2);
    VeloSaccCueIncongruentR = max(Velocity(R_SaccCueIncongruent,:),[],2);
    
    % check if the minimum amount of saccade amplitude conditions that are
    % represented in the index vectors
    NminL = min(sum( [L_SaccCueCongruent & SaccPos==[2,4], L_SaccCueIncongruent & SaccPos==[2,4]] ));
    NminR = min(sum( [R_SaccCueCongruent & SaccPos==[1,3], R_SaccCueIncongruent & SaccPos==[1,3]] ));
    % get indices for congruent and incongruent trials and saccades to the
    % left and right
    % randomize and take first Nmin trials 
    SaccCueInd = {L_SaccCueCongruent, L_SaccCueIncongruent, R_SaccCueCongruent, R_SaccCueIncongruent};
    PosInd = [2 4; 2 4; 1 3; 1 3];
    SaccCueMin = [NminL, NminL, NminR, NminR];
    % for each of the four variants, we assert that an equal minimum number
    % of trials from 6 and 12 degrees are included!
    for isc_ind = 1:length(SaccCueInd)
        tmp = SaccCueInd{isc_ind};
        tmp = { find(tmp & SaccPos==PosInd(isc_ind,1)), find(tmp & SaccPos==PosInd(isc_ind,2)) };
        tmp = cellfun(@(x) x(randperm(length(x))), tmp, 'UniformOutput', false);
        tmp = cellfun(@(x) x(1:SaccCueMin(isc_ind)), tmp, 'UniformOutput', false);
        tmp = sort(cat(1, tmp{:}));
        tmp2 = false(ntrial,1);
        tmp2(tmp) = true;
        SaccCueInd{isc_ind} = tmp2;
    end
    [L_SaccCueC, L_SaccCueI, R_SaccCueC, R_SaccCueI] = deal(SaccCueInd{:});
    
    % label
    caselabelSacc = {'congruent','incongruent'};
    
    
    % ear data with only congruent/incongruent trials
    EarLCueIncongruent = ALL_EAR(L_SaccCueC,:);
    EarLCueCongruent = ALL_EAR(L_SaccCueI,:);
    EarRCueIncongruent = ALL_EAR(R_SaccCueC,:);
    EarRCueCongruent = ALL_EAR(R_SaccCueI,:);
    
    % we take the absolute of the hilbert transform and compute the
    % average power over the EMREO
    % left
    HILB_L_Incon = abs(hilbert(mean(EarLCueIncongruent,1)));
    HILB_L_Con   = abs(hilbert(mean(EarLCueCongruent,1)));
    InconMeanPower_L = mean(HILB_L_Incon(:,stat_win));
    ConMeanPower_L   = mean(HILB_L_Con(:,stat_win));
    % right
    HILB_R_Incon = abs(hilbert(mean(EarRCueIncongruent,1)));
    HILB_R_Con   = abs(hilbert(mean(EarRCueCongruent,1)));
    InconMeanPower_R = mean(HILB_R_Incon(:,stat_win));
    ConMeanPower_R   = mean(HILB_R_Con(:,stat_win));
    
    
    % -----------------------------------
    subplot(4,4,[9,13]);
    bar(gca,[1],congruentSaccCue);
    hold on
    bar(gca,[2],incongruentSaccCue);
    %ylim([0 1]);
    
    ctext = sprintf('n = %d', sum(Both_SaccCueCongruent));
    text(1,-0.20,ctext,'HorizontalAlignment','center');
    itext = sprintf('n = %d', sum(~Both_SaccCueCongruent));
    text(2,-0.20,itext,'HorizontalAlignment','center');
    xticks([1 2]);
    xticklabels(caselabelSacc);
    ylabel('Sensitivity d''');
    header = 'All Trials';
    title(header);
    
    
    % plot RT 
    % -----------------------------------
    subplot(4,4,[10,14]);
    cfg = [];
    cfg.connect = false;
    cfg.jitterrange = 0.2;
    cfg.XAxis = [1];
    
    barwithdots(cfg,congruentSaccC_RT);
    hold on
    cfg.XAxis = [2];
    barwithdots(cfg,incongruentSaccC_RT);
    box on
    ylim([0 2000]);
    
    ctext = sprintf('n = %d', length(congruentSaccC_RT));
    text(1,-150,ctext,'HorizontalAlignment','center');
    itext = sprintf('n = %d', length(incongruentSaccC_RT));
    text(2,-150,itext,'HorizontalAlignment','center');
    
    xticks([1 2]);
    xticklabels(caselabelSacc);
    ylabel('Reaction Time (ms)');
    header = ['Correct Trials and Sound played'];
    title(header);
    
    % -----------------------------------
    % plot EMREOs in congruent and incongruent trials
    
    % plot congruent trial EMREOs
    ax = subplot(4,4,[11,12]);
    hold on
    [~,hp1] = fb_errorshade(tax_ear, mean(EarLCueIncongruent), sem(EarLCueIncongruent), [0.5 0.1 0]);
    [~,hp2] = fb_errorshade(tax_ear, mean(EarLCueCongruent), sem(EarLCueCongruent), [0.8 0 0]);
    grid on
    
    % reset y axis
    ylimits = repmat(max(ax.YLim),[1,2]) .* [-1 1];
    ax.YLim = ylimits * 1.2;
    
    % gray out not relevant area
    fill([tax_ear(1) stat_bound(1) stat_bound(1) tax_ear(1)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    fill([stat_bound(2) tax_ear(end) tax_ear(end) stat_bound(2)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    
    axpos = ax.Position;
    legend([hp2 hp1],caselabelSacc,'Location','northeastoutside');
    set(ax,'Position',axpos);
    
    

    header = sprintf(['EMREO in left trials (n=%d)' newline '%d congruent, %d incongruent'],2*NLeft,2*NminL,NminL);
    title(header);
    
    % plot inclose allcongruent trial EMREOs
    ax = subplot(4,4,[15,16]);
    hold on
    [~,hp1] = fb_errorshade(tax_ear, mean(EarRCueIncongruent), sem(EarRCueIncongruent), [0 0.1 0.5]);
    [~,hp2] = fb_errorshade(tax_ear, mean(EarRCueCongruent), sem(EarRCueCongruent), [0 0 0.8]);
    grid on
    
    % reset y axis
    ylimits = repmat(max(ax.YLim),[1,2]) .* [-1 1];
    ax.YLim = ylimits * 1.2;
    
    % gray out not relevant area
    fill([tax_ear(1) stat_bound(1) stat_bound(1) tax_ear(1)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    fill([stat_bound(2) tax_ear(end) tax_ear(end) stat_bound(2)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    
    axpos = ax.Position;
    legend([hp2 hp1],caselabelSacc,'Location','northeastoutside');
    set(ax,'Position',axpos);
    header = sprintf(['EMREO in right trials (n=%d)' newline '%d congruent, %d incongruent'],2*NRight,2*NminR,NminR);
    title(header);
    
    
    % -----------------------------------
    % figure title
    header = 'Sacc Direction x [Sound Position] x Cue direction';
    ckfiguretitle(header);
    
    
    % save figure
    fname = sprintf([figurepath Subname 'E%d' '_BehaviorSpatialCue.png'],Experiment);
    print('-dpng',fname);
    
    
    
    % -----------------------------------
    % plot peak velocity distribution per factor and direction
    figure
    subplot(221);
    histogram(VeloSoundCueCongruentL,50);
    hold on
    histogram(VeloSoundCueIncongruentL,50);
    xlim([200, 800]);
    title('left validity');
    
    subplot(222);
    histogram(VeloSoundCueCongruentR,50);
    hold on
    histogram(VeloSoundCueIncongruentR,50);
    xlim([200, 800]);
    title('right validity');
    
    subplot(223);
    histogram(VeloSaccCueCongruentL,50);
    hold on
    histogram(VeloSaccCueIncongruentL,50);
    xlim([200, 800]);
    title('left congruency');
    
    subplot(224);
    h1 = histogram(VeloSaccCueCongruentR,50);
    hold on
    h2 = histogram(VeloSaccCueIncongruentR,50);
    xlim([200, 800]);
    legend([h1 h2],{'con','incon'},'Location','northeast');
    title('right congruency');
    
    % -----------------------------------
    % figure title
    header = 'Cue information and saccade peak velocity';
    ckfiguretitle(header);
    
    
    % save figure
    fname = sprintf([figurepath Subname 'E%d' '_PeakVeloc_SpatialCue.png'],Experiment);
    print('-dpng',fname);
    
    
    % save data to struct
    SumMetrics.SpatialCue.Informative.NLeft = [sum(IndCongruentLeft) sum(IndIncongruentLeft)];
    SumMetrics.SpatialCue.Informative.NRight = [sum(IndCongruentRight) sum(IndIncongruentRight)];
    SumMetrics.SpatialCue.Informative.NResp = [length(congruentSoundCue) length(incongruentSoundCue)];
    SumMetrics.SpatialCue.Informative.Resp = [congruentSoundCue incongruentSoundCue];
    SumMetrics.SpatialCue.Informative.Dprime = [dprime_conSoundCue dprime_inconSoundCue];
    SumMetrics.SpatialCue.Informative.SigDetBias = [crit_conSoundCue crit_inconSoundCue];
    SumMetrics.SpatialCue.Informative.RT = [median(congruentSC_RT) median(incongruentSC_RT)];
    SumMetrics.SpatialCue.Informative.PeakVelo = [median(VeloSoundCueCongruentL) median(VeloSoundCueIncongruentL) median(VeloSoundCueCongruentR) median(VeloSoundCueIncongruentR) ];
    SumMetrics.SpatialCue.Informative.EMREO = expand_dims([mean(EarLInformative); mean(EarLUninformative); mean(EarRInformative); mean(EarRUninformative)],1);
    SumMetrics.SpatialCue.Informative.MeanPower_L = [UninfMeanPower_L InfMeanPower_L];
    SumMetrics.SpatialCue.Informative.MeanPower_R = [UninfMeanPower_R InfMeanPower_R];
    SumMetrics.SpatialCue.Informative.time = tax_ear;
    SumMetrics.SpatialCue.Informative.Label = {'left valid','left valid','right valid','right valid'};
    
    SumMetrics.SpatialCue.Congruency.NLeft = [sum(L_SaccCueC) sum(L_SaccCueI)];
    SumMetrics.SpatialCue.Congruency.NRight = [sum(R_SaccCueC) sum(R_SaccCueI)];
    SumMetrics.SpatialCue.Congruency.NResp = [length(congruentSaccCue) length(incongruentSaccCue)];
    SumMetrics.SpatialCue.Congruency.Resp = [congruentSaccCue incongruentSaccCue];
    SumMetrics.SpatialCue.Congruency.Dprime = [dprime_conSaccCue dprime_inconSaccCue];
    SumMetrics.SpatialCue.Congruency.SigDetBias = [crit_conSaccCue crit_inconSaccCue];
    SumMetrics.SpatialCue.Congruency.RT = [median(congruentSaccC_RT) median(incongruentSaccC_RT)];
    SumMetrics.SpatialCue.Congruency.PeakVelo = [median(VeloSaccCueCongruentL) median(VeloSaccCueIncongruentL) median(VeloSaccCueCongruentR) median(VeloSaccCueIncongruentR)];
    SumMetrics.SpatialCue.Congruency.EMREO = expand_dims([mean(EarLCueCongruent); mean(EarLCueIncongruent); mean(EarRCueCongruent); mean(EarRCueIncongruent)],1);
    SumMetrics.SpatialCue.Congruency.MeanPower_L = [InconMeanPower_L ConMeanPower_L];
    SumMetrics.SpatialCue.Congruency.MeanPower_R = [InconMeanPower_R ConMeanPower_R];
    SumMetrics.SpatialCue.Congruency.time = tax_ear;
    SumMetrics.SpatialCue.Congruency.Label = {'left congruent','left incongruent','right congruent','right incongruent'};
    
    % ---------------------------------------------------
    % are EMREOs different when presenting a saptial cue?
    % -> compare EMREOs of Experiment 2 and 3 in GroupStats
    
    % compare EMREOs in those conditions with the average EMREO across all
    % trials

    % resample EMREOs from N samples with replacement and look if conditions
    % EMREOs lie outside of some percentile
    
    % did participants start their saccade later in incongruent trials?
    
    % ---------------------------------------------------
    % how do we save this so we can concatenate in group analysis?
    % save name is experiment specific, so we should be fine
    
    % save data to struct
    % i.e. SumMetrics.CueBehavior;
    
    
    
    
end



%% Phase related behavior
% get behavior relative to the phase of the EMREO 
%
% CAVE: the actual window could also be defined to participant specific EMREOs
%
% plot EMREO and sound onset histogram between [-20 100] ms 
% define time window of interest (TWOI) by a full cycle starting from 0 ms
%

% invert all left facing trials
ALL_EAR_absphase = ALL_EAR;
ALL_EAR_absphase(sacc_left,:) = ALL_EAR(sacc_left,:) .* -1;

% define outer bounds for plotting
outer_bound = [-20 100];
outer_win = (tax_ear > outer_bound(1) & tax_ear < outer_bound(2));
outer_phase = tax_ear(outer_win);

% select smaller time window with outer bounds
ALL_EAR_absphase = ALL_EAR_absphase(:,outer_win);
meanALL_EAR = mean(ALL_EAR_absphase,1);
maxALL_EAR = max(abs(meanALL_EAR));
% compute phase angle in radians
EAR_phase = angle(hilbert(meanALL_EAR));

% find the first peak within a preinformed window (between 0 and 40 ms)
[~,locs] = findpeaks(meanALL_EAR,'SortStr','descend');
ipeak = locs(outer_phase(locs) > 0 & outer_phase(locs) < 40);
% if there are more than one peaks, take the biggest
ipeak = ipeak(1); 
% get full cycle around peak
cross_zero = abs( [diff(sign(meanALL_EAR)) 1]);
Icross_zero = find(cross_zero);

% get first crossing before and second crossing after peak
startI = find(Icross_zero < ipeak, 1, 'last');
% if we find no zero crossing, begin at 0 ms
if isempty(startI)
    startWin = round(outer_bound(1) * ARG.resamplefs / 1000) * -1;
else
    startWin = Icross_zero(startI);
end
% take only the next zero crossing or 40 ms from start
stopI = find(Icross_zero > ipeak, 2);
if length(stopI) > 1
    stopWin = Icross_zero(stopI(2));
elseif isempty(stopI)
    stopWin = round(0.04 * ARG.resamplefs) + startWin;
end
% index for ALL_EAR_absphase
phase_boundI = [startWin stopWin];
% convert to ms
phase_bound = outer_phase(phase_boundI);


% get all trials where sounds were played in that window
trials_during_EMREO = (relativeOnTime > phase_bound(1) & relativeOnTime < phase_bound(2));
% get saccade time for those trials (determines the sign)
during_all   = relativeOnTime(all([trials_during_EMREO trialplayed],2));
% get saccade time in only correct trials during EMREO
during_all_correct  = relativeOnTime(all([trials_during_EMREO trialplayed Analysis_log.EventLog(:,4)],2));

% for plotting only
% find sound onsets during outer bounds and during conservative EMREO 
trials_during_outerbounds = (relativeOnTime > outer_bound(1) & relativeOnTime < outer_bound(2));
% get saccade time for those trials (determines the sign)
during_outerbounds = relativeOnTime(all([trials_during_outerbounds trialplayed],2));
% get saccade time in only correct trials during EMREO
during_outerbounds_correct = relativeOnTime(all([trials_during_outerbounds trialplayed Analysis_log.EventLog(:,4)],2));


% define time vector around EMREO
tax_win = (tax_ear > phase_bound(1) & tax_ear < phase_bound(2));
tax_phase = tax_ear(tax_win);


% find where exactly in the vector each sound onset is 
% this is for all trials where sound occured during the EMREO window (s.o.)
[~,phase_idx] = max(outer_phase > during_all,[],2); % first index where onset > time vector
Phase_val = EAR_phase(:,phase_idx);
% compute inter trial phase vector
ITPV = mean(exp(1i.*Phase_val));


% do the same only for the correctly answered trials
Phase_val_cor = EAR_phase(:,phase_idx);
resp_all = Analysis_log.EventLog(all([trials_during_EMREO trialplayed],2),4);
corr_trials = (resp_all==1);
false_trials = (resp_all==0);
% invert falsely answered trials
inv_phase = Phase_val_cor(false_trials) + pi;
inv_phase(inv_phase>pi) = inv_phase(inv_phase>pi) - 2*pi;
Phase_val_cor(false_trials) = inv_phase;
% compute inter trial phase vector
ITPVc = mean(exp(1i.*Phase_val_cor));


% compute circular kernel density to plot 
bin_center = linspace(-pi,pi,25);
[pdf.val,sigma.val] = circ_ksdensity(Phase_val,bin_center,'std');
[pdf.cor,sigma.cor] = circ_ksdensity(Phase_val_cor,bin_center,'std');
% close circle (somehow i can't figure out why the function doesn't do this on it's own)
pdf.val(end) = pdf.val(1);
pdf.cor(end) = pdf.cor(1);


% ----------------------------------------------------
% set parameters and plot data
corColor = [0 1 0] .* 0.8;
falColor = [1 0 1] .* 0.8;
allColor = [0 0 1] .* 0.8;

figure('Position',[230 136 1478 811]);

% ----------------------------------------------------
hh(1) = subplot(3,3,4);
hold on
% fill area with phase bounds
fill([phase_bound fliplr(phase_bound)],[-maxALL_EAR -maxALL_EAR maxALL_EAR maxALL_EAR] .* 1.6, 'r','FaceAlpha',0.2,'EdgeColor','none');
% plot lines per saccade direction and mean phase adjusted line
p1 = plot(outer_phase,mean(ALL_EAR_absphase(sacc_right,:)),'b-.','LineWidth',0.5);
p2 = plot(outer_phase,mean(ALL_EAR_absphase(sacc_left,:)),'r-.','LineWidth',0.5);
p3 = plot(outer_phase,meanALL_EAR,'k-','LineWidth',2.0);
% plot sound onset points in outer bounds
p4 = plot(during_outerbounds, maxALL_EAR*1.3,'Color',allColor,'Marker','.');
p5 = plot(during_outerbounds_correct, maxALL_EAR*1.5,'Color',corColor,'Marker','.');
xlim(outer_bound);
ylim([-maxALL_EAR maxALL_EAR] .* 1.6);
grid on

xlabel('time (ms)');
ylabel('EMREO amplitude (SD)');
axpos = hh(1).Position;
lh = legend([p1 p2 p3 p4(1) p5(1)], {'right','left','mean','onsets','corr. onsets'},'Location','southeastoutside');
lh.Position = lh.Position + [0.12 0.25 0 0];
hh(1).Position = axpos;

% ----------------------------------------------------
% plot response histogram for onsets during outer bounds
Nbins = 75;
mhp = round(max(hist(during_outerbounds, Nbins)) * 1.2);
hh(2) = subplot(3,3,1);
% fill area with phase bounds
fill([phase_bound fliplr(phase_bound)],[0 0 mhp mhp], 'r','FaceAlpha',0.2,'EdgeColor','none');
hold on
histogram(during_outerbounds, Nbins,'FaceColor',allColor);
histogram(during_outerbounds_correct, Nbins,'FaceColor',corColor);
xlim(outer_bound);
ylim([0 mhp]);
header = 'Onset distribution';
title(header);


% ----------------------------------------------------
% plot EMREO phase
hh(3) = subplot(3,3,7);
% fill area with phase bounds
fill([phase_bound fliplr(phase_bound)],[-4 -4 4 4] .* 1.4, 'r','FaceAlpha',0.2,'EdgeColor','none');
hold on
plot(outer_phase,EAR_phase,'k-','LineWidth',2.0);
xlim(outer_bound);
ylim([-4 4]);
grid on
yticks([-pi:pi/2:pi]);
yticklabels({'-pi','-pi/2','0','pi/2','pi'});
xlabel('time (ms)');
ylabel('EMREO phase (radians)');


% ----------------------------------------------------
% now we check for relation of response to EMREO amplitude
[~,phase_idx] = max(outer_phase > during_all,[],2); % first index where onset > time vector
amp_all = meanALL_EAR(phase_idx)';
RT = Analysis_log.RT(all([trials_during_EMREO trialplayed],2));

% logisitc regression fit AMPLITUDE vs response
mdlAMP = fitglm(amp_all,resp_all,'Distribution','binomial');
% logistic function
logfunc = @(x,b0,b1) 1./(1+exp(-(b0 + b1.*x)));

betaAMP = mdlAMP.Coefficients.Estimate;
tx_amp = [-1:0.1:1] * maxALL_EAR * 1.2;
yfit = logfunc(tx_amp,betaAMP(1),betaAMP(2));

% plot
hh(4) = subplot(3,3,5);
plot(amp_all(corr_trials),resp_all(corr_trials),'o','Color',corColor);
hold on
plot(amp_all(false_trials),resp_all(false_trials),'o','Color',falColor);
plot(tx_amp,yfit,'Color','k');
xlim([-maxALL_EAR maxALL_EAR] * 1.2);

ax = gca;
limits = [ax.XAxis.Limits(2) ax.YAxis.Limits(2)];
betatxt = sprintf(['logit. regression' newline 'intercept = %.3f' newline 'beta = %.3f'],betaAMP);
text(limits(1)*0.9,limits(2)*0.2,betatxt,'HorizontalAlignment','right');
ylim([-0.05 1.05]);
xlabel('EMREO amplitude (SD)');
ylabel('Response');




% ----------------------------------------------------
% regression AMPLITUDE vs reaction time
amp_corr = amp_all(corr_trials);
phase_corr = Phase_val(corr_trials)';
RT_corr = log(RT(corr_trials));

beta_amp   = [ones(size(amp_corr)) amp_corr] \ RT_corr;
beta_pha = [ones(size(phase_corr)) phase_corr] \ RT_corr;

hh(5) = subplot(3,3,6);
plot(amp_corr,RT_corr,'Marker','o','LineStyle','none','Color',allColor);
hold on
xlim([-maxALL_EAR maxALL_EAR] * 1.2);

ax = gca;
limits = ax.XAxis.Limits';
ytop = ax.YAxis.Limits(2) * 1.02;
yhat = [ones(size(limits)) limits] * beta_amp;
plot(limits,yhat,'k-');
betatxt = sprintf('intercept = %.3f   beta = %.3f',beta_amp);
text(limits(2),ytop,betatxt,'HorizontalAlignment','right');
xlabel('EMREO amplitude (SD)');
ylabel('log[Reaction Time (ms)]');




% ----------------------------------------------------
% Polar response plot
hh(6) = subplot(3,3,8);
% all onsets
polarplot(Phase_val,ones(size(Phase_val)).* 1.0,'o','Color',allColor);
hold on
polarplot([0,angle(ITPV)],[0,abs(ITPV)],'-','Color',allColor,'LineWidth',3.0);
% correct onsets
polarplot(Phase_val_cor(false_trials),ones(size(Phase_val_cor(false_trials))).*1.15,'*','Color',falColor);
polarplot(Phase_val_cor(corr_trials),ones(size(Phase_val_cor(corr_trials))).*1.15,'o','Color',corColor);
polarplot([0,angle(ITPVc)],[0,abs(ITPVc)],'-','LineWidth',3.0,'Color',corColor);
% ksdensity
polarplot(bin_center,pdf.val .* (1/max(pdf.val)) .* 0.9,'Color',allColor);
polarplot(bin_center,pdf.cor .* (1/max(pdf.cor)) .* 0.9,'Color',corColor);
rlim([0 1.25]);
thetaticks([0:90:270]); 
thetaticklabels({[char(177) ' pi'],'-pi/2','0','pi/2'});
vectxt = sprintf(['all theta = %.3f' newline 'norm = %.3f' newline 'corr theta = %.3f' newline 'norm = %.3f'], ...
    angle(ITPV), abs(ITPV),angle(ITPVc),abs(ITPVc));
th = text(0.4,2.8,vectxt,'HorizontalAlignment','right');
metriclabel = 'Response';
text(pi,1.8,metriclabel,'HorizontalAlignment','center','Rotation',90);
% -> this should now show all sound onsets relative to phase
%
% also plot the EMREO and overlay all sound onsets



% ----------------------------------------------------
% test if RT of correct trials are uniformaly distributed
circRT = log(RT(corr_trials));
[pval,z] = circ_rtest(circRT);

% Polar RT plot
hh(7) = subplot(3,3,9);
% plot response times along unity circle
polarplot(Phase_val(corr_trials),circRT,'*','Color',corColor);
hold on
polarplot(Phase_val(false_trials),log(RT(false_trials)),'*','Color',falColor);
thetaticks([0:90:270]); 
thetaticklabels({[char(177) ' pi'],'-pi/2','0','pi/2'});
rlim([4 8]);
raytxt = sprintf(['correct trials' newline 'Rayleigh z = %.3f' newline 'pval = %.3f'],z,pval);
th2 = text(0.4, 14, raytxt, 'HorizontalAlignment','right');
metriclabel = 'log[Reaction Time (ms)]';
text(pi,10,metriclabel,'HorizontalAlignment','center','Rotation',90);


% save plot
header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_PhaseBehavior.png'],Experiment);
print('-dpng',fname);



% save data to struct
SumMetrics.PhaseBehavior.Nonset = length(during_all);
SumMetrics.PhaseBehavior.Ncorr  = length(during_all_correct);
% EMREO curve
SumMetrics.PhaseBehavior.EMREO.data = mean(ALL_EAR_absphase);
SumMetrics.PhaseBehavior.EMREO.time = {tax_phase};
% phase vector
SumMetrics.PhaseBehavior.Vector.All     = [angle(ITPV)  abs(ITPV) ];
SumMetrics.PhaseBehavior.Vector.Correct = [angle(ITPVc) abs(ITPVc)];
% response logistic regression
SumMetrics.PhaseBehavior.Resp.EMREOamp = mdlAMP.Coefficients.Estimate';
% reaction time linear regression
SumMetrics.PhaseBehavior.RT.EMREOamp = beta_amp';




%% save summary metrics to file
save(sumname,'-struct','SumMetrics');



end
