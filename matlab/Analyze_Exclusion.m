function Analyze_Exclusion(Exclude)
%
% take data from experiment 1 and analyze EMREOs to exclude participants
% with bad data
%


close all

% get ExpList
USECOMP = 2
ExpList;
defdirs;

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% which participants to consider
consider = true(length(SubList),1);
% we dont consider the first eight, because they were collected to fine tune
% the paradigm. actual collection started at S09
consider(1:8) = false;
consider = find(consider)';

% exclude count from considered
Exclude = Exclude + 8;

for S = consider

    % skip participant if we dont include them
    if ismember(S,Exclude)
        continue
    end

    % subject name
    Subname = SubList(S).name

    % ----------------------------------------------------------------------------
    % general parameters
    ARG.resamplefs = 1000;
    ARG.zscore = globalP.zscore; % if whole data matrix should be zscored

    % parameters for filtering and data inclusion - We expect EMREO around 35 Hz
    ARG_ear.zscore = false;
    ARG_ear.hpfilter   = 'no';  
    ARG_ear.hpfreq     = 10;
    ARG_ear.lpfilter   = 'no';  
    ARG_ear.lpfreq     = 50;
    ARG_ear.prestim    = -1.5;
    ARG_ear.poststim   =  2.0;
    
    
    ARG_eye.Window = [-1.5,2.0]; % seconds
    ARG_eye.Align = 'onset';
    ARG_eye.ppd = globalP.ppd;
    ARG_eye.resamplefs = ARG.resamplefs;
    
    % set experiment in a for loop
    Experiment = [1 2 3];
    Ntrials_per_exp = [161 144 144]; % number of trials per block in each experiment


    ppd = globalP.ppd;
    Midpoint = globalP.centerpoint ./ ppd; % screen center




    %% --------------------------------------------------------------------------
    % Load Event data
    for exp = Experiment
        ARG_event.exp = exp;
        [event,~] = load_event_blocks(S,SubList,datapath_log,ARG_event);
        % account for different name in experiment 1
        if exp == 1
            event.EventSequence = event.Sequence;
        end
        event.EventSequence = event.EventSequence(:,1);
        event = keepfields(event,'EventSequence');
        
        % transform condition if from the first experiment
        if exp == 1
            % check which conditions where vertical saccades so we can dismiss
            % them from the other data structures as well
            %
            % set condition label to 0, because later those are sorted out
            % anyways!
            set_to_false = any(event.EventSequence == [1 2 4 6 8],2);
            event.EventSequence(set_to_false) = 0;
            % convert all to labels consistent with other experiments
            event.EventSequence(~set_to_false) = (event.EventSequence(~set_to_false) - 1) ./2;
            
        end
        % add label from which experiment the trials originate
        event.explabel = ones(size(event.EventSequence)) * exp;
        Events{exp} = event;
    end
    
    
    % append experiments
    % assumes same fieldnames in events cell
    Events = cell2mat(Events);
    eventfields = fieldnames(Events);
    for ifield = 1:length(eventfields)
        % assumes they are all row vectors
        EventLog.(eventfields{ifield}) = cell2mat({Events(:).(eventfields{ifield})}');
    end
    clear Events event
    
    
    
    %% --------------------------------------------------------------------------
    % Load Ear data
    Ear_data = [];
    for exp = Experiment
        ARG_ear.exp = exp;
        ARG_ear.Ntrials = Ntrials_per_exp(exp);
        Ear_data{exp} = load_eardata_blocks(S,SubList,datapath_ear,ARG_ear);
    end
    Ear_data = ft_appenddata([],Ear_data{:});
    
    % resample to new sample rate
    cfg = [];
    cfg.resamplefs = ARG.resamplefs;
    Ear_data = ft_resampledata(cfg, Ear_data);
    
    
    
    %% --------------------------------------------------------------------------
    % Load Eye data
    Sacc = [];
    TimingLog = [];
    for exp = Experiment
        ARG_eye.exp = exp;
        [Sacc{exp},TimingLog{exp}] = load_eyedata_blocks(S,SubList,datapath_eye,ARG_eye);
    end
    
    % concatenate all experiments
    Sacc = ft_appenddata([],Sacc{:});
    TimingLog = cat(1,TimingLog{:});
    
    % put into one structure to clean trials
    Eye_data.Window = ARG_eye.Window;
    Eye_data.Sacc = Sacc;
    Eye_data.TimingLog = TimingLog;


    
    % --------------------------------------------------------------------------
    % CLEAN TRIALS
    % now we extract the Ear data locked to saccade onset / offset
    % convert to matrix

    Analysis_log = [];
    Analysis_log.TimingLog = TimingLog;
    Analysis_log.Condition = EventLog.EventSequence(:,1);
    Analysis_log.ExpLabel  = EventLog.explabel;

    % clean trials here
    cfg = [];
    cfg.fsample = ARG.resamplefs;
    cfg.window = [-0.15 0.2]; % window in seconds to cut around saccade onset
    [Analysis_log,Ear_data_clean,Eye_data_clean,TrialStats] = clean_trialsFB03(cfg,Analysis_log,Ear_data,Eye_data);
    

    % unpack ear data
    ALL_EAR = Ear_data_clean.ALL_EAR_ONSET;
    tax_ear = Ear_data_clean.tax_ear;

    % zscore if desired
    if ARG.zscore
        ALL_EAR = (ALL_EAR - mean(ALL_EAR(:))) / std(ALL_EAR(:));
        emreounit = 'SD';
    else
        % convert from uV to mV
        ALL_EAR = ALL_EAR ./ 1000; 
        emreounit = 'mV';
    end

    % unpack stats
    Ngood_trials = TrialStats.Ngood_trials;

    fprintf('Retaining %d trials \n',length(TrialStats.good_trials));




    %% -----------------------------------------------------------------
    % display mean EAR trace per direction

    % get time course for left and right direction
    % right
    k=[1 3];
    curr_cond = find(any(Analysis_log.Condition==k,2));
    nright = length(curr_cond);
    tmp_right = ALL_EAR((curr_cond),:);
    % left
    k=[2 4];
    curr_cond = find(any(Analysis_log.Condition==k,2));
    nleft = length(curr_cond);
    tmp_left = ALL_EAR((curr_cond),:);
    
    % compute amplitude over baseline
    EMREOs = {tmp_right, tmp_left};
    % baseline and emreo win must be of same length
    baselinewin = tax_ear < 0;
    emreowin = tax_ear > 0 & tax_ear < 50;
    for icond = 1:length(EMREOs)
        % get emero and subtract mean from baseline
        emreo = mean(EMREOs{icond},1);
        emreo = emreo - mean(emreo);
        baseline(icond) = mean(abs(emreo(baselinewin)));
        emreoamp(icond) = mean(abs(emreo(emreowin)));
    end
    
    % check if emreo amp in at least one direction left or right is bigger
    % than 1.5 times the baseline
    thresholdflag = sum(emreoamp > 1.5 * baseline);
    


	% close all figures
    close all


    % plot time course
    figure('Position',[351 697 1079 252])
    tiledlayout(1,3)
    
    % left and right
    nexttile(1,[1,2]);
    fb_errorshade(tax_ear,mean(tmp_right),sem(tmp_right),'b');
    hold on
    fb_errorshade(tax_ear,mean(tmp_left),sem(tmp_left),'r');
    header = sprintf('Ear: right(b, n=%d)/left(r, n=%d)',nright,nleft);
    xlabel('Time (ms)');
    ylabel('Amplitude (mV)');
    title(header);
    grid on
    

    nexttile(3,[1,1]);
    % abs baseline 
    hold on
    plot(1, baseline(1),'bo')
    plot(1, baseline(2),'ro')
    % abs emreo
    plot(2, emreoamp(1),'b.','MarkerSize',12)
    plot(2, emreoamp(2),'r.','MarkerSize',12)
    grid on
    xlim([0.5 2.5]);
    xticks([1 2]);
    xticklabels({'baseline','emreo'});

    
    
    header = sprintf('%s_%d',Subname, Experiment);
    if thresholdflag > 0
        titlecolor = 'r';
    else
        titlecolor = 'k';
    end
    ckfiguretitle(header,titlecolor,10);


    fname = sprintf([figurepath 'Exclusion_' Subname '.png']);
    print('-dpng',fname);

    

end


end

