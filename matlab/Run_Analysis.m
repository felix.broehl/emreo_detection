%
% Run the whole analysis pipeline for FB03
%

close all
clearvars

% open parallel pool
%thispool = manageParpool(-2);

% get subject list 
ExpList


% only for exploration purposes
goodsubs = find([SubList.exclude]==0);
%goodsubs = [19];


% analyze participant table
History = Analyze_SummaryDataset(goodsubs);


% define exclusion criteria based on EMREO data
%Analyze_Exclusion(History.excluded);


% analyze the sound intensity and transform to dB
%Analyze_Thresholds_decibel(goodsubs);


% go through all subject for individual analysis steps
for S = goodsubs
    
    % analyze TEOAEs
    %Analyze_Click_EM(S);
    
    % Analyze EMREO of all experiments and do statistics
    %Analyze_EMREO(S);
    
    % analyze EMREO replication
    %Analyze_EyeMov_1(S);
    
    % Analyze EMREO with sound
    Experiment = 2;
    %Analyze_EyeMov_2(S,Experiment);
    
    % Analyze EMREO with sound and spatial cue
    Experiment = 3;
    %Analyze_EyeMov_2(S,Experiment);
    
    
    
    % NOT INCLUDED IN MANUSCRIPT
    % get periodic component of cochlear PSD
    % this part of the analysis has not yet been fully implemented
    
    Experiment = 2;
    %Analyze_CochActivity(S,Experiment);
    
    Experiment = 3;
    %Analyze_CochActivity(S,Experiment);
    
    
end

% close parallel pool
%delete(thispool);


% do group-level analysis
%Analyze_GroupStats(goodsubs);


% plot final figures for the project
%PlotFinalFigure_1;
%PlotFinalFigure_2;
%PlotFinalFigure_3;
PlotFinalFigure_4;
%PlotFinalFigure_5;



fprintf('\n\n\n');
fprintf('DONE WITH ANALYSIS! \n');
