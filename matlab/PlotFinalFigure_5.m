function PlotFinalFigure_5
%
% plot the final figures for project FB03
%


close all

% get ExpList
USECOMP = 3;
ExpList;
defdirs;


% load data
filename = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
GroupStats = load(filename);

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% get colors
ColorScheme = FB03_Colorscheme;

% color p values if samller than p_critval
p_critval = globalP.Sign_Alpha;


% initialize figure count
FigNumber = 5;


%%
% plot group EMREOs for informative/non-informative trials and
% congruent/incongruent trials
% plot behavior next to that

% We change the labels left and right to contra and ipsilateral, because we
% always recorded in the same ear (the right one)


% left and right, always recorded in the right ear
dirlabel = {'contralateral','ipsilateral'};

% get data and dimensions
Cuefieldnames = fieldnames(GroupStats.CueBehavior);
FactorNames = {'Cue-Sound Validity','Cue-Saccade Congruency'};
Ncuefields = length(Cuefieldnames);
tax_ear = GroupStats.CueBehavior.Informative.time(1,:);
tax_stat = GroupStats.CueBehavior.Informative.stat_time;
Ndirections = GroupStats.CueBehavior.Informative.Ndirections;
CueVarnames = {'invalid','valid';'incongruent','congruent'};
% get p-values, test statistics and effect size
Pvals = [GroupStats.CueBehavior.Informative.pval; GroupStats.CueBehavior.Congruency.pval];
pvals_adjusted = fb_stat_padjust(Pvals,'benjamini-hochberg');
Tstat = [GroupStats.CueBehavior.Informative.tval; GroupStats.CueBehavior.Congruency.tval];
BF = [GroupStats.CueBehavior.Informative.BF; GroupStats.CueBehavior.Congruency.BF];
cohensD = [GroupStats.CueBehavior.Informative.cohensD; GroupStats.CueBehavior.Congruency.cohensD];

GroupStats.CueBehavior.Informative.EMREO;


% print GLME anova results from EMREO comparison
targets = {'Informative','Congruency'};
for icuefield = 1:Ncuefields
    GLME_ANOVA = GroupStats.CueBehavior.(targets{icuefield}).GLME_ANOVA;
    fprintf('CUE ANOVA %s \n', FactorNames{icuefield});
    disp(GLME_ANOVA);
    fprintf('\n');
end


% get behavior results and create table
RespVars = {'Response','SigDetBias','RT'};
RowFactors = {'Pval','BF','cohen d'};
Nfacs = length(RowFactors); % pval, BF, cohen's d
Nresp = length(RespVars); % response variables, d' and RT
Nvars = length(FactorNames); % variables
TableRows = Nfacs * Nresp;
TableCols = Nvars;
% reshape into table
TableArray = NaN(TableRows,TableCols);
iter = 1;
for ivar = 1:Nvars
    for iresp = 1:Nresp
        tableind = [1:Nfacs] + (iter-1)*Nfacs;
        tmpvar = [pvals_adjusted(ivar,iresp) BF(ivar,iresp) cohensD(ivar,iresp)];
        TableArray(tableind) = tmpvar;
        iter = iter + 1;
    end
end

% table row and col labels
delimiter = '_';
% row names
RowFacs = repmat(RowFactors,[1,Nresp]);
RowVars = flatten(repmat(RespVars,[Nfacs,1]))';
RowNames = cellfun(@(x,y) strcat(x,delimiter,y),RowVars,RowFacs,'UniformOutput',false);
% columns names
colvarnames = cellfun(@(x) replace(x,' ','_'),FactorNames,'UniformOutput',false);
colvarnames = cellfun(@(x) replace(x,'-','_'),colvarnames,'UniformOutput',false);
% create and display stat table
StatTable = array2table(TableArray,'VariableNames',colvarnames,'RowNames',RowNames)
% matlab saves col names but not row names to file
StatTable = cat(2,RowNames',StatTable);

% write table to file
snamex = sprintf([finalfigurepath 'CueBehavior_Stats_Fig_%d.xls'],FigNumber);
writetable(StatTable,snamex);

fprintf('Cue Behavior t stat \n');
disp(Tstat);

% define half of the subplot positions
nrows = 4;
ncols = 6;
skipf = nrows*ncols/2;
subposA = [1 2 3];
subposB = [7 8 9];
subposC = [4 10];
subposD = [5 11];
subposE = [6 12];
posshiftup = [0.02 -0.02];
behaviorshift = 0.02;
emreoshiftleft = 0.035;
labelposshift = [0.05 0.05; 0.05 0.1]; % per icuefield


% plot parameters
EMREOYmax = [25 40];
subpLineWidth = 1.0; % Line thickness of panels

% bar plot parameters
cfg = [];
cfg.jitterrange = 0.2;
cfg.offset = 0.2;
cfg.FaceColor = ColorScheme.MeanMagenta;
cfg.WhiskerLineWidth = 1.5;
cfg.OutlineWidth = 1.0; % bar outline
cfg.MarkerEdgeWidth = 1.0; 


figure('Position',[200 120 1450 850])
HH = [];

for icuefield = 1:Ncuefields
    % plot mean EMREO with sign. cluster for both directions in two axes A
    % and B
    
    % get data
    thisfield = Cuefieldnames{icuefield};
    stat_clusters = GroupStats.CueBehavior.(thisfield).Clusters;
    
    % LEFT emreos -----------------------------
    hh = subplot(nrows,ncols,subposA + skipf*(icuefield-1));
    HH = [HH, hh];
    % get data
    emreo = GroupStats.CueBehavior.(thisfield).EMREO(:,[1,2],:);
    plot([tax_ear(1) tax_ear(end)],[0 0],'-','Color',[1 1 1]*0.8);
    hold on
    % uninformative
    [~,p2] = fb_errorshade(tax_ear,sq(mean(emreo(:,2,:),1))',sq(sem(emreo(:,2,:),1))',ColorScheme.CueEMREO(2,:),'EdgeColor','none');
    %informative
    [~,p1] = fb_errorshade(tax_ear,sq(mean(emreo(:,1,:),1))',sq(sem(emreo(:,1,:),1))',ColorScheme.CueEMREO(1,:),'EdgeColor','none');
    ymax = EMREOYmax(1);
    ylimits = [-ymax ymax];
    plot([0 0], [ylimits], 'k--');
    
    % get significant clusters, if any
    idir = 1;
    for isign = 1:size(stat_clusters,1)
        if ~isempty(stat_clusters{isign,idir})
            % report cluster statistics
            fprintf('cluster statistics: %s %s \n',thisfield,dirlabel{idir});
            fprintf(['pvalue:' repmat('%.3f ',size(stat_clusters{isign,idir}.p)) '\n'],stat_clusters{isign,idir}.p);
            fprintf(['stat:' repmat('%.3f ',size(stat_clusters{isign,idir}.stat)) '\n'],stat_clusters{isign,idir}.stat);
            fprintf(['peak effect:' repmat('%.3f ',size(stat_clusters{isign,idir}.peakCohensD)) '\n'],stat_clusters{isign,idir}.peakCohensD);
            fprintf('\n');
            % then for each sign. cluster plot bar and print temporal
            % boundaries
            for iclus = 1:length(stat_clusters{isign,idir}.p)
                pvalclus = stat_clusters{isign,idir}.p(iclus);
                maxsum = stat_clusters{isign,idir}.stat(iclus);
                cluseffect = stat_clusters{isign,idir}.peakCohensD(iclus);
                maskSig = find(stat_clusters{isign,idir}.mask==iclus);
                if pvalclus < p_critval
                    plot(tax_stat(maskSig),ones(size(maskSig)) .* ymax .* 1.1, 'k-','LineWidth',5.0);
                    temp_bound = stat_clusters{isign,idir}.boundaries(:,iclus);
                    fprintf('cluster %d: %.1f to %.1f ms, p=%.3f maxsum=%.2f cohensDpeak=%.2f \n', ...
                        iclus, temp_bound, pvalclus, maxsum, cluseffect);
                end
            end
            fprintf('\n\n');
        end
    end
    % mark saccade direction
    text(-45,ylimits(2)*0.70,['[-12, -6] degrees' newline 'Contralateral'],'FontSize',10,'FontWeight','bold');
    lh = legend([p2 p1],CueVarnames(icuefield,:));
    lh.FontSize = 10;
    legend boxoff
    xlim([-50 150]);
    ylim(ylimits);
    yticks([-20:10:20]);
    ylabel('Amplitude (mV)');
    % shift slightly
    hh.Position(1) = hh.Position(1) - emreoshiftleft;
    hh.Position(2) = hh.Position(2) + posshiftup(icuefield);
    
    
    
    % RIGHT emreos ----------------------------
    hh = subplot(nrows,ncols,subposB + skipf*(icuefield-1));
    HH = [HH, hh];
    % get data
    emreo = GroupStats.CueBehavior.(thisfield).EMREO(:,[3,4],:);
    plot([tax_ear(1) tax_ear(end)],[0 0],'-','Color',[1 1 1]*0.8)
    hold on
    % uninformative
    [~,p2] = fb_errorshade(tax_ear,sq(mean(emreo(:,2,:),1))',sq(sem(emreo(:,2,:),1))',ColorScheme.CueEMREO(4,:),'EdgeColor','none');
    %informative
    [~,p1] = fb_errorshade(tax_ear,sq(mean(emreo(:,1,:),1))',sq(sem(emreo(:,1,:),1))',ColorScheme.CueEMREO(3,:),'EdgeColor','none');
    ymax = EMREOYmax(2);
    ylimits = [-ymax ymax];
    plot([0 0], [ylimits], 'k--');
    % get significant clusters, if any
    idir = 2;
    for isign = 1:size(stat_clusters,1)
        if ~isempty(stat_clusters{isign,idir})
            % report cluster statistics
            fprintf('cluster statistics: %s %s \n',thisfield,dirlabel{idir});
            fprintf(['pvalue:' repmat('%.3f ',size(stat_clusters{isign,idir}.p)) '\n'],stat_clusters{isign,idir}.p);
            fprintf(['stat:' repmat('%.3f ',size(stat_clusters{isign,idir}.stat)) '\n'],stat_clusters{isign,idir}.stat);
            fprintf(['peak effect:' repmat('%.3f ',size(stat_clusters{isign,idir}.peakCohensD)) '\n'],stat_clusters{isign,idir}.peakCohensD);
            fprintf('\n');
            % then for each sign. cluster plot bar and print temporal
            % boundaries
            for iclus = 1:length(stat_clusters{isign,idir}.p)
                pvalclus = stat_clusters{isign,idir}.p(iclus);
                maxsum = stat_clusters{isign,idir}.stat(iclus);
                cluseffect = stat_clusters{isign,idir}.peakCohensD(iclus);
                maskSig = find(stat_clusters{isign,idir}.mask==iclus);
                if pvalclus < p_critval
                    plot(tax_stat(maskSig),ones(size(maskSig)) .* ymax .* 1.1, 'k-','LineWidth',5.0);
                    temp_bound = stat_clusters{isign,idir}.boundaries(:,iclus);
                    fprintf('cluster %d: %.1f to %.1f ms, p=%.3f maxsum=%.2f cohensDpeak=%.2f \n', ...
                        iclus, temp_bound, pvalclus, maxsum, cluseffect);
                end
            end
            fprintf('\n\n');
        end
    end
    % mark saccade direction
    text(-45,ylimits(2)*0.70,['[12, 6] degrees' newline 'Ipsilateral'],'FontSize',10,'FontWeight','bold');
    lh = legend([p2 p1],CueVarnames(icuefield,:));
    lh.FontSize = 10;
    legend boxoff
    if icuefield == Ncuefields
        xlabel('Time (ms)');
    end
    xlim([-50 150]);
    ylim(ylimits);
    ylabel('Amplitude (mV)');
    % shift slightly
    hh.Position(1) = hh.Position(1) - emreoshiftleft;
    hh.Position(2) = hh.Position(2) + posshiftup(icuefield);
    
    
    
    % -------------------------------------------------
    % plot behavior Response and RT in two axes C and D
    
    % Sensitivity
    hh = subplot(nrows,ncols,subposC + skipf*(icuefield-1));
    HH = [HH, hh];
    Resp = GroupStats.CueBehavior.(thisfield).Resp;
    
    barwithdots(cfg,Resp);
    % print BF value
    tcolor = 'k';
    fweight = 'normal';
    fangle = 'normal';
    ptext = sprintf('BF=%.2f',BF(icuefield,1));
    text(1.5, 3.7, ptext,'Color',tcolor,'FontWeight',fweight,'FontAngle',fangle,'HorizontalAlignment','center');
    
    % offset x ticklabels
    xticklabels([]);
    ymax = max(hh.YAxis.Limits);
    ypos = -labelposshift(icuefield,:) .* ymax;
    ticktext = CueVarnames(icuefield,end:-1:1);
    text([1 2],ypos,ticktext,'FontWeight','bold','HorizontalAlignment','center');
    ylim([0 4]);
    yticks([0:1:4]);
    ylabel('Sensitivity (d prime)');
    % shift slightly
    hh.Position(2) = hh.Position(2) + posshiftup(icuefield);
    hh.Position(1) = hh.Position(1) + 0*behaviorshift;
    
    
    % Bias
    hh = subplot(nrows,ncols,subposD + skipf*(icuefield-1));
    HH = [HH, hh];
    Bias = GroupStats.CueBehavior.(thisfield).SigDetBias;
    
    barwithdots(cfg,Bias);
    % print BF value
    tcolor = 'k';
    fweight = 'normal';
    fangle = 'normal';
    ptext = sprintf('BF=%.2f',BF(icuefield,2));
    text(1.5, 2.75, ptext,'Color',tcolor,'FontWeight',fweight,'FontAngle',fangle,'HorizontalAlignment','center');
    
    % offset x ticklabels
    xticklabels([]);
    ymax = max(hh.YAxis.Limits);
    ypos = -labelposshift(icuefield,:) .* ymax;
    ticktext = CueVarnames(icuefield,end:-1:1);
    text([1 2],ypos,ticktext,'FontWeight','bold','HorizontalAlignment','center');
    
    ylim([0 3]);
    yticks([0:1:3]);
    ylabel('Detection bias');
    % shift slightly
    hh.Position(2) = hh.Position(2) + posshiftup(icuefield);
    hh.Position(1) = hh.Position(1) + 1*behaviorshift;
    
    
    % Reaction time
    hh = subplot(nrows,ncols,subposE + skipf*(icuefield-1));
    HH = [HH, hh];
    RT = GroupStats.CueBehavior.(thisfield).RT;
    
    barwithdots(cfg,RT);
    % print BF value
    tcolor = 'k';
    fweight = 'normal';
    fangle = 'normal';
    ptext = sprintf('BF=%.2f',BF(icuefield,3));
    text(1.5, 1300, ptext,'Color',tcolor,'FontWeight',fweight,'FontAngle',fangle,'HorizontalAlignment','center');
    
    % offset x ticklabels
    xticklabels([]);
    ymax = max(hh.YAxis.Limits);
    ypos = -labelposshift(icuefield,:) .* ymax;
    ticktext = CueVarnames(icuefield,end:-1:1);
    text([1 2],ypos,ticktext,'FontWeight','bold','HorizontalAlignment','center');
    
    ylim([0 1400]);
    yticks([0:200:1400]);
    ylabel('Reaction time (ms)');
    % shift slightly
    hh.Position(2) = hh.Position(2) + posshiftup(icuefield);
    hh.Position(1) = hh.Position(1) + 2*behaviorshift;
    
end



% set axes specifics for all
for ii = 1:length(HH)
    HH(ii).XAxis.FontWeight = 'bold';
    HH(ii).YAxis.FontWeight = 'bold';
    HH(ii).XAxis.FontSize = 12;
    HH(ii).YAxis.FontSize = 12;
    HH(ii).XAxis.LineWidth = subpLineWidth;
    HH(ii).YAxis.LineWidth = subpLineWidth;
end




% add panel labels
labellist = {'A','C','B','D'};
HHList = [1 3 6 8];
for ii = 1:length(labellist)
    hh = HH(HHList(ii));
    [phout,axinv] = panellabel(hh,labellist{ii});
    phout.Position(1) = phout.Position(1) + 0.2;
    if ismember(ii,[2,4])
        phout.Position(1) = phout.Position(1) + 0.3;
    end
end


% set cue field title
for ii = 1:length(FactorNames)
    titletext = sprintf('%s',FactorNames{ii});
    hh = HH([1:5] + (ii-1)*5);
    [phtitle,axinv] = paneltitle(hh,titletext);
end

% add inlays to emreo plots
dirfieldnames = {'NLeft','NRight'};
HHemreo = HH([1 2 6 7]);

% inlay with number of trials across participants
condcolorind = [1 2; 3 4];
iter = 1;
for icuefield = 1:Ncuefields
    for idir = 1:Ndirections
        % get field names
        thisfield = Cuefieldnames{icuefield};
        dirfield = dirfieldnames{idir};
        % get n trial data and colors
        ntrials = fliplr(GroupStats.CueBehavior.(thisfield).(dirfield));
        % create inlay and plot
        thishh = HHemreo(iter);
        ymax = EMREOYmax(idir);
        inlay_pos = [120 -ymax*0.6 22 ymax*0.5];
        inlay_hh = inlay_subplot(thishh,inlay_pos);
        boxplot(ntrials,'BoxStyle','filled','Symbol','');
        % get boxes and whisker handles
        bbh = findobj(inlay_hh,'tag','Box');
        bwh = findobj(inlay_hh,'tag','Whisker');
        bmh = findobj(inlay_hh,'tag','Median');
        % flip ud because of weird MATLAB, last obj normally first in list
        bbh = flipud(bbh);
        bwh = flipud(bwh);
        bmh = flipud(bmh);
        % get condition color
        CondColors = flipud(ColorScheme.CueEMREO(condcolorind(idir,:),:));
        % change color
        for ii = 1:length(bbh)
            bbh(ii).Color = CondColors(ii,:);
            bwh(ii).Color = CondColors(ii,:);
            bmh(ii).XData = ii + [-0.15 0.15]; % more narrow median
            bmh(ii).Color = 'w';
        end
        if icuefield ==  1   
            ylim([50 200]);
            yticks([50, 125, 200]);
        else
            ylim([50 75]);
            yticks([50, 75]);
        end
        xticklabels([]);
        xticks([]);
        ylabel('N trials','FontSize',8);
        inlay_hh.XAxis.Visible = 'off';
        inlay_hh.YAxis.FontWeight = 'bold';
        inlay_hh.YAxis.FontSize = 8;
        box off

        iter = iter + 1;
    end
end


% save to file
fig = gcf;
fname = sprintf([finalfigurepath 'Figure_%d.png'],FigNumber);
exportgraphics(fig,fname,'Resolution',300);

pname = sprintf([finalfigurepath 'Figure_%d.pdf'],FigNumber);
exportgraphics(fig,pname,'Resolution',300);

tname = sprintf([finalfigurepath 'Figure_%d.eps'],FigNumber);
exportgraphics(fig,tname,'Resolution',300);
pause(0.5);



end
