function PlotFinalFigure_1
%
% plot the final figures for project FB03
%


close all

% get ExpList
USECOMP = 3;
ExpList;
defdirs;


% load data
filename = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
GroupStats = load(filename);

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% get colors
ColorScheme = FB03_Colorscheme;

% color p values if samller than p_critval
p_critval = globalP.Sign_Alpha;


% initialize figure count
FigNumber = 1;



%%
% plot a schematic figure with the dots on the screen, the sound locations
% and the cue
% below we can plot the timing of the trials

% Notes:
% aimed to be a double column figure

% load speaker icon
icon = imread([finalfigurepath 'top_down_speaker.png']);
button_press = imread([finalfigurepath 'button_press.png']);

% screen parameters
ScreenSize = globalP.ScreenSize;
centerpoint = globalP.centerpoint;

% icon positions, use inlay subplots, so we dont have to resample the icon
inlay_pos = [-2 -2 4 4] .* globalP.ppd;
inlay_pos = inlay_pos + [centerpoint 0 0] + [0 6 0 0] .* globalP.ppd;
inlay_offset = [-12 12] .* globalP.ppd;
cmap  = [0 0 0; 1 1 1];
cmap2 = [1 1 1; 0 0 0];

% cue bar parameter
cue_length = 2.5 * globalP.ppd;
cue_width = 1.0 * globalP.ppd;
cue_offset = cue_length / 2 + 1.5 * globalP.ppd;
% create cue bar 
cue_bar = [-cue_length/2 -cue_width/2 cue_length cue_width];
cue_bar = [centerpoint 0 0] + cue_bar;
cue_bar(1) = cue_bar(1) + cue_offset;

% axis limits based on 14 degrees boundary
ScreenBounds = [-14 -14 14 14] .* globalP.ppd;
ScreenBounds = ScreenBounds + [centerpoint centerpoint];
xlimits = ScreenBounds([1,3]);
ylimits = ScreenBounds([2,4]);

% conditions
conditions = [-12 -6 6 12];
textoffset = 2.0 * globalP.ppd;

% figure parameters
nrows = 2;
ncols = 3;

figure('Position',[497 368 1069 609]);
tiledlayout(nrows,ncols,'TileSpacing','tight');

% -----------------------------------
% top row of subplots with spatial design

% plot experiment 1 setup
hh(1) = nexttile(1);
%rectangle('Position',[0 0 ScreenSize],'Curvature',0.1);
hold on
% plot center dot
plot(centerpoint(1), centerpoint(2),'o','MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor',[0.6 0.6 0.6]);
text(centerpoint(1) + textoffset, centerpoint(2) - textoffset,'0', 'HorizontalAlignment','center');
% plot target dots
for icond = 1:length(conditions)
    dotX = centerpoint(1) + conditions(icond) * globalP.ppd;
    dotY = centerpoint(2);
    plot(dotX,dotY,'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor',ColorScheme.SaccCondColors(icond,:));
    text(dotX, dotY - textoffset, num2str(conditions(icond)),'HorizontalAlignment','center');
end
% plot vertical target dots
for icond = 1:length(conditions)
    dotX = centerpoint(1);
    dotY = centerpoint(2) + conditions(icond) * globalP.ppd;
    plot(dotX,dotY,'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor',ColorScheme.VertCondColors(icond,:));
    text(dotX + textoffset, dotY, num2str(conditions(icond)),'HorizontalAlignment','center');
end
% text contra and ipsilateral labels below horizontal dots
textX = centerpoint(1) - 9 * globalP.ppd;
textY = centerpoint(2) - 2.5*textoffset;
text(textX,textY,'contra','HorizontalAlignment','center');
textX = centerpoint(1) + 9 * globalP.ppd;
text(textX,textY,'ipsi','HorizontalAlignment','center');
% axes
axis off
xlim(xlimits);
ylim(ylimits);
% set fixed aspect ratio
pbaspect([diff(xlimits) diff(ylimits) 1]);
title('Experiment 1');


% plot experiment 2 setup, horizontal dots, sound location
hh(2) = nexttile(2);
hold on
% plot center dot
plot(centerpoint(1), centerpoint(2),'o','MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor',[0.6 0.6 0.6]);
text(centerpoint(1), centerpoint(2) - textoffset,'0', 'HorizontalAlignment','center');
% plot target dots
for icond = 1:length(conditions)
    dotX = centerpoint(1) + conditions(icond) * globalP.ppd;
    dotY = centerpoint(2);
    plot(dotX,dotY,'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor',ColorScheme.SaccCondColors(icond,:));
    text(dotX, dotY - textoffset, num2str(conditions(icond)),'HorizontalAlignment','center');
end
% text contra and ipsilateral labels below horizontal dots
textX = centerpoint(1) - 9 * globalP.ppd;
textY = centerpoint(2) - 2.5*textoffset;
text(textX,textY,'contra','HorizontalAlignment','center');
textX = centerpoint(1) + 9 * globalP.ppd;
text(textX,textY,'ipsi','HorizontalAlignment','center');
% axes
xlim(xlimits);
ylim(ylimits);
% set fixed aspect ratio
pbaspect([diff(xlimits) diff(ylimits) 1]);
title('Experiment 2');
axis off



% plot experiment 3 setup, horizontal dots, sound location, cue bar
hh(3) = nexttile(3);
hold on
% plot center dot
plot(centerpoint(1), centerpoint(2),'o','MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor',[0.6 0.6 0.6]);
text(centerpoint(1), centerpoint(2) - textoffset,'0', 'HorizontalAlignment','center');
% plot target dots
for icond = 1:length(conditions)
    dotX = centerpoint(1) + conditions(icond) * globalP.ppd;
    dotY = centerpoint(2);
    plot(dotX,dotY,'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor',ColorScheme.SaccCondColors(icond,:));
    text(dotX, dotY - textoffset, num2str(conditions(icond)),'HorizontalAlignment','center');
end
% text contra and ipsilateral labels below horizontal dots
textX = centerpoint(1) - 9 * globalP.ppd;
textY = centerpoint(2) - 2.5*textoffset;
text(textX,textY,'contra','HorizontalAlignment','center');
textX = centerpoint(1) + 9 * globalP.ppd;
text(textX,textY,'ipsi','HorizontalAlignment','center');
% plot cue bar
rectangle('Position',cue_bar,'FaceColor','m','EdgeColor','none');
xlim(xlimits);
ylim(ylimits);
% set fixed aspect ratio
pbaspect([diff(xlimits) diff(ylimits) 1]);
title('Experiment 3');
axis off


% -----------------------------------
% bottom row of subplots with temporal design

% basic parameters
lineX = [1:1000];
basicline = zeros(size(lineX));
lineweight = 1.5;
ylimits = [-8.5 1];

% experiment 1 temporal design
hh(4) = nexttile(4);
% fixation
fixline = basicline;
fixline(100:500) = 1;
% target line
targetline = basicline;
targetline(500:900) = 1;
% eye position line
eyeposline = basicline;
eyeposline(580:680) = [0:0.01:1];
eyeposline(680:end) = 1;
% plot
hold on
plot(lineX,fixline,'r','LineWidth',lineweight);
plot(lineX,targetline - 2,'b','LineWidth',lineweight);
% eye position line
plot(lineX,eyeposline - 4,'k','LineWidth',lineweight);
% text line labels
text(-100, 0,'Fixation','FontWeight','bold','HorizontalAlignment','right','VerticalAlignment','bottom');
text(-100,-2,'Target','FontWeight','bold','HorizontalAlignment','right','VerticalAlignment','bottom');
text(-100,-4,'Eye Position','FontWeight','bold','HorizontalAlignment','right','VerticalAlignment','bottom');
text(-100,-6,'Sound Onset','FontWeight','bold','HorizontalAlignment','right','VerticalAlignment','bottom');
text(-100,-8,'Cue','FontWeight','bold','HorizontalAlignment','right','VerticalAlignment','bottom');
% axes
xticks([0,1000]);
xticklabels([]);
xlabel('Time');
hh(4).XAxis.FontWeight = 'bold';
ylim(ylimits);
yticks([]);
hh(4).YAxis.Visible = 'off';

% experiment 2
hh(5) = nexttile(5);
% fixation
fixline = basicline;
fixline(100:600) = 1;
% target line
targetline = basicline;
targetline(600:900) = 1;
% eye position line
eyeposline = basicline;
eyeposline(680:780) = [0:0.01:1];
eyeposline(780:end) = 1;
% plot 
hold on
% time stamp vertical line
plot([900 900],ylimits,'k--','LineWidth',1.0);
% fixation line
plot(lineX,fixline,'r','LineWidth',lineweight);
% target line
plot(lineX,targetline - 2,'b','LineWidth',lineweight);
% eye position line
plot(lineX,eyeposline - 4,'k','LineWidth',lineweight);
% sound onsets
plot([640 640],[0 1] - 6,'Color',ColorScheme.OnsetGroups(1,:),'LineWidth',2);
plot([730 730],[0 1] - 6,'Color',ColorScheme.OnsetGroups(2,:),'LineWidth',2);
plot([820 820],[0 1] - 6,'Color',ColorScheme.OnsetGroups(3,:),'LineWidth',2);
plot(lineX,basicline - 6,'k','LineWidth',1.0,'LineStyle','--');
% axes
xticks([0,1000]);
xticklabels([]);
hh(5).XAxis.FontWeight = 'bold';
ylim(ylimits);
yticks([]);
hh(5).YAxis.Visible = 'off';




% experiment 3
hh(6) = nexttile(6);
% fixation
fixline = basicline;
fixline(100:600) = 1;
% target line
targetline = basicline;
targetline(600:900) = 1;
% eye position line
eyeposline = basicline;
eyeposline(680:780) = [0:0.01:1];
eyeposline(780:end) = 1;
% cue line
cueline = basicline;
cueline(250:350) = 1;
% plot 
hold on
% time stamp vertical line
plot([900 900],ylimits,'k--','LineWidth',1.0);
% fixation line
plot(lineX,fixline,'r','LineWidth',lineweight);
% target line
plot(lineX,targetline - 2,'b','LineWidth',lineweight);
% eye position line
plot(lineX,eyeposline - 4,'k','LineWidth',lineweight);
% sound onsets
plot([640 640],[0 1] - 6,'Color',ColorScheme.OnsetGroups(1,:),'LineWidth',2);
plot([730 730],[0 1] - 6,'Color',ColorScheme.OnsetGroups(2,:),'LineWidth',2);
plot([820 820],[0 1] - 6,'Color',ColorScheme.OnsetGroups(3,:),'LineWidth',2);
plot(lineX,basicline - 6,'k','LineWidth',1.0,'LineStyle','--');
% cue line
plot(lineX,cueline - 8,'m','LineWidth',lineweight);
% axes
xticks([0,1000]);
xticklabels([]);
hh(6).XAxis.FontWeight = 'bold';
ylim(ylimits);
yticks([]);
hh(6).YAxis.Visible = 'off';



% add panel labels and inlays
ph = panellabel(hh(1),'A');
ph.Position(2) = 1.0; % lift up
ph.Position(1) = ph.Position(1) + 0.5; % shift right

ph = panellabel(hh(2),'B');
ph.Position(2) = 1.0; % lift up
ph.Position(1) = ph.Position(1) + 0.5; % shift right
% plot speaker icons at -6 and 6 degrees
for ii = 1:2
    inpos = inlay_pos + [inlay_offset(ii) 0 0 0];
    inlay_handle = inlay_subplot(hh(2),inpos);
    imagesc(icon);
    colormap(inlay_handle,cmap);
    axis square
    axis off
end

ph = panellabel(hh(3),'C');
ph.Position(2) = 1.0; % lift up
ph.Position(1) = ph.Position(1) + 0.5; % shift right
for ii = 1:2
    inpos = inlay_pos + [inlay_offset(ii) 0 0 0];
    inlay_handle = inlay_subplot(hh(3),inpos);
    imagesc(icon);
    colormap(inlay_handle,cmap);
    axis square
    axis off
end

% inlay button press image
inlay_pos = [800 -10 200 1];
inlay_hh = inlay_subplot(hh(5),inlay_pos);
imagesc(button_press);
colormap(inlay_hh,cmap2)
axis square
axis off

% inlay button press image
inlay_pos = [800 -10 200 1];
inlay_hh = inlay_subplot(hh(6),inlay_pos);
imagesc(button_press);
colormap(inlay_hh,cmap2)
axis square
axis off



% save to file
fig = gcf;
fname = sprintf([finalfigurepath 'Figure_%d.png'],FigNumber);
exportgraphics(fig,fname,'Resolution',300);

pname = sprintf([finalfigurepath 'Figure_%d.pdf'],FigNumber);
exportgraphics(fig,pname,'Resolution',300);

tname = sprintf([finalfigurepath 'Figure_%d.eps'],FigNumber);
exportgraphics(fig,tname,'Resolution',300);
pause(0.5);


clear HH HC hh ph inlay_handle
close all

end
