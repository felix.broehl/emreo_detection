function PlotFinalFigure_3
%
% plot the final figures for project FB03
%


close all

% get ExpList
USECOMP = 3;
ExpList;
defdirs;


% load data
filename = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
GroupStats = load(filename);

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% get colors
ColorScheme = FB03_Colorscheme;

% color p values if samller than p_critval
p_critval = globalP.Sign_Alpha;


% initialize figure count
FigNumber = 3;


%%
% plot behavior data from exp. 2 and 3 in one figure

% Notes:
% make this a 2 column figure

% get data
Behavior = GroupStats.Behavior;
RespVars = {'Response','SigDetBias','RT'};
ncols = size(Behavior{1}.Response,3);
% variable names and labels
target_vars = GroupStats.Behavior{1}.VarNames;
target_labels = GroupStats.Behavior{1}.VarLabels;

% take p-values and adjust within figure - metric x variable x exp.
pvals = cat(3,Behavior{1}.pval, Behavior{2}.pval);
pvals_adjusted = fb_stat_padjust(pvals,'benjamini-hochberg');
% get test statistics and effect sizes
tvals = cat(3,Behavior{1}.tval, Behavior{2}.tval);
df = Behavior{1}.df;
% convert t stat to bayes factor
BF10 = fb_stat_tstat2BF(tvals,df);
BF10(BF10<1) = -1./BF10(BF10<1); % tranform to opposite hypothesis interp.
cohensD = cat(3,Behavior{1}.EffectSize, Behavior{2}.EffectSize);


% create table and export as excel
RowFactors = {'Pval','BF','cohen d'};
ExpNames = {'Exp2','Exp3'};
Nfacs = length(RowFactors); % pval, BF, cohen's d
Nresp = length(RespVars); % response variables, d' and RT
Nvars = length(target_vars); % variables
Nexp = length(Behavior); % Exp. 2 and 3
TableRows = Nfacs * Nresp;
TableCols = Nvars * Nexp;
% reshape into table
TableArray = NaN(TableRows,TableCols);
iter = 1;
for ivar = 1:Nvars
    for iexp = 1:Nexp 
        for iresp = 1:Nresp
            tableind = [1:Nfacs] + (iter-1)*Nfacs;
            tmpvar = [pvals_adjusted(iresp,ivar,iexp) BF10(iresp,ivar,iexp) cohensD(iresp,ivar,iexp)];
            TableArray(tableind) = tmpvar;
            iter = iter + 1;
        end
    end
end

% table row and col labels
delimiter = '_';
% row names
RowFacs = repmat(RowFactors,[1,Nresp]);
RowVars = flatten(repmat(RespVars,[Nfacs,1]))';
RowNames = cellfun(@(x,y) strcat(x,delimiter,y),RowVars,RowFacs,'UniformOutput',false);
% columns names
colvarnames = cellfun(@(x) replace(x,' ','_'),target_vars,'UniformOutput',false);
colvarnames = cellfun(@(x) replace(x,'-','_'),colvarnames,'UniformOutput',false);
ColExps = repmat(ExpNames,[1,Nvars]);
ColVars = flatten(repmat(colvarnames,[Nexp,1]))';
ColNames = cellfun(@(x,y) strcat(x,delimiter,y),ColVars,ColExps,'UniformOutput',false);
% create and display stat table
StatTable = array2table(TableArray,'VariableNames',ColNames,'RowNames',RowNames)
% matlab saves col names but not row names to file
StatTable = cat(2,RowNames',StatTable);

% write table to file
snamex = sprintf([finalfigurepath 'Behavior_Stats_Fig_%d.xls'],FigNumber);
writetable(StatTable,snamex);


% barwithdots specifications
cfg = [];
cfg.jitterrange = 0.2;
cfg.offset = 0.2;
cfg.WhiskerLineWidth = 1.5;
cfg.OutlineWidth = 1.0; % bar outline
cfg.MarkerEdgeWidth = 1.0; 

% bar positions 
barpos = [1 3; 2 4] + [-0.2 0.2];
% plot parameters
subpLineWidth = 1.0; % Line thickness of panels


figure('Position',[236 75 1450 920]);
tlay = tiledlayout(Nresp,ncols,'Padding','compact');
HH = [];

for icol = 1:ncols
    % top row detection sensitivity
    hh = nexttile(icol);
    HH = [HH, hh];
    hold on
    for iexp = 1:Nexp
        data = sq(Behavior{iexp}.Response(:,:,icol));
        thispos = barpos(:,iexp);
        cfg.XAxis = thispos;
        cfg.FaceColor = ColorScheme.BehaviorGroup{icol};
        barwithdots(cfg,data);
        % text BF on top
        tcolor = 'k';
        ptext = sprintf('BF=%.2f',BF10(1,icol,iexp));
        text(thispos(1),3.7,ptext,'Color',tcolor);
        if icol == 1
            ylabel('Sensitivity (d prime)');
        end
    end
    hold off
    xticks(barpos(:));
    if ~ismember(icol,[1,ncols])
        xticklabels(repmat(target_labels(icol,:),1,2));
    else
        % offset x ticklabels
        xticklabels([]);
        ymax = max(hh.YAxis.Limits);
        ypos = -[0.05 0.13 0.05 0.13] .* ymax;
        xpos = barpos(:);
        ticktext = repmat(target_labels(icol,:),1,2);
        for it = 1:length(ticktext)
            text(xpos(it),ypos(it),ticktext{it},'FontWeight','bold',...
                'FontSize',12,'HorizontalAlignment','center')
        end
    end
    xlim([0 5]);
    ylim([0 4]);
    yticks([0:4]);
    title(target_vars{icol});
    
    
    % middle row detection bias
    hh = nexttile(icol + ncols);
    HH = [HH, hh];
    hold on
    for iexp = 1:Nexp
        data = sq(Behavior{iexp}.SigDetBias(:,:,icol));
        thispos = barpos(:,iexp);
        cfg.XAxis = thispos;
        cfg.FaceColor = ColorScheme.BehaviorGroup{icol};
        barwithdots(cfg,data);
        % text BF on top
        tcolor = 'k';
        ptext = sprintf('BF=%.2f',BF10(2,icol,iexp));
        text(thispos(1),2.75,ptext,'Color',tcolor);
        if icol == 1
            ylabel('Detection bias');
        end
    end
    hold off
    xticks(barpos(:));
    if ~ismember(icol,[1,ncols])
        xticklabels(repmat(target_labels(icol,:),1,2));
    else
        % offset x ticklabels
        xticklabels([]);
        ymax = max(hh.YAxis.Limits);
        ypos = -[0.05 0.13 0.05 0.13] .* ymax;
        xpos = barpos(:);
        ticktext = repmat(target_labels(icol,:),1,2);
        for it = 1:length(ticktext)
            text(xpos(it),ypos(it),ticktext{it},'FontWeight','bold',...
                'FontSize',12,'HorizontalAlignment','center')
        end
    end
    xlim([0 5]);
    ylim([0 3]);
    yticks([0:3]);% add title that indicates experiment 2 and 3 data
    xpos = mean(barpos);
    datatext = {'Exp. 2','Exp. 3'};
    for it = 1:length(datatext)
        text(xpos(it),3.22,datatext{it},'Color',[1 1 1]*0.5,'Fontweight','bold','FontSize',12, ...
            'HorizontalAlignment','center');
    end
    
    
    % bottom row RT
    hh = nexttile(icol + 2*ncols);
    HH = [HH, hh];
    hold on
    for iexp = 1:Nexp
        data = sq(Behavior{iexp}.RT(:,:,icol));
        thispos = barpos(:,iexp);
        cfg.XAxis = thispos;
        cfg.FaceColor = ColorScheme.BehaviorGroup{icol};
        barwithdots(cfg,data);
        % text BF on top
        tcolor = 'k';
        fweight = 'normal';
        fangle = 'normal';
        ptext = sprintf('BF=%.2f',BF10(3,icol,iexp));
        text(thispos(1),1300,ptext,'Color',tcolor,'FontWeight',fweight,'FontAngle',fangle);
        if icol == 1
            ylabel('Reaction time (ms)');
        end
    end
    hold off
    xticks(barpos(:));
    % set x tick labels 
    if ~ismember(icol,[1,ncols])
        xticklabels(repmat(target_labels(icol,:),1,2));
    else
        % offset x ticklabels
        xticklabels([]);
        ymax = max(hh.YAxis.Limits);
        ypos = -[0.05 0.13 0.05 0.13] .* ymax;
        xpos = barpos(:);
        ticktext = repmat(target_labels(icol,:),1,2);
        for it = 1:length(ticktext)
            text(xpos(it),ypos(it),ticktext{it},'FontWeight','bold',...
                'FontSize',12,'HorizontalAlignment','center')
        end
    end
    xlim([0 5]);
    ylim([0 1400]);
    yticks([0:200:1400]);
    % add title that indicates experiment 2 and 3 data
    xpos = mean(barpos);
    datatext = {'Exp. 2','Exp. 3'};
    for it = 1:length(datatext)
        text(xpos(it),1500,datatext{it},'Color',[1 1 1]*0.5,'Fontweight','bold','FontSize',12, ...
            'HorizontalAlignment','center');
    end
end


% set axes specifics for all
for ii = 1:length(HH)
    HH(ii).XAxis.FontWeight = 'bold';
    HH(ii).YAxis.FontWeight = 'bold';
    HH(ii).XAxis.FontSize = 12;
    HH(ii).YAxis.FontSize = 12;
    HH(ii).XAxis.LineWidth = subpLineWidth;
    HH(ii).YAxis.LineWidth = subpLineWidth;
end



% save to file
fig = gcf;
fname = sprintf([finalfigurepath 'Figure_%d.png'],FigNumber);
exportgraphics(fig,fname,'Resolution',300);

pname = sprintf([finalfigurepath 'Figure_%d.pdf'],FigNumber);
exportgraphics(fig,pname,'Resolution',300);

tname = sprintf([finalfigurepath 'Figure_%d.eps'],FigNumber);
exportgraphics(fig,tname,'Resolution',300);
pause(0.5);


close all
clear HH hh



end
