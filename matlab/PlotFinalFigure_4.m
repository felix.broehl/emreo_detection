function PlotFinalFigure_4
%
% plot the final figures for project FB03
%


close all

% get ExpList
USECOMP = 3;
ExpList;
defdirs;


% load data
filename = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
GroupStats = load(filename);

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% get colors
ColorScheme = FB03_Colorscheme;

% color p values if samller than p_critval
p_critval = globalP.Sign_Alpha;


% initialize figure count
FigNumber = 4;



%%
% figure with velocity and sound onsets and behavior

% Notes:
% make this a 1 or 1.5 column figure, whatever looks nicer

% get data
tax_eye = GroupStats.Onsets.tax_eye;
tax_window = GroupStats.Onsets.tax_win;
tax_win_boundaries = round([tax_window(1) tax_window(end)]);
VelOn = GroupStats.Onsets.VelOn;
normVelOn = max(mean(VelOn,1));
OnTimeGroups = GroupStats.Onsets.OnTime;
NtGroups = length(OnTimeGroups);
nsubs = size(VelOn,1);

% get behavioral data
% get data
Response = GroupStats.OnsetBehavior.Response;
LinCoeff = GroupStats.OnsetBehavior.LinCoeff;

% get EMREO behavior 
EMREO_Behavior = GroupStats.EMREOBehavior;
ncond = length(EMREO_Behavior.CondEMREO);
tax_ear = EMREO_Behavior.time;
tax_stat = EMREO_Behavior.time_stat;

% print condition x response EMREO comparison ANOVA
GLME_ANOVA = EMREO_Behavior.GLME_ANOVA;
fprintf('BEHAVIOR EMREO ANOVA \n');
disp(GLME_ANOVA);


% get pvalues, test statistics and effect sizes
GLME_pval = GroupStats.OnsetBehavior.GLME_pval;
p_linbeta = GroupStats.OnsetBehavior.StatReg.pval;
nObs = GroupStats.OnsetBehavior.GLME_nObs;
nObs = nObs(1); % should be all the same
% linreg time window
linwin = [-0.25 0.2]' * 1000; % convert to ms


Tstat = GroupStats.OnsetBehavior.StatReg.Tstat;
Fstat_Dprime = [ GroupStats.OnsetBehavior.GLME{1}.Dprime.anova.FStat(2); GroupStats.OnsetBehavior.GLME{2}.Dprime.anova.FStat(2) ];
Fstat_Bias = [ GroupStats.OnsetBehavior.GLME{1}.SigDetBias.anova.FStat(2); GroupStats.OnsetBehavior.GLME{2}.SigDetBias.anova.FStat(2) ];
cohensD = GroupStats.OnsetBehavior.StatReg.cohensD;
% % anova degrees of freedom
Fdf1 = GroupStats.OnsetBehavior.GLME{1}.Dprime.anova.DF1(2);
Fdf2 = GroupStats.OnsetBehavior.GLME{1}.Dprime.anova.DF2(2);
% get bayes Factor for ANOVA and ttest
BF10_A = bf.bfFromF(Fstat_Dprime,Fdf1,Fdf2,nObs);
BF10_T = fb_stat_tstat2BF(Tstat,nsubs-1);
BF10 = [BF10_A BF10_T];
BF10(BF10<1) = -1./BF10(BF10<1); % tranform to opposite hypothesis interp.
BF_Bias = stat_bfFromF(Fstat_Dprime,Fdf1,Fdf2,nsubs);
BF_Bias(BF_Bias<1) = -1./BF_Bias(BF_Bias<1);
% labels and names
Nexp = GroupStats.OnsetBehavior.Nexp;
ExpNames = GroupStats.OnsetBehavior.ExpNames;

% correct for multiple testing
pval_adjust = fb_stat_padjust([GLME_pval p_linbeta],'benjamini-hochberg');

% print statistics
fprintf('ONSET PERFORMANCE \n');
for iexp = 1:2
    fprintf('Exp. # %d \n',iexp);
    fprintf('SENSITIVITY \n');
    fprintf('F(%d|%d) = %.2f   p = %.2f   BF = %.2f \n',Fdf1,Fdf2,Fstat_Dprime(iexp),pval_adjust(iexp,1),BF10(iexp,1));
    fprintf('BIAS \n');
    fprintf('F(%d|%d) = %.2f   p = %.2f   BF = %.2f \n',Fdf1,Fdf2,Fstat_Bias(iexp),pval_adjust(iexp,2),BF_Bias(iexp));
    fprintf('RT \n');
    fprintf('T(%d) = %.2f   p = %.2f   BF = %.2f \n',nsubs-1,Tstat(iexp),pval_adjust(iexp,4),BF10(iexp,2));
end
    
fprintf('Cohens D reaction time \n');
disp(cohensD);


% plot parameters
Nbins = 50;
BinWidth = diff(tax_win_boundaries) / Nbins;
BinEdges = [tax_win_boundaries(1):BinWidth:tax_win_boundaries(2)];

% plot parameters
subpLineWidth = 1.0; % Line thickness of panels

% subplot positions
nrows = 12;
ncols = 6;
subposB = [1]; % dist sound to onset
subsizeB = [2 3];
subposC = [13 43]; % sensitivity
subsizeC = [5 2];
subposD = [15 45]; % RT model
subsizeD = [5 1];
subposE = [4 22 40 58]; % EMREO behavior
subsizeE = [3 3];

% linreg colors
jetmap = jet(nsubs);

% barwithdots parameters
cfg = [];
cfg.jitterrange = 0.2;
cfg.offset = 0.2;
cfg.connect = true;
cfg.FaceColor = ColorScheme.OnsetGroups;
cfg.FaceAlpha = 0.3;
cfg.WhiskerLineWidth = 1.5;
cfg.OutlineWidth = 1.0; % bar outline
cfg.MarkerEdgeWidth = 1.0; 
% EMREO behavior plot parameters
cfg_anova = cfg;
cfg_anova.connect = false;
cfg_anova.BarWidth = 0.6;


% y max for condition ERMEO plots
EMREOYmax = [25 25 40 40];
CondDirLabel = {'Contralateral','Contralateral','Ipsilateral','Ipsilateral'};

% n trials inlay parameters
inlay_pos = [120 -35 22 15];


% -----------------------------------
% plot to figure
figure('Position',[152 218 1285 718]);
tiledlayout(nrows,ncols,'Padding','compact','TileSpacing','compact');
HH = [];


% onset histograms
hh = nexttile(subposB,subsizeB);
HH = [HH, hh];
hold on
for igroup = 1:NtGroups
    histogram(OnTimeGroups{igroup},BinEdges,'FaceColor',ColorScheme.OnsetGroups(igroup,:),'Normalization','probability');
end
% velocity per participants and mean
plot(tax_eye, VelOn./normVelOn.*0.1, 'Color', [0.6 0.5 0.7]);
hold on
plot(tax_eye, mean(VelOn,1)./normVelOn.*0.1,'LineWidth',2.5,'Color',[0.6 0.0 0.8]);
% axes
xlim(tax_win_boundaries);
% include zero
tickmarklocations = sort([floor(tax_window(1)):100:ceil(tax_window(end)), 0]);
xticks(tickmarklocations);
box off
xlabel('Time w. r. t. saccade onset (ms)');
ylabel('Probability');


% sound onset group and RT plots
for iexp = 1:Nexp
    % Reponse plot ---------------------------
    hh = nexttile(subposC(iexp),subsizeC);
    HH = [HH, hh];
    % get data
    resp = sq(Response(iexp,:,:));
    barwithdots(cfg,resp);
    % text
    tcolor = 'k';
    fweight = 'normal';
    fangle = 'normal';
    ptext = sprintf('BF=%.2f',BF10(iexp,1));
    text(1.75,3.7,ptext,'Color',tcolor,'FontWeight',fweight,'FontAngle',fangle);
    % axes
    xlim([0 4]);
    ylim([0 4]);
    ylabel('Sensitivity (d prime)');
    xticklabels({'before','during','after'});
    yticks([0:1:4]);
    
    % Beta[log(RT)] plot --------------------
    hh = nexttile(subposD(iexp),subsizeD);
    HH = [HH, hh];
    % create lines with weights
    beta = sq(LinCoeff(iexp,:,:));
    ncoeff = size(beta,2);
    % convert back to ms weights, using exponents from order
    beta = beta ./ 1000.^(repmat([0:ncoeff-1],nsubs,1));
    yhat = [ones(size(linwin)) linwin] * beta';
    yhat = exp(yhat); 
    
    % plot data
    % average coefficient
    hold on
    for isub = 1:nsubs
        plot(linwin,yhat(:,isub),'-','Color',jetmap(isub,:));
    end
    plot(linwin,mean(yhat,2),'k.-','LineWidth',2.5);
    % text
    tcolor = 'k';
    fweight = 'normal';
    fangle = 'normal';
    ptext = sprintf('BF=%.2f',BF10(iexp,2));
    text(100,1300,ptext,'Color',tcolor,'FontWeight',fweight,'FontAngle',fangle,...
        'HorizontalAlignment','center');
    
    % axes
    box off
    xlim([linwin(1) linwin(end)]);
    ylim([0 1400]);
    yticks([0:200:1400]);
    % axes
    if iexp == Nexp
        xlabel('Time w. r. t. saccade onset (ms)');
    end
    ylabel(['Reaction time w. r. t.' newline 'sound onset (ms)']);
    
end

    
% plot EMREO for correct and incorrect trials 
for icond = 1:ncond
    hh = nexttile(subposE(icond),subsizeE);
    HH = [HH, hh];
    % get data
    CondData = EMREO_Behavior.CondEMREO{icond};
    stat_clusters = EMREO_Behavior.Clusterstats;
    CondLabel = sprintf('%d degrees', EMREO_Behavior.CondLabel(icond));
    % colors - grey and sacc condition color
    CondColors = [ [1 1 1] * 0.5; ColorScheme.SaccCondColors(icond,:)];
    
    % plot data
    ymax = EMREOYmax(icond);
    plot([tax_ear(1) tax_ear(end)],[0 0],'-','Color',[1 1 1]*0.8);
    hold on
    % incorrect first
    [~, hp1] = fb_errorshade(tax_ear, sq(mean(CondData(:,2,:),1))', sq(sem(CondData(:,2,:),1))', CondColors(1,:));
    [~, hp2] = fb_errorshade(tax_ear, sq(mean(CondData(:,1,:),1))', sq(sem(CondData(:,1,:),1))', ...
        CondColors(2,:));
    for isign = 1:size(stat_clusters,1)
        if ~isempty(stat_clusters{isign})
            % report cluster statistics
            fprintf('cluster statistics: %s \n',CondLabel);
            fprintf(['pvalue:' repmat('%.3f ',size(stat_clusters{isign}.p)) '\n'],stat_clusters{isign}.p);
            fprintf(['stat:' repmat('%.3f ',size(stat_clusters{isign}.stat)) '\n'],stat_clusters{isign}.stat);
            fprintf(['peak effect:' repmat('%.3f ',size(stat_clusters{isign}.peakCohensD)) '\n'],stat_clusters{isign}.peakCohensD);
            fprintf('\n');
            % then for each sign. cluster plot bar and print temporal
            % boundaries
            for iclus = 1:length(stat_clusters{isign}.p)
                pvalclus = stat_clusters{isign}.p(iclus);
                maxsum = stat_clusters{isign}.stat(iclus);
                cluseffect = stat_clusters{isign}.peakCohensD(iclus);
                maskSig = find(stat_clusters{isign}.mask==iclus);
                if pvalclus < p_critval
                    plot(tax_stat(maskSig),ones(size(maskSig)) .* ymax .* 0.9, 'k-','LineWidth',5.0);
                    temp_bound = stat_clusters{isign}.boundaries(:,iclus);
                    fprintf('cluster %d: %.1f to %.1f ms, p=%.3f maxsum=%.2f cohensDpeak=%.2f \n', ...
                        iclus, temp_bound, pvalclus, maxsum, cluseffect);
                end
            end
            fprintf('\n\n');
        end
    end
    plot([0 0], [-ymax ymax], 'k--');
    % set condition label
    dirlabel = CondDirLabel{icond};
    text(-45,ymax*0.70,[CondLabel newline dirlabel],'FontSize',10,'FontWeight','bold');
    % axes
    xlim([-50 150]);
    ylim([-ymax ymax]);
    lh = legend([hp1 hp2],{'incorrect','correct'});
    lh.FontSize = 10;
    legend boxoff
    if icond == ncond
        xlabel('Time (ms)');
    end
    ylabel('Amplitude (mV)');
    
end


% set axes specifics for all
for ii = 1:length(HH)
    HH(ii).XAxis.FontWeight = 'bold';
    HH(ii).YAxis.FontWeight = 'bold';
    HH(ii).XAxis.FontSize = 10;
    HH(ii).YAxis.FontSize = 10;
    HH(ii).XAxis.LineWidth = subpLineWidth;
    HH(ii).YAxis.LineWidth = subpLineWidth;
end

% create panel labels for testing
panellabels = {'A','B','C','D'};
iter = 1;
for ih = [1 2 4 6]
    phout = panellabel(HH(ih),panellabels{iter});
    phout.Position(1) = phout.Position(1) + 0.25;
    iter = iter + 1;
end




% number of trials inlays 
for icond = 1:ncond
    % inlay with number of trials per condition across participants
    ymax = EMREOYmax(icond);
    % get ntrials data
    ntrials = [EMREO_Behavior.Nfalse(:,icond) EMREO_Behavior.Ntrue(:,icond)];
    inlay_pos = [120 -ymax*0.66 22 ymax*0.5];
    thisax = HH(end-4+icond);
    inlay_hh = inlay_subplot(thisax,inlay_pos);
    boxplot(ntrials,'BoxStyle','filled','Symbol','');
    % get boxes and whisker handles
    bbh = findobj(inlay_hh,'tag','Box');
    bwh = findobj(inlay_hh,'tag','Whisker');
    bmh = findobj(inlay_hh,'tag','Median');
    % flip ud because of weird MATLAB, last obj normally first in list
    bbh = flipud(bbh);
    bwh = flipud(bwh);
    bmh = flipud(bmh);
    % colors - grey and sacc condition color
    CondColors = [ [1 1 1] * 0.5; ColorScheme.SaccCondColors(icond,:)];
    % change color
    for ii = 1:length(bbh)
        bbh(ii).Color = CondColors(ii,:);
        bwh(ii).Color = CondColors(ii,:);
        bmh(ii).XData = ii + [-0.15 0.15]; % more narrow median
        bmh(ii).Color = 'w';
    end
    ylim([0 100]);
    yticks([0, 50, 100]);
    ylabel('N trials','FontSize',8);
    xticklabels([]);
    xticks([]);
    inlay_hh.XAxis.Visible = 'off';
    inlay_hh.YAxis.FontWeight = 'bold';
    inlay_hh.YAxis.FontSize = 8;
    box off
end


% save to file
fig = gcf;
fname = sprintf([finalfigurepath 'Figure_%d.png'],FigNumber);
exportgraphics(fig,fname,'Resolution',300);

pname = sprintf([finalfigurepath 'Figure_%d.pdf'],FigNumber);
exportgraphics(fig,pname,'Resolution',300);

tname = sprintf([finalfigurepath 'Figure_%d.eps'],FigNumber);
exportgraphics(fig,tname,'Resolution',300);
pause(0.5);


close all
clear HH hh

end
