
%% new study participants
SubList(1).name = 'FB03_S01';
SubList(1).eog = false; % no eog data recorded
SubList(1).exclude = 1; % set to 1 later
SubList(1).comment = 'Behavior did not work, otherwise very good - exclude this data!!!';

SubList(2).name = 'FB03_S02';
SubList(2).eog = false; % no eog data recorded
SubList(2).exclude = 1; % probably 1
SubList(2).comment = 'behavior did not work; bad ET due to strong glasses; too sleepy; sounds with 2 ms length';

SubList(3).name = 'FB03_S03';
SubList(3).eog = false; % no eog data recorded
SubList(3).exclude = 1; % probably 1
SubList(3).comment = 'behavior did not work; sound with 2 ms length';

SubList(4).name = 'FB03_S04';
SubList(4).eog = false; % no eog data recorded
SubList(4).exclude = 1; 
SubList(4).comment = 'ear data did not work, therefore exclude';

SubList(5).name = 'FB03_S05';
SubList(5).eog = false; % no eog data recorded
SubList(5).exclude = 1; 
SubList(5).comment = 'same participant as S01, but behavior worked here';

SubList(6).name = 'FB03_S06';
SubList(6).eog = false; % no eog data recorded
SubList(6).exclude = 1; 
SubList(6).comment = 'sc.threshold adjusted  to 75% from E2 block 2 onwards';

SubList(7).name = 'FB03_S07';
SubList(7).eog = false; % no eog data recorded
SubList(7).exclude = 1; 
SubList(7).comment = 'sc.threshold adjusted  to 80% from E2 block 2 onwards';

SubList(8).name = 'FB03_S08';
SubList(8).eog = false; % no eog data recorded
SubList(8).exclude = 1; 
SubList(8).comment = 'sc.threshold adjusted  to 80% from E2 block 2 onwards';


% from here on we use 80-100% of sc.threshold as seed and iteratively attenuate
% sound intensity to achieve ~55 to 70% accuracy!
%
SubList(9).name = 'FB03_S09';
SubList(9).eog = false; % no eog data recorded
SubList(9).exclude = 1; 
SubList(9).comment = 'bad EMREO! therefore exclude';

SubList(10).name = 'FB03_S10';
SubList(10).eog = false; % no eog data recorded
SubList(10).exclude = 0; 
SubList(10).comment = '';

SubList(11).name = 'FB03_S11';
SubList(11).eog = false; % no eog data recorded
SubList(11).exclude = 0; 
SubList(11).comment = 'E1 B3 was saved as B2 for .mat and .edf files. changed of pc';

SubList(12).name = 'FB03_S12';
SubList(12).eog = false; % no eog data recorded
SubList(12).exclude = 0; 
SubList(12).comment = '';

SubList(13).name = 'FB03_S13';
SubList(13).eog = false; % no eog data recorded
SubList(13).exclude = 0; 
SubList(13).comment = 'screwed up E1 B2, so updated files';

SubList(14).name = 'FB03_S14';
SubList(14).eog = false; % no eog data recorded
SubList(14).exclude = 0; 
SubList(14).comment = '';

SubList(15).name = 'FB03_S15';
SubList(15).eog = false; % no eog data recorded
SubList(15).exclude = 1; 
SubList(15).comment = 'bad ear signal quality';

SubList(16).name = 'FB03_S16';
SubList(16).eog = false; % no eog data recorded
SubList(16).exclude = 0; 
SubList(16).comment = '';

SubList(17).name = 'FB03_S17';
SubList(17).eog = false; % no eog data recorded
SubList(17).exclude = 1; 
SubList(17).comment = '';

SubList(18).name = 'FB03_S18';
SubList(18).eog = false; % no eog data recorded
SubList(18).exclude = 0; 
SubList(18).comment = '';


% this participant was recorded by me an jenny together
SubList(19).name = 'FB03_S19';
SubList(19).eog = false; % no eog data recorded
SubList(19).exclude = 0; 
SubList(19).comment = 'missing blocks 1-3 from Exp. 3';


% from here on forward data was collected from jenny only
SubList(20).name = 'FB03_S20';
SubList(20).eog = false; % no eog data recorded
SubList(20).exclude = 1; 
SubList(20).comment = 'bad ear data quality, not enough samples for power in TEOAE file';

SubList(21).name = 'FB03_S21';
SubList(21).eog = false; % no eog data recorded
SubList(21).exclude = 0; 
SubList(21).comment = 'not enough samples for power in TEOAE file';

SubList(22).name = 'FB03_S22';
SubList(22).eog = false; % no eog data recorded
SubList(22).exclude = 1; 
SubList(22).comment = 'very bad data quality overall';

SubList(23).name = 'FB03_S23';
SubList(23).eog = false; % no eog data recorded
SubList(23).exclude = 1; 
SubList(23).comment = 'most data is missing';

SubList(24).name = 'FB03_S24';
SubList(24).eog = false; % no eog data recorded
SubList(24).exclude = 0; 
SubList(24).comment = 'very bad performance in Exp. 3, otherwise okay';

SubList(25).name = 'FB03_S25';
SubList(25).eog = false; % no eog data recorded
SubList(25).exclude = 1; 
SubList(25).comment = 'bad ear signal quality';

SubList(26).name = 'FB03_S26';
SubList(26).eog = false; % no eog data recorded
SubList(26).exclude = 1; 
SubList(26).comment = 'bad ear signal quality';

SubList(27).name = 'FB03_S27';
SubList(27).eog = false; % no eog data recorded
SubList(27).exclude = 1; 
SubList(27).comment = 'bad ear signal quality';

SubList(28).name = 'FB03_S28';
SubList(28).eog = false; % no eog data recorded
SubList(28).exclude = 0; 
SubList(28).comment = '';

SubList(29).name = 'FB03_S29';
SubList(29).eog = false; % no eog data recorded
SubList(29).exclude = 1; 
SubList(29).comment = 'bad ear signal quality - subj. was very tired';

SubList(30).name = 'FB03_S30';
SubList(30).eog = false; % no eog data recorded
SubList(30).exclude = 0; 
SubList(30).comment = '';

SubList(31).name = 'FB03_S31';
SubList(31).eog = false; % no eog data recorded
SubList(31).exclude = 1; 
SubList(31).comment = 'bad ear signal quality';

SubList(32).name = 'FB03_S32';
SubList(32).eog = false; % no eog data recorded
SubList(32).exclude = 1; 
SubList(32).comment = 'bad ear signal quality - Exp. 3 missing';

SubList(33).name = 'FB03_S33';
SubList(33).eog = false; % no eog data recorded
SubList(33).exclude = 0; 
SubList(33).comment = '';

SubList(34).name = 'FB03_S34';
SubList(34).eog = false; % no eog data recorded
SubList(34).exclude = 0; 
SubList(34).comment = '';



