function Analyze_Thresholds_decibel(goodsubs)
%
% load staircase results from FB03 and take mean + std over thresholds
% 
% we measured sound intensity in dB with respect to varying instensity 
% factors and use that to map individual factors to dB intensity
%

close all

% data directory
data_log = 'E:\FB03\data_Log';


% get subject list 
USECOMP = 3
ExpList;
defdirs;


% get file list and take only staircase files
filelist = dir(data_log);
skipflag = contains({filelist.name},'Staircase');
filelist = filelist(skipflag);

% thresholds
Thresholds = zeros(length(goodsubs),1);
MeanRunning = zeros(length(goodsubs),1);

% go through all subject for individual analysis steps
iter = 1;
for S = goodsubs
    RunningThreshold = [];
    for exp = [2 3]
        ARG_event.exp = exp;
        [event,~] = load_event_blocks(S,SubList,datapath_log,ARG_event);
        RunningThreshold = cat(1,RunningThreshold,event.EventLog(:,5));
    end
    MeanRunning(iter) = mean(RunningThreshold);
    
    % find subject data
    subsearch = sprintf('S%02d',S);
    ifile = find(contains({filelist.name},subsearch));
    if length(ifile) > 1
        ifile = ifile(1);
    end
    
    % get filename
    scname = strjoin({filelist(ifile).folder, filelist(ifile).name},'\');
    sc = load(scname,'sc');
    
    Thresholds(iter) = sc.sc.threshold;
    iter = iter + 1;
    
end

% adapted thresholds
AdaptedThresholds = Thresholds .* MeanRunning;

% map thresholds on dB
Amp = [0.02 0.05 0.09 0.13 0.2 0.25 0.3]';
dB =  [28.5 31.2 36.5 38.5 43.5 45.0 46.5]';

npoints = length(Amp);
offset = ones(size(Amp));
beta = [offset Amp] \ dB;
populationdB = [ones(size(AdaptedThresholds)) AdaptedThresholds]*beta;

% plot function
figure
subplot(121)
plot(Amp,dB,'bo');
hold on
plot(Amp,[offset Amp]*beta);
xlabel('Amp');
ylabel('dB');
title('estimated function');


subplot(122)
% bar plot parameters
cfg = [];
cfg.jitterrange = 0.2;
cfg.offset = 0.2;
cfg.WhiskerLineWidth = 1.5;
cfg.OutlineWidth = 1.0; % bar outline
cfg.MarkerEdgeWidth = 1.0; 
cfg.connect = false;

barwithdots(cfg,populationdB);
xlim([0 2]);
ylabel('dB');

% annotate mean and sd of population mean
ax = gca;
ymax = ax.YAxis.Limits(2);
txtstring = sprintf(['mean = %.02f' newline 'SD = %.02f' newline 'SEM = %.02f'], ...
    mean(populationdB),std(populationdB),sem(populationdB));
text(0.2,0.9*ymax,txtstring,'HorizontalAlignment','left');

title('Population thresholds');


header = 'Sound Intensity';
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_SoundIntensity.png']);
print('-dpng',fname);
pause(0.5);


% -------------------------------------------------------------------------
% close all figures after each segment, otherwise there will be too many
close all
fprintf('\n');
fprintf('close all figures here \n');

end
