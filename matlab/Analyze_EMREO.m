function Analyze_EMREO(S)
%
% load ear data from all blocks and compute statisitcal significance of the
% EMREO signal across all trials
% 

close all

% get ExpList
USECOMP = 3
ExpList;
defdirs;

Subname = SubList(S).name

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% structure in which we save summary metrics from individual analyses
sumname = sprintf([datapath_res 'SumMetrics_FB03_S%02d_all.mat'],S);
SumMetrics = [];
SumMetrics.date = date;


%% ----------------------------------------------------------------------------
% general parameters
ARG.resamplefs = globalP.ResampleFs;
ARG.zscore = globalP.zscore; % if whole data matrix should be zscored

% parameters for filtering and data inclusion - We expect EMREO around 35 Hz
ARG_ear.zscore = false;
ARG_ear.hpfilter   = 'no';  
ARG_ear.hpfreq     = 10;
ARG_ear.lpfilter   = 'no';  
ARG_ear.lpfreq     = 50;
ARG_ear.prestim    = -1.5;
ARG_ear.poststim   =  2.0;


ARG_eye.Window = [-1.5,2.0]; % seconds
ARG_eye.Align = 'onset';
ARG_eye.ppd = globalP.ppd;
ARG_eye.resamplefs = ARG.resamplefs;

% set experiment in a for loop
Experiment = [1 2 3];
sign_threshold = globalP.Sign_Alpha * 100; % 5 percent
Ntrials_per_exp = [161 144 144]; % number of trials per block in each experiment


% save data to struct
SumMetrics.samplerate = ARG.resamplefs;


%% --------------------------------------------------------------------------
% Load Event data
for exp = Experiment
    ARG_event.exp = exp;
    [event,~] = load_event_blocks(S,SubList,datapath_log,ARG_event);
    % account for different name in experiment 1
    if exp == 1
        event.EventSequence = event.Sequence;
    end
    event.EventSequence = event.EventSequence(:,1);
    event = keepfields(event,'EventSequence');
    
    % transform condition if from the first experiment
    if exp == 1
        % check which conditions where vertical saccades so we can dismiss
        % them from the other data structures as well
        %
        % set condition label to 0, because later those are sorted out
        % anyways!
        set_to_false = any(event.EventSequence == [1 2 4 6 8],2);
        event.EventSequence(set_to_false) = 0;
        % convert all to labels consistent with other experiments
        event.EventSequence(~set_to_false) = (event.EventSequence(~set_to_false) - 1) ./2;
        
    end
    % add label from which experiment the trials originate
    event.explabel = ones(size(event.EventSequence)) * exp;
    Events{exp} = event;
end


% append experiments
% assumes same fieldnames in events cell
Events = cell2mat(Events);
eventfields = fieldnames(Events);
for ifield = 1:length(eventfields)
    % assumes they are all row vectors
    EventLog.(eventfields{ifield}) = cell2mat({Events(:).(eventfields{ifield})}');
end
clear Events event



%% --------------------------------------------------------------------------
% Load Ear data
for exp = Experiment
    ARG_ear.exp = exp;
    ARG_ear.Ntrials = Ntrials_per_exp(exp);
    Ear_data{exp} = load_eardata_blocks(S,SubList,datapath_ear,ARG_ear);
end
Ear_data = ft_appenddata([],Ear_data{:});

% resample to new sample rate
cfg = [];
cfg.resamplefs = ARG.resamplefs;
Ear_data = ft_resampledata(cfg, Ear_data);



%% --------------------------------------------------------------------------
% Load Eye data
for exp = Experiment
    ARG_eye.exp = exp;
    [Sacc{exp},TimingLog{exp}] = load_eyedata_blocks(S,SubList,datapath_eye,ARG_eye);
end

% concatenate all experiments
Sacc = ft_appenddata([],Sacc{:});
TimingLog = cat(1,TimingLog{:});

% put into one structure to clean trials
Eye_data.Window = ARG_eye.Window;
Eye_data.Sacc = Sacc;
Eye_data.TimingLog = TimingLog;


%% CLEAN TRIALS WITH FUNCTION

Analysis_log = [];
Analysis_log.TimingLog = TimingLog;
Analysis_log.Condition = EventLog.EventSequence(:,1);
Analysis_log.ExpLabel  = EventLog.explabel;

% clean trials here
cfg = [];
cfg.fsample = ARG.resamplefs;
cfg.window = [-0.15 0.2]; % window in seconds to cut around saccade onset
cfg.TFR = true; % returns TRF Ear data 
cfg.TFR_window = [-0.6 0.8]; % in seconds - for TFR analysis
[Analysis_log,Ear_data_clean,Eye_data_clean,TrialStats] = clean_trialsFB03(cfg,Analysis_log,Ear_data,Eye_data);

% unpack eye data again
Sacc = Eye_data_clean.Sacc;
TimingLog = Eye_data_clean.TimingLog;
% find column based on label
VelColumn = strcmp(Sacc.label,'EYE_V');
AccColumn = strcmp(Sacc.label,'EYE_Acc');
Velocity = cellfun(@(x) x(VelColumn,:), Sacc.trial,'UniformOutput',false);
Velocity = cat(1,Velocity{:});
Acceleration = cellfun(@(x) x(AccColumn,:), Sacc.trial,'UniformOutput',false);
Acceleration = cat(1,Acceleration{:});

% unpack ear data
ALL_EAR = Ear_data_clean.ALL_EAR_ONSET;
tax_ear = Ear_data_clean.tax_ear;
ALL_EAR_TFR = Ear_data_clean.ALL_EAR_TFR;
tax_tfr = Ear_data_clean.tax_tfr;

% zscore if desired
if ARG.zscore
    ALL_EAR = (ALL_EAR - mean(ALL_EAR(:))) / std(ALL_EAR(:));
    ALL_EAR_TFR = (ALL_EAR_TFR - mean(ALL_EAR_TFR(:))) / std(ALL_EAR_TFR(:));
    emreounit = 'SD';
else
    % convert from uV to mV
    ALL_EAR = ALL_EAR ./ 1000; 
    ALL_EAR_TFR = ALL_EAR_TFR ./ 1000;
    emreounit = 'mV';
end


% unpack stats
Ngood_trials = TrialStats.Ngood_trials;


% save to metrics
SumMetrics.TrialStats = TrialStats;

fprintf('\n');
fprintf('Retaining %d of %d trials (%.2f %%) \n\n',Ngood_trials,TrialStats.ntrial,Ngood_trials/TrialStats.ntrial);



%% -----------------------------------------------------------------
% display saccade end points per horizontal condition

% tranform condition label to degrees as the independent variable
degrees = Analysis_log.Degrees; % index corresponds to the condition labels
x_con = Analysis_log.SaccPosCondition;


onset_offset = Analysis_log.TimingLog(:,[1,2]);
Pos0 = zeros(Ngood_trials,2); 
Pos1 = Pos0;


for itrial = 1:Ngood_trials
    trial_time = onset_offset(itrial,:);
    Pos0(itrial,:) = Sacc.trial{itrial}([2,3],trial_time(1));
    Pos1(itrial,:) = Sacc.trial{itrial}([2,3],trial_time(2));
end

% plot saccade start and end points


%% display mean velocity and acceleration
% ----------------------------------------------------------------
% How well are different eye movement metrics correlated?
% Velocity, duration, acceleration

% time window
tax_eye = [ARG_eye.Window(1):1/ARG.resamplefs:ARG_eye.Window(2)-1/ARG.resamplefs] .* 1000;

% saccade direction
% right saccades
k=[1:2:12]; % all odd condition numbers
curr_cond = find(any(Analysis_log.Condition==k,2));
velo_right = Velocity(curr_cond,:);
acc_right  = Acceleration(curr_cond,:);

% left saccades
k=[0:2:12]; % all even condition numbers
curr_cond = find(any(Analysis_log.Condition==k,2));
velo_left = Velocity(curr_cond,:);
acc_left  = Acceleration(curr_cond,:);

% saccade distance
% six degrees saccades
k=[1,2, 5,6, 9,10]; % 
curr_cond = find(any(Analysis_log.Condition==k,2));
velo_six = Velocity(curr_cond,:);
acc_six  = Acceleration(curr_cond,:);

% twelve degrees saccades
k=[3,4, 7,8, 11,12]; % all odd condition numbers
curr_cond = find(any(Analysis_log.Condition==k,2));
velo_twelve = Velocity(curr_cond,:);
acc_twelve  = Acceleration(curr_cond,:);


figure('Position',[174 494 1066 484]);
subplot(221);
hold on
plot(tax_eye,nanmean(velo_right));
plot(tax_eye,nanmean(velo_left));
xlim([-50 150]);
xlabel('time (ms)');
ylabel('Velocity (�/s)');
legend({'right','left'});

subplot(222);
hold on
plot(tax_eye,nanmean(velo_six));
plot(tax_eye,nanmean(velo_twelve));
xlim([-50 150]);
xlabel('time (ms)');
ylabel('Velocity (�/s)');
legend({'six','twelve'});

subplot(223);
hold on
plot(tax_eye,nanmean(acc_right));
plot(tax_eye,nanmean(acc_left));
xlim([-50 150]);
xlabel('time (ms)');
ylabel('Acceleration (�/s^{2})');
legend({'right','left'});

subplot(224);
hold on
plot(tax_eye,nanmean(acc_six));
plot(tax_eye,nanmean(acc_twelve));
xlim([-50 150]);
xlabel('time (ms)');
ylabel('Acceleration (�/s^{2})');
legend({'six','twelve'});


fname = sprintf([figurepath Subname '_VeloAcc_AllExperiments.png']);
print('-dpng',fname);


% ---------------------------------------------------
% now look at how well peak velocity and saccade duration and amplitude are
% correlated
Pos = Pos1 - Pos0;
Amp = sqrt(sum(Pos.^2,2));
Duration = diff(TimingLog(:,[1,2]),[],2);
PeakVel = max(Velocity,[],2);
PeakAcc = max(Acceleration,[],2);

% compute only amplitude on x axis
XAmp = Pos1(:,1) - Pos0(:,1);
degrees = Analysis_log.Degrees;
[~,ii] = sort(degrees);
for icond = 1:length(degrees)
    cond_ind = Analysis_log.SaccPosCondition == icond;
    XAmplitude{icond} = XAmp(cond_ind);
    StartPosX{icond} = Pos0(cond_ind,1);
    EndPosX{icond} = Pos1(cond_ind,1);
end
% sort XAmplitude by degrees value
XAmplitude = XAmplitude(ii);
StartPosX = StartPosX(ii);
EndPosX = EndPosX(ii);

% compute correlation coefficients
EyeMetricsCorr = corrcoef([Amp,Duration,PeakVel,PeakAcc]);
MetricLabels = {'Amp','Dur','Vel_{Peak}','Acc_{Peak}'};

% plot
figure('Position',[903 300 801 644])

colormap(summer)
imagesc(EyeMetricsCorr);
colorbar
axis square

xticks([1:length(MetricLabels)]);
xticklabels(MetricLabels);
yticks([1:length(MetricLabels)]);
yticklabels(MetricLabels);
header = 'Eye Metric correlations';
title(header);


% plot true horizontal amplitude against peak eye velocity
figure
plot([-20 20],[0 0],'-','Color',[1 1 1]*0.6);
hold on
plot([0 0],[-1500 1500],'-','Color',[1 1 1]*0.6);
% for visibility purposes invert velocity for contralateral saccades
plot(XAmp,sign(XAmp).*PeakVel,'rx');
xlabel('eye amplitude (degrees)');
ylabel('velocity (degree/s)');
% we could try to fit linear or sigmoid curves through the data

ckfiguretitle(['Horiz. Eye Amplitude vs. Peak Velocity' newline 'All trials']);

fname = sprintf([figurepath Subname '_Eye_Amp_PeakVelo.png']);
print('-dpng',fname);


% save data to structure
SumMetrics.Eye.label = {'right','left','six','twelve'};
SumMetrics.Eye.time = tax_eye;
SumMetrics.Eye.velocity = expand_dims([nanmean(velo_right); nanmean(velo_left); nanmean(velo_six); nanmean(velo_twelve)], 1);
SumMetrics.Eye.acceleration = expand_dims([nanmean(acc_right); nanmean(acc_left); nanmean(acc_six); nanmean(acc_twelve)], 1);
SumMetrics.Eye.correlation = expand_dims(EyeMetricsCorr,1);
SumMetrics.Eye.XAmplitude = XAmplitude;
SumMetrics.Eye.StartPosX = StartPosX;
SumMetrics.Eye.EndPosX = EndPosX;
SumMetrics.Eye.CorrLabel = MetricLabels;


%% display mean EAR trace per condition
% -----------------------------------------------------------------
% Can we replicate the EMREO with all trials?


% start figure
figure('Position',[162 121 1665 833]); 

% ---------------------------------------------------
% mean EMREO across left and right saccades
subplot(2,4,[1,2]);

% left and right
% right saccades
curr_cond = find(any(x_con==[1,3],2));
nright = length(curr_cond);
tmp_right = ALL_EAR((curr_cond),:);
rightmean = mean(tmp_right);
fb_errorshade(tax_ear,mean(tmp_right),sem(tmp_right),'b');
hold on

% left saccades 
curr_cond = find(any(x_con==[2,4],2));
nleft = length(curr_cond);
tmp_left = ALL_EAR((curr_cond),:);
leftmean = mean(tmp_left);
fb_errorshade(tax_ear,mean(tmp_left),sem(tmp_left),'r');
% set y limits
ax = gca;
toplim = max(abs(ax.YAxis.Limits));
ylim([-toplim,toplim]);
xlabel('time (ms)');
ytext = sprintf('Amplitude (%s)',emreounit);
ylabel(ytext);
header = sprintf('Ear: right(b, n=%d)/left(r, n=%d)',nright,nleft);
title(header);
grid on
header = sprintf('%s Experiment 1+2+3',Subname);
ckfiguretitle(header,'k',10);
lh = legend({'+/- SEM','ipsilateral','+/- SEM','contralateral'});


% ---------------------------------------------------
% plot both mean curves and all trials mean with one direction flipped
subplot(2,4,[5,6]);
plot(tax_ear,rightmean,'b--');
hold on
plot(tax_ear,leftmean .* -1,'r--');
plot(tax_ear,mean([leftmean.*-1;rightmean],1),'k','LineWidth',1.2);
grid on
% set y limits
ax = gca;
toplim = max(abs(ax.YAxis.Limits));
ylim([-toplim,toplim]);
xlabel('time (ms)');
ytext = sprintf('Amplitude (%s)',emreounit);
ylabel(ytext);
legend({'ipsilateral','contralateral inverted','mean'});


% ---------------------------------------------------
% plot EMREOs based on the four endpoint conditions 
subplot(2,4,[3,4]);
% color from magenta to green
initcolor = [1 0 1; 0 1 0] ;
interpcolor = interp_colormap(initcolor,length(degrees)) .* 0.7;
% order color palette according to condition labels
[~,sortI] = sort(degrees);
[~,sortII] = sort(sortI);
ncond = [];
interpcolor = interpcolor(sortII,:);
iter = 1;
for icond = sortI
    curr_cond = find(any(x_con==icond,2));
    ncond(iter) = length(curr_cond);
    tmp_cond = ALL_EAR((curr_cond),:);
    tmp_mean(icond,:) = mean(tmp_cond);
    %fb_errorshade(tax_ear,mean(tmp_cond),sem(tmp_cond),interpcolor(icond,:));
    pp(icond) = plot(tax_ear,mean(tmp_cond),'Color',interpcolor(icond,:));
    hold on
    iter = iter + 1;
end
grid on
% text number of trials
ax = gca;
toplim = max(abs(ax.YAxis.Limits));
ylim([-toplim,toplim]);
interpcolor = interpcolor(sortI,:);
for icond = 1:length(degrees)
    condstring = sprintf('n=%d',ncond(icond));
    ytext = toplim * 0.9 - (icond-1) * toplim*0.1;
    text(-140,ytext,condstring,'Color',interpcolor(icond,:),'HorizontalAlignment','left');
end

xlabel('time (ms)');
ytext = sprintf('Amplitude (%s)',emreounit);
ylabel(ytext);
% print sorted degrees label
legend(string(degrees(sortI)));
header = 'mean EMREO traces per condition';
title(header);


% intra individual SNR plot
% compute mean absolute amplitude (the mean was already subtracted)
meantrialamp = mean(abs(hilbert(ALL_EAR')),1);
yminmax = [min(meantrialamp) max(meantrialamp)];
% find trial closest to the median across trials
[~,trialInd] = min(abs(meantrialamp - median(meantrialamp)));
% plot superposition of EMREO and single representative trial
subplot(2,4,7);
p1 = plot(tax_ear,ALL_EAR(trialInd,:),'-','Color',[1 1 1] .* 0.6,'LineWidth',0.5);
hold on
p2 = plot(tax_ear,rightmean,'b-','LineWidth',1.2);
p3 = plot(tax_ear, leftmean,'r-','LineWidth',1.2);
xlim([tax_ear(1) tax_ear(end)]);
%ylim(yminmax);
xlabel('time (ms)');
ytext = sprintf('Amplitude (%s)',emreounit);
ylabel(ytext);
legend([p1 p2 p3], {'single trial','ipsilateral','contralateral'});

header = 'Signal to noise Ratio';
title(header);

% histogram of mean power
boundaries = globalP.EMREOwindow;
subwin = (tax_ear > boundaries(1) & tax_ear < boundaries(2));
ax = subplot(2,4,8);
Nbins = 100;
Lpow = mean(abs(hilbert(leftmean(subwin))));
Rpow = mean(abs(hilbert(rightmean(subwin))));
hp = histogram(meantrialamp,Nbins,'Orientation','horizontal');
hold on
plot([0,ax.XLim(2)],[Rpow,Rpow],'b');
plot([0,ax.XLim(2)],[Lpow,Lpow],'r');
ylabel('average Power per trial');
% text time window considered for EMREO power
powtxt = sprintf('EMREO power during %d to %d ms',boundaries);
text(ax.XLim(2) * 1.1, ax.YLim(1), powtxt,'HorizontalAlignment','left','Rotation',90);


% figure title
fname = sprintf([figurepath Subname '_EMREO_AllExperiments.png']);
print('-dpng',fname);


% save data to structure
SumMetrics.EMREO.unit = emreounit;
SumMetrics.EMREO.label = {'ipsilateral','contralateral','mean'};
SumMetrics.EMREO.time = tax_ear;
SumMetrics.EMREO.data = expand_dims([mean(tmp_right); mean(tmp_left); mean([leftmean.*-1;rightmean],1)], 1);
SumMetrics.EMREO.datacond = expand_dims(tmp_mean(sortI,:),1);
SumMetrics.EMREO.ntrials = expand_dims(ncond,1);
SumMetrics.EMREO.condlabel = expand_dims(degrees(sortI),1);
SumMetrics.EMREO.SNR = expand_dims([Lpow Rpow] ./ median(meantrialamp),1);
SumMetrics.EMREO.SNRmediantrial = {ALL_EAR(trialInd,:)};




%% TFR analysis EMREO

% time-freq windoe to compute average power
temp_win = globalP.EMREOwindow ./ 1000; % convert from ms to s
freq_win = globalP.EMREOfreqwindow;

% separate Ear data by condition
% tranform condition label to degrees as the independent variable
degrees = Analysis_log.Degrees; % corresponds to the condition labels
ncond = length(degrees);
cond_index = Analysis_log.SaccPosCondition;

% left right condition label
condlabel = {'contra -12','contra -6','ipsi +6','ipsi +12'};

figure('Position',[321 542 1204 371]);
for icondx = 1:ncond
    Ear_TFR = Ear_data;
    Ear_TFR.trialinfo = 1:sum(cond_index==icondx);
    Ear_TFR.trial = mat2cell(ALL_EAR_TFR(cond_index==icondx,:),ones(1,sum(cond_index==icondx)),length(tax_tfr))';
    Ear_TFR.time = repmat({tax_tfr ./ 1000}, [sum(cond_index==icondx),1])';

    % set TFR parameters
    cfg = [];
    cfg.output = 'pow';
    cfg.method = 'mtmconvol';
    cfg.taper = 'hanning';
    cfg.foi = [16:1:44];
    cfg.t_ftimwin = 7./cfg.foi;
    cfg.toi = [tax_tfr(1)/1000 : 0.01 : tax_tfr(end)/1000];
    
    % analysis
    freq_ear = ft_freqanalysis(cfg,Ear_TFR);
    % average power
    t_win = temp_win(1) < freq_ear.time & temp_win(2) > freq_ear.time;
    f_win = freq_win(1) < freq_ear.freq & freq_win(2) > freq_ear.freq;
    temp_spec_density(icondx) = mean(flatten(freq_ear.powspctrm(1,f_win,t_win)));
    
    % plot 
    p_cfg = [];
    p_cfg.baseline = [-0.20 -0.10];
    p_cfg.channel = 'Erg1';
    p_cfg.xlim = [tax_ear(1) tax_ear(end)]./1000;
    p_cfg.title = condlabel{icondx};
    subplot(1,ncond,icondx);
    ft_singleplotTFR(p_cfg,freq_ear);
    axis square
    
    drawnow
end

header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname '_EMREO_TRF.png']);
print('-dpng',fname);


% save data to structure
SumMetrics.TFR.t_win = temp_win;
SumMetrics.TFR.f_win = freq_win;
SumMetrics.TFR.spec_density = temp_spec_density;
SumMetrics.TFR.label = condlabel;




%% Characterize EMREO by resampling
% -----------------------------------------------------------------
% Is the EMREO a stereotypical signal?

% compute several target variables:
% first 4 peaks with minimum peak prominence - amplitude and timing
% and the integral of the EMREO in predefined time window

% important is that we DO NOT normalize the EMREO here, so that we contain
% the between subject variance for the PCA and clustering analysis

% resample (100) trials for 1000 times with replacement and repeat with
% average across trials within each participant
% do this spearately for saccades to the left and right


% tranform condition label to degrees as the independent variable
degrees = Analysis_log.Degrees; % corresponds to the condition labels
x_con = Analysis_log.SaccPosCondition;


% set time window
boundaries = globalP.EMREOwindow;
subwin = (tax_ear > boundaries(1) & tax_ear < boundaries(2));
tax_signal = tax_ear(subwin);
% maximum number of peaks that we want to find
Nmaxpeaks = 3;
% min peak prominence without normalizing EMREO
PeakPromFactor = 0.2;

% slice Ear data for smaller time window
copy_EAR = ALL_EAR(:,subwin);

% compute mean absolute power across whole data
meantrialamp = abs(hilbert(ALL_EAR')');
meantrialamp = mean(meantrialamp(:));


% set parameters
ndraws = 1000;
target_label = {'peak amp','peak-to-peak amp','area'};

% right 12 saccades
curr_cond = any(x_con==3,2);
EAR_RIGHT12 = copy_EAR(curr_cond,:);
Sacc_RIGHT12 = Sacc.trial(curr_cond);

% right 6 saccades
curr_cond = any(x_con==1,2);
EAR_RIGHT6 = copy_EAR(curr_cond,:);
Sacc_RIGHT6 = Sacc.trial(curr_cond);

% left 6 saccades
curr_cond = any(x_con==2,2);
EAR_LEFT6 = copy_EAR(curr_cond,:);
Sacc_LEFT6 = Sacc.trial(curr_cond);

% left 12 saccades
curr_cond = any(x_con==4,2);
EAR_LEFT12 = copy_EAR(curr_cond,:);
Sacc_LEFT12 = Sacc.trial(curr_cond);

EARdata = {EAR_LEFT12, EAR_LEFT6, EAR_RIGHT6, EAR_RIGHT12};
Saccdata = {Sacc_LEFT12, Sacc_LEFT6, Sacc_RIGHT6, Sacc_RIGHT12};
cond_title = [-12 -6 6 12];
nconditions = length(EARdata);
cond_factor = sign(degrees);


% allocate space
Target_vars = zeros(nconditions,3,ndraws);
EMREOS = zeros(nconditions,length(tax_signal),ndraws);
ResampleCorrCoef = zeros(nconditions,ndraws,ndraws);
SamplingFeatures = nan(nconditions,ndraws,2*Nmaxpeaks+2);
TrueFeatures = nan(nconditions,2*Nmaxpeaks+2);

% if we want to do a cluster analysis after PCA, we do NOT want to
% normalize here, because this would remove between participant variance!!!

% sample trials with repetition, do it once with true EMREO as well
tic
for icond = 1:nconditions
    ntrials_draw = size(EARdata{icond},1);
    
    % compute min peak prominence with respect to the EMREO of all trials
    PeakProm = (max(mean(EARdata{icond},1)) - min(mean(EARdata{icond},1))) * PeakPromFactor;
    
    for idraw = 1:ndraws+1
        % randomly choose trials with replacement, or the true EMREO mean
        if idraw == ndraws+1
            emreo = EARdata{icond};
        else
            randind = randi(ntrials_draw,ntrials_draw,1);
            emreo = EARdata{icond}(randind,:);
        end
        
        % compute power, then average
        emreopow = mean(flatten(abs(hilbert(emreo'))));
        emreo = mean(emreo,1);
        % center around zero, take absolute
        emreo = emreo - mean(emreo);
        emreoplot = emreo;
        emreo = abs(emreo);
        
        % find zero crossings, min prominence
        [pks,locs] = findpeaks(emreo,'MinPeakProminence',PeakProm);
        % we assume at most (Nmaxpeaks) peaks
        max_amp = nan(1,Nmaxpeaks);
        max_ind = nan(1,Nmaxpeaks);
        if length(pks) <= Nmaxpeaks
            max_amp(1:length(pks)) = pks;
            max_ind(1:length(pks)) = tax_signal(locs);
        else
            max_amp = pks(1:Nmaxpeaks);
            max_ind = tax_signal(locs(1:Nmaxpeaks));
        end
        % compute absolute area under signal
        integral = trapz(emreo);
        
        % features into one vector
        featurevec = [max_amp max_ind integral emreopow];
        
        % save features
        if idraw == ndraws+1
            TrueFeatures(icond,:) = featurevec;
        else
            % save all features for cluster analysis
            SamplingFeatures(icond,idraw,:) = featurevec;
            % save selected features for initial inspection
            Target_vars(icond,1,idraw) = max(emreoplot);
            Target_vars(icond,2,idraw) = max(emreoplot) - min(emreo);
            Target_vars(icond,3,idraw) = integral;
            % save EMREO, save sign again if negative
            EMREOS(icond,:,idraw) = emreoplot;
        end
        
        % resample the gaze vectors and average them as well
        % with randind get start and end position
        % subtract start from end, take mean gaze across resampled trials
        % save position for later
        SaccTime = Analysis_log.TimingLog(randind,[3,4]);
        pos0 = Saccdata{icond}(randind);
        
    end
    ResampleCorrCoef(icond,:,:) = corrcoef(sq(EMREOS(icond,:,:)));
end
toc


% ---------------------------------------------------
% set inlay position for top row
xmaxdiff = tax_signal(end) - tax_signal(1);

% plot sampling data
figure('Position',[152 145 1710 833]);

for icond = 1:nconditions
    sh = subplot(2,nconditions,icond);
    plot(tax_signal,sq(EMREOS(icond,:,:)));
    hold on
    plot(tax_signal,zeros(size(tax_signal)),'k-');
    xlim([tax_signal(1), tax_signal(end)]);
    ylim([repmat(max(abs(sh.YAxis.Limits)) * 1.5, [1,2]) .* [-1 1]]);
    ylimits = sh.YAxis.Limits;
    ymaxdiff = diff(ylimits); % -1 to 1 for corrcoef
    
    xlabel('time (ms)');
    ytext = sprintf('Amplitude (%s)',emreounit);
    ylabel(ytext);
    header = sprintf('%d degrees', cond_title(icond));
    title(header);
    % get correlation data from lower triangular
    tril_ind = find(tril(ones([ndraws,ndraws]),-1)~=0);
    corrdata = flatten(ResampleCorrCoef(icond,:,:));
    corrdata = corrdata(tril_ind);
    % text number of trials
    restxt = sprintf('n = %d trials with replacement',size(EARdata{icond},1));
    text(5,ylimits(1) + ymaxdiff*0.1,restxt,'HorizontalAlignment','left');
    % text median correlation coefficient next to inlay
    corrstring = sprintf(['median' newline 'r=%.2f'],median(corrdata));
    text(50,ylimits(2) - ymaxdiff*0.15,corrstring,'Color','r','HorizontalAlignment','right');
    
    % create correlation inlay with lower triangular
    inlay_pos = [52 ylimits(2)-ymaxdiff*0.3 xmaxdiff ymaxdiff] .* [1 1 0.2 0.2];
    inlay = inlay_subplot(sh,inlay_pos);
    hgram = histogram(inlay,corrdata,20);
    inlay.XAxisLocation = 'top'; % reset to top
    lowprctile = prctile(hgram.Data,2);
    % set x axis limits
    lowbound = 0.75;
    if lowprctile < 0.75; lowbound = lowprctile; end
    xlim([lowbound 1]);
    xticks([lowbound 1]);
    ssy = sprintf('%.2f',lowbound);
    xticklabels({ssy, '1'});
    yticklabels([]);
    
    ax = subplot(2,nconditions,icond + nconditions);
    tv_coef(icond,:,:) = corrcoef(sq(Target_vars(icond,:,:))');
    imagesc(sq(tv_coef(icond,:,:)));
    ax.CLim = [0 1];
    colorbar
    colormap summer
    axis square
    xticks([1:length(target_label)]);
    yticks([1:length(target_label)]);
    xticklabels(target_label);
    ax.XAxis.TickLabelRotation = 45;
    if icond == 1
        yticklabels(target_label);
        ax.YAxis.TickLabelRotation = 45;
    else
        yticklabels([]);
    end
end


header = ['Left col. sampled EMREOS, Right col. confusion matrix' newline ...
    'Top row right saccades, Bottom row left saccades'];
ckfiguretitle(header);

fname = sprintf([figurepath Subname '_EMREO_sampling.png']);
print('-dpng',fname);



% lower triangular indices
%tril_ind = find(tril(ones(size(tv1_coef)),-1)~=0);
% we probably do this later in the group analysis

% ---------------------------------------------------
% save data to structure
SumMetrics.Sampling.label = target_label;
SumMetrics.Sampling.tv_coef = expand_dims(tv_coef,1);
SumMetrics.Sampling.Target_vars = expand_dims(Target_vars,1);
SumMetrics.Sampling.CondLabel = cond_title;
SumMetrics.Sampling.SamplingFeatures = expand_dims(SamplingFeatures,1);
SumMetrics.Sampling.TrueFeatures = expand_dims(TrueFeatures,1);




%% Principal component analysis on design matrix (Ear data)

% PCA parameter
Ncomp = 20; % to reduce work load

% select a smaller time window for the PCA
pca_time_limits = globalP.EMREOwindow;
pca_window = [tax_ear > pca_time_limits(1) & tax_ear < pca_time_limits(2)];
tax_pca = tax_ear(pca_window);

% separate Ear data by condition
% tranform condition label to degrees as the independent variable
degrees = Analysis_log.Degrees; % corresponds to the condition labels
ncond = length(degrees);
cond_index = Analysis_log.SaccPosCondition;
% planned saccade amplitude based on condition
x_con = degrees(cond_index)';
cond_ind = x_con == sort(degrees);

fprintf('\n compute PCA over conditions \n');
tic
coeff = []; coeffRand = [];
Explained = []; ExpRand = [];
for icond = 1:ncond
    % get data
    PCA_EAR = ALL_EAR(cond_ind(:,icond),pca_window);
    
    % do pca on ear data 
    [cf, score, latent, ~, expl] = pca(PCA_EAR,'NumComponents',Ncomp);
    coeff{icond} = cf;
    Explained{icond} = expl;
    
    % randomize by circshifting rows 
    [nrow,ncol] = size(PCA_EAR);
    nrand = 1000;
    cfRand = zeros(nrand,ncol,Ncomp);
    explainedRand = zeros(nrand,Ncomp);
    parfor irand = 1:nrand
        EAR_RAND = zeros(size(PCA_EAR));
        for irow = 1:nrow
            % circ shift by random amount
            rand_shift = round(rand * ncol);
            EAR_RAND(irow,:) = circshift(PCA_EAR(irow,:),rand_shift,2);
        end
        [cfr,~,~,~,expr] = pca(EAR_RAND,'NumComponents',Ncomp);
        cfRand(irand,:,:) = cfr;
        explainedRand(irand,:) = expr([1:Ncomp],:);
    end 
    coeffRand{icond} = cfRand;
    Score{icond} = score(:,1:10);
    ExpRand{icond} = explainedRand;
end
toc


% ---------------------------------------------------
% plot data
figure('Position',[96 180 1713 768]);
for icond = 1:ncond
    subplot(2,ncond,icond);
    hi = imagesc(coeff{icond}(:,[1:10])');
    % set x axis
    hi.XData = tax_pca;
    hold on
    plot([0 0],[0 11],'r--','LineWidth',2.0);
    xlim([tax_pca(1) tax_pca(end)]);
    xlabel('Time (ms)');
    ylabel('Nth component');


    subplot(2,ncond,icond+ncond)
    p1 = plot(Explained{icond});
    hold on
    p2 = plot(median( ExpRand{icond},1),'k-');
    p3 = plot(prctile( ExpRand{icond},5,1),'k--');
    plot(prctile( ExpRand{icond},95,1),'k--');
    xlim([0 10]);
    xlabel('Nth component');
    ylabel('Explained variance (%)');
    legend([p1 p2 p3],{'true','shuffled','5th/95th percentile'},'Location','northeast');
end

header = 'Principal component analysis on Ear data';
ckfiguretitle(header);

fname = sprintf([figurepath Subname '_EMREO_PCA.png']);
print('-dpng',fname);


% save data to struct
SumMetrics.PCA.Coeff = coeff;
SumMetrics.PCA.CoeffRand = cfRand;
SumMetrics.PCA.Score = Score;
SumMetrics.PCA.Explained = Explained;
SumMetrics.PCA.ExplainedRand = ExpRand;
SumMetrics.PCA.time = tax_pca;



%% perform median split on velocity and acceleration
% -----------------------------------------------------------------
% does intraconditional variance predict first peak latency and height?

% do we want to save the peaks and troughs and compare across participants?

% get smaller window around EMREO [-5 to 70 ms]
boundaries = globalP.EMREOwindow;
subwin = (tax_ear > boundaries(1) & tax_ear < boundaries(2));
tax_emreo = tax_ear(subwin);
EMREO = ALL_EAR(:,subwin);

% to find peak velocities we search the first 50 ms of eye movements
eye_boundaries = [0 50];
eye_win = ( tax_eye > eye_boundaries(1) & tax_eye < eye_boundaries(2) );

% compute median 
CondAmp = unique(abs(degrees));
Namp = length(CondAmp);

% we must also flip the sign of one of the conditions
% this can be done with the degrees array
% get the sign for each trial
cond_sign = sign(degrees);
cond_sign = sum( (cond_index == [1 2 3 4]) .* cond_sign, 2);
EMREO = EMREO .* cond_sign;


% for group level analysis
% first compute mean EMREO and find first peaks
for icond = 1:Namp
    % select subset of trials per condition
    cond_label = find(CondAmp(icond) == abs(degrees));
    cond_ind = any(cond_index == cond_label,2);
    
    % compute averag EMREO
    condEMREO = mean(EMREO(cond_ind,:),1);
    
    % find peaks with minimum height based on one SD
    minPeakHeight = 0.5 .* std(condEMREO);
    [pks,locs] = findpeaks(condEMREO,'MinPeakProminence',minPeakHeight);
    AllPeaksEMREO(icond,:) = [pks(1) locs(1)];
end


% individual analysis
% each condition pooled by distance
for icond = 1:Namp
    % select subset of trials per condition
    cond_label = find(CondAmp(icond) == abs(degrees));
    cond_ind = any(cond_index == cond_label,2);
    % find maximum velocity and acceleration in first 50 ms
    [PeakVelo,IPeakV] = max(Velocity(:,eye_win),[],2);
    [PeakAcce,IPeakA] = max(Acceleration(:,eye_win),[],2);
    
    % compute median of peak distribution across trials in this condition
    EyeMedian = median([PeakVelo(cond_ind) PeakAcce(cond_ind)],1,'omitnan');
    % find all trials slower or faster than median and in this condition
    velo_split_index = [all([cond_ind, PeakVelo < EyeMedian(1)],2), all([cond_ind, PeakVelo > EyeMedian(1)],2) ];
    acce_split_index = [all([cond_ind, PeakAcce < EyeMedian(2)],2), all([cond_ind, PeakAcce > EyeMedian(2)],2) ];
    % compute average EMREO in those trials
    VeloSplitEMREO = [mean(EMREO(velo_split_index(:,1),:),1,'omitnan'); mean(EMREO(velo_split_index(:,2),:),1,'omitnan')];
    AcceSplitEMREO = [mean(EMREO(acce_split_index(:,1),:),1,'omitnan'); mean(EMREO(acce_split_index(:,2),:),1,'omitnan')];
    % concatenate velocity and acceleration EMREOs
    SplitEMREOdata = cat(1,expand_dims(VeloSplitEMREO,1),expand_dims(AcceSplitEMREO,1));
    
    % find peaks in EMREOs
    % isplit == 1 is lower, isplit == 2 is higher than median
    for isplit = 1:2
        % define minimum peak height by one standard deviation
        % for velocity split
        minPeakHeight = 0.5 .* std(VeloSplitEMREO(isplit,:));
        [pks,locs] = findpeaks(VeloSplitEMREO(isplit,:), 'MinPeakProminence',minPeakHeight);
        PeaksEMREOdata(1,isplit,:) = [pks(1) locs(1)];
        % for acceleration split
        minPeakHeight = 0.5 .* std(AcceSplitEMREO(isplit,:));
        [pks,locs] = findpeaks(AcceSplitEMREO(isplit,:), 'MinPeakProminence',minPeakHeight);
        PeaksEMREOdata(2,isplit,:) = [pks(1) locs(1)];
    end
    
    % save data
    PeaksEye{icond} = [PeakVelo PeakAcce];
    PeaksEMREO{icond} = PeaksEMREOdata;
    Medians{icond} = EyeMedian;
    CondInd{icond} = cond_ind;
    SplitEMREO{icond} = SplitEMREOdata;
end



% ---------------------------------------------------
% plot the meadian split EMREOs
Nbins = 150;
SplitColor = [0 0 1; 1 0 0];
ylimfactor = 1.5; % scale y limits on emreo plots

figure('Position',[110 118 1717 860]);

for icond = 1:Namp
    % get condition label
    cond_ind = CondInd{icond};
    
    subplot(3,4,[1,2]+(icond-1)*Namp);
    p1 = plot(tax_eye,nanmean(Velocity(CondInd{icond},:),1));
    yyaxis right
    p2 = plot(tax_eye,nanmean(Acceleration(CondInd{icond},:),1));
    xlim([-50, 150]);
    xlabel('time (ms)');
    header = sprintf('abs(%d degrees) conditions',CondAmp(icond));
    title(header);
    legend([p1 p2],{'Velocity','Acceleration'},'Location','northeast');
    
    % plot histogram of velocity peak height
    ax = subplot(3,4,5+(icond-1)*Namp);
    histogram(PeaksEye{icond}(cond_ind,1),Nbins);
    hold on
    plot(repmat(Medians{icond}(1),1,2),[0,ax.YAxis.Limits(2)],'r-');
    xlabel('velocity (degree/s)');
    title('Velocity');
    
    % plot histogram of acceleration peak height
    ax = subplot(3,4,6+(icond-1)*Namp);
    histogram(PeaksEye{icond}(cond_ind,2),Nbins);
    hold on
    plot(repmat(Medians{icond}(2),1,2),[0,ax.YAxis.Limits(2)],'r-');
    xlabel('acceleration (degree/s^{2})');
    title('Acceleration');
    
    % get ylimits for subplots
    ymax = max(abs(SplitEMREO{icond}(:))) .* ylimfactor;
    % plot velocity split EMREOs
    subplot(3,4,9+(icond-1)*Namp);
    % smaller than median
    p1 = plot(tax_emreo,sq(SplitEMREO{icond}(1,1,:)),'Color',SplitColor(1,:));
    hold on
    % bigger than median
    p2 = plot(tax_emreo,sq(SplitEMREO{icond}(1,2,:)),'Color',SplitColor(2,:));
    % text latency of first peak
    text(0,-ymax*0.6,'first Peak at:');
    peaklattxt = sprintf('%.2f ms',tax_emreo(PeaksEMREO{icond}(1,1,2)) ./ ARG.resamplefs .* 1000);
    text(0,-ymax*0.75,peaklattxt,'Color',SplitColor(1,:));
    peaklattxt = sprintf('%.2f ms',tax_emreo(PeaksEMREO{icond}(1,2,2)) ./ ARG.resamplefs .* 1000);
    text(0,-ymax*0.9,peaklattxt,'Color',SplitColor(2,:));
    % axes specifications
    xlim([tax_emreo(1) tax_emreo(end)]);
    ylim([-ymax ymax]);
    xlabel('time (ms)');
    legend([p1 p2],{'lower','higher'},'Location','northeast');
    
    % plot acceleration split EMREOs
    subplot(3,4,10+(icond-1)*Namp);
    % smaller than median
    p1 = plot(tax_emreo,sq(SplitEMREO{icond}(2,1,:)),'Color',SplitColor(1,:));
    hold on
    % bigger than median
    p2 = plot(tax_emreo,sq(SplitEMREO{icond}(2,2,:)),'Color',SplitColor(2,:));
    % text latency of first peak
    text(0,-ymax*0.6,'first Peak at:');
    peaklattxt = sprintf('%.2f ms',tax_emreo(PeaksEMREO{icond}(2,1,2)) ./ ARG.resamplefs .* 1000);
    text(0,-ymax*0.75,peaklattxt,'Color',SplitColor(1,:));
    peaklattxt = sprintf('%.2f ms',tax_emreo(PeaksEMREO{icond}(2,2,2)) ./ ARG.resamplefs .* 1000);
    text(0,-ymax*0.9,peaklattxt,'Color',SplitColor(2,:));
    % axes specifications
    xlim([tax_emreo(1) tax_emreo(end)]);
    ylim([-ymax ymax]);
    xlabel('time (ms)');
    legend([p1 p2],{'lower','higher'},'Location','northeast');
    
    drawnow
end


header = sprintf(['%s Experiment 1+2+3' newline 'EMREOs split by eye velocity/acceleration'],Subname);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname '_EMREO_velocity_split.png']);
print('-dpng',fname);


% ---------------------------------------------------
% save data to structure
SumMetrics.Split.PeaksEye = PeaksEye;
SumMetrics.Split.PeaksEMREO = PeaksEMREO;
SumMetrics.Split.Medians = Medians;
SumMetrics.Split.CondInd = CondInd;
SumMetrics.Split.CondAmp = CondAmp;
SumMetrics.Split.time = tax_emreo;
SumMetrics.Split.AllPeaksEMREO = expand_dims(AllPeaksEMREO,1);
SumMetrics.Split.SplitLabel = {'lower','higher'};



%% Regression analysis on EMREO traces
% -----------------------------------------------------------------
% the procedure with the chosen window is described in the Gruters paper
% 2018 PNAS


% ---------------------------------------------------
% get predictor variables: planned and true saccade amplitude

% tranform condition label to degrees as the independent variable
degrees = Analysis_log.Degrees; % corresponds to the condition labels
cond_index = Analysis_log.SaccPosCondition;
% x_con : conditional saccade amplitude
% x_dis : true saccade displacement 
% X_var : saccade variance
% X_dev : mean conditional deviance from target

% planned saccade amplitude based on condition
x_con = degrees(cond_index)';
centerpoint = globalP.centerpoint ./ globalP.ppd; % screen center point
x_fix = (Pos0(:,1) - centerpoint(1)); % variance around central fixation dot
x_end = (Pos1(:,1) - centerpoint(1)) - x_con; % variance around all end points
% start and end positions
x_start = x_fix;
x_target = (Pos1(:,1) - centerpoint(1));

% sanity check: how well are the predictors decorrelated?
amp_c = abs( Pos1(:,1) - centerpoint(1));
amp_t = abs( Pos1(:,1) - Pos0(:,1));
amplitude_diff = (amp_c - amp_t);
predictive_deviance = median(amplitude_diff);
% if the fixation does not predict the saccade direction, then amp_c and
% amp_t should be similar and the deviance should be very close to zero.

% small figure
figure
histogram(amp_c,100);
hold on
histogram(amp_t,100);
histogram(amplitude_diff,100);
xticks([-12:6:12]);
legend({'-center','-fix','diff'});
header = sprintf('trial-wise diff - median=%.3f',predictive_deviance);
title(header);

% ---------------------------------------------------
% print figure
fname = sprintf([figurepath Subname '_EMREO_pred_decorr.png']);
print('-dpng',fname);


% ---------------------------------------------------
% get EMREO data
% take smaller window around EMREO [-25 to 100 ms]
subwin = (tax_ear > -25 & tax_ear < 100);
tax_emreo = tax_ear(subwin);
boundaries = globalP.EMREOwindow;
sign_window = (tax_emreo > boundaries(1) & tax_emreo < boundaries(2));
EMREO = ALL_EAR(:,subwin);
npoint = size(EMREO,2);



% ---------------------------------------------------
% regression analysis with all saccades
RegModels = [];
predictors = {[x_con x_fix x_end], [x_fix x_end], [x_con x_end], [x_con x_fix], [x_start x_target]};
predtitles = {'Constant','Horiz. cond. displacement','Horiz. fixation var.','Horiz. endpoint var.'};
npred = length(predictors);
offset = ones(size(x_con));

% fit models for all given predictors
for ipred = 1:npred
    x_pred = predictors{ipred};
    [~, ncol] = size(x_pred);
    beta = [offset x_pred]\EMREO;

    % evaluate the regression model
    yhat = [offset x_pred] * beta;
    R_square = 1 - sum((EMREO - yhat).^2,1) ./ sum((EMREO - mean(EMREO,1)).^2);
    
    % log likelihood
    n = size(EMREO,1);
    r = EMREO - yhat;
    sigma = std(r);
    LL = -n*log(2*pi)/2 - n.*log(sigma.^2)./2 - (1./(2.*sigma.^2))*(yhat-EMREO)'*(yhat-EMREO);
    BIC = (ncol+1) *log(n) - 2*LL;

    % randomize with Monte-Carlo procedure, exchange condition labels
    nrand = 1000;
    betaRand = zeros(ncol+1,length(beta),nrand);
    for irand = 1:nrand
        % fit model
        Xrand = x_pred(randperm(length(x_pred)),:);
        betaRand(:,:,irand) = [offset Xrand]\EMREO;

        % evaluate model
        yhat_r = [offset Xrand] * sq(betaRand(:,:,irand));
        R_squareRand(irand,:) = 1 - sum((EMREO - yhat_r).^2,1) ./ sum((EMREO - mean(EMREO,1)).^2);
    end


    % find significant EMREO points, above 95th percentile
    sign_points = beta([2:end],:) < sq(prctile(betaRand([2:end],:,:),sign_threshold,3)) | beta([2:end],:) > sq(prctile(betaRand([2:end],:,:),100-sign_threshold,3));
    prc_sign = sum(sign_points(:,sign_window),2) / sum(sign_window);
    % find significant clusters
    clusters = [];
    for icol = 1:ncol
        clusters(icol,:) = conncomp_binary1d(sign_points(icol,:),1,1);
        nclus(icol) = max(clusters(icol,:));
    end
    
    % save to struct
    RegModels{ipred}.beta = beta;
    RegModels{ipred}.betaRand = betaRand;
    RegModels{ipred}.LL = LL;
    RegModels{ipred}.BIC = BIC;
    RegModels{ipred}.R_square = R_square;
    RegModels{ipred}.R_squareRand = R_squareRand;
    RegModels{ipred}.prc_sign = prc_sign;
    RegModels{ipred}.clusters = clusters;
    RegModels{ipred}.nclus = nclus;
    RegModels{ipred}.unit = 'degrees';
end

% compute unique and redundant variances
% unique variance provided by conditional displacement
R2_con = RegModels{1}.R_square - RegModels{2}.R_square;
% unique variance provided by start position variance
R2_fix = RegModels{1}.R_square - RegModels{3}.R_square;
% unique variance provided by end position variance
R2_end = RegModels{1}.R_square - RegModels{4}.R_square;
% redundant variance 
R2_red = RegModels{1}.R_square - R2_con - R2_fix - R2_end;

% ---------------------------------------------------
% plot data
figure('Position',[79 215 1762 752]);
nrows = 2;
ifullmodel = 1;
npredfull = size(predictors{1},2);
for ipred = 1:npredfull
    % plot eye displacement betas B_dh(t)
    subplot(nrows,npredfull,ipred);
    plot(tax_emreo,median(sq(RegModels{ifullmodel}.betaRand(ipred+1,:,:)),2),'k');
    hold on
    plot(tax_emreo,prctile(sq(RegModels{ifullmodel}.betaRand(ipred+1,:,:)),95,2),'k--');
    plot(tax_emreo, RegModels{ifullmodel}.beta(ipred+1,:), 'r-');
    % plot significant points with thick line
    nclus = RegModels{ifullmodel}.nclus(ipred);
    for iclus = 1:nclus
        clus_points = find(RegModels{ifullmodel}.clusters(ipred,:)==iclus);
        clus_start = clus_points(1);
        clus_end   = clus_points(end);
        plot(tax_emreo(clus_start:clus_end), RegModels{ifullmodel}.beta(ipred+1,clus_start:clus_end),'r','LineWidth',3.0);
    end
    plot(tax_emreo,prctile(sq(RegModels{ifullmodel}.betaRand(ipred+1,:,:)), 5,2),'k--');
    xlim([tax_emreo(1) tax_emreo(end)]);
    ax = gca;
    ylimit = ax.YAxis.Limits(2);
    sign_string = sprintf(['%.3f %%' newline 'significant'],RegModels{ifullmodel}.prc_sign(ipred));
    text(-20,ylimit*0.8,sign_string,'FontWeight','bold');
    xlabel('Time (ms)');
    ylabel('Regression beta (mV/Degree)');
    title([predtitles{ipred+1}]);
    legend({'scrambled','5th/95th percentile','true','p<0.05'},'Location','southeast');
end
    
% plot constant C(t)
subplot(nrows,npredfull,ipred+1);
plot(tax_emreo,median(sq(RegModels{ifullmodel}.betaRand(1,:,:)),2),'k'); 
hold on
plot(tax_emreo,prctile(sq(RegModels{ifullmodel}.betaRand(1,:,:)),95,2),'k--');
plot(tax_emreo, RegModels{ifullmodel}.beta(1,:), 'r-');
plot(tax_emreo,prctile(sq(RegModels{ifullmodel}.betaRand(1,:,:)), 5,2),'k--');
xlim([tax_emreo(1) tax_emreo(end)]);
xlabel('Time (ms)');
ylabel('Regression beta (mV/Degree)');
title('Constant C(t)');
legend({'scrambled','5th/95th percentile','true'},'Location','southeast');

% plot variance explained and unique variances
ax = subplot(nrows,npredfull,ipred+2);
p1 = plot(tax_emreo,RegModels{1}.R_square,'k');
hold on
p2 = plot(tax_emreo,R2_con,'r-');
p3 = plot(tax_emreo,R2_fix,'b-');
p4 = plot(tax_emreo,R2_end,'g-');
xlim([tax_emreo(1) tax_emreo(end)]);
% set y limit to start at zero
ax.YAxis.Limits(1) = 0;
xlabel('Time (ms)');
ylabel('Explained variance (%)');
title('Model R^{2}');
legend([p1 p2 p3 p4],{'total','cond. disp.','fix pos var.','end pos var.'},'Location','northeast');


header = sprintf(['%s Experiment 1+2+3' newline 'intraindividual regression analysis'],Subname);
ckfiguretitle(header,'k',10);


% ---------------------------------------------------
% print figure
fname = sprintf([figurepath Subname '_EMREO_regression_FullModel.png']);
print('-dpng',fname);


% ---------------------------------------------------
% save data to structure
SumMetrics.Stats.EMREO = {EMREO};
SumMetrics.Stats.time = tax_emreo;
SumMetrics.Stats.SaccAmplitudeDiff = amplitude_diff;
SumMetrics.Stats.predictors = predictors;
SumMetrics.Stats.predtitles = predtitles;
SumMetrics.Stats.RegModels = RegModels;
SumMetrics.Stats.PredictiveDeviance = predictive_deviance;
SumMetrics.Stats.AmpC = {amp_c};
SumMetrics.Stats.AmpT = {amp_t};








%% saccade amplitude to EMREO amplitude correlation
% -----------------------------------------------------------------

% we look whether the intercondition correation between both amplitudes
% holds also for the intertrial variance and if yes, if those scale with
% the same magnitude or not

% for robustness we have to use resampling or a median split


% first, try out equipopulated bins and a regression analysis on first peak
% height
PeakAmp = cellfun(@(x) x(1,:,1),PeaksEMREO,'UniformOutput',false)';

% plot if desired
% figure
% plot([6,6],PeakAmp{1},'bo');
% hold on
% plot([12,12],PeakAmp{2},'ro');



% We take the highest peak in the first 25 ms of the saccade!
degrees = Analysis_log.Degrees; % corresponds to the condition labels
degreesOrder = [3,2,4,1]; % the correct order of degrees
[~,I] = sort(degreesOrder);
cond_sign = sign(degrees);
x_con = Analysis_log.SaccPosCondition;
x_con = degrees(x_con)';

% compute saccade amplitude
Pos = Pos1 - Pos0;
Amp = sqrt(sum(Pos.^2,2));
% compute only horizontal amplitude
XAmp = Pos1(:,1) - Pos0(:,1);

% get smaller window from EMREOs
peak_bound = [0 25];
peak_win = ( tax_ear > peak_bound(1) & tax_ear < peak_bound(2) );
EMREOpeak = ALL_EAR(:,peak_win);

% binning parameters
Nbins = 5;
prcbounds = [0:100/Nbins:100];

% allocate space
PeakAmp = zeros(Nbins,length(degrees));
PeakLat = zeros(Nbins,length(degrees));
bin_center = zeros(Nbins,length(degrees));

for icond = 1:length(degrees)
    cond_ind = Analysis_log.SaccPosCondition == icond;
    Xtmp = XAmp(cond_ind);
    binedges = prctile(Xtmp,prcbounds);
    bin_center(:,icond) = binedges(1:end-1) + diff(binedges);
    bin_ind = sum(Xtmp > binedges(1:end-1),2);
    
    % take EMREOs from condition, sort into bins
    EMREOtmp = EMREOpeak(cond_ind,:) .* cond_sign(icond);
    for ibin = 1:max(bin_ind)
        tmp = EMREOtmp(bin_ind == ibin,:);
        [pks,locs] = findpeaks(mean(tmp,1),'SortStr','descend');
        if isempty(pks)
            [pks,locs] = max(mean(tmp,1));
        end
        PeakAmp(ibin,icond) = pks(1) * cond_sign(icond);
        PeakLat(ibin,icond) = locs(1);
    end
end

% order columns to correct left to right order
PeakAmp = PeakAmp(:,I);
bin_center = bin_center(:,I); % convert to degrees

% colormap
initcolor = [1 0 1; 0 1 0] .* 0.7;
initcolor = interp_colormap(initcolor,numel(PeakAmp));


% ---------------------------------------------------
% plot
figure('Position',[441 182 998 729]);
yyaxis right
h = histogram(XAmp,100,'Normalization','probability','FaceAlpha',0.1);
ylabel('Probability');

yyaxis left
hold on
plot(0,0,'k+');
for idot = 1:numel(PeakAmp)
    plot(bin_center(idot),PeakAmp(idot),'o','Color',initcolor(idot,:),'MarkerSize',8,'LineWidth',1.5);
end

% axes properties
grid on
xlim([-15 15]);
xticks(sort([degrees 0]));
xlabel('Saccade Amplitude (degrees)');
ytext = sprintf('Peak Amplitude (%s)',emreounit);
ylabel(ytext);

% align zero on right y axis to left one
ax = gca;
yscaling = ax.YAxis(1).Limits(1) / ax.YAxis(1).Limits(2);
rylim = ax.YAxis(2).Limits * 2;
ax.YAxis(2).Limits = [rylim(2) * yscaling, rylim(2)];
plot(ax.XAxis.Limits,[0,0],'k--');

header = sprintf(['S%02d EMREO peak amplitude' newline 'separated by equipopulated bins per condition'],S);
ckfiguretitle(header);


fname = sprintf([figurepath Subname '_EMREO_peak_variance_binning_method.png']);
print('-dpng',fname);




%% Effect of Cue presentation on EMREO and pupil size
% -----------------------------------------------------------------
% Can we predict the EMREO trace with the saccade amplitude per condition?
% compare EMREOs from Experiment 2 to EMREO from Experiment 3
% does the presentation of a sound cue in any direction shape the EMREO?

% find trials from experiment 2 and 3, separate them by sacc direction
ExpLabel = Analysis_log.ExpLabel;
SaccPosCondition = Analysis_log.SaccPosCondition;
degrees = Analysis_log.Degrees;
SaccPosCondition = degrees(SaccPosCondition)';
% negative: left    positive: right
directions = unique(sign(SaccPosCondition));

% define temporal bounadries for statistical analysis
stat_bound = globalP.EMREOwindow;
stat_win = (tax_ear > stat_bound(1) & tax_ear < stat_bound(2));
tax_stat = tax_ear(stat_win);
ntimepoints = length(tax_stat);
Nrand = 2000;
% minimum cluster size defined by time
clustersize = (2 * ARG.resamplefs) / 1000;

% cluster stat parameters
cfg.critvaltype = 'par'; % 'prctile' % type of threshold to apply. Usual 'par'
cfg.critval = 2 ; % critical cutoff value for cluster members if parametric
cfg.conn = 4; % connectivity criterion (for 2D 4 or 8 for 3D 6,18,26) 
cfg.clusterstatistic = 'maxsum';
cfg.minsize = clustersize; % minimal cluster size
cfg.pval = 0.05; % threshold to select signifciant clusters
%cfg.df = degrees of freedom. Only needed for effect size.


% ---------------------------------------------------
for idir = 1:length(directions)
    for exp = 1:2
        condind = all( [ExpLabel == exp+1, sign(SaccPosCondition) == directions(idir)], 2);
        EMREOattention{exp,idir} = ALL_EAR(condind,:);
    end
    
    % statistically compare same direction EMREOs between experiments
    Ntrials = cell2mat(cellfun(@(x) size(x,1),EMREOattention(:,idir),'UniformOutput',false));
    Nsum = sum(Ntrials);
    % take only time window where we want to compare EMREOs
    EMREOcomp = cellfun(@(x) x(:,stat_win),EMREOattention(:,idir),'UniformOutput',false);
    
    % do welch's t test with randomization ---------
    X1 = EMREOcomp{1};
    X2 = EMREOcomp{2};
    t_true = zeros(1,ntimepoints);
    t_boot = zeros(1,ntimepoints,Nrand);
    % compute t stat at each time point
    tic
    for ipoint = 1:ntimepoints
        % compute true test statistic
        s_delta = sqrt( std(X1(:,ipoint)).^2/Ntrials(1) + std(X2(:,ipoint)).^2/Ntrials(2) );
        t_true(ipoint) = (mean(X1(:,ipoint)) - mean(X2(:,ipoint))) / s_delta;
        % randomize for each time point by changing class labels
        for irand = 1:Nrand
            % pool both groups
            XX = [X1(:,ipoint); X2(:,ipoint)];
            XX = XX(randperm(Nsum)); % shuffle
            % draw new groups of initial size without replacement
            X1_boot = XX([1:Ntrials(1)]);
            X2_boot = XX([Ntrials(1):Nsum-1]);
            % compute randomized test statistic
            s_delta = sqrt( std(X1_boot).^2/Ntrials(1) + std(X2_boot).^2/Ntrials(2) );
            t_boot(1,ipoint,irand) = (mean(X1_boot) - mean(X2_boot)) / s_delta;
        end
    end
    clusterstats{idir} = eegck_clusterstats(cfg,t_true,t_boot);
    toc
end


% ---------------------------------------------------
% get pupil size from eye tracking data
condind = ExpLabel == [2 3];
PupilDiameter = cellfun(@(x) x(4,:), Sacc.trial,'UniformOutput',false);
PupilDiameter = cat(1,PupilDiameter{:});

% plot
tracecolors = {[0.9 0.1 0; 0.5 0.2 0.3], [0 0.1 0.9; 0.2 0.3 0.5]};
dirlabel = {'Left','Right'};

figure('Position',[162 116 1735 850]);

subpos = [1,3];
for idir = 1:length(directions)
    ax = subplot(length(directions),2,subpos(idir));
    % exp 2 trace
    mTrace2 = mean(EMREOattention{1,idir},1);
    sTrace2 = sem(EMREOattention{1,idir},1);
    % exp 3 trace
    mTrace3 = mean(EMREOattention{2,idir},1);
    sTrace3 = sem(EMREOattention{2,idir},1);
    
    % plot traces
    [~, p1] = fb_errorshade(tax_ear,mTrace2,sTrace2,tracecolors{idir}(1,:));
    hold on
    [~, p2] = fb_errorshade(tax_ear,mTrace3,sTrace3,tracecolors{idir}(2,:));
    % reset y axis
    ylimits = repmat(max(ax.YLim),[1,2]) .* [-1 1];
    ax.YLim = ylimits * 1.2;
    
    % plot any clusters 
    if ~isempty(clusterstats{idir})
        maskInd = find(clusterstats{idir}.maskSig);
        plot(tax_stat(maskInd),ones(size(maskInd)) .* ylimits(2) .* 1.1, 'k-','LineWidth',5.0);
    end
    
    % gray out not relevant area
    fill([tax_ear(1) stat_bound(1) stat_bound(1) tax_ear(1)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    fill([stat_bound(2) tax_ear(end) tax_ear(end) stat_bound(2)], flatten([ylimits; ylimits])*1.2,[1 1 1]*0.5,'FaceAlpha',0.3,'EdgeColor','none');
    
    grid on
    xlim([-100 150]);
    xlabel('time (ms)');
    ytext = sprintf('Amplitude (%s)',emreounit);
    ylabel(ytext);
    legend([p1 p2],{'Exp. 2','Exp. 3'});
    
    header = sprintf('%s Saccades',dirlabel{idir});
    title(header);
end

% plot pupil diameter
expcolors = [0.6 0.0 0.8; 0.4 0.3 0.5];
ax = subplot(length(directions),2,[2 4]);
exp2pupil = PupilDiameter(condind(:,1),:);
exp3pupil = PupilDiameter(condind(:,2),:);
hold on
%plot(tax_eye,mean(exp2pupil));
[~, p1] = fb_errorshade(tax_eye,mean(exp2pupil),sem(exp2pupil),expcolors(1,:));
[~, p2] = fb_errorshade(tax_eye,mean(exp3pupil),sem(exp3pupil),expcolors(2,:));
ylimits = ax.YAxis.Limits;
plot([0,0],ylimits,'k--');
ax.YAxis.Limits = ylimits;
legend([p1 p2],{'Exp. 2','Exp. 3'});
xlabel('Time (ms)');
ylabel('Pupil diameter (unit?)');
header = 'Presaccadic pupil diameter';
title(header);


header = sprintf(['%s EMREO in Exp. 2 and 3' newline 'Attentional modulation of EMREOs'],Subname);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname '_EMREO_Exp23_Attention.png']);
print('-dpng',fname);


% save data to structure
SumMetrics.Attention.Clusters = clusterstats;
SumMetrics.Attention.PupilDiameterExp2 = [mean(exp2pupil),sem(exp2pupil)];
SumMetrics.Attention.PupilDiameterExp3 = [mean(exp3pupil),sem(exp3pupil)];





%% save summary metrics to file
save(sumname,'-struct','SumMetrics');



% only to save data for christoph
EarData = ALL_EAR;
time = tax_ear;
ConditionLabel = Analysis_log.Condition;
ConditionLabel(ConditionLabel > 8) = ConditionLabel(ConditionLabel > 8) - 8;
ConditionLabel(ConditionLabel > 4) = ConditionLabel(ConditionLabel > 4) - 4;
ConditionLabelNames = {'6�  right', '6�  left', '12�  right', '12�  left'};
datatype = 'trials x time';

SaveStruct.EarData = EarData;
SaveStruct.ddatatype = datatype;
SaveStruct.time = time;
SaveStruct.ConditionLabel = ConditionLabel;
SaveStruct.ConditionLabelNames = ConditionLabelNames;





end

