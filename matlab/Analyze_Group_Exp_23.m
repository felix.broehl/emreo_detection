function Output = Analyze_Group_Exp_23(CatData,goodsubs,ExpNumber)
%
% Analyze_Group_Exp_23(CatData,goodsubs,ExpNumber)
%
% does the same analysis on data from Experiment 2 and 3
% specific analyses, such as considering the spatial cue are done outside
% this function
%


% get ExpList
USECOMP = 3;
ExpList;
defdirs;

% get colors
ColorScheme = FB03_Colorscheme;

% -------------------------------------------------------------------------
% Onset statistics
OnsetStats = CatData.Onsets.OnsetStats;
% normalization factor
Ntrials_per_sub = sq(sum(OnsetStats,3));


% 1st dimension:      participant data 
% 2nd dimension:      onset group (where they were supposed to occur)
% 3rd dimension:      where they actually occured


% -------------------------------------------------------------------------
% Behavior
% statistics: due to the central limit theorem we can assume normality for
% the group of intraindividual medians that we test against zero
%


% -------------------------------------
% sound onset group behavior
label = CatData.Behavior.SoundOnset.Label(1,:);
Response = CatData.Behavior.SoundOnset.Dprime;
SigDetBias = CatData.Behavior.SoundOnset.SigDetBias;
RT = CatData.Behavior.SoundOnset.RT;

% test Resp and RT with ANOVA, adjust for multiple tests
var_test = cat(3,Response,RT);
for ivar = 1:size(var_test,3)
    [p_sndgroup(ivar),anovatab{ivar},stats{ivar}] = anova1(sq(var_test(:,:,ivar)),[],'off');
end
p_sndgroup_adjust = fb_stat_padjust(p_sndgroup,'benjamini-hochberg');

% barwithdots specifications
cfg = [];
cfg.jitterrange = 0.2;

figure('Position',[468 142 417 834]);
subplot(211);
barwithdots(cfg,Response);
xticklabels(label);
ylim([0 4]);
yticks([0:1:4]);
ptext =  sprintf('p = %0.3f',p_sndgroup_adjust(1));
text(2.8, 3.7, ptext, 'Color','r');
ylabel('Response (%)');

subplot(212);
barwithdots(cfg,RT);
xticklabels(label);
ylim([0 1200]);
yticks([0:200:1200]);
ptext =  sprintf('p = %0.3f',p_sndgroup_adjust(2));
text(2.8, 1100, ptext, 'Color','r');
ylabel('Reaction time (ms)');

header = sprintf(['EyeMov %d Sound Onset Groups' newline 'Response and Reaction Time'],ExpNumber);
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_E%d_SoundOnsetBehavior.png'],ExpNumber);
print('-dpng',fname);




% -------------------------------------
% find the names of the target variables for behavior in struct
target_vars = [];
target_labels = {};
targetfields = fieldnames(CatData.Behavior);
skipOnsetindex = find(~strcmp('SoundOnset',targetfields) & ~strcmp('Overall',targetfields));
targetfields = targetfields(skipOnsetindex);
for ifield = 1:length(targetfields)
    ifieldname = targetfields{ifield};
    target_vars{ifield} = CatData.Behavior.(ifieldname).Name{1,:};
    target_labels(ifield,:) = CatData.Behavior.(ifieldname).Label(1,:);
end


% get responses and reaction times 
Response = zeros(length(goodsubs),2,length(targetfields));
RT = zeros(size(Response)); % copy empty variable of same size
SigDetBias = zeros(size(Response));
for ivar = 1:length(targetfields)
    Response(:,:,ivar) = CatData.Behavior.(targetfields{ivar}).Dprime;
    SigDetBias(:,:,ivar) = CatData.Behavior.(targetfields{ivar}).SigDetBias;
    RT(:,:,ivar) = CatData.Behavior.(targetfields{ivar}).RT;
end


% two-sided paired t-test against zero
var_test = cat(3,Response,SigDetBias,RT);
var_diff = sq(diff(var_test,[],2));
df = size(var_diff,1) - 1;
% compute t test statistic
tmap = mean(var_diff,1)./(std(var_diff,[],1)./sqrt(length(goodsubs)));
% reshape tmap to better separate response, bias and RT
tmap = reshape(tmap,[size(target_labels,1),3])';
% convert to p-value
pval = 2*(1 - tcdf(abs(tmap),length(goodsubs)-1));
% adjust for multiple testing
pval_adjust = fb_stat_padjust(pval,'benjamini-hochberg');

% effect size
cohensD = abs(mean(var_diff,1))./std(var_diff,[],1);
cohensD = reshape(cohensD,[size(target_labels,1),3])';

% bar colors
tracecolors = {...
    [0.9 0.1 0.0; 0.0 0.1 0.9], ...
    [0.2 0.8 0.6; 0.2 0.5 0.3], ...
    [0.2 0.8 0.6; 0.2 0.5 0.3], ...
    [0.2 0.8 0.6; 0.2 0.5 0.3]};


% plot responses and reaction time
figure('Position',[522 125 1172 852]);
cfg = [];
cfg.jitterrange = 0.2;
% Reponse
for ivar = 1:length(targetfields)
    subplot(3,4,ivar);
    cfg.FaceColor = tracecolors{ivar};
    barwithdots(cfg,sq(Response(:,:,ivar)));
    % axes
    title(targetfields{ivar});
    xticklabels(CatData.Behavior.(targetfields{ivar}).Label(1,:));
    ylim([0 4]);
    yticks([0:4]);
    % text
    ptext =  sprintf('p = %0.3f',pval_adjust(ivar));
    text(1.8, 3.7, ptext, 'Color','r');
    if ivar == 1
        ylabel('Response (%)');
    end
end

% signal detection bias
for ivar = 1:length(targetfields)
    subplot(3,4,ivar+4);
    cfg.FaceColor = tracecolors{ivar};
    barwithdots(cfg,sq(SigDetBias(:,:,ivar)));
    % axes
    xticklabels(CatData.Behavior.(targetfields{ivar}).Label(1,:));
    ylim([0 2.5]);
    yticks([0:0.5:2.5]);
    % text
    ptext =  sprintf('p = %0.3f',pval_adjust(ivar + 4));
    text(1.8, 2.2, ptext, 'Color','r');
    if ivar == 1
        ylabel('Detection bias');
    end
end

% RT
for ivar = 1:length(targetfields)
    subplot(3,4,ivar+8);
    cfg.FaceColor = tracecolors{ivar};
    barwithdots(cfg,sq(RT(:,:,ivar)));
    % axes
    xticklabels(CatData.Behavior.(targetfields{ivar}).Label(1,:));
    ylim([0 1400]);
    yticks([0:200:1400]);
    % text
    ptext =  sprintf('p = %0.3f',pval_adjust(ivar + 8));
    text(1.8, 1300, ptext, 'Color','r');
    if ivar == 1
        ylabel('Reaction time (ms)');
    end
end

header = sprintf(['EyeMov %d Behavior' newline 'Response and Reaction Time'],ExpNumber);
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_E%d_Behavior.png'],ExpNumber);
print('-dpng',fname);



% save data to struct
Output.Behavior.Response = Response;
Output.Behavior.SigDetBias = SigDetBias;
Output.Behavior.RT = RT;
Output.Behavior.pval = pval;
Output.Behavior.tval = tmap;
Output.Behavior.df = df;
Output.Behavior.EffectSize = cohensD;
Output.Behavior.VarNames = target_vars;
Output.Behavior.VarLabels = target_labels;



%% EMREO and behavior

% correlation between EMREO amplitude and behavior 
CondLabel = CatData.EarBehavior.CondLabel(1,:);
LooEMREO = CatData.EarBehavior.LooEMREO;
LooDprime = CatData.EarBehavior.LooDprime;
LooBias = CatData.EarBehavior.LooBias;
Resp_code = CatData.EarBehavior.Resp_code;
ncond = length(CondLabel);

for icond = 1:ncond
    subplot(2,ncond,icond);
    hold on
    % for each participant plot their sensitivity
    for isub = 1:length(goodsubs)
        plot(LooEMREO{isub,icond},LooDprime{isub,icond},'.');
        % plot mean as one dot
        % -> plot(mean(ERMREO),mean(Dprime),'.');
    end
    if icond == 1
        ylabel('delta sensitivity (dprime)');
    end
    
    % plot response code
    subplot(2,ncond,icond+ncond);
    hold on
    % plot bias in relation to delta EMREO
    for isub = 1:length(goodsubs)
        plot(LooEMREO{isub,icond},Resp_code{isub,icond},'.');
    end
    ylim([-0.5 1.5]);
    yticks([0 1]);
    if icond == 1
        ylabel('delta detection bias');
    end
    
    % xlabel for column
    xlabel('delta EMREO');
end

header = 'EMREO amplitude and detection sensitivity';
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_E%d_EMREO_Detection.png'],ExpNumber);
print('-dpng',fname);



% -------------------------------------------------------------------------
% analyze all conditions together
PooledEMREO = cell(length(goodsubs),1);
PooledResp = cell(length(goodsubs),1);
for isub = 1:length(goodsubs)
    PooledEMREO{isub} = cat(1,LooEMREO{isub,:});
    PooledResp{isub} = cat(1,Resp_code{isub,:});
end

deltaEMREO = cat(1,PooledEMREO{:});
resp_code = cat(1,PooledResp{:});
resp_code = logical(resp_code);

% compute t value
s_delta = sqrt( std(deltaEMREO(resp_code)).^2/sum(resp_code) + std(deltaEMREO(~resp_code)).^2/sum(~resp_code) );
tval = (mean(deltaEMREO(resp_code)) - mean(deltaEMREO(~resp_code))) / s_delta;
% approximate degrees of freedom based on Welch-Satterthwaite equation
N1 = sum(resp_code);
N2 = sum(~resp_code);
s1 = std(deltaEMREO( resp_code)).^2/N1;
s2 = std(deltaEMREO(~resp_code)).^2/N2;
df = (s1 + s2).^2 / ( s1^2/(N1-1) + s2^2/(N2-1) );
% convert to p-value
pval = 2*(1 - tcdf(abs(tval),df));


% plot parameters
cfg = [];
cfg.connect = false;
cfg.jitterrange = 0.4;
cfg.FaceColor = [1 1 1];

% plot data
figure('Position',[650 82 478 899]);
cfg.XAxis = [1];
barwithdots(cfg,deltaEMREO(resp_code));
hold on
cfg.XAxis = [2];
barwithdots(cfg,deltaEMREO(~resp_code));

% text p value
ptxt = sprintf('p=%.3f',pval);
text(1.5,1.25,ptxt,'FontWeight','bold','Color','r','HorizontalAlignment','center');

ylim([-1.5 1.5]);
xticks([1 2]);
xticklabels({'correct','false'});
xlabel('Response');
ylabel('delta EMREO amp');


header = ['EMREO amplitude distribution rel. to response' newline 'single trials pooled across participants'];
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_E%d_EMREO_Response.png'],ExpNumber);
print('-dpng',fname);



%%
% --------------------------------------------------------------
% two way ANOVA with condition x resp_code between participants!
TrueMetric = CatData.EarBehavior.TrueMeanPower;
FalseMetric = CatData.EarBehavior.FalseMeanPower;
Ntrue = CatData.EarBehavior.Ntrue;
Nfalse = CatData.EarBehavior.Nfalse;

EMREOData = cat(3,TrueMetric, FalseMetric);
[nsubs,ncond,nresp] = size(EMREOData);


% repeated measures ANOVA with GLME 
% create data array
data = [TrueMetric(:); FalseMetric(:)];
degrees = CatData.EarBehavior.CondLabel(1,:);
condition = CatData.EarBehavior.CondLabel(:);
condition = repmat(condition,2,1);
response = [ones(numel(data)/2,1); zeros(numel(data)/2,1) ];
SubID = repmat([1:nsubs]',ncond*nresp,1);
Table = cat(2,data,condition,response,SubID);

% convert to table
Table = array2table(Table,'VariableNames',{'data','cond','resp','Sub'});
Table.Sub = categorical(Table.Sub);
Table.cond = categorical(Table.cond);
Table.resp = categorical(Table.resp);


% git GLME with both variables, their interaction and a random effect for
% subjects (making it a repeated measures test)
GLME_Model = fitglme(Table, ...
    'data ~ 1 + cond + resp + cond:resp + (1 | Sub) ', ...
    'Distribution', 'Normal', 'Link', 'Identity', 'FitMethod', 'MPL',...
    'DummyVarCoding', 'reference', 'PLIterations', 500, 'InitPLIterations', 20)

% return anova results
GLME_anova = anova(GLME_Model)

% plot parameters
cfg = [];
cfg.connect = false;
ticklabels = num2cell(degrees);
ticklabels = cellfun(@(x) num2str(x),ticklabels, 'UniformOutput',false);


% plot data
figure('Position',[462 414 996 538]);
subplot(121);
% correct and incorrect
cfg.XAxis = [1:ncond];
cfg.FaceColor = [0 0 1];
cfg.FaceAlpha = 0.3;
barwithdots(cfg,TrueMetric);
hold on
cfg.XAxis = [1:ncond] + ncond;
cfg.FaceColor = [1 0 0];
cfg.FaceAlpha = 0.3;
barwithdots(cfg,FalseMetric);

% axes
xlim([0, ncond*nresp + 1]);
xticks([1:ncond*nresp]);
xticklabels([ticklabels ticklabels]);
xlabel('correct trials (blue) --- false trials(red)');
ylabel('mean EMREO power');

% plot individual difference
subplot(122);
cfg.XAxis = [1:ncond];
cfg.FaceColor = [1 0 1];
cfg.FaceAlpha = 0.3;
barwithdots(cfg,TrueMetric - FalseMetric);


header = 'EMREO Power Condition x Response';
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_E%d_EMREO_ANOVA.png'],ExpNumber);
print('-dpng',fname);




% plot EMREOs for correct and incorrect in each condition
tax_ear = CatData.EarBehavior.time(1,:);
figure('Position',[604 158 719 821])
for icond = 1:ncond
    subplot(ncond,1,icond);
    CondData = sq(CatData.EarBehavior.Raw(:,icond,:,:)); 
    [~, hp1] = fb_errorshade(tax_ear, sq(mean(CondData(:,2,:),1))', sq(sem(CondData(:,2,:),1))', [1 1 1] * 0.7);
    hold on
    [~, hp2] = fb_errorshade(tax_ear, sq(mean(CondData(:,1,:),1))', sq(sem(CondData(:,1,:),1))', ...
        ColorScheme.SaccCondColors(icond,:));
    xlim([-50 150]);
    legend([hp1 hp2],{'incorrect','correct'});
end
header = 'EMREO Behavior';
ckfiguretitle(header);

fname = sprintf([figurepath 'GroupAnalysis_E%d_EMREO_Behavior.png'],ExpNumber);
print('-dpng',fname);


% save data to struct
Output.EarBehavior.TrueMetric = TrueMetric;
Output.EarBehavior.FalseMetric = FalseMetric;
Output.EarBehavior.Ntrue = Ntrue;
Output.EarBehavior.Nfalse = Nfalse;
Output.EarBehavior.ncond = ncond;
Output.EarBehavior.nresp = nresp;
Output.EarBehavior.GLME_coeff = GLME_Model.Coefficients;
Output.EarBehavior.GLME_equation = GLME_Model.Formula;
Output.EarBehavior.GLME_stats = GLME_Model.ModelCriterion;
Output.EarBehavior.GLME_anova = GLME_anova;





%% ----------------------------------------------------------------
% Phase Behavior

% get beta for logistic and linear regressions, and test them against zero
% separately

target_vars = {'Resp','RT'};
ind_var = {'Amplitude','Phase'};
ind_var_name = {'EMREOamp','EMREOpha'}; % fieldnames of indepedent variables
ResponseBeta = CatData.PhaseBehavior.Resp.EMREOamp;
RTBeta = CatData.PhaseBehavior.RT.EMREOamp;
% remove intercept
ResponseBeta = sq(ResponseBeta(:,2,:));
RTBeta = sq(RTBeta(:,2,:));


% one-sided t-test against zero
var_test = cat(2,ResponseBeta,RTBeta);
% compute t test statistic
tmap = mean(var_test,1)./(std(var_test,[],1)./sqrt(length(goodsubs)-1));
% convert to p-value
pval = 2*(1 - tcdf(abs(tmap),length(goodsubs)-1));
% adjust for multiple testing
pval_adjust = fb_stat_padjust(pval,'benjamini-hochberg');


% barwithdots specifications
cfg = [];
cfg.jitterrange = 0.2;
cfg.connect = false;

% text to print pvals
pval_text = cellfun(@(x) sprintf('p = %.3f',x),num2cell(pval_adjust),'UniformOutput',false);


% plot
figure('Position',[440 338 585 509]);
ax = subplot(121);
barwithdots(cfg,ResponseBeta);
ylimits = ax.YAxis.Limits;
ylim([ylimits(1) ylimits(2)*1.2]);
texty = ylimits(2) * 1.1;
text([1], [texty], pval_text(1),'Color','r','HorizontalAlignment','center');
xlabel('EMREO');
xticklabels(ind_var);
ylabel('logistic beta');


ax = subplot(122);
barwithdots(cfg,RTBeta);
ylimits = ax.YAxis.Limits;
ylim([ylimits(1) ylimits(2)*1.2]);
texty = ylimits(2) * 1.1;
text([1], [texty], pval_text(2),'Color','r','HorizontalAlignment','center');
xlabel('EMREO');
xticklabels(ind_var);
ylabel('linear beta');

header = sprintf(['EyeMov %d Amplitude / Phase Behavior' newline 'regression beta against zero'],ExpNumber);
ckfiguretitle(header);

fname = sprintf([figurepath 'GroupAnalysis_E%d_PhaseBehavior.png'],ExpNumber);
print('-dpng',fname);



end
