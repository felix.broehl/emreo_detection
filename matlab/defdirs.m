
% add function directory to path
addpath('func\');

if USECOMP==1
    % psychophysics set up 
    basepath = 'Z:\FB03\';
    cnslab = 'C:\Users\eeglab\Desktop\CNSLAB\FB03\log'; % desktop/cnslab/FB03/log or something!
    data = 'Z:\FB03\data\';
    exp = 'Z:\FB03\experiment\';
    figures = 'Z:\FB03\figures\';
    matlab = 'Z:\FB03\matlab\';
  
  
elseif USECOMP==2
    % NAS server
    addpath('Y:\Matlab\fieldtrip-20171001');
    addpath('Y:\Matlab\fieldtrip-20171001\utilities')
    addpath('Y:\Matlab\fieldtrip-20171001\fileio')
    addpath('Y:\Matlab\ckmatlab\eegck')
    addpath('Y:\Matlab\ckmatlab\ckeye')
    figurepath = 'C:/Users/fbroehl/Documents/FB03/figures/';
    finalfigurepath = 'C:/Users/fbroehl/Documents/FB03/figures/finalfigures/';
    datapath_ear =  'Z:/FB03/data_Ear/';
    datapath_eye =  'Z:/FB03/data_ET/';
    datapath_log =  'Z:/FB03/data_Log/';
    datapath_res = 'C:/Users/fbroehl/Documents/FB03/data_Results/';
  
elseif USECOMP==3
    % desktop PC
    addpath('Y:\Matlab\fieldtrip-20171001');
    addpath('Y:\Matlab\fieldtrip-20171001\utilities')
    addpath('Y:\Matlab\fieldtrip-20171001\fileio')
    addpath(genpath('Y:\Matlab\ckmatlab\'))

    addpath(genpath('Z:\FB00\fb_utils\'));
    addpath('Z:\FB00\toolboxes\bayesFactor');
    installBayesFactor;
    
    figurepath = 'C:/Users/fbroehl/Documents/FB03/figures/';
    finalfigurepath = 'C:/Users/fbroehl/Documents/FB03/figures/finalfigures/';
    datapath_ear =  'E:/FB03/data_Ear/';
    datapath_eye =  'E:/FB03/data_ET/';
    datapath_log =  'E:/FB03/data_Log/';
    datapath_res = 'C:/Users/fbroehl/Documents/FB03/data_Results/';
  
  
  
  
elseif USECOMP==4
    % desktop PC
    datapath = 'I:/FB03/data/';
    figurepath = 'I:/FB03/figures/';
    
end
