function Analyze_EyeMov_1(S) 
%
% analyze first part of experiment
%
% the condition labels in the log files describe the following conditions:
%   1) center
%   2) 6�  down 
%   3) 6�  right 
%   4) 6�  up 
%   5) 6�  left
%   6) 12� down 
%   7) 12� right 
%   8) 12� up 
%   9) 12� left
%

close all

% get ExpList
USECOMP = 3
ExpList;
defdirs;

Experiment = 1; % which EyeMov part
Subname = SubList(S).name

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% structure in which we save summary metrics from individual analyses
sumname = sprintf([datapath_res 'SumMetrics_FB03_S%02d_%d.mat'],S,Experiment);
SumMetrics = [];
SumMetrics.date = date;


%% ----------------------------------------------------------------------------
% general parameters
ARG.resamplefs = globalP.ResampleFs;
ARG.zscore = globalP.zscore; % if whole data matrix should be zscored

% parameters for filtering and data inclusion - We expect EMRO around 50 Hz
ARG_ear.zscore = false;
ARG_ear.hpfilter   = 'no';  
ARG_ear.hpfreq     = 10;
ARG_ear.lpfilter   = 'no';  
ARG_ear.lpfreq     = 50;
ARG_ear.prestim    = -0.2;
ARG_ear.poststim   = 1.2;
ARG_ear.exp = Experiment;
ARG_ear.Ntrials = 161;

ARG_eog.hpfilter   = 'no';  
ARG_eog.hpfreq     = 20;
ARG_eog.lpfilter   = 'yes';  
ARG_eog.lpfreq     = 80;
ARG_eog.exp = Experiment;


ARG_eye.Window = [-0.1,0.4]; % seconds
ARG_eye.Align = 'onset';
ARG_eye.ppd = globalP.ppd;
ARG_eye.exp = Experiment;
ARG_eye.resamplefs = ARG.resamplefs;

ARG_event.exp = Experiment;

ppd = globalP.ppd;
Midpoint = globalP.centerpoint ./ ppd; % screen center


% save data to struct
SumMetrics.samplerate = ARG.resamplefs;



%% --------------------------------------------------------------------------
% Load Ear data
Ear_data = load_eardata_blocks(S,SubList,datapath_ear,ARG_ear);


% resample to target fs
cfg = [];
cfg.resamplefs = ARG.resamplefs;
Ear_data = ft_resampledata(cfg, Ear_data);
    

%% --------------------------------------------------------------------------
% Load Event data
[EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath_log,ARG_event);



%% --------------------------------------------------------------------------
% Load Eye data
[Sacc,TimingLog] = load_eyedata_blocks(S,SubList,datapath_eye,ARG_eye);

% put into one structure to clean trials
Eye_data.Window = ARG_eye.Window;
Eye_data.Sacc = Sacc;
Eye_data.TimingLog = TimingLog;



%% CLEAN TRIALS
% now we extract the Ear data locked to saccade onset / offset
% convert to matrix

Analysis_log = [];
Analysis_log.TimingLog = TimingLog;
Analysis_log.Condition = EventLog.EventLog(:,2);

% clean trials here
cfg = [];
cfg.fsample = ARG.resamplefs;
cfg.window = [-0.15 0.2]; % window in seconds to cut around saccade onset
[Analysis_log,Ear_data_clean,Eye_data_clean,TrialStats] = clean_trialsFB03(cfg,Analysis_log,Ear_data,Eye_data);


% unpack eye data again
Sacc = Eye_data_clean.Sacc;
TimingLog = Eye_data_clean.TimingLog;
% find column based on label
VelColumn = strcmp(Sacc.label,'EYE_V');
AccColumn = strcmp(Sacc.label,'EYE_Acc');
Velocity = cellfun(@(x) x(VelColumn,:), Sacc.trial,'UniformOutput',false);
Velocity = cat(1,Velocity{:});
Acceleration = cellfun(@(x) x(AccColumn,:), Sacc.trial,'UniformOutput',false);
Acceleration = cat(1,Acceleration{:});


% unpack ear data
ALL_EAR = Ear_data_clean.ALL_EAR_ONSET;
tax_ear = Ear_data_clean.tax_ear;

% zscore if desired
if ARG.zscore
    ALL_EAR = (ALL_EAR - mean(ALL_EAR(:))) / std(ALL_EAR(:));
    emreounit = 'SD';
else
    % convert from uV to mV
    ALL_EAR = ALL_EAR ./ 1000; 
    emreounit = 'mV';
end

% unpack stats
Ngood_trials = TrialStats.Ngood_trials;

fprintf('Retaining %d trials \n',length(TrialStats.good_trials));



%% Analysis

% plot saccade start and end positions
ntrial = size(Sacc.trial,2);

figure
hold on
iter = 1;
for itrial = 1:ntrial
    if isnan(TimingLog(itrial,1))
        continue
    end
    % get start and end position
    pos0 = sq(Sacc.trial{itrial}([2,3],TimingLog(itrial,1)))' - Midpoint;
    pos1 = sq(Sacc.trial{itrial}([2,3],TimingLog(itrial,2)))' - Midpoint;
    % plot positions
    plot(pos0(1),pos0(2),'kx');
    plot(pos1(1),pos1(2),'ro');
    
    iter = iter + 1;
end
drawnow
title('Fixations (x) and saccadic (o) end points')
xlabel('degrees');
ylabel('degrees');
axis([-16 16 -16 16])
header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fprintf('Found good saccades in %d of %d trials\n',iter,ntrial);

fname = sprintf([figurepath Subname 'E%d' '_Saccade.png'],Experiment);
print('-dpng',fname);



%% -----------------------------------------------------------------
% display mean EAR trace per direction


figure;
clf; 

% up and down
subplot(2,1,1);
k=[2,6];
curr_cond = find(any(Analysis_log.Condition==k,2));
ndown = length(curr_cond);
tmp_down = ALL_EAR((curr_cond),:);
fb_errorshade(tax_ear,mean(tmp_down),sem(tmp_down),'b');
hold on

k=[4,8];
curr_cond = find(any(Analysis_log.Condition==k,2));
nup = length(curr_cond);
tmp_up = ALL_EAR((curr_cond),:);
fb_errorshade(tax_ear,mean(tmp_up),sem(tmp_up),'r');
header = sprintf('Ear: down(b, n=%d)/up(r, n=%d)',ndown,nup);
title(header);
grid on


% left and right
subplot(2,1,2);
k=[3,7];
curr_cond = find(any(Analysis_log.Condition==k,2));
nright = length(curr_cond);
tmp_right = ALL_EAR((curr_cond),:);
fb_errorshade(tax_ear,mean(tmp_right),sem(tmp_right),'b');
hold on

k=[5,9];
curr_cond = find(any(Analysis_log.Condition==k,2));
nleft = length(curr_cond);
tmp_left = ALL_EAR((curr_cond),:);
fb_errorshade(tax_ear,mean(tmp_left),sem(tmp_left),'r');
header = sprintf('Ear: right(b, n=%d)/left(r, n=%d)',nright,nleft);
title(header);
grid on
header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);


fname = sprintf([figurepath Subname 'E%d' '_EMREO.png'],Experiment);
print('-dpng',fname);


% save data to structure
SumMetrics.EMREO.label = {'down','up','right','left'};
SumMetrics.EMREO.time = tax_ear;
SumMetrics.EMREO.data = expand_dims([mean(tmp_down); mean(tmp_up); mean(tmp_right); mean(tmp_left)], 1);



%% plot only the vertical EMREOs for all conditions
cond_ind = [6 2 4 8]; % down to up
degrees = [-12 -6 6 12];
ncond = length(cond_ind);

% color from magenta to green
initcolor = [1 0 1; 0 1 0] ;
interpcolor = interp_colormap(initcolor,length(degrees)) .* 0.7;

% plot vertical EMREOs per degree condition
figure
hold on
tmp_emreo = [];
for icond = 1:ncond
    k = cond_ind(icond);
    curr_cond = find(any(Analysis_log.Condition==k,2));
    tmp_emreo(icond,:) = mean(ALL_EAR((curr_cond),:),1);
    pp(icond) = plot(tax_ear,tmp_emreo(icond,:),'Color',interpcolor(icond,:));
    Ntrials(icond) = length(curr_cond);
end


% save data to structure
SumMetrics.EMREOvertical.unit = emreounit;
SumMetrics.EMREOvertical.time = tax_ear;
SumMetrics.EMREOvertical.datacond = expand_dims(tmp_emreo,1);
SumMetrics.EMREOvertical.condlabel = expand_dims(degrees,1);
SumMetrics.EMREOvertical.direction = {'vertical'};
SumMetrics.EMREOvertical.ntrials = expand_dims(Ntrials,1);



%% 
% analyze power spectrum of ear data

cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'pow';
cfg.taper = 'hanning';
cfg.foilim = [2 128];
cfg.toi = 0;

freq = ft_freqanalysis(cfg,Ear_data);

figure('Position',[170 586 1070 392]);
subplot(121);
plot(freq.freq,freq.powspctrm);
xlim([0,45]);
xlabel('Frequency (Hz)');
ylabel('Power');
subplot(122);
plot(freq.freq,freq.powspctrm);
header = sprintf('%s_%d EMREO Ear data spectrum',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_EMREO_spectrum.png'],Experiment);
print('-dpng',fname);



%% save summary metrics to file
save(sumname,'-struct','SumMetrics');



end

