function Analyze_GroupStats(goodsubs)
%
% load summary statistics from each good participant and do group level
% statistics
%

% TODO:
%
% TEOAE:
% analyze TEAOEs - check whether there are already standadized methods
% this can be correlated to the EMREOs later
%
% EMREO all experiments:
%
% EyeMov 2 and 3:
%
% CochActivity:
% load periodic signal from FOOOF analysis (if we decide to do this)
%


close all

% get ExpList
USECOMP = 3;
ExpList;
defdirs;

% set parameters from global parameters
globalP = FB03_AnalysisParameters;

% get colors
ColorScheme = FB03_Colorscheme;


% file name template
nametemp = [datapath_res 'SumMetrics_'];
Expnames = {'TEOAE','all','1','2','3'}; % include Coch results later


% structure in which we save summary metrics from individual analyses
sumname = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
centerpoint = globalP.centerpoint; % screen center point
GroupStats = [];
GroupStats.date = date;
GroupStats.goodsubs = goodsubs;
GroupStats.centerpoint = centerpoint;

% pixel to degree scale
ppd = globalP.ppd;


%% 
% allocate variables
% EyeMov all data
EyeMovA = cell(1,goodsubs(end));

% EyeMov 1 data
EyeMov1 = cell(1,goodsubs(end));
EMREO1 = [];

% EyeMov 2 data
EyeMov2 = cell(1,goodsubs(end));

% EyeMov 3 data
EyeMov3 = cell(1,goodsubs(end));

% TEAOE data
TEOAE = cell(1,goodsubs(end));


% load data from disk
fprintf('loading data from participants: \n');
for S = goodsubs
    
    % get subject name
    Subname = SubList(S).name;
    fprintf([Subname ' \n']);
    
    
    % load data from Click_EM -------------------------------------
    filename = sprintf([nametemp '%s_%s.mat'],Subname,Expnames{1});
    TEOAE{S} = load(filename);
    
    
    % load data from All EMREO analysis ---------------------------
    filename = sprintf([nametemp '%s_%s.mat'],Subname,Expnames{2});
    EyeMovA{S} = load(filename);

    
    % load data from EyeMov 1 -------------------------------------
    filename = sprintf([nametemp '%s_%s.mat'],Subname,Expnames{3});
    EyeMov1{S} = load(filename);
    
    % take mean EMREO curve in both directions and plot together 
    EMREO1 = cat(3,EMREO1,EyeMov1{S}.EMREO.data);
    
    
    % load data from EyeMov 2 -------------------------------------
    filename = sprintf([nametemp '%s_%s.mat'],Subname,Expnames{4});
    EyeMov2{S} = load(filename);
    
    
    % load data from EyeMov 3 -------------------------------------
    filename = sprintf([nametemp '%s_%s.mat'],Subname,Expnames{5});
    EyeMov3{S} = load(filename);
end


%% Concatenate
% concatenate data from all participants

% concatenate TEOAE
TEOAE = cell2mat(TEOAE(goodsubs));
TEOAE = CatStructRecursive(TEOAE); 

% concatenate EyeMovA
EyeMovA = cell2mat(EyeMovA(goodsubs));
EyeMovA = CatStructRecursive(EyeMovA); 

% concatenate EyeMov1
EyeMov1 = cell2mat(EyeMov1(goodsubs));
EyeMov1 = CatStructRecursive(EyeMov1); 

% concatenate EyeMov2
EyeMov2 = cell2mat(EyeMov2(goodsubs));
EyeMov2 = CatStructRecursive(EyeMov2);

% concatenate EyeMov3
EyeMov3 = cell2mat(EyeMov3(goodsubs));
EyeMov3 = CatStructRecursive(EyeMov3);



%% Parameters
% define parameters for statistics
% number of participants
nsub = length(goodsubs);

% assert all experiments were analyzed at the same sample rate
%SRates = [EyeMovA.samplerate(:); EyeMov1.samplerate(:); EyeMov2.samplerate(:); EyeMov3.samplerate(:)];
SRates = globalP.ResampleFs; 
warning('change code in line 117-125 after rerunnung analyze scripts');
if length(unique(SRates)) > 1
    error('Sample rate of all experiments must be consistent!');
else
    samplerate = EyeMov2.EMREO.samplerate(1);
end

% minimum cluster size defined by time, for EMREO tests
clustersize = (2 * samplerate) / 1000;
% number of randomizations
Nrand = 10000;

% cluster stat parameters
cfg_stat.critvaltype = 'par'; % 'prctile' % type of threshold to apply. Usual 'par'
cfg_stat.critval = abs(tinv(globalP.Sign_Alpha,nsub-1)) ; % critical cutoff value for cluster members if parametric
cfg_stat.conn = 8; % connectivity criterion (for 2D 4 or 8 for 3D 6,18,26) 
cfg_stat.clusterstatistic = 'maxsum';
cfg_stat.minsize = clustersize; % minimal cluster size
cfg_stat.pval = globalP.Sign_Alpha; % threshold to select signifciant clusters
cfg_stat.df = nsub - 1; % degrees of freedom. Only needed for effect size.



%% TEOAE 
% get TEOAE spectral density and first EMREO peak amplitude, then correlate
% across participants

% get TEOAE
SpecDensity = TEOAE.io_spec_density;
ControlDensity = TEOAE.control_density;
SpecDensity = SpecDensity ./ ControlDensity;

% get first peak amplitude from true features structure
TrueFeatures = EyeMovA.Sampling.TrueFeatures;
TrueFeatures = sq(TrueFeatures(:,4,1));
rr = corr(SpecDensity,TrueFeatures);

% EMREO TFR temporal-spectral density
EMREOSpecDensity = EyeMovA.TFR.spec_density;
meanEMREODensity = mean(EMREOSpecDensity,2);

% plot parameter
cmap = jet(nsub);

% plot
figure
hold on
for isub = 1:nsub
    ph(isub) = plot(SpecDensity(isub),meanEMREODensity(isub),'o','Color',cmap(isub,:));
end
subjlabel = num2cell(goodsubs);
subjlabel = cellfun(@(x) ['S' char(string(x))], subjlabel,'UniformOutput',false);
legend(ph,subjlabel);
header = sprintf('%.3f',rr);
title(header);


%% EyeMov all
tax_ear = EyeMovA.EMREO.time(1,:);
label = EyeMovA.EMREO.label(1,:);
EMREOA = EyeMovA.EMREO.data;
emreounit = EyeMovA.EMREO.unit(1,:);


% -------------------------------------------------------------------------
% compute percent of good trials
nbad_seq = sum(EyeMovA.TrialStats.bad_seq,2); % vertical trials from Exp. 1
Ngood_trials = EyeMovA.TrialStats.Ngood_trials ./ (EyeMovA.TrialStats.ntrial - nbad_seq);
% print percent of removed trials
fprintf('Percent removed trials across participants:\n');
fprintf('%.3f mean,  %.3f std \n\n',mean(Ngood_trials),std(Ngood_trials));

figure('Position',[520 297 465 679]);
cfg = [];
cfg.jitterrange = 0.2;
cfg.connect = false;
barwithdots(cfg,Ngood_trials);
xlim([0.25 1.75]);
ylim([0 1]);

header = 'percent retained trials of 1796 trials total';
ckfiguretitle(header);

fname = sprintf([figurepath 'GroupAnalysis_All_Ngood_trials.png']);
print('-dpng',fname);
pause(0.5);




% create figure that shows how many trials per exclusion criterion we
% removed
trialstatnames = fieldnames(EyeMovA.TrialStats);
iter = 1;
CriterionLabel = {};
NCriterionEx = [];
for ifield = 1:length(trialstatnames)
    fname = trialstatnames{ifield};
    if islogical(EyeMovA.TrialStats.(fname))
        if strcmp(fname,'good_trials')
            continue
        end
        NCriterionEx(:,iter) = sum(EyeMovA.TrialStats.(fname),2);
        CriterionLabel{iter} = replace(fname,'_',' ');
        iter = iter + 1;
    end
end

ColNames = cellfun(@(x) replace(x,' ','_'),CriterionLabel,'UniformOutput',false);
TableSub = array2table(NCriterionEx,'VariableNames',ColNames);
msCriterionEx = [mean(NCriterionEx,1); std(NCriterionEx,[],1) ];
RowNames = {'Mean','Std'};
TableAgg = array2table(msCriterionEx,'VariableNames',ColNames,'RowNames',RowNames);

% plot number of excluded trials per criterion
figure('Position',[429 405 811 573]);
barwithdots(cfg,NCriterionEx);
xticklabels(CriterionLabel);

header = sprintf('Number of excluded trials per criterion');
ckfiguretitle(header);

fname = sprintf([figurepath 'GroupAnalysis_All_Nexcluded.png']);
print('-dpng',fname);
pause(0.5);



% save to group stats
GroupStats.Ratio.Ngood_trials = Ngood_trials;
GroupStats.Ratio.SubCriterion = TableSub;
GroupStats.Ratio.AggregateCriterion = TableAgg;



% -------------------------------------------------------------------------
% plot EMREO for each individual 
P = nextpow2(length(goodsubs)); % determine min N of subplots

figure('Position',[208 127 1514 816]);
for isub = 1:length(goodsubs)
    subnum = goodsubs(isub);
    ax = subplot(P,P,isub);
    hold on
    plot(tax_ear,sq(EMREOA(isub,1,:)), '-','LineWidth',0.1,'Color',[0 0 1]);
    plot(tax_ear,sq(EMREOA(isub,2,:)), '-','LineWidth',0.1,'Color',[1 0 0]);
    ymax1 = max(abs(ax.YAxis.Limits));
    plot([0 0], [-200 200], 'k--');
    xlim([-50 150]);
    ax.YAxis.Limits = [-ymax1 ymax1];
    
    if isub > floor((length(goodsubs)-1)/P) * P
        xlabel('time (ms)');
    end
    if mod(isub,P) == 1
        ytext = sprintf('Amplitude (%s)',emreounit);
        ylabel(ytext);
    end
    
    header = sprintf('S %02d',subnum);
    title(header);
end

header = 'All single participant EMREOs';
ckfiguretitle(header);

fname = sprintf([figurepath 'GroupAnalysis_All_Single_EMREO.png']);
print('-dpng',fname);
pause(0.5);




% -------------------------------------------------------------------------
% plot SNR for left and right EMREOs, with examples and the PCA explained
% variance?
SNR = sq(EyeMovA.EMREO.SNR);

% marker position
[nrows,ncols] = size(SNR);
% individual data jitter
X = repmat([1:ncols],[nrows,1]);
jitter = randn(size(X)) .* 0.03;
offset = 0.3 * 1; % shift to side of boxplots
X = X + jitter + offset;


figure('Position',[199 386 900 592]);

% plot SNR
subplot(2,3,[1,4]);
boxplot(SNR);
hold on
plot(X(:,1),SNR(:,1),'.','MarkerSize',12,'Color','r');
plot(X(:,2),SNR(:,2),'.','MarkerSize',12,'Color','b');
xticklabels({'left','right'});
ylabel('SNR');

% plot high and low SNR examples
subplot(2,3,[2,3]);
% get data
[~,ind] = max(SNR(:,2));
dataMax = sq(EyeMovA.EMREO.data(ind,1:2,:));
medianMax = EyeMovA.EMREO.SNRmediantrial{ind};
tax_snr = EyeMovA.EMREO.time(ind,:);
%plot
p1 = plot(tax_snr,medianMax,'-','Color',[1 1 1] .* 0.6,'LineWidth',0.5);
hold on
p2 = plot(tax_snr,dataMax(1,:),'b-','LineWidth',1.2);
p3 = plot(tax_snr,dataMax(2,:),'r-','LineWidth',1.2);
ylim([-150 150]);
ylabel('mV');


subplot(2,3,[5,6]);
% get data
[~,ind] = min(SNR(:,2));
dataMin = sq(EyeMovA.EMREO.data(ind,1:2,:));
medianMin = EyeMovA.EMREO.SNRmediantrial{ind};
tax_snr = EyeMovA.EMREO.time(ind,:);
%plot
p1 = plot(tax_snr,medianMin,'-','Color',[1 1 1] .* 0.6,'LineWidth',0.5);
hold on
p2 = plot(tax_snr,dataMin(1,:),'b-','LineWidth',1.2);
p3 = plot(tax_snr,dataMin(2,:),'r-','LineWidth',1.2);
ylim([-150 150]);
xlabel('Time (ms)');
ylabel('mV');


% figure title
header = ['SNR with max/min examples'];
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_All_EMREO_SNR.png']);
print('-dpng',fname);
pause(0.5);



% save data to group stats structure
GroupStats.SNR.SNR = mean(SNR,2);
GroupStats.SNR.Maxdata = cat(1,dataMax,medianMax);
GroupStats.SNR.Mindata = cat(1,dataMin,medianMin);


% -------------------------------------------------------------------------
% plot EMREO for each of the four conditions and superimpose participants
CondEMREO = EyeMovA.EMREO.datacond;
tax_emreo = EyeMovA.EMREO.time(1,:);
[nsubs,ncond,~] = size(CondEMREO);
condlabel = sq(EyeMovA.EMREO.condlabel);
condlabel = condlabel(1,:);

% initialze as empty
ymax1 = [];

% color palette
condcolor = {'r','r','b','b'};

CondData = zeros(ncond,length(tax_emreo),nsubs);

figure('Position',[38 240 1842 692]);
for icond = 1:ncond
    % plot all participants as individual lines
    hh = subplot(1,ncond,icond);
    HH(icond) = hh;
    % get data and zscore
    data = sq(CondEMREO(:,icond,:))';
    dataz = zscore(data,1);
    % shift each EMREO down by a fixed amount * S(i)
    dataoffset = max(abs(dataz(:))) * 2.2;
    dataz = dataz + [nsubs-1:-1:0]*dataoffset;
    plot([tax_emreo(1) tax_emreo(end)],repmat([0:nsubs-1]'*dataoffset,1,2),'-','Color',[1 1 1]*0.8);
    hold on
    plot(tax_emreo,dataz,'LineWidth',1.5);
    plot([0 0], [-dataoffset nsubs*dataoffset], 'k--');
    xlim([-50 150]);
    ylim([-dataoffset nsubs*dataoffset]);
    
    % collect data for later
    CondData(icond,:,:) = data;
    %CondData{icond} = dataz;
    GroupData{icond} = [mean(data,2)'; sem(data,2)'];
    Offset(icond) = dataoffset;
    
    % axes labels, titles
    xlabel('Time (ms)');
    if icond == 1
        ytext = sprintf('Amplitude (SD)');
        ylabel(ytext);
    end
    header = sprintf('%d degrees',condlabel(icond));
    title(header);
end

% create panel label to test
phout = panellabel(HH,'A')


% figure title
header = ['Mean EMREOs across conditions for all participants'];
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_All_EMREO_conditions.png']);
print('-dpng',fname);
pause(0.5);


% save to group stats
GroupStats.CondEMREO.tax = tax_emreo;
GroupStats.CondEMREO.data = CondData;
GroupStats.CondEMREO.MeanSem = GroupData;
GroupStats.CondEMREO.Offset = Offset;
GroupStats.CondEMREO.label = condlabel;
GroupStats.CondEMREO.Ntrials = sq(EyeMovA.EMREO.ntrials);


% -------------------------------------------------------------------------
% plot mean velocity for left / right and 6 / 12 degrees saccades
tax_eye = EyeMovA.Eye.time(1,:);
label = EyeMovA.Eye.label(1,:);


VeloA = EyeMovA.Eye.velocity;
AcceA = EyeMovA.Eye.acceleration;

figure('Position',[680 370 1188 608]);
subplot(221);
hold on
plot(tax_eye,sq(VeloA(:,1,:)), '-','LineWidth',0.1,'Color',[0.5 1 0.5]);
plot(tax_eye,sq(VeloA(:,2,:)), '-','LineWidth',0.1,'Color',[1 0.5 1]);
p1 = plot(tax_eye,sq(mean(VeloA(:,1,:),1)), 'g-','LineWidth',2.5);
p2 = plot(tax_eye,sq(mean(VeloA(:,2,:),1)), 'm-','LineWidth',2.5);
xlim([-50 150]);
legend([p1,p2], label{1:2});
xlabel('Time (ms)');
ylabel('Velocity (�/s)');

subplot(222);
hold on
plot(tax_eye,sq(VeloA(:,3,:)), '-','LineWidth',0.1,'Color',[0.5 1 0.5]);
plot(tax_eye,sq(VeloA(:,4,:)), '-','LineWidth',0.1,'Color',[1 0.5 1]);
p1 = plot(tax_eye,sq(mean(VeloA(:,3,:),1)), 'g-','LineWidth',2.5);
p2 = plot(tax_eye,sq(mean(VeloA(:,4,:),1)), 'm-','LineWidth',2.5);
xlim([-50 150]);
legend([p1,p2], label{3:4});
xlabel('Time (ms)');
ylabel('Velocity (�/s)');

subplot(223);
hold on
plot(tax_eye,sq(AcceA(:,1,:)), '-','LineWidth',0.1,'Color',[0.5 1 0.5]);
plot(tax_eye,sq(AcceA(:,2,:)), '-','LineWidth',0.1,'Color',[1 0.5 1]);
p1 = plot(tax_eye,sq(mean(AcceA(:,1,:),1)), 'g-','LineWidth',2.5);
p2 = plot(tax_eye,sq(mean(AcceA(:,2,:),1)), 'm-','LineWidth',2.5);
xlim([-50 150]);
legend([p1,p2], label{1:2});
xlabel('Time (ms)');
ylabel('Acceleration (�/s^{2})');


subplot(224);
hold on
plot(tax_eye,sq(AcceA(:,3,:)), '-','LineWidth',0.1,'Color',[0.5 1 0.5]);
plot(tax_eye,sq(AcceA(:,4,:)), '-','LineWidth',0.1,'Color',[1 0.5 1]);
p1 = plot(tax_eye,sq(mean(AcceA(:,3,:),1)), 'g-','LineWidth',2.5);
p2 = plot(tax_eye,sq(mean(AcceA(:,4,:),1)), 'm-','LineWidth',2.5);
xlim([-50 150]);
legend([p1,p2], label{3:4});
xlabel('Time (ms)');
ylabel('Acceleration (�/s^{2})');

header = ['EyeMov All Experiments Velocity (top)' newline 'and Acceleration (bottom) all participants'];
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_All_EyeMovement.png']);
print('-dpng',fname);
pause(0.5);



% -------------------------------------------------------------------------
% plot EMREO regression analysis across participants
RegModels = EyeMovA.Stats.RegModels;
RegBeta = cellfun(@(x) x.beta, RegModels(:,1), 'UniformOutput',false);
RegBeta = cat(3,RegBeta{:});
% randomized model weights
RegBetaRand = cellfun(@(x) x.betaRand, RegModels(:,1), 'UniformOutput',false);
RegBetaRand = cat(4,RegBetaRand{:});
MedianBetaRand = sq(median(RegBetaRand,3));
PrcBetaRand = cat(4, sq(prctile(RegBetaRand,5,3)), sq(prctile(RegBetaRand,95,3)) );
PrcBetaRand = permute(PrcBetaRand,[4,1,2,3]);

% get distribution of amplitude in x axis for all participants
XAmp = EyeMovA.Eye.XAmplitude;
StartPosX = EyeMovA.Eye.StartPosX;
EndPosX = EyeMovA.Eye.EndPosX;
for icond = 1:ncond
    XAmplitude{icond} = cat(1,XAmp{:,icond}) ./ ppd;
    StartX{icond} = cat(1,StartPosX{:,icond}) - centerpoint(1) ./ ppd;
    EndX{icond} = cat(1,EndPosX{:,icond}) - centerpoint(1) ./ ppd;
end

% collect model R square
for imod = 1:size(RegModels,2)
    R2 = cellfun(@(x) x.R_square, RegModels(:,imod), 'UniformOutput',false);
    R2 = cat(1,R2{:});
    R_square(imod,:,:) = R2;
end
% compute unique variance of the left-out factor
for imod = 2:size(RegModels,2)
    uniqueR_square(imod-1,:,:) = R_square(1,:,:) - R_square(imod,:,:);
end


% and randomized model R square
R_squareRand = cellfun(@(x) x.R_squareRand, RegModels(:,1), 'UniformOutput',false);
R_squareRand = cat(4,R_squareRand{:});
% dimensions and time vector
npred = size(RegBeta,1);
tax_reg = EyeMovA.Stats.time(1,:);
ntimepoints = length(tax_reg);


% do cluster based test of model weights against zero
stat_clusters = cell(2,npred);
t_true = zeros(npred,ntimepoints);
t_boot = zeros(npred,ntimepoints,Nrand);

% we dont need to test the intercept
for ipred = 1:npred
    X = sq(RegBeta(ipred,:,:));
    
    % do t-test against zero
    t_true(ipred,:) = mean(X,2) ./ (std(X,[],2) ./ sqrt(nsub));
    for irand = 1:Nrand
        % randomly flip sign
        XR = X .* (2*randi([0 1],1,nsub)-1);
        t_boot(ipred,:,irand) = mean(XR,2) ./ (std(XR,[],2) ./ sqrt(nsub));
    end
    % compute cluster, effect sizes, and store in variable
    [posclus, negclus] = eegck_clusterstats(cfg_stat,sq(t_true(ipred,:))',sq(t_boot(ipred,:,:)));
    diffcluster = {posclus; negclus};
    % compute peak and mean effect size for cluster
    for i = 1:length(diffcluster)
        if ~isempty(diffcluster{i})
            for iclus = 1:length(diffcluster{i}.p)
                maskSig = diffcluster{i}.mask==iclus;
                tclus = t_true(maskSig);
                % peak and mean effect size
                diffcluster{i}.peakCohensD(iclus) = max(tclus) / sqrt(nsub);
                diffcluster{i}.meanCohensD(iclus) = mean(tclus) / sqrt(nsub);
                % compute temporal boundaries of cluster
                temp_bound = tax_reg(maskSig);
                temp_bound = [min(temp_bound) max(temp_bound)];
                diffcluster{i}.boundaries(:,iclus) = temp_bound;
            end
        end
    end
    stat_clusters(:,ipred) = diffcluster;
end


% predictor names
PredTitles = EyeMovA.Stats.predtitles(1,:);

% plot mean weights
figure('Position',[53 582 1731 396]);
for ipred = 1:npred
    ax = subplot(1,npred,ipred);
    % plot true betas with SEM
    [~,p1] = fb_errorshade(tax_reg,mean(RegBeta(ipred,:,:),3),sem(RegBeta(ipred,:,:),3),[1 0 0]);
    hold on
    [~,p2] = fb_errorshade(tax_reg,mean(MedianBetaRand(ipred,:,:),3),sq(mean(PrcBetaRand(:,ipred,:,:),4)));
    xlim([tax_reg(1) tax_reg(end)]);
    ylimits = ax.YAxis.Limits;
    plot([0 0], ylimits,'k--');
    xlabel('Time (ms)');
    if ipred == 1
        ylabel('Regression Beta (mV/degree)');
    end
    legend([p1 p2],{'true','shuffled'});
    title(PredTitles{ipred});
end

fname = sprintf([figurepath 'GroupAnalysis_All_Regression.png']);
print('-dpng',fname);
pause(0.5);

% -> taking the standard error across participants is really not
% informative, since this does not show how much the weights are above
% chance within participants

% have a short look at how well the predictors are decorrelated
PredictiveDeviance = EyeMovA.Stats.PredictiveDeviance;

% save to group stats struct
GroupStats.Regress.Beta = RegBeta;
GroupStats.Regress.BetaRand = RegBetaRand;
GroupStats.Regress.R_square = R_square;
GroupStats.Regress.Unique_R_square = uniqueR_square;
GroupStats.Regress.R_squareRand = sq(R_squareRand);
GroupStats.Regress.Clusters = stat_clusters;
GroupStats.Regress.time = tax_reg;
GroupStats.Regress.XAmplitude = XAmplitude;
GroupStats.Regress.StartPosX = StartX;
GroupStats.Regress.EndPosX = EndX;
GroupStats.Regress.PredTitles = PredTitles;
GroupStats.Regress.EMREOunit = EyeMovA.EMREO.unit(1,:);



% compare our model with the model requested by the reviewer
% get peak R square and SEM from both models
[peakR2(1), ~]       = max(sq(mean(R_square(1,:,:),2)));
[peakR2(2), peakInd] = max(sq(mean(R_square(5,:,:),2)));
PeakSem(1) = sq(sem(R_square(1,:,peakInd),2));
PeakSem(2) = sq(sem(R_square(5,:,peakInd),2));

figure('Position',[286 499 1022 371])
tiledlayout(1,2)

nexttile(1)
fb_errorshade(tax_reg,sq(mean(R_square(1,:,:),2))',sq(sem(R_square(1,:,:),2))','r');
textstring = sprintf(['R^{2}_{peak} = %.3f' newline 'SEM_{peak} = %.3f'],peakR2(1),PeakSem(1));
text(50,0.2,textstring);

xlim([tax_reg(1) tax_reg(end)]);
title(['Model 1' newline 'Cond_{x} + Var_{fix} + Var_{end}']);
xlabel('Time (ms');
ylabel('R^{2}');

nexttile(2)
fb_errorshade(tax_reg,sq(mean(R_square(5,:,:),2))',sq(sem(R_square(5,:,:),2))','b');
textstring = sprintf(['R^{2}_{peak} = %.3f' newline 'SEM_{peak} = %.3f'],peakR2(2),PeakSem(2));
text(50,0.2,textstring);
xlim([tax_reg(1) tax_reg(end)]);
title(['Model 2' newline 'Pos_{fix} + Pos_{end}']);




% -------------------------------------------------------------------------
% do PCA on the EMREO features from the resampling analysis
SamplingFeatures = EyeMovA.Sampling.SamplingFeatures;
[nsubs,ncond,nrand,nfeatures] = size(SamplingFeatures);
SamplingFeatures = permute(SamplingFeatures,[3,1,2,4]);
SamplingFeatures = flatten(SamplingFeatures,[1,2]);
CondLabel = EyeMovA.Sampling.CondLabel(1,:);

% we can reuse the sampling data mean and SD to approx. zscore the true
% data and transform them into the PC space. then compare with the cluster
% means
TrueFeatures = EyeMovA.Sampling.TrueFeatures;



% divide participants into two groups by visual inspection fo the EMREO
grp1 = [10 12 16 21 24 28 33];
grp2 = [11 13 14 18 19 30];
jetmap = jet(nsubs);

% allocate variables as empty
msdata = [];
stdata = [];
clusmean = [];
Coeff = [];

figure('Position',[358 63 1248 922])
for icond = 1:ncond
    ax = subplot(ncond/2,ncond/2,icond);
    % normalize each column before PCA
    samplingdata = sq(SamplingFeatures(:,icond,:));
    msdata(icond,:) = mean(samplingdata,1,'omitnan'); % save for later
    stdata(icond,:) = std(samplingdata,[],1,'omitnan');
    samplingdata = (samplingdata - msdata(icond,:)) ./ stdata(icond,:);
    % augment NaN values with zeros, since design matrix is centered
    samplingdata(isnan(samplingdata)) = 0;
    [coeff,score,latent,tsquared,explained] = pca(samplingdata);
    Coeff(icond,:,:) = coeff;
    % plot data in principal component space
    hold on
    for isub = 1:nsubs
        subind = [1:nrand] + (isub-1) * nrand;
        ph = plot(score(subind,1),score(subind,2),'.','Color',jetmap(isub,:),'MarkerSize',8);
        pi(isub) = ph(1);
        % compute cluster mean
        clusmean(icond,isub,:) = mean(score(subind,[1,2]),1);
        pause(0.1);
    end
    
    % get true emreo features and project them into PCA space
    truefeat = sq(TrueFeatures(:,icond,:));
    % normalize with resampling parameters, augment NaN with zeros
    truefeat = (truefeat - msdata(icond,:)) ./ stdata(icond,:);
    truefeat(isnan(truefeat)) = 0;
    % project into PCA space 
    coeff = sq(Coeff(icond,:,:));
    pcafeat = truefeat * coeff;
    
    % plot the participant true cluster means
    for isub = 1:nsubs
        % set marker color depending on group
        dotcolor = 'g';
        if ismember(goodsubs(isub),grp1)
            dotcolor = 'm';
        end
        plot(sq(clusmean(icond,isub,1)),sq(clusmean(icond,isub,2)),'o','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor',dotcolor);
        plot(pcafeat(isub,1),pcafeat(isub,2),'o','MarkerSize',4,'Color','k');
        plot([sq(clusmean(icond,isub,1)) pcafeat(isub,1)],[sq(clusmean(icond,isub,2)) pcafeat(isub,2)],'--','Color','k');
        
    end
    % axes limits
    xlim([-5 6 ]);
    ylim([-6 10]);
    grid on
    xlabel('PC 1');
    ylabel('PC 2');
    header = sprintf('%d degrees',CondLabel(icond));
    title(header);
end
% create participant labels for legend
grouplabels = arrayfun(@num2str, goodsubs, 'UniformOutput',false);
grouplabels = cellfun(@(a,b) strjoin({a,b},''),repmat({'S'},[1,length(goodsubs)]),grouplabels,'UniformOutput',false);
% save subplot position and reset
pos = ax.Position;
legend([pi],grouplabels,'Location','eastoutside');
ax.Position = pos;

header = 'PCA on resampled EMREO feature data';
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_All_SamplingFeatureCluster.png']);
print('-dpng',fname);
pause(0.5);



% -------------------------------------------------------------------------
% plot the explained variance from the PCA on the ear data matrix
Explained = EyeMovA.PCA.Explained;
ExpRand = EyeMovA.PCA.ExplainedRand;
Coeff = EyeMovA.PCA.Coeff;
Score = EyeMovA.PCA.Score;
ncond = size(Explained,2);
Ncomp = 10;

figure('Position',[311 136 1287 827]);
for icond = 1:ncond
    ax = subplot(3,ncond,icond);
    expvar = cat(2,Explained{:,icond})';
    exprand = cat(3,ExpRand{:,icond});
    hold on
    p2 = plot(sq(mean(median(exprand(:,[1:Ncomp],:),1),3)),'LineWidth',1.2,'Color','m');
    p3 = plot(sq(mean(prctile(exprand(:,[1:Ncomp],:),5,1),3)),'--','LineWidth',1.2,'Color','m');
    plot(sq(mean(prctile(exprand(:,[1:Ncomp],:),95,1),3)),'--','LineWidth',1.2,'Color','m');
    p1 = plot(expvar(:,[1:10])','Color',[1 1 1]*0.5);
    xlim([0 Ncomp+1]);
    xticks([0:5:Ncomp]);
    xlabel('Nth component');
    ylabel('Explained variance (%)');
    if icond == ncond
        pos = ax.Position;
        legend([p1(1) p2 p3],{'subj. data','shuffled','5th/95th percentile'},'Location','northeast');
        ax.Position = pos;
    end

    % plot PC1 vs PC2 for each participant
    ax = subplot(3,ncond,icond+ncond);
    bar(mean(expvar(:,[1:10])),'FaceColor',[0 0.5 1],'FaceAlpha',0.5);
    hold on
    errorbar(mean(expvar(:,[1:10])),sem(expvar(:,[1:10])),'LineStyle','none','Color','k');
    box off
    xlim([0 Ncomp+1]);
    xlabel('Nth component');
    ylabel('Explained variance (%)');
    
    % plot first two component coefficients
    ax = subplot(3,ncond,icond+(2*ncond));
    coefficient = cat(3,Coeff{:,icond});
    hold on
    p1 = plot(sq(coefficient(:,1,:)),'b');
    ylabel('PCA coefficients');
    if icond == ncond
        pos = ax.Position;
        legend([p1(1)], {'coeff1', 'coeff2'},'Location','northeast');
        ax.Position = pos;
    end
end

header = 'Across participant PCA - explained variance';
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_All_EMREO_PCAvariance.png']);
print('-dpng',fname);
pause(0.5);



% do another plot but average across participants and conditions
expvar = cat(2,Explained{:})';

figure
% bars with error whiskers on top
bar(mean(expvar(:,[1:10])),'FaceColor',[0 0.5 1],'FaceAlpha',0.5);
hold on
errorbar(mean(expvar(:,[1:10])),sem(expvar(:,[1:10])),'LineStyle','none','Color','k');
% axes
xlim([0 Ncomp+1]);
xticks([0:5:Ncomp]);
xlabel('Nth component');
ylabel('Explained variance (%)');
box off

header = 'Across participant and conditions PCA - explained variance';
ckfiguretitle(header);


% save to group stats
GroupStats.PCA.ExpVar = expvar;
GroupStats.PCA.Coefficients = cell2mat(reshape(Coeff,[1,1,nsubs,ncond]));



% -------------------------------------------------------------------------
% errorbars across correlation coefficients between EMREO markers from
% sampling with repetition
tv_coef = EyeMovA.Sampling.tv_coef;

dim_names = EyeMovA.Sampling.label(1,:);
ndim_names = length(dim_names);
row_names = repmat(dim_names,[ndim_names,1]);
col_names = repmat(dim_names',[1,ndim_names]);
delimiter = repmat({' , '},[ndim_names,ndim_names]);
comb_names = strcat(row_names,delimiter,col_names);
% lower triangular indices
tril_ind = find(tril(ones(ndim_names,ndim_names),-1)~=0);
comb_names = comb_names(tril_ind);
cond_title = EyeMovA.Sampling.CondLabel(1,:);


% plot parameters
cfg = [];
cfg.jitterrange = 0.2;
cfg.connect = false;

% plot
figure('Position',[606 192 842 669]);
iter = 1;
for icond = 1:length(cond_title)
    % get coefficients for condition, flatten square mat, select lower
    % triangular
    condcoef = sq(tv_coef(:,icond,:,:));
    condcoef = reshape(condcoef, [length(goodsubs),ndim_names.^2]);
    condcoef = condcoef(:,tril_ind);
    
    subplot(2,2,iter);
    barwithdots(cfg,condcoef);
    xticklabels(comb_names);
    xtickangle(45);
    ylabel('rho');
    header = 'saccades to right';
    title(header);
    
    iter = iter + 1;
end


header = 'EMREO marker correlation';
ckfiguretitle(header);



% -------------------------------------------------------------------------
% take a look at peak amplitude and latencies 
% use data from resampling analysis
% results are in paper
PeakAmp = EyeMovA.Sampling.TrueFeatures(:,:,1:3);
PeakInd = EyeMovA.Sampling.TrueFeatures(:,:,4:6);
CondLabel = EyeMovA.Sampling.CondLabel(1,:);
ncond = length(CondLabel);

jetmap = jet(nsubs);
summap = summer(3);
ind_diff = [];

% figure
figure('Position',[390 257 801 637]);
for icond = 1:ncond
    amp = sq(PeakAmp(:,icond,:));
    ind = sq(PeakInd(:,icond,:));
    % compute mean difference
    ind_diff = cat(1,ind_diff,[ind(:,1) diff(ind(:,1:2),[],2)]);
    
    
    % plot
    subplot(2,2,icond);
    hold on
    for ipeak = 1:3
        for isub = 1:nsubs
            plot(ind(isub,ipeak),amp(isub,ipeak),'o','MarkerFaceColor',summap(ipeak,:)); %,'MarkerEdgeColor',jetmap(isub,:));
        end
    end
    xlabel('Time');
    ylabel('Amplitude');
end

header = 'first three peaks per condition';
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_EMREO_peaklatencies.png']);
print('-dpng',fname);
pause(0.5);


% save data to struct
GroupStats.Latencies.Mean = mean(ind_diff,1);
GroupStats.Latencies.Std = std(ind_diff,[],1);


% -------------------------------------------------------------------------
% plot first peak latency within participants 
% 6 and 12 degrees, split by velocity and acceleration
PeaksEMREO = EyeMovA.Split.PeaksEMREO;
Nmetrics = 2;
PeaksEMREO = cellfun(@(x) sq(x(:,:,2)), PeaksEMREO,'UniformOutput',false);
PeaksEMREO = cell2mat(PeaksEMREO);
PeaksEMREO = reshape(PeaksEMREO,[2,length(goodsubs),2,2]);
PeaksEMREO = permute(PeaksEMREO,[2,1,3,4]);
% -> sub x metric x condition x split
%convert to milliseconds
peakstime = EyeMovA.Split.time(1,:);
PeaksEMREO = peakstime(PeaksEMREO);

CondLabel = EyeMovA.Split.CondAmp(1,:);
SplitLabel = {'lower','higher'};

% two subplots for both conditions, comparison between splits
cfg = [];
cfg.connect = true;
cfg.jitterrange = 0.2;

% plot
figure('Position',[440 220 614 733]);

for icond = 1:2
    ax = subplot(2,1,icond);
    for imetric = 1:Nmetrics
        % get data
        tmp = sq(PeaksEMREO(:,imetric,icond,:));
        tickloc = [1,2] + 2*(imetric-1);
        
        cfg.XAxis = tickloc;
        barwithdots(cfg,tmp);
    end
    xticks([1:tickloc(end)]);
    ticknames = repmat(SplitLabel,[1,Nmetrics]);
    xticklabels(ticknames);
    ytop = ax.YLim(2);
    text(1.5,-ytop*0.15,'Velocity','HorizontalAlignment','center','FontWeight','bold');
    text(3.5,-ytop*0.15,'Acceleration','HorizontalAlignment','center','FontWeight','bold');
    %xlabel('Velocity      Acceleration');
    ylabel('Time (ms)');
    header = sprintf('abs(%d) degrees',CondLabel(icond));
    title(header);
end

header = ['first EMREO peak latency' newline 'median split group analysis'];
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_MedianSplit_EMREO_latency.png']);
print('-dpng',fname);
pause(0.5);



% -------------------------------------------------------------------------
% get mean/median peak velocity and acceleration per participant and
% compare that to the first EMREO peak latency
CondAmp = EyeMovA.Split.CondAmp(1,:);
Namp = length(CondAmp);
tax_split = EyeMovA.Split.time(1,:);
% get median data, reshape -> subs x condition x metric (vel / acc)
EyeMedians = EyeMovA.Split.Medians;
EyeMedians = cell2mat(EyeMedians);
EyeMedians = reshape(EyeMedians,[length(goodsubs),Namp,size(EyeMedians,2)/Namp]);
Nmetrics = size(EyeMedians,3);
% same shenanigans with EMREO Peaks
PeaksEMREO = EyeMovA.Split.AllPeaksEMREO;

% plot data
figure ('Position',[421 319 754 654]);
iter = 1;

Xlabels = {'Velocity (ppx/s)','Acceleration (ppx/s^{2})'};
ColLabels = {'abs(6) degrees','abs(12) degrees'};

warning('double check dimensions and their labels again!');

for icond = 1:Namp
    for imetric = 1:Nmetrics
        % get medians
        Xvar = sq(EyeMedians(:,icond,imetric));
        % get location i.e., latency 
        Yvar = sq(PeaksEMREO(:,icond,2));
        Yvar = tax_split(Yvar);
        
        ax = subplot(2,2,iter);
        plot(Xvar,Yvar,'bo');
        ytop = ax.YLim(2) - diff(ax.YLim) * 0.1;
        xmin = ax.XLim(1) + diff(ax.XLim) * 0.1;
        pcorr = corrcoef(Xvar,Yvar);
        corrtxt = sprintf('r=%.3f',pcorr(1,2));
        text(xmin,ytop,corrtxt);
        xlabel(Xlabels{icond});
        ylabel('EMREO peak latency (ms)');
        
        header = ColLabels{imetric};
        title(header);
        
        iter = iter + 1;
    end
end

header = ['Correlation between Velocity/Acceleration and' newline 'first EMREO peak latency across participants'];
ckfiguretitle(header);

fname = sprintf([figurepath 'GroupAnalysis_Velocity_EMREO_latency.png']);
print('-dpng',fname);
pause(0.5);



% -------------------------------------------------------------------------
% pool EMREO feature correlation matrices across participants
CoefMat = EyeMovA.Sampling.tv_coef;
MCoefMat = sq(mean(CoefMat,1));
SCoefMat = sq(std(CoefMat,[],1));

% sizes and lower triangular indices
ncond = size(MCoefMat,1);
coefsize = size(MCoefMat,2);
trilind = find(tril(ones(coefsize,coefsize),-1));

% create tick labels
ticklabel1 = EyeMovA.Sampling.label(1,:);
ticklabel1 = repmat(ticklabel1,[coefsize,1]);
ticklabel2 = ticklabel1';
delimiter = repmat({' - '},[coefsize,coefsize]);
ticklabels = cellfun(@(x,y,z) strcat(x,y,z), ticklabel1,delimiter, ticklabel2, 'UniformOutput',false);
ticklabels = ticklabels(trilind);

% subplot condition title
ColLabels = EyeMovA.Sampling.CondLabel(1,:);

% plot parameters
cfg = [];
cfg.connect = false;
cfg.jitterrange = 0.2;

% plot
figure('Position',[138 494 1641 360]);
for icond = 1:ncond
    subplot(1,ncond,icond);
    data = sq(CoefMat(:,icond,:,:));
    data = flatten(data,[2,3]);
    data = data(:,trilind);
    barwithdots(cfg,data);
    if icond == 1
        xticklabels(ticklabels);
    end
    ylim([-1 1]);
    % change x and y axis
    view([90 -90]);
    header = sprintf('%d degrees',ColLabels(icond));
    title(header);
end

header = 'Pooled Correlation coefficients across participants';
ckfiguretitle(header);



% -------------------------------------------------------------------------
% close all figures after each segment, otherwise there will be too many
close all
fprintf('\n');
fprintf('close all figures here \n');



%% EyeMov 1
% plot mean EMREO per participant
tax_ear = EyeMov1.EMREO.time(1,:);
label = EyeMov1.EMREO.label(1,:);

EMREO1 = EyeMov1.EMREO.data;

% plot
figure('Position',[678 129 1077 703]);
% up and down
subplot(211);
hold on
plot(tax_ear,sq(EMREO1(:,1,:)), '-','LineWidth',0.1,'Color',[0.5 1 0.5]);
plot(tax_ear,sq(EMREO1(:,2,:)), '-','LineWidth',0.1,'Color',[1 0.5 1]);
p1 = plot(tax_ear,sq(mean(EMREO1(:,1,:),1)), 'g-','LineWidth',2.5);
p2 = plot(tax_ear,sq(mean(EMREO1(:,2,:),1)), 'm-','LineWidth',2.5);
legend([p1,p2], label(1:2));

subplot(212);
hold on
plot(tax_ear,sq(EMREO1(:,3,:)), '-','LineWidth',0.1,'Color',[0.5 1 0.5]);
plot(tax_ear,sq(EMREO1(:,4,:)), '-','LineWidth',0.1,'Color',[1 0.5 1]);
p3 = plot(tax_ear,sq(mean(EMREO1(:,3,:),1)), 'g-','LineWidth',2.5);
p4 = plot(tax_ear,sq(mean(EMREO1(:,4,:),1)), 'm-','LineWidth',2.5);
legend([p3,p4], label(3:4));

header = 'EyeMov 1 EMREO all participants';
ckfiguretitle(header);



% -------------------------------------------------------------------------
% plot vertical EMREO for each of the four conditions 
CondEMREO = EyeMov1.EMREOvertical.datacond;
tax_emreo = EyeMov1.EMREOvertical.time(1,:);
[nsubs,ncond,~] = size(CondEMREO);
condlabel = sq(EyeMov1.EMREOvertical.condlabel);
condlabel = condlabel(1,:);

% color from magenta to green
initcolor = [1 0 1; 0 1 0] ;
interpcolor = interp_colormap(initcolor,length(condlabel)) .* 0.7;

% allocate space
CondData = zeros(ncond,length(tax_emreo),nsubs);

figure('Position',[38 240 1842 692]);
for icond = 1:ncond
    % get data and zscore
    data = sq(CondEMREO(:,icond,:))';
    fb_errorshade(tax_emreo,mean(data,2)',sem(data,2)',interpcolor(icond,:));
    xlim([-50 150]);
    

    % get data and zscore
    dataz = zscore(data,1);
    % offset to shift each EMREO up/down by a fixed amount * S(i)
    dataoffset = max(abs(dataz(:))) * 2.2;
    Offset(icond) = dataoffset;


    % collect data for later
    CondData(icond,:,:) = data;
    GroupData{icond} = [mean(data,2)'; sem(data,2)'];
end

% axes labels, titles
xlabel('Time (ms)');
ytext = sprintf('Amplitude');
ylabel(ytext);
header = sprintf('%d degrees',condlabel(icond));
title(header);


% figure title
header = ['Vertical EMREOs across all participants'];
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_All_EMREO_vertical.png']);
print('-dpng',fname);
pause(0.5);


% save to group stats
GroupStats.EMREOvertical.tax = tax_emreo;
GroupStats.EMREOvertical.data = CondData;
GroupStats.EMREOvertical.MeanSem = GroupData;
GroupStats.EMREOvertical.label = condlabel;
GroupStats.EMREOvertical.Offset = Offset;
GroupStats.EMREOvertical.Ntrials = sq(EyeMov1.EMREOvertical.ntrials);



% -------------------------------------------------------------------------
% close all figures after each segment, otherwise there will be too many
close all
fprintf('\n');
fprintf('close all figures here \n');


%% EyeMov 2 

% -------------------------------------------------------------------------
% do analysis in this function
OutputExp2 = Analyze_Group_Exp_23(EyeMov2,goodsubs,2);


% -------------------------------------------------------------------------
% close all figures after each segment, otherwise there will be too many
close all
fprintf('\n');
fprintf('close all figures here \n');



%% EyeMov 3 

% -------------------------------------------------------------------------
% do analysis in this function
OutputExp3 = Analyze_Group_Exp_23(EyeMov3,goodsubs,3);


% -------------------------------------------------------------------------
% close all figures after each segment, otherwise there will be too many
close all
fprintf('\n');
fprintf('close all figures here \n');


% concatenate behavioral data from exp. 2 and 3
BehavioralData = {OutputExp2.Behavior, OutputExp3.Behavior};

% save data to struct
GroupStats.Behavior = BehavioralData;



%% EyeMov 2 and 3 

% get data for both experimetns
Exp23Data = {EyeMov2, EyeMov3};
Nexp = length(Exp23Data);


% -------------------------------------------------------------------------
% sound onset behavior
% for both experiments we compute the behavior so that we can correct the
% pvalues across all tests
for iexp = 1:Nexp
    % get data
    CatData = Exp23Data{iexp};
    % sound onset group behavior
    label = CatData.Behavior.SoundOnset.Label(1,:);
    % dpendent variable names
    VarNames = {'Dprime','SigDetBias','RT'};
    resp = CatData.Behavior.SoundOnset.Dprime;
    bias = CatData.Behavior.SoundOnset.SigDetBias;
    rt = CatData.Behavior.SoundOnset.RT;
    ngroups = size(resp,2);
    
    % test Resp and RT with GLME, equals a repeated measures ANOVA
    % create labels
    condlabel = flatten(repmat([1:ngroups],nsubs,1));
    SubID = repmat([1:nsubs]',ngroups,1);
    
    % concat so we can loop easily
    var_test = cat(3,resp,bias,rt);
    GMLE{iexp} = [];
    for ivar = 1:size(var_test,3)
        % get data and create table
        var_data = sq(var_test(:,:,ivar));
        Table = cat(2,var_data(:),condlabel,SubID);
        Table = array2table(Table,'VariableNames',{'data','cond','Sub'});
        Table.Sub = categorical(Table.Sub);
        Table.cond = categorical(Table.cond);
        
        % fit glme
        GLME_Model = fitglme(Table, ...
            'data ~ 1 + cond + (1 | Sub) ', ...
            'Distribution', 'Normal', 'Link', 'Identity', 'FitMethod', 'MPL',...
            'DummyVarCoding', 'reference', 'PLIterations', 500, 'InitPLIterations', 20)
        GLME{iexp}.(VarNames{ivar}).Coefficients = GLME_Model.Coefficients;
        GLME{iexp}.(VarNames{ivar}).Equation = GLME_Model.Formula;
        GLME{iexp}.(VarNames{ivar}).stats = GLME_Model.ModelCriterion;
        GLME{iexp}.(VarNames{ivar}).anova = anova(GLME_Model)
        GLME_pval(iexp,ivar) = GLME_Model.Coefficients.pValue(2);
        nObs(iexp,ivar) = size(GLME_Model.ObservationInfo,1);
        
        % estimate effect size
        beta = fixedEffects(GLME_Model);
        y_est  = [condlabel SubID]*beta(2:end);
        y_cond = [condlabel]*beta(2);
        residual = var_data(:) - y_est;
        % total effect size
        Eta2 = var(y_est)/var(var_data(:));
        % partial effect size for effect of x2 on y
        Eta2_cond = var(y_cond)/(var(y_cond)+var(residual));
    end
    Response(iexp,:,:) = resp;
    SigDetBias(iexp,:,:) = bias;
    RT(iexp,:,:) = rt;
    
    % next, take the regression weights of the log(RT) model and test
    % against zero (excluding the intercept)
    lincoeff = CatData.Behavior.SoundOnset.LinCoeff;
    LinCoeff(iexp,:,:) = lincoeff;
    
    % compute t test statistic
    tmap = mean(lincoeff(:,2),1)./(std(lincoeff(:,2),[],1)./sqrt(length(goodsubs)));
    tstat(iexp,1) = tmap;
    % convert to p-value
    p_linbeta(iexp,1) = 2*(1 - tcdf(abs(tmap),length(goodsubs)-1));
    cohensD(iexp,1) = abs(mean(lincoeff(:,2),1)) ./ std(lincoeff(:,2),[],1);
end


% save to group stats
GroupStats.OnsetBehavior.TrialReactionTime = [EyeMov2.Behavior.SoundOnset.ReactionTime, EyeMov3.Behavior.SoundOnset.ReactionTime];
GroupStats.OnsetBehavior.TrialOnTime = [EyeMov2.Behavior.SoundOnset.OnTime, EyeMov3.Behavior.SoundOnset.OnTime];
GroupStats.OnsetBehavior.Response = Response;
GroupStats.OnsetBehavior.SigDetBias = SigDetBias;
GroupStats.OnsetBehavior.RT = RT;
GroupStats.OnsetBehavior.LinCoeff = LinCoeff;
GroupStats.OnsetBehavior.GLME = GLME;
GroupStats.OnsetBehavior.GLME_pval = GLME_pval;
GroupStats.OnsetBehavior.GLME_nObs = nObs;
GroupStats.OnsetBehavior.StatReg.pval = p_linbeta;
GroupStats.OnsetBehavior.StatReg.Tstat = tstat;
GroupStats.OnsetBehavior.StatReg.cohensD = cohensD;
GroupStats.OnsetBehavior.Nexp = Nexp;
GroupStats.OnsetBehavior.ExpNames = {'2','3'};




% -------------------------------------------------------------------------
% sound onset in relation to saccade onset and offset ---------------------
VelOn2 = sq(EyeMov2.Onsets.VelOn);
VelOff2 = sq(EyeMov2.Onsets.VelOff);

VelOn3 = sq(EyeMov3.Onsets.VelOn);
VelOff3 = sq(EyeMov3.Onsets.VelOff);

VelOn = mean(cat(3,VelOn2,VelOn3),3);
VelOff = mean(cat(3,VelOff2,VelOff3),3);

% set baseline to zero
VelOn = VelOn - min(VelOn,[],2);
VelOff = VelOff - min(VelOff,[],2);

% time vectors
tax_eye = EyeMov2.Onsets.tax_eye(1,:);
tax_window = EyeMov2.Onsets.tax_window(1,:);
tax_window = tax_eye(tax_eye > tax_window(1) & tax_eye < tax_window(2));


% concatenate the relativeOnTime and EventPoint vectors across exp. 2 and 3
OnTime = cat(2,EyeMov2.Onsets.OnTime, EyeMov3.Onsets.OnTime);
OnTime = cellfun(@(x,y) cat(1,x,y),OnTime(:,1),OnTime(:,2),'UniformOutput',false);
OffTime = cat(2,EyeMov2.Onsets.OffTime, EyeMov3.Onsets.OffTime);
OffTime = cellfun(@(x,y) cat(1,x,y),OffTime(:,1),OffTime(:,2),'UniformOutput',false);
EventPoint = sq(cat(3,EyeMov2.Onsets.EventPoint, EyeMov3.Onsets.EventPoint));
EventPoint = cellfun(@(x,y) cat(1,x,y),EventPoint(:,1),EventPoint(:,2),'UniformOutput',false);


% group onsets across participants
OnTimeGroups = cell(3,1);
OffTimeGroups = cell(3,1);
% create a pooled distribution of sound onset and offsets
for isub = 1:length(goodsubs)
    EventTimes = EventPoint{isub};
    relativeOnTime = OnTime{isub};
    relativeOffTime = OffTime{isub};
    for ii = 1:3
        OnTimeGroups{ii} = cat(1,OnTimeGroups{ii},relativeOnTime(EventTimes==ii));
        OffTimeGroups{ii} = cat(1,OffTimeGroups{ii},relativeOffTime(EventTimes==ii));
    end
end


% get sound onset bin data from all participants, normalize
OnsetStats2 = EyeMov2.Onsets.OnsetStats;
OnsetStats3 = EyeMov3.Onsets.OnsetStats;
OnsetStats = cat(1,OnsetStats2,OnsetStats3);
OnsetStats = sq(sum(OnsetStats,1));
OnsetStats = OnsetStats(2:end,:) ./ sum(OnsetStats(1,:));


% plot to figure
figure('Position',[129 386 1616 592])
% velocity aligned to saccade onset
ax1 = subplot(2,3,1);
plot(tax_eye, VelOn, 'Color', [0.6 0.5 0.7]);
hold on
plot(tax_eye, mean(VelOn,1),'LineWidth',2.5,'Color',[0.6 0.0 0.8]);
xlim([tax_window(1) tax_window(end)]);
ax1.YAxis.Limits(1) = 0;
ylabel('Velocity (a.u.)');
header = 'Aligned to Saccade Onset';
title(header);

% onset histograms
ax2 = subplot(2,3,4);
hold on
histogram(OnTimeGroups{1},100,'FaceColor','r');
histogram(OnTimeGroups{2},100,'FaceColor','g');
histogram(OnTimeGroups{3},100,'FaceColor','b');
xlim([tax_window(1) tax_window(end)]);
box on
xlabel('Time (ms)');
ylabel('Bin count');

% velocity aligned to saccade offset
ax3 = subplot(2,3,2);
plot(tax_window, VelOff, 'Color', [0.6 0.5 0.7]);
hold on
plot(tax_window, mean(VelOff,1),'LineWidth',2.5,'Color',[0.6 0.0 0.8]);
xlim([tax_window(1) tax_window(end)]);
ax3.YAxis.Limits(1) = 0;
header = 'Aligned to Saccade Offset';
title(header);

% offset histograms
ax4 = subplot(2,3,5);
hold on
histogram(OffTimeGroups{1},100,'FaceColor','r');
histogram(OffTimeGroups{2},100,'FaceColor','g');
histogram(OffTimeGroups{3},100,'FaceColor','b');
xlim([tax_window(1) tax_window(end)]);
box on
xlabel('Time (ms)');


% plot where sound onsets land
ax5 = subplot(2,3,[3,6]);
bh = bar(OnsetStats','grouped');
bh(1).FaceColor = [0.8 0.2 0];
bh(2).FaceColor = [0 0.8 0.2];
bh(3).FaceColor = [0.2 0 0.8];
xticklabels({'before','during','after'});
xlabel('actual sound onset bin');
ylabel('Normalized bin count');
legendtxt = {'before','during','after'};
legend(legendtxt);
title('sound onsets actually occured');


% create panel labels for testing
phoutA = panellabel([ax1 ax2 ax3 ax4],'A');
phoutB = panellabel([ax5],'B');


header = 'Sound onsets to saccades';
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_E23_SaccadesAndSoundOnsets.png']);
print('-dpng',fname);
pause(0.5);



% save to group stats
GroupStats.Onsets.tax_eye = tax_eye;
GroupStats.Onsets.tax_win = tax_window;
GroupStats.Onsets.VelOn = VelOn;
GroupStats.Onsets.VelOff = VelOff;
GroupStats.Onsets.OnTime = OnTimeGroups;
GroupStats.Onsets.OffTime = OffTimeGroups;
GroupStats.Onsets.OnsetStats = OnsetStats;





% -------------------------------------------------------------------------
% pool EMREO difference and response data across exp. 2 and 3
LooEMREO2 = EyeMov2.EarBehavior.LooEMREO;
Resp_code2 = EyeMov2.EarBehavior.Resp_code;

LooEMREO3 = EyeMov3.EarBehavior.LooEMREO;
Resp_code3 = EyeMov3.EarBehavior.Resp_code;

LooEMREO = cell(nsubs,1);
Resp_code = cell(nsubs,1);
for isub = 1:nsubs
    LooEMREO{isub} = cat(1,LooEMREO2{isub,:},LooEMREO3{isub,:});
    Resp_code{isub} = cat(1,Resp_code2{isub,:},Resp_code3{isub,:});
end


% -------------------------------------------------------------------------
% compare EMREO between experiment 2 and 3 across participants
Exp2emreo = EyeMov2.EMREO.mean;
Exp3emreo = EyeMov3.EMREO.mean;
[nsub,ncond,~] = size(Exp2emreo);
samplerate = EyeMov2.EMREO.samplerate(1);
assert(samplerate == EyeMov3.EMREO.samplerate(1), 'Sample rates must be the same');


% define temporal bounadries for statistical analysis
stat_bound = globalP.EMREOwindow;
stat_win = (tax_ear > stat_bound(1) & tax_ear < stat_bound(2));
tax_stat = tax_ear(stat_win);
ntimepoints = length(tax_stat);
Nrand = 1000;


% allocate space
clusterstats = cell(2,ncond);
t_true = zeros(ncond,ntimepoints);
t_boot = zeros(ncond,ntimepoints,Nrand);

for icond = 1:ncond
    % do t test with randomization ---------
    X1 = sq(Exp2emreo(:,icond,stat_win));
    X2 = sq(Exp3emreo(:,icond,stat_win));
    XD = X1 - X2;

    % compute t stat at each time point
    tic
    % compute true test statistic
    t_true(icond,:) = mean(XD,1)./(std(XD,[],1)./sqrt(nsub));
    % randomize for each time point by changing class labels
    for irand = 1:Nrand
        % pool both groups
        XDR = XD .* (2*randi([0 1],nsubs,1) - 1);
        t_boot(icond,:,irand) = mean(XDR,1)./(std(XDR,[],1)./sqrt(nsub));
    end
    % compute cluster, effect sizes, and store in variable
    [posclus, negclus] = eegck_clusterstats(cfg_stat,sq(t_true(icond,:))',sq(t_boot(icond,:,:)));
    diffcluster = {posclus; negclus};
    % compute peak and mean effect size for cluster
    for i = 1:length(diffcluster)
        if ~isempty(diffcluster{i})
            for iclus = 1:length(diffcluster{i}.p)
                maskSig = diffcluster{i}.mask==iclus;
                tclus = t_true(maskSig);
                diffcluster{i}.peakCohensD(iclus) = max(tclus) / sqrt(nsub);
                diffcluster{i}.meanCohensD(iclus) = mean(tclus) / sqrt(nsub);
                % compute temporal boundaries of cluster
                temp_bound = tax_stat(maskSig);
                temp_bound = [min(temp_bound) max(temp_bound)];
                diffcluster{i}.boundaries(:,iclus) = temp_bound;
            end
        end
    end
    clusterstats(:,icond) = diffcluster;
    toc
end



% plot statistics data
figure('Position',[285 149 1098 833])
for icond = 1:ncond
    subplot(ncond,1,icond);
    p1 = plot(tax_stat,t_true(icond,:),'r','LineWidth',1.5);
    hold on
    p2 = plot(tax_stat,sq(median(t_boot(icond,:,:),3)),'k');
    p3 = plot(tax_stat,sq(prctile(t_boot(icond,:,:),1,3)),'k--');
    plot(tax_stat,sq(prctile(t_boot(icond,:,:),99,3)),'k--');
    xlabel('Time (ms)');
    ylabel('t value');
    legend([p1 p2 p3],{'true','shuffled','1st/99th percentile'});
end


header = 'Stat. Comp. between EMREO in Exp 2 and 3';
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_E23_EMREO_DiffTest.png']);
print('-dpng',fname);
pause(0.5);





% -------------------------------------------------------------------------
% compare EMREO between correct and incorrect trials across both
% experiments (2 and 3)
BehaviorEMREO = cat(5, EyeMov2.EarBehavior.Raw, EyeMov3.EarBehavior.Raw);
BehaviorEMREO = sq(mean(BehaviorEMREO,5));
tax_ear = EyeMov2.EarBehavior.time(1,:);
% aggregate values 
TrueMetric = cat(3, EyeMov2.EarBehavior.TrueMeanPower, EyeMov3.EarBehavior.TrueMeanPower);
TrueMetric = sq(mean(TrueMetric,3));
FalseMetric = cat(3, EyeMov2.EarBehavior.FalseMeanPower, EyeMov3.EarBehavior.FalseMeanPower);
FalseMetric = sq(mean(FalseMetric,3));
Ntrue = EyeMov2.EarBehavior.Ntrue + EyeMov3.EarBehavior.Ntrue;
Nfalse = EyeMov2.EarBehavior.Nfalse + EyeMov3.EarBehavior.Nfalse;
ncond = size(BehaviorEMREO,2);
nresp = size(BehaviorEMREO,3);
CondLabel = EyeMov2.EarBehavior.CondLabel(1,:);

% allocate variables
t_true = zeros(ncond,ntimepoints);
t_boot = zeros(ncond,ntimepoints,Nrand);
CondEMREO = cell(1,ncond);

% plot EMREOs for correct and incorrect in each condition
figure('Position',[604 158 719 821])
for icond = 1:ncond
    subplot(ncond,1,icond);
    CondData = sq(BehaviorEMREO(:,icond,:,:)); 
    CondEMREO{icond} = CondData;
    
    % compare time courses with cluster statistics
    XD = sq(diff(CondData(:,:,stat_win),[],2));

    % compute t stat at each time point
    t_true(icond,:) = mean(XD,1)./(std(XD,[],1)./sqrt(nsub));
    % randomize for each time point by changing class labels
    for irand = 1:Nrand
        % randomly flip sign
        XDR = XD .* (2*randi([0 1],nsubs,1) - 1);
        t_boot(icond,:,irand) = mean(XDR,1)./(std(XDR,[],1)./sqrt(nsub));
    end
    % compute cluster, effect sizes, and store in variable
    [posclus, negclus] = eegck_clusterstats(cfg_stat,sq(t_true(icond,:))',sq(t_boot(icond,:,:)));
    diffcluster = {posclus; negclus};
    for i = 1:length(diffcluster)
        if ~isempty(diffcluster{iclus})
            for iclus = 1:length(diffcluster{i}.p)
                maskSig = diffcluster{i}.mask==iclus;
                tclus = t_true(icond,maskSig);
                diffcluster{i}.peakCohensD(iclus) = max(tclus) / sqrt(nsub);
                diffcluster{i}.meanCohensD(iclus) = mean(tclus) / sqrt(nsub);
                % compute temporal boundaries of cluster
                temp_bound = tax_stat(maskSig);
                temp_bound = [min(temp_bound) max(temp_bound)];
                diffcluster{i}.boundaries(:,iclus) = temp_bound;
            end
        end
    end
    CondCluster{icond} = diffcluster;
    
    % plot data
    [~, hp1] = fb_errorshade(tax_ear, sq(mean(CondData(:,2,:),1))', sq(sem(CondData(:,2,:),1))', [1 1 1] * 0.7);
    hold on
    [~, hp2] = fb_errorshade(tax_ear, sq(mean(CondData(:,1,:),1))', sq(sem(CondData(:,1,:),1))', ...
        ColorScheme.SaccCondColors(icond,:));
    xlim([-50 150]);
    legend([hp1 hp2],{'incorrect','correct'});
end
header = 'EMREO Behavior';
ckfiguretitle(header);



% % repeated measures ANOVA with GLME 
% create data array
BehaviorAggData = [TrueMetric(:); FalseMetric(:)];
degrees = EyeMov2.EarBehavior.CondLabel(1,:);
condition = EyeMov2.EarBehavior.CondLabel(:);
condition = repmat(condition,2,1);
response = [ones(numel(BehaviorAggData)/2,1); zeros(numel(BehaviorAggData)/2,1) ];
SubID = repmat([1:nsubs]',ncond*nresp,1);
Table = cat(2,BehaviorAggData,condition,response,SubID);

% convert to table
Table = array2table(Table,'VariableNames',{'y','cond','resp','Sub'});
Table.Sub = categorical(Table.Sub);
Table.cond = categorical(Table.cond);
Table.resp = categorical(Table.resp);


% git GLME with both variables, their interaction and a random effect for
% subjects (making it a repeated measures test)
GLME_Model = fitglme(Table, ...
    'y ~ 1 + cond + resp + cond:resp + (1 | Sub) ', ...
    'Distribution', 'Normal', 'Link', 'Identity', 'FitMethod', 'MPL',...
    'DummyVarCoding', 'reference', 'PLIterations', 500, 'InitPLIterations', 20)

% return anova results
GLME_anova = anova(GLME_Model);

% compute bayes factor for ANOVA results
BF10 = [];
nObs = size(GLME_Model.ObservationInfo,1);
for ii = 1:size(GLME_anova,1)
    BF10(ii) = bf.bfFromF(GLME_anova.FStat(ii),GLME_anova.DF1(ii),GLME_anova.DF2(ii),nObs);
end
BF10(BF10<1) = -1./BF10(BF10<1);
GLME_anova.BF = BF10';
disp(GLME_anova);


% x tick labels
ticklabels = num2cell(degrees);
ticklabels = cellfun(@(x) num2str(x),ticklabels, 'UniformOutput',false);


% plot data
figure('Position',[462 414 996 538]);
subplot(121);
% correct and incorrect
cfg.XAxis = [1:ncond];
cfg.FaceColor = [0 0 1];
cfg.FaceAlpha = 0.3;
barwithdots(cfg,TrueMetric);
hold on
cfg.XAxis = [1:ncond] + ncond;
cfg.FaceColor = [1 0 0];
cfg.FaceAlpha = 0.3;
barwithdots(cfg,FalseMetric);

% axes
xlim([0, ncond*nresp + 1]);
xticks([1:ncond*nresp]);
xticklabels([ticklabels ticklabels]);
xlabel('correct trials (blue) --- false trials(red)');
ylabel('mean EMREO amplitude');

% plot individual difference
subplot(122);
cfg.XAxis = [1:ncond];
cfg.FaceColor = [1 0 1];
cfg.FaceAlpha = 0.3;
barwithdots(cfg,TrueMetric - FalseMetric);
xticklabels(ticklabels);
xlabel('diff between correct and false trials');



% save data to struct
GroupStats.EMREOBehavior.Ntrue = Ntrue;
GroupStats.EMREOBehavior.Nfalse = Nfalse;
GroupStats.EMREOBehavior.CondLabel = CondLabel;
% EMREO time course results
GroupStats.EMREOBehavior.CondEMREO = CondEMREO;
GroupStats.EMREOBehavior.time = tax_ear;
GroupStats.EMREOBehavior.time_stat = tax_stat;
GroupStats.EMREOBehavior.Clusterstats = diffcluster;
% EMREO aggregate GLME results
GroupStats.EMREOBehavior.GLME_Model = GLME_Model;
GroupStats.EMREOBehavior.GLME_ANOVA = GLME_anova;




% -------------------------------------------------------------------------
% analysis of cue impact --------------------------------------------------
% behavior between valid/invalid and congruent/incongruent spatial cues
targets = {'Informative','Congruency'};
for itarget = 1:length(targets)
    RespInf = EyeMov3.SpatialCue.(targets{itarget}).Dprime;
    BiasInf = EyeMov3.SpatialCue.(targets{itarget}).SigDetBias;
    RTInf = EyeMov3.SpatialCue.(targets{itarget}).RT;
    sacclabel = EyeMov3.SpatialCue.(targets{itarget}).Label(1,:);
    NLeft = EyeMov3.SpatialCue.(targets{itarget}).NLeft;
    NRight = EyeMov3.SpatialCue.(targets{itarget}).NRight;

    % one-sided paired t-test against zero
    var_test = cat(3,RespInf,BiasInf,RTInf);
    var_diff = sq(diff(var_test,[],2));
    % compute t test statistic
    tmap = mean(var_diff,1)./(std(var_diff,[],1)./sqrt(length(goodsubs)));
    % convert to bayes factor
    BF10 = fb_stat_tstat2BF(tmap,nsubs-1);
    BF10(BF10<1) = -1./BF10(BF10<1);
    % convert to p-value
    pval = 2*(1 - tcdf(abs(tmap),length(goodsubs)-1));
    % adjust for multiple testing
    pval_adjust = fb_stat_padjust(pval,'benjamini-hochberg');
    cohensD = abs(mean(var_diff,1)) ./ std(var_diff,[],1);
    
    % save to struct
    GroupStats.CueBehavior.(targets{itarget}).NLeft = NLeft;
    GroupStats.CueBehavior.(targets{itarget}).NRight = NRight;
    GroupStats.CueBehavior.(targets{itarget}).Resp = RespInf;
    GroupStats.CueBehavior.(targets{itarget}).SigDetBias = BiasInf;
    GroupStats.CueBehavior.(targets{itarget}).RT = RTInf;
    GroupStats.CueBehavior.(targets{itarget}).pval = pval;
    GroupStats.CueBehavior.(targets{itarget}).tval = tmap;
    GroupStats.CueBehavior.(targets{itarget}).BF = BF10;
    GroupStats.CueBehavior.(targets{itarget}).cohensD = cohensD;
    GroupStats.CueBehavior.(targets{itarget}).Label = sacclabel;
end

% color palette
tracecolors = [0.6 0.0 0.8; 0.4 0.3 0.5];


% bar graph parameters
cfg = [];
cfg.jitterrange = 0.2;
cfg.FaceColor = tracecolors;

% plot
figure
subplot(121);
barwithdots(cfg,RespInf);
xticklabels(sacclabel);
yticks([0:4]);
ptext =  sprintf('p = %0.3f',pval_adjust(1));
text(1.8, 1.1, ptext, 'Color','r');
ylabel('Sensitivity d''');
title('Cue x Sound');

subplot(122);
barwithdots(cfg,RTInf);
xticklabels(sacclabel);
ylim([0 1200]);
yticks([0:200:1200]);
ptext =  sprintf('p = %0.3f',pval_adjust(2));
text(1.8, 1100, ptext, 'Color','r');
ylabel('Reaction time (ms)');
title('Cue x Sacc');


header = 'EyeMov 3  - Spatial Behavior';
ckfiguretitle(header);


fname = sprintf([figurepath 'GroupAnalysis_Spatial_Behavior.png']);
print('-dpng',fname);
pause(0.5);


% -> if there is no difference in performance between a correct and
% incorrect spatial cue, we can argue that in fact the relevant features
% for spatial hearing were not relevant in the task, therefore no effect on
% the EMREO would be expected! (this is good)



% --------------------------------------------------------
% compare EMREOs within these factor (validity/congruency)
% we use a cluster based approach on the time course and a repeated
% measures ANOVA on the average power


% get data
mfieldnames = fieldnames(EyeMov3.SpatialCue);
Nfields = length(mfieldnames);
Ndirections = 2;


% use cluster based statistics to compare time course
% we take the same statistical parameters as before
% allocate space
t_true = zeros(Nfields,Ndirections,ntimepoints);
t_boot = zeros(Nfields,Ndirections,ntimepoints,Nrand);

% go through both informativeness and congruency
for ifield = 1:Nfields
    clusterstats = cell(2,2);
    thisfield = mfieldnames{ifield};
    EMREO = EyeMov3.SpatialCue.(thisfield).EMREO;
    
    % separate directions
    for idir = 1:Ndirections
        dirind = [1 2] + Ndirections * (idir-1);
        emreodir = EMREO(:,dirind,stat_win);
        % compute difference
        XD = sq(diff(emreodir,[],2));
        
        % compute t stat at each time point
        tic
        % compute true test statistic
        t_true(ifield,idir,:) = mean(XD,1)./(std(XD,[],1)./sqrt(nsub));
        % randomize for each time point by changing class labels
        for irand = 1:Nrand
            % randomly flip sign
            XDR = XD .* (2*randi([0 1],nsubs,1) - 1);
            t_boot(ifield,idir,:,irand) = mean(XDR,1)./(std(XDR,[],1)./sqrt(nsub));
        end
        % compute cluster, effect sizes, and store in variable
        [posclus, negclus] = eegck_clusterstats(cfg_stat,sq(t_true(ifield,idir,:))',sq(t_boot(ifield,idir,:,:)));
        diffcluster = {posclus; negclus};
        % compute peak and mean effect size for cluster
        for i = 1:length(diffcluster)
            if ~isempty(diffcluster{i})
                for iclus = 1:length(diffcluster{i}.p)
                    maskSig = diffcluster{i}.mask==iclus;
                    tclus = t_true(maskSig);
                    diffcluster{i}.peakCohensD(iclus) = max(tclus) / sqrt(nsub);
                    diffcluster{i}.meanCohensD(iclus) = mean(tclus) / sqrt(nsub);
                    % compute temporal boundaries of cluster
                    temp_bound = tax_stat(maskSig);
                    temp_bound = [min(temp_bound) max(temp_bound)];
                    diffcluster{i}.boundaries(:,iclus) = temp_bound;
                end
            end
        end
        clusterstats(:,idir) = diffcluster;
        toc
    end
    
    % save to struct
    GroupStats.CueBehavior.(thisfield).EMREO = EMREO;
    GroupStats.CueBehavior.(thisfield).Clusters = clusterstats;
    GroupStats.CueBehavior.(thisfield).stat_time = tax_stat;
    GroupStats.CueBehavior.(thisfield).time = EyeMov3.SpatialCue.(thisfield).time;
    GroupStats.CueBehavior.(thisfield).Ndirections = Ndirections;
end


% plot statistics data
figure('Position',[285 314 1421 668])
iter = 1;
for ifield = 1:Nfields
    thisfield = mfieldnames{ifield};
    for idir = 1:Ndirections
        subplot(Nfields,Ndirections,iter);
        % set line color if we have a significant cluster
        linecolor = 'r';
        if ~isempty(GroupStats.CueBehavior.(thisfield).Clusters{1,idir}) || ~isempty(GroupStats.CueBehavior.(thisfield).Clusters{2,idir})
            linecolor = 'g';
        end
        
        p1 = plot(tax_stat,sq(t_true(ifield,idir,:)),linecolor,'LineWidth',1.5);
        hold on
        p2 = plot(tax_stat,sq(median(t_boot(ifield,idir,:,:),4)),'k');
        p3 = plot(tax_stat,repmat(cfg_stat.critval,size(tax_stat)),'k--');
        plot(tax_stat, -repmat(cfg_stat.critval,size(tax_stat)),'k--');
        xlim([tax_stat(1) tax_stat(end)]);
        xlabel('Time (ms)');
        ylabel('t value');
        legend([p1 p2 p3],{'true','shuffled','a = 0.01'});
        
        iter = iter + 1;
    end
end


header = 'Stat. Comp. Validity (top) and Congruent (bottom)';
ckfiguretitle(header);

% save to file
fname = sprintf([figurepath 'GroupAnalysis_E3_EMREO_CueTest.png']);
print('-dpng',fname);
pause(0.5);


% create ANOVA using GLME to compare aggregate EMREO power by factors and
% saccade direction
DirPowerName = {'MeanPower_L','MeanPower_R'};
for ifield = 1:Nfields
    thisfield = mfieldnames{ifield};
    for idir = 1:length(DirPowerName)
        DirPower = EyeMov3.SpatialCue.(thisfield).(DirPowerName{idir});
        MeanPower(:,:,idir) = DirPower;
        dirlabel = repmat(idir,size(DirPower));
        Direction(:,:,idir) = dirlabel;
    end
    Factorlabel = repmat([1 2],[nsubs,1,length(DirPowerName)]);
    SubID = repmat([1:nsubs]',length(DirPowerName)*Nfields,1);
    
    % create Table
    Table = cat(2,MeanPower(:),Direction(:),Factorlabel(:),SubID);
    Table = array2table(Table,'VariableNames',{'data','Dir','Fac','Sub'});
    Table.Dir = categorical(Table.Dir);
    Table.Fac = categorical(Table.Fac);
    Table.Sub = categorical(Table.Sub);
    
    % fit ANOVA using GLME
    GLME_Model = fitglme(Table, ...
    'data ~ 1 + Dir + Fac + Dir:Fac + (1 | Sub) ', ...
    'Distribution', 'Normal', 'Link', 'Identity', 'FitMethod', 'MPL',...
    'DummyVarCoding', 'reference', 'PLIterations', 500, 'InitPLIterations', 20)

    % return anova results
    GLME_anova = anova(GLME_Model);
    
    % compute bayes factor for ANOVA results
    BF10 = [];
    nObs = size(GLME_Model.ObservationInfo,1);
    for ii = 1:size(GLME_anova,1)
        BF10(ii) = bf.bfFromF(GLME_anova.FStat(ii),GLME_anova.DF1(ii),GLME_anova.DF2(ii),nObs);
    end
    BF10(BF10<1) = -1./BF10(BF10<1);
    GLME_anova.BF = BF10';
    disp(GLME_anova);
    
    GroupStats.CueBehavior.(thisfield).GLME_Model = GLME_Model;
    GroupStats.CueBehavior.(thisfield).GLME_ANOVA = GLME_anova;
end



% -------------------------------------------------------------------------
% close all figures after each segment, otherwise there will be too many
close all
fprintf('\n');
fprintf('close all figures here \n');




%%
% -------------------------------------------------------------------------
% save GroupStats to file, so we can make final figures for the paper in
% another script

save(sumname,'-struct','GroupStats');
fprintf(repmat('\n',[1,4]));


end
