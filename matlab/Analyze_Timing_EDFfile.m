%
% load eyetracking data and log files, compare saccade data from both types
%
% compare timings and see whether sounds are indeed played before, during
% or after the saccade
%


close all
clearvars

% get ExpList
USECOMP = 2;
ExpList
defdirs;

% select subject
S = 12;

ppd = 54;    % pixels per degree

ARG.MAXLAT = 600;
ARG.trial_per_block = 209;

%% --------------------------------------------------------------------------
% Load Event data
[EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath);


%%
% Load Eye Tracking data
ETdata = [];
for isub = S
    for l = 1:length(SubList{isub}.Eyemov_mat)
        
        fname = [datapath ARG_Matlab{isub}{l}.etname([1:end-4]) '.asc']

        [ET,Eye,Vel,Acc] = eegck_readEDF_v4(fname);
        [Sacc{l},TimingLog{l}] = find_saccades(ARG_eye,ET,Eye,Vel,Acc);
        
    end
    % concatenate all blocks
    Sacc = ft_appenddata([],Sacc{:});
    TimingLog = cat(1,TimingLog{:});
end




%%
% get trials with presaccadic sound onset
presacc_flag = EventLog(:,4);

% get fixation points
cvec = 'krgbcrgbc';
ntrial = size(ETdata,2);
Midpoint = [960,540]; % screen center
figure(1);clf; hold on
Log_trial_fix = zeros(1,ntrial);
FixPosTrl = zeros(ntrial,2);
for t=1:ntrial
   
  % determine intended position
  trial_val_ET = ETdata(t).TRIALID_m;
  log_id = find(EventLog(:,1)==trial_val_ET);
  condition = EventLog(t,2);
  
  if condition == 0
      continue
  end
  
  % switch colors according to direction, ignore size
  if condition>5
    condition = condition-4;
  end
  
  ontime = [ETdata(t).DISPLAY_ON_t 0];
  ontime = max(ontime);
  
   
  % find  longest fixation starting after display onset
  if ~isempty(ETdata(t).fixation)
    j = find (  (ETdata(t).fixation(:,1)>ontime).*(ETdata(t).fixation(:,1)<=(ontime+ARG.MAXLAT)));
    if ~isempty(j)
      dur = ETdata(t).fixation(j,3);
      [~,o] = max(dur);
      fix = ETdata(t).fixation(j(o),:);
      fix_pos = fix([4,5])-Midpoint;
      FixPosTrl(t,:) = fix_pos;
      plot(fix_pos(1),fix_pos(2),['o' cvec(condition)]);
      Log_trial_fix(t) = 1;
    end
  end
  
end
title('Fixations (o) and saccadic (.) end points')
axis([-700 700 -700 700])


%%
% now try something similar for the saccades

Analysis_log = ones(ntrial,6)*NaN;
for t = 1:ntrial
   
  % determine intended position
  trial_val_ET = ETdata(t).TRIALID_m;
  log_id = find(EventLog(:,1)==trial_val_ET);
  condition = EventLog(t,2);

  % on presenting fixation dot
  ontime = [ETdata(t).DISPLAY_ON_t 0];
  ontime = max(ontime);
  
  % when dot moves to target position
  %t_ontime = max(0, ETdata(t).TARGET_t - ETdata(t).TRIALID_t(2));
  
  %----------------------------------------------------------------
  % find  largest saccade starting in first 300ms after display onset
  if ~isempty(ETdata(t).saccade)
      % find any saccade in first 600 ms after display onset
    j = find ( (ETdata(t).saccade(:,1)>ontime).*(ETdata(t).saccade(:,1)<(ontime+ARG.MAXLAT)));
    if ~isempty(j)
      % take largest saccade
      amps = sqrt((ETdata(t).saccade(j,4)-ETdata(t).saccade(j,6)).^2 + (ETdata(t).saccade(j,5)-ETdata(t).saccade(j,7)).^2);
      [~,o] = max(amps);
      sacc = ETdata(t).saccade(j(o),:);

      % get the start time of the saccadic movement
      sacc_time = sacc(3); % time in samples relative to trial onset. 
      % edf file header says it is sampled at 1000 Hz (1 ms samples)
      % convert this to time relative to display onset, as this corresponds to the trigger in the
      % EEG data
      sacc_late = sacc(1) - ontime;
      sacc_start = sacc([4,5])-Midpoint;
      sacc_end = sacc([6,7])-Midpoint;
      Sacc_duration(t) = sacc_time;
      Sacc_latency(t) = sacc_late;
      Sacc_EndPos(t,:) = sacc_end;
      Analysis_log(t,:) = [sacc_time sacc_start sacc_end condition];
      plot(sacc_end(1),sacc_end(2),'.','Color',ones(1,3)*0.5);
     
    else
      Analysis_log(t,:) = ones(1,6)*NaN;
    end
  end
  
end % trials

figure
subplot(211)
histogram(Sacc_duration,ntrial);

subplot(212)
histogram(Sacc_latency,ntrial);

fprintf(' - all saccades - \n');
fprintf('median latency:    %.3f s \n',median(Sacc_latency));
fprintf('median duration:   %.3f s \n',median(Sacc_duration));


