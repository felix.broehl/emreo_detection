function Analyze_CochActivity(S,Experiment)
%
% analyze the pretarget cochlear activity
% pretarget time point is aligned to zero (EEG trigger)
%
% new fieldrip versions are able to perform IRASA and FOOF based on the
% following articles
% IRASA: https://www.fieldtriptoolbox.org/example/irasa/
% FOOOF: https://www.fieldtriptoolbox.org/example/fooof/
%

close all

% get ExpList
USECOMP = 3
ExpList
defdirs;

Subname = SubList(S).name

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% structure in which we save summary metrics from individual analyses
sumname = sprintf([datapath_res 'SumMetrics_FB03_S%0d_Coch%02d.mat'],S,Experiment);
SumMetrics = [];
SumMetrics.date = date;


%% ----------------------------------------------------------------------------
% generel paramters
ARG.resamplefs = globalP.ResampleFs;

% parameters for filtering and data inclusion - We expect EMREO around 35 Hz
ARG_ear.zscore = true;
ARG_ear.hpfilter   = 'yes';  
ARG_ear.hpfreq     = 500;
ARG_ear.lpfilter   = 'yes';  
ARG_ear.lpfreq     = 2000;
ARG_ear.new_fs     = ARG.resamplefs;
ARG_ear.Ntrials = 144;

ARG_ear.prestim    = -1.7;
ARG_ear.poststim   =  0.0;
ARG_ear.exp = Experiment;


ARG_freq.bandcenter = [750:10:1250]; % center freqs
ARG_freq.bandwidth = [-30 30];
ARG_freq.mod_range = [1:0.5:30];


% save data to struct
SumMetrics.samplerate = ARG.resamplefs;


%% --------------------------------------------------------------------------
% Load Ear data
Ear_data = load_eardata_blocks(S,SubList,datapath_ear,ARG_ear);

% downsample data
cfg = [];
cfg.resamplefs = ARG_ear.new_fs;
Ear_data = ft_resampledata(cfg,Ear_data);
ear_fsample = Ear_data.fsample;


%%
% --- quality check ---
% get data, mean and std of all data points
trialdata = cat(1,Ear_data.trial{:});
m_trial = mean(trialdata(:));
std_trial = std(trialdata(:));
CLim = m_trial + [-2.5*std_trial 2*std_trial];
tax = Ear_data.time{1};

% plot
figure('Position',[406 552 1074 409]);
subplot(2,2,[1,3]);
imagesc(tax,Ear_data.trialinfo,trialdata',CLim);
colorbar;
axis square
xlabel('time');
ylabel('trials');
title('all trial data');

% mean trial data
subplot(222);
plot(tax,mean(trialdata,1));
xlim([tax(1) tax(end)]);
title('mean across trials');

% single trial example
subplot(224);
plot(tax,trialdata(4,:));
xlim([tax(1) tax(end)]);
title('example - trial 1');

header = sprintf('%s_%d',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_CochActivity_Quality.png'],Experiment);
print('-dpng',fname);



% -------------------------------------------
% average trials
cfg = [];
cfg.removemean = 'yes';
cfg.keeptrials = 'no';
Ear_data = ft_timelockanalysis(cfg,Ear_data);

% pack into cell so following code works
Ear_data.avg = {Ear_data.avg};


% filter data and compute amplitude envelope 
tic
fprintf('filtering and hilbert transforming trials \n');
for itrial = 1:length(Ear_data.avg)
    trial_filt = zeros(length(ARG_freq.bandcenter),size(Ear_data.avg{itrial},2));
    for iband = 1:length(ARG_freq.bandcenter)
        % filter coefficients
        i_ab = ARG_freq.bandwidth + ARG_freq.bandcenter(iband);
        [b,a] = butter(4,2*i_ab/ear_fsample);
        % filter, hilbert, absolute
        trial_filt(iband,:) = abs(hilbert(filtfilt(b,a,Ear_data.avg{itrial}')))';
    end
    Ear_data.avg{itrial} = trial_filt;
end
% create surrogate channel label based on center frequencies
Ear_data.label = cellstr(char(string(ARG_freq.bandcenter')));
toc

% fit structure to do the following freq analysis
Ear_dataAvg = Ear_data;
Ear_dataAvg.trial = Ear_data.avg;
Ear_dataAvg.time  = {Ear_data.time};
Ear_dataAvg = removefields(Ear_dataAvg,{'var','avg','dof'});

% get power spectrum  
cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'pow';
cfg.taper = 'hanning';
cfg.channel = 'all';
cfg.foi = ARG_freq.mod_range;
Ear_freq = ft_freqanalysis(cfg,Ear_dataAvg);

% plot power spectrum modulation
figure('Position',[921 81 560 420]);
imagesc(ARG_freq.bandcenter,ARG_freq.mod_range,Ear_freq.powspctrm);
axis xy
colorbar;
xlabel('cochlear frequency (Hz)');
ylabel('modulation frequency (Hz)');

header = sprintf('%s_%d Right Ear',Subname, Experiment);
ckfiguretitle(header,'k',10);

fname = sprintf([figurepath Subname 'E%d' '_CochActivity.png'],Experiment);
print('-dpng',fname);



% ------
% next step would be to extract the periodic components of the PSD (PSDper)
% with FOOOF or something similar
% then save to file
%
% we then can compute the amplitude modulation index between experiment 2
% and 3 by (PSDper_3 - PSDper_2) / (PSDper_3 + PSDper_2) * 100
%
% see K�hler paper 2021 BMC Biology
% ------


% save data to struct
SumMetrics.CochMod.Bandcenter = ARG_freq.banedcenter;
SumMetrics.CochMod.ModRange = ARG_freq.mod_range;
SumMetrics.CochMod.Powspctrm = Ear_freq.powspctrm;



%% save summary metrics to file
save(sumname,'-struct','SumMetrics');




end
