function PlotFinalFigure_2
%
% plot the final figures for project FB03
%


close all

% get ExpList
USECOMP = 3;
ExpList;
defdirs;


% load data
filename = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
GroupStats = load(filename);

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% get colors
ColorScheme = FB03_Colorscheme;

% color p values if samller than p_critval
p_critval = globalP.Sign_Alpha;


% initialize figure count
FigNumber = 2;



%%
% plot first figure with (A) all EMREOs, (B) SNR, (C) PCA of raw data

% Notes:
% make this a double column figure
% panellable function does not position labels properly


% EMREO data
EMREOdata = zscore(GroupStats.CondEMREO.data,[],2);
GroupData = GroupStats.CondEMREO.MeanSem;
Offset = GroupStats.CondEMREO.Offset;
dataoffset = ceil(max(Offset));
tax_ear = GroupStats.CondEMREO.tax;
ncond = size(EMREOdata,1);
nsubs = size(EMREOdata,3);
condlabel = GroupStats.CondEMREO.label;
NtrialsH = GroupStats.CondEMREO.Ntrials;

% vertical EMREO data
tax_vert = GroupStats.EMREOvertical.tax;
EMREOdataV = zscore(GroupStats.EMREOvertical.data,[],2);
OffsetV = GroupStats.CondEMREO.Offset;
dataoffsetV = ceil(max(OffsetV));
VerticalMeanSEM = GroupStats.EMREOvertical.MeanSem;
vert_label = GroupStats.EMREOvertical.label;
nvertcond = size(VerticalMeanSEM,2);
NtrialsV = GroupStats.EMREOvertical.Ntrials;


% get regression data from struct
RegBeta = GroupStats.Regress.Beta;
RegBetaRand = GroupStats.Regress.BetaRand;
R_square = GroupStats.Regress.R_square;
R_squareRand = GroupStats.Regress.R_squareRand;
unique_R_square = GroupStats.Regress.Unique_R_square;
tax_reg = GroupStats.Regress.time;
stat_clusters = GroupStats.Regress.Clusters;
PredTitles = GroupStats.Regress.PredTitles;
PredTitleAlt = {'Full Model','C(t)','cond_{x}','var_{fix}','var_{end}'};
EMREOunit = GroupStats.Regress.EMREOunit;
% dimensions 
npred = size(RegBeta,1);
degrees = GroupStats.CondEMREO.label;

% print variance around fixation and target
FixVar = std(cat(1,GroupStats.Regress.StartPosX{:}));
EndVar = cellfun(@(x,y) x-y,GroupStats.Regress.EndPosX,num2cell(degrees),'UniformOutput',false);
EndVar = cellfun(@std,EndVar);
fprintf('\n');
fprintf('Variance Fixation point +- std \n');
fprintf('%.2f   degrees \n',FixVar);
fprintf('\n');
fprintf('Variance End point (left to right) +- std \n');
fprintf([repmat('%.2f   ',1,length(degrees)) ' degrees \n'],EndVar);



% get Peak Latencies data and print results
MeanLatencies = GroupStats.Latencies.Mean;
SDLatencies = GroupStats.Latencies.Std;
npeaks = length(MeanLatencies);
fprintf('\n');
fprintf('PEAK LATENCIES in ms across subj. and cond. +- std \n');
fprintf(repmat('%.2f   ',1,npeaks),MeanLatencies);
fprintf('\n');
fprintf(repmat('%.2f   ',1,npeaks),SDLatencies);
fprintf('\n \n');

% grand average plot y limits
ylimits = [-35 35];

% plot parameters
subpLineWidth = 1.0; % Line thickness of panels

% n trials inlay parameters
inlay_pos = [100 -28 42 20];


% % REGRESSION RIGHT VARIANT
% FigPosition = [391 62 990 930];
% % define subplot positions
% Nsubrows = 8;
% ncols = 5;
% % individual horizontal (row 1) and vertical (row 2) EMREOs
% Asubpos = [1 2 3 4; 16 17 18 19]; 
% Asubsize = [3 1];
% % grand average plots
% Bsubpos = [31 33]; 
% Bsubsize = [2 2];
% % regression plots
% Csubpos = [5 15 25];
% Csubsize = [2 1];
% % small regression plots
% Dsubpos = 35;
% Dsubsize = [1 1];


% REGRESSION BOTTOM VARIANT % 1000 x 12000
FigPosition = [391 73 850 1000];
% define subplot positions
Nsubrows = 10;
ncols = 4;
% individual horizontal (row 1) and vertical (row 2) EMREOs
Asubpos = [1 2 3 4; 13 14 15 16]; 
Asubsize = [3 1];
% grand average plots
Bsubpos = [25 27]; 
Bsubsize = [2 2];
% regression plots
Csubpos = [33 34 35];
Csubsize = [2 1];
% small regression plots
Dsubpos = 36;
Dsubsize = [1 1];


% subplot position shift
shiftdown = 0.02;
% panel label right shift
phrshift = 0.4;

CondDirLabel = {'Contralateral','Contralateral','Ipsilateral','Ipsilateral'};

% colormap for emreo
cmap = fb_plot_colormaps('bwrlight');
scalefactor = max(abs(EMREOdata(:)));


figure('Position',FigPosition);
tlay = tiledlayout(Nsubrows,ncols,'Padding','compact','TileSpacing','compact');
HH = [];
LH = []; % legend handle for grand average plots

% data for horizontal and verical EMREOs to loop through
SubjectEMREO = {EMREOdata, EMREOdataV};
SubjectOffset = {dataoffset, dataoffsetV};

% loop through direiction, horizontal then vertical
for idir = 1:length(SubjectOffset)
    % loop through conditions, -12 to 12 degrees
    for icond = 1:ncond
        % plot all participants as individual lines
        hh = nexttile(Asubpos(idir,icond),Asubsize);
        HH = [HH, hh];
        CondEMREO = sq(SubjectEMREO{idir}(icond,:,:));
        suboffset = SubjectOffset{idir};
        
        % define individual baseline
        baselines = repmat([nsubs-1:-1:0]'*suboffset,1,2);
        
        hold on
        % plot color shading behind emreos
        colormap(hh,cmap);
        EMREOz = CondEMREO; 
        EMREOz = EMREOz ./ max(abs(EMREOz));
        for isub = 1:nsubs
            emreo = EMREOz(:,isub);
            shiftup = baselines(isub,1);
            dx = [tax_ear(1:10:end) fliplr(tax_ear(1:10:end))];
            dy = [ones(1,71) * -suboffset/2, ones(1,71) * suboffset/2] + shiftup;
            dc = [emreo(1:10:end)' fliplr(emreo(1:10:end)')];
            pah = patch(dx,dy,dc,'EdgeColor','none');
        end
        
        % plot baselines
        plot([tax_ear(1) tax_ear(end)],baselines,'-','Color',[1 1 1]*0.4);
        
        % EMREOs in condition color
        EMREO = CondEMREO + baselines(:,1)';
        plot(tax_ear,EMREO,'LineWidth',2.0,'Color','k');
        
        % saccade onset time
        plot([0 0], [-suboffset nsubs*suboffset], 'k--');
        
        % add colorbar
        if icond == ncond && idir == 2
            ch = colorbar('Location','southoutside'); 
            ch.Position = [0.64 0.255 0.1 0.013];
            ch.Position = [0.82 0.4 0.1 0.013];
            % shift subplot up
            ch.Ticks = [ch.Limits(1) 0 ch.Limits(2)];
            ch.TickLabels = {'-1','0','1'};
            ch.Label.String = 'norm. Amplitude';
            ch.Label.Position(1:2) = [-2.2 1];
            ch.LineWidth = subpLineWidth;
            ch.FontWeight = 'bold';
            ch.FontSize = 8;
        end
        
        % axes labels, titles
        xlim([-50 150]);
        yticks(baselines(end:-1:1,1));
        ylim([-suboffset/2 (nsubs-0.5)*suboffset]);
        if icond == 1
            subjlabel = fliplr(num2cell(GroupStats.goodsubs));
            subjlabel = cellfun(@(x) ['S' char(string(x))], subjlabel,'UniformOutput',false);
            yticklabels(subjlabel);
            hh(icond).YAxis.FontWeight = 'bold';
            if idir == 2
                xlabel('Time (ms)');
            end
        else
            yticklabels([]);
        end

        if idir == 1
            header = sprintf(['%d degrees'],condlabel(icond));
            title(header,'FontSize',12);
        end
    end
end


% grand average EMREOs per condition
hh = nexttile(Bsubpos(1),Bsubsize);
HH = [HH, hh];
plot([tax_ear(1) tax_ear(end)],[0 0],'-','Color',[1 1 1]*0.8)
hold on
for icond = 1:ncond
    [~,ph(icond)] = fb_errorshade(tax_ear,GroupData{icond}(1,:),GroupData{icond}(2,:), ...
        ColorScheme.SaccCondColors(icond,:),'FaceAlpha',0.2,'EdgeColor','none');
end
% saccade onset time
plot([0 0], [ylimits], 'k--');
% text horizontal saccades
text(50,30,'Horizontal','FontSize',12,'FontWeight','bold','HorizontalAlignment','center','VerticalAlignment','middle');
% axes
xlim([-50 150]);
ylim(ylimits);
xlabel('Time (ms)');
ylabel('Amplitude (mV)');
% legend
lh = legend(ph,string(condlabel),'Location','northeast','NumColumns',2);
LH = [LH, lh];
legend boxoff


% plot vertical grand average EMREOs
hh = nexttile(Bsubpos(2),Bsubsize);
HH = [HH, hh];
plot([tax_vert(1) tax_vert(end)],[0 0],'-','Color',[1 1 1]*0.8)
hold on
for icond = 1:nvertcond
    [~,ph(icond)] = fb_errorshade(tax_vert,VerticalMeanSEM{icond}(1,:),VerticalMeanSEM{icond}(2,:), ...
        ColorScheme.VertCondColors(icond,:),'FaceAlpha',0.2,'EdgeColor','none');
end
% saccade onset time
plot([0 0], [ylimits], 'k--');
% text horizontal saccades
text(50,30,'Vertical','FontSize',12,'FontWeight','bold','HorizontalAlignment','center','VerticalAlignment','middle');
% axes
xlim([-50 150]);
ylim(ylimits);
% legend
lh = legend(ph,string(vert_label),'Location','northeast','NumColumns',2);
LH = [LH, lh];
legend boxoff




% ---- REGRESSION SUBPLOTS ----

% plot weights across participants
% first plot constant factor
hh = nexttile(Csubpos(1),Csubsize);
HH = [HH, hh];

% plot 
plot([tax_reg(1) tax_reg(end)],[0 0],'-','Color',[1 1 1]*0.8);
hold on
% plot true betas with SEM
[hp,p1] = fb_errorshade(tax_reg,mean(RegBeta(1,:,:),3),sem(RegBeta(1,:,:),3), ...
    ColorScheme.Weights(1,:),'EdgeColor','none');
% plot cluster bars

ymax = max(abs(mean(RegBeta(1,:,:),3)) + sem(RegBeta(1,:,:),3));
ipred = 1;
% get significant clusters, if any
for isign = 1:size(stat_clusters,1)
    if ~isempty(stat_clusters{isign,ipred})
        % report cluster statistics
        fprintf('cluster statistics: %s \n',PredTitles{ipred});
        fprintf(['pvalue:' repmat('%.3f ',size(stat_clusters{isign,ipred}.p)) '\n'],stat_clusters{isign,ipred}.p);
        fprintf(['stat:' repmat('%.3f ',size(stat_clusters{isign,ipred}.stat)) '\n'],stat_clusters{isign,ipred}.stat);
        fprintf(['peak effect:' repmat('%.3f ',size(stat_clusters{isign,ipred}.peakCohensD)) '\n'],stat_clusters{isign,ipred}.peakCohensD);
        fprintf('\n');
        % then for each sign. cluster plot bar and print temporal
        % boundaries
        for iclus = 1:length(stat_clusters{isign,ipred}.p)
            maskSig = find(stat_clusters{isign,ipred}.mask==iclus);
            if any(tax_reg(maskSig) < 0)
                continue
            end
            pvalclus = stat_clusters{isign,ipred}.p(iclus);
            maxsum = stat_clusters{isign,ipred}.stat(iclus);
            cluseffect  =stat_clusters{isign,ipred}.peakCohensD(iclus);
            if pvalclus < p_critval
                plot(tax_reg(maskSig),ones(size(maskSig)) .* ymax .* (1+(ipred-1)*0.3), 'Color',ColorScheme.Weights(ipred,:),'LineWidth',5.0);
                temp_bound = stat_clusters{isign,ipred}.boundaries(:,iclus);
                fprintf('cluster %d: %.1f to %.1f ms, p=%.3f maxsum=%.2f cohensDpeak=%.2f \n', ...
                        iclus, temp_bound, pvalclus, maxsum, cluseffect);
            end
        end
        fprintf('\n\n');
    end
end

% axes
box off
xlim([tax_reg(1) tax_reg(end)]);
ylimits = round([-ymax ymax].*1.2);
plot([0 0], ylimits,'k--');
ylim(ylimits);
xlabel('Time (ms)');
title(PredTitles{1});



% plot predictor weights
hh = nexttile(Csubpos(2),Csubsize);
HH = [HH, hh];
plot([tax_reg(1) tax_reg(end)],[0 0],'-','Color',[1 1 1]*0.8);
hold on
% go through predictors
for ipred = 2:npred
    % plot true betas with SEM
    [~,ph(ipred)] = fb_errorshade(tax_reg,mean(RegBeta(ipred,:,:),3),sem(RegBeta(ipred,:,:),3), ...
        ColorScheme.Weights(ipred,:),'EdgeColor','none');
end

% plot the cluster bars
ymax = max(flatten(abs(mean(RegBeta(2:end,:,:),3)) + sem(RegBeta(2:end,:,:),3)));
for ipred = 2:npred
    % get significant clusters, if any
    for isign = 1:size(stat_clusters,1)
        if ~isempty(stat_clusters{isign,ipred})
            % report cluster statistics
            fprintf('cluster statistics: %s \n',PredTitles{ipred});
            fprintf(['pvalue:' repmat('%.3f ',size(stat_clusters{isign,ipred}.p)) '\n'],stat_clusters{isign,ipred}.p);
            fprintf(['stat:' repmat('%.3f ',size(stat_clusters{isign,ipred}.stat)) '\n'],stat_clusters{isign,ipred}.stat);
            fprintf(['peak effect:' repmat('%.3f ',size(stat_clusters{isign,ipred}.peakCohensD)) '\n'],stat_clusters{isign,ipred}.peakCohensD);
            fprintf('\n');
            % then for each sign. cluster plot bar and print temporal
            % boundaries
            for iclus = 1:length(stat_clusters{isign,ipred}.p)
                pvalclus = stat_clusters{isign,ipred}.p(iclus);
                maxsum = stat_clusters{isign,ipred}.stat(iclus);
                cluseffect = stat_clusters{isign,ipred}.peakCohensD(iclus);
                maskSig = find(stat_clusters{isign,ipred}.mask==iclus);
                if pvalclus < p_critval
                    plot(tax_reg(maskSig),ones(size(maskSig)) .* ymax + (1+(ipred-1)*0.75), 'Color',ColorScheme.Weights(ipred,:),'LineWidth',5.0);
                    temp_bound = stat_clusters{isign,ipred}.boundaries(:,iclus);
                    fprintf('cluster %d: %.1f to %.1f ms, p=%.3f maxsum=%.2f cohensDpeak=%.2f \n', ...
                        iclus, temp_bound, pvalclus, maxsum, cluseffect);
                end
            end
            fprintf('\n\n');
        end
    end
end
clear ph
% axes
box off
xlim([tax_reg(1) tax_reg(end)]);
ylimits = [-ymax ymax].*2.5;
plot([0 0], ylimits,'k--');
hh.YAxis.Limits = ylimits;
title('Eye position predictors');



% plot model R square
hh = nexttile(Csubpos(3),Csubsize);
HH = [HH, hh];
hold on
% full model R square
cvec = ColorScheme.Weights;
[~,ph_full] = fb_errorshade(tax_reg,sq(mean(R_square(1,:,:),2))',sq(sem(R_square(1,:,:),2))','k','EdgeColor','none');
for imod = 1:size(unique_R_square,1)-1
    uniqueR2 = sq(unique_R_square(imod,:,:));
    [~,ph(imod)] = fb_errorshade(tax_reg,mean(uniqueR2),sem(uniqueR2),cvec(imod+1,:),'EdgeColor','none');
end
ylimits = hh.YAxis.Limits;
plot([0 0], ylimits,'k--');
hh.YAxis.Limits = ylimits;
xlim([tax_reg(1) tax_reg(end)]);
box off
title('Unique variance (R^{2})');



% zoomed in variance plot for last two predictors
hh = nexttile(Dsubpos,Dsubsize);
HH = [HH, hh];
hold on
% full model R square
cvec = ColorScheme.Weights;
for imod = size(unique_R_square,1)-1:size(unique_R_square,1)
    uniqueR2 = sq(unique_R_square(imod-1,:,:));
    fb_errorshade(tax_reg,mean(uniqueR2),sem(uniqueR2),cvec(imod,:),'EdgeColor','none');
end
ylimits = [0 0.005]; % hh.YAxis.Limits;
plot([0 0], ylimits,'k--');
hh.YAxis.Limits = ylimits;
xlim([tax_reg(1) tax_reg(end)]);
box off


% shift legends in grand average plots
for ii = 1:length(LH)
    lh = LH(ii);
    lh.FontSize = 8;
    lh.Position(1) = lh.Position(1) + 0.005;
    lh.Position(2) = lh.Position(2) - 0.004;
end

% plot legend from on top, and reset position
ylim5 = HH(9).YAxis.Limits;
ylim6 = HH(10).YAxis.Limits;
lh = legend(HH(end),[ph_full p1 ph],{'Full Model',PredTitleAlt{2:end}},...
    'Location','south','NumColumns',2);
% position legend as well
lh.Position = lh.Position + [0 -0.15 0 0];
lh.FontSize = 10;
legend boxoff
% reset limits in grand average plots
HH(9).YAxis.Limits = ylim5;
HH(10).YAxis.Limits = ylim6;



% set axes specifics for all
for ii = 1:length(HH)
    HH(ii).XAxis.FontWeight = 'bold';
    HH(ii).YAxis.FontWeight = 'bold';
    HH(ii).XAxis.FontSize = 10;
    HH(ii).YAxis.FontSize = 10;
    HH(ii).XAxis.LineWidth = subpLineWidth;
    HH(ii).YAxis.LineWidth = subpLineWidth;
end

% add horizontal and vertical label
phout = paneltitle(HH(1:4),'Horizontal');
phout = paneltitle(HH(5:8),'Vertical');


% add panel label A
panellabels = {'A','B','C','D','E','F'};
iter = 1;
for ipanel = [1 5 9 11 12 13]
    phout = panellabel(HH(ipanel),panellabels{iter});
    phout.Position(1) = phout.Position(1) + phrshift;
    phout.Position(2) = phout.Position(2) + 0.085;
    if ipanel > 9
        phout.Position(2) = phout.Position(2) - 0.04;
    end
    iter = iter + 1;
end


% insert inlay for grand averages
HHind = HH([9 10]);
InlayColors = {ColorScheme.SaccCondColors, ColorScheme.VertCondColors};
InlayLimits = {[270 330],[28 42]};
InlayTicks = {[280, 300, 320],[30, 35, 40]};
NtrialData = {NtrialsH,NtrialsV};

for iinlay = 1:2
    % inlay with number of trials per condition across participants
    thisax = HHind(iinlay);
    inlay_hh = inlay_subplot(thisax,inlay_pos);
    boxplot(NtrialData{iinlay},'BoxStyle','filled','Symbol','');
    % get boxes and whisker handles
    bbh = findobj(inlay_hh,'tag','Box');
    bwh = findobj(inlay_hh,'tag','Whisker');
    bmh = findobj(inlay_hh,'tag','Median');
    % flip ud because of weird MATLAB, last obj normally first in list
    bbh = flipud(bbh);
    bwh = flipud(bwh);
    bmh = flipud(bmh);
    % change color
    ColorCode = InlayColors{iinlay};
    for ii = 1:length(bbh)
        bbh(ii).Color = ColorCode(ii,:);
        bwh(ii).Color = ColorCode(ii,:);
        bmh(ii).XData = ii + [-0.15 0.15]; % more narrow median
        bmh(ii).Color = 'w';
    end
    ylim(InlayLimits{iinlay});
    yticks(InlayTicks{iinlay});
    ylabel('N trials','FontSize',8,'FontWeight','bold');
    xticklabels([]);
    xticks([]);
    inlay_hh.XAxis.Visible = 'off';
    inlay_hh.YAxis.FontWeight = 'bold';
    inlay_hh.YAxis.FontSize = 8;
    box off
end




% save to file
fig = gcf;
fname = sprintf([finalfigurepath 'Figure_%d.png'],FigNumber);
exportgraphics(fig,fname,'Resolution',300);

pname = sprintf([finalfigurepath 'Figure_%d.pdf'],FigNumber);
exportgraphics(fig,pname,'Resolution',300);

tname = sprintf([finalfigurepath 'Figure_%d.eps'],FigNumber);
exportgraphics(fig,tname,'Resolution',300);
pause(0.5);


clear HH HC hh ph
close all



end
