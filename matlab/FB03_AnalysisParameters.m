function out = FB03_AnalysisParameters
%
% set global predefined parameters for the analysis
%

% screen details
out.ScreenSize = [1920 1080];
out.centerpoint = [960 540]; % screen center point
out.ppd = 23;

% desired sample rate
out.ResampleFs = 2000;

% should ear data be zscored
out.zscore = false; 


% define trial exclusion criteria
out.RT_limit = 0.5; % max reaction time in seconds
out.Ear_SD = 2.5; % max SD variance per trial in ear data
out.Ear_SD2 = 3.0; % max SD variance in all trial in ear data
out.Fix_SD = 3.0; % max variance deviation from initial fixation point
out.Pos_Max = 16; % max position in degrees during saccades
% we also exclude trials based on bad sequence and NaN values


% temporal window in which we analyze EMREOs
out.EMREOwindow = [0 75]; % in milliseconds
% window roughly around the first peak
out.FirstPeakWin = [0 25];

% frequency window in which we analyze EMREOs
out.EMREOfreqwindow = [30 45];

% statistics parameters
out.Sign_Alpha = 0.05; 


end
