function Analyze_Click_EM(S)
% this script loads and analyzes the TEOAEs recorded prior to
% experimentation. Paradigm and analysis are according to Probst et al.
% 1991, Probst et al. 1986, Bennett et al. 2010
%
% first, trials are high pass filtered at 300 Hz and aligned based on the
% click tone peaks
% then time-frequeny I/O response is determined in a window 9-20 ms after
% click tone in 0.8 - 3.5 kHz range. The spectrum is integrated to yield
% an average spectral density response measure.
%

close all

% get ExpList
USECOMP = 3
ExpList
defdirs;


Subname = SubList(S).name;
filename = sprintf([datapath_ear 'FB03_S%02d' '_TEOAE.bdf'],S);

% set parameters from global parameters
globalP = FB03_AnalysisParameters;


% structure in which we save summary metrics from individual analyses
sumname = sprintf([datapath_res 'SumMetrics_FB03_S%02d_TEOAE.mat'],S);
SumMetrics = [];
SumMetrics.date = date;


resamplefs = globalP.ResampleFs;
% check highpass filter frequency. Probst 1991 speaks of a cutoff freq of
% about 300-500 Hz
hpfreq = 300;

% save data to struct
SumMetrics.samplerate = resamplefs;


%%
%----------------------------------------------------------
% params for epoching
trialdef.prestim = -0.1; % Trial definition
trialdef.poststim = 0.2; % sec
Trig_range = [1:100]; % triggers to look for (trials). 100 click sounds


%------------------------------------------------------
% define trials
fprintf('\n Processing %s \n',filename);
Do_Power = false;
cfg = [];
cfg.dataset     = filename;
event = ft_read_event(cfg.dataset);
hdr   = ft_read_header(cfg.dataset);

eventS = ft_filter_event(event, 'type','STATUS');

% if the spectrum trial (#101) was recorded, put into another trialdef
if eventS(end).value == 101 && eventS(end).sample > 500
    % separate power trial from other click trials
    eventPow = eventS(end);
    eventS(end) = [];
    
    % define power trial
    begsample = eventPow(1).sample + trialdef.prestim * hdr.Fs;
    endsample = eventPow(1).sample + 15.0 * hdr.Fs;
    offset    = trialdef.prestim * hdr.Fs;
    val = eventPow(1).value;
    trlPow = round([begsample endsample offset val]);
    
    % check if we actually have enough samples to do power analysis
    if (hdr.nSamples - begsample) / hdr.Fs > 15
        Do_Power = true;
    end
end

val = []; t_onset = [];
c = 1;
for k=1:length(eventS)
    if sum(eventS(k).value==Trig_range) && (eventS(k).sample>500)
        val(c) = eventS(k).value;
        t_onset(c) = eventS(k).sample;
        c = c + 1;
    end
end

%-------------------------------------------------------------------------------
% create fieldtrip trial structure - make sure to onset the onset
trl = [];
for t = 1:length(t_onset)
    ts = t_onset(t); % use precise stim onset
    begsample     = ts + trialdef.prestim * hdr.Fs;
    endsample     = ts + trialdef.poststim * hdr.Fs - 1;
    offset        = trialdef.prestim * hdr.Fs;
    trl(t,:) = round([begsample endsample offset val(t)]);
end
Good_trls = find(trl(:,2) < hdr.nSamples );
trl = trl(Good_trls,:);

%-------------------------------------------------------------------------------
% data processing and loading
cfg = [];
cfg.dataset     = filename;
cfg.trl = trl;
cfg.demean     = 'no';
cfg.detrend = 'no';
cfg.polyremoval   = 'no';

cfg.lpfilter   = 'no' ;  % apply lowpass filter
cfg.lpfreq     = 0.5;
cfg.lpfiltord = 4;
cfg.lpfiltdir = 'twopass' ;  % apply highpass filter

cfg.hpfilter   = 'yes';  
cfg.hpfreq     = hpfreq;
cfg.hpfiltord = 4;
cfg.hpfiltdir = 'twopass';    % apply highpass filter

cfg.reref         = 'no';                      % referencing
cfg.continuous = 'yes';
data = ft_preprocessing(cfg);

% define background spectrum trial and preprocess w/o filtering
if Do_Power
    cfg.trl = trlPow;
    cfg.lpfilter = 'no';
    cfg.hpfilter = 'no';
    dataPow = ft_preprocessing(cfg);
end

%-------------------------------------------------------------------------------
% select relevant channel and convert to matrix
cfg = [];
cfg.channel = 'Erg1';
data = ft_selectdata(cfg,data);

if Do_Power
    dataPow = ft_selectdata(cfg,dataPow);
end

%%
% start analysis here

% for a more precise measure we can find the maximum peak and align all
% trial along this.

ntrial = length(data.trial); % fixed in recording script
alltrial = cat(1,data.trial{:});


% ---- TEOAE analysis ----
% index every 4th trial where polarity is inverted
invtrial = [4:4:ntrial];
trialpolarity = ones(ntrial,1);
trialpolarity(invtrial) = -1;

% exclude bad channels ---------
% find trials with SD exceeding three times whole data SD
max_amp = std(alltrial(:),'omitnan') * 3.0;
trial_sd = std(alltrial,[],2);
bad_sd = (trial_sd > max_amp);

% z score all sample points
trial_sample_sd = (alltrial - mean(alltrial(:)))./std(alltrial(:));
% find trials with more than 50 samples with SD > 3
bad_sd2 = (sum(abs(trial_sample_sd) > 3,2)) > 50; 

% WE DO NOT EXCLUDE TRIALS FOR NOW!
% select only good trials
good_trials = true(1,ntrial);
good_trials(bad_sd) = 0; % outlier trials
good_trials(bad_sd2) = 0; % trials with outlier samples
%alltrial = alltrial(good_trials,:);
%trialpolarity = trialpolarity(good_trials);
%ntrials = sum(good_trials);
ntrials = ntrial;

% go through all trials and find max peaks (or troughs every 4th trial)
frame = [-0.025, 0.05];
tx = [frame(1):1/data.fsample:frame(2)];
win = round(tx * data.fsample);
trialdata = zeros(ntrials,size(tx,2));

for itrial = 1:ntrials
    [~,idx] = max(alltrial(itrial,:));
    if trialpolarity(itrial) == -1
        [~,idx] = min(alltrial(itrial,:));
    end
    trialdata(itrial,:) = alltrial(itrial,win+idx);
    % create control data from prestim time, implicitely define new zero
    controldata(itrial,:) = alltrial(itrial,1:round(diff(frame) * data.fsample));
end


% take predefined time window after trigger, analyze TEOAE power
io_twindow = [9 30] ./ 1000; % in s
io_fwindow = [800 3500]; % in Hertz
io_tx = [io_twindow(1):1/data.fsample:io_twindow(2)];
io_tx = io_tx(1:end-1);
io_win = (io_twindow(1) < tx) & (io_twindow(2) > tx);

% data structure
io_data = data;
io_data.time = repmat({io_tx},[1,ntrials]);
io_data.trial = mat2cell(trialdata(:,io_win),ones(1,ntrials),sum(io_win))';
io_data = removefields(io_data,{'sampleinfo','trialinfo'});

% set control spectrum data structure
control_data = io_data;
control_data.trial = mat2cell(controldata(:,io_win),ones(1,ntrials),sum(io_win))';

% freq analysis parameter
cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'pow';
cfg.taper = 'hanning';
cfg.foi = [io_fwindow(1):50:io_fwindow(2)];
cfg.toi = 0;

% freq analysis
io_freq = ft_freqanalysis(cfg,io_data);
% integrate spectrum
spec_density = trapz(io_freq.powspctrm);

% control
control_freq = ft_freqanalysis(cfg,control_data);
% integrate spectrum
control_density = trapz(control_freq.powspctrm);


% ---- spectrum analysis ----
% this analysis only pertains to the background noise trial
if Do_Power
    cfg = [];
    cfg.method = 'mtmfft';
    cfg.output = 'pow';
    cfg.taper = 'hanning';
    cfg.foilim = [2 128];
    cfg.toi = 0;

    freq = ft_freqanalysis(cfg,dataPow);
end


%% plot data

% plot data
figure('Position',[305 549 560 420]);
subplot(121);
plot(tx,mean(trialdata,1));
xlim(frame);
header = sprintf('average TEOAE (n=%d trials)',ntrial);
title(header);


% without further alignment
% mark that the clicks are delayed, therefore they are alignet at about 40
% ms
dtime = data.time{1};
winraw = find([dtime > frame(1)+0.05 & dtime < frame(2)+0.05]);
xtime = dtime(winraw);

subplot(122);
plot(xtime,mean(alltrial(:,winraw)));
header = sprintf('average TEOAE (no extra alignment)');
title(header);

ckfiguretitle('TEOAE');
fname = sprintf([figurepath Subname '_TEOAE.png']);
print('-dpng',fname);


% plot power spectrum
if Do_Power
    figure('Position',[303 60 1070 392]);
    subplot(121);
    plot(freq.freq,freq.powspctrm);
    xlim([0,45]);
    xlabel('Frequency (Hz)');
    ylabel('Power');
    subplot(122);
    plot(freq.freq,freq.powspctrm);
    ckfiguretitle('EMREO Ear data spectrum - unfiltered');
end


% collect data 
SumMetrics.data = mean(alltrial(:,winraw));
SumMetrics.time = xtime;
SumMetrics.Ngood_trials = ntrials;

% spectral analysis
SumMetrics.io_freq = io_freq.freq;
SumMetrics.io_spec_density = spec_density;
SumMetrics.control_freq = control_freq.freq;
SumMetrics.control_density = control_density;


%% save summary to file
save(sumname,'-struct','SumMetrics');


end
