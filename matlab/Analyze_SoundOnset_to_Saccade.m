%
% load eyetracking data and log files, compare saccade data from both types
%
% ONLY DATA FROM EyeMov_1.m !!!
%
% compare timings and see whether sounds are indeed played before, during
% or after the saccade
%
% this first of all serves as a quality check that estimating the saccade
% parameters allows for a reasonable inference about saccades in the next
% blocks
%


close all
clearvars

% get ExpList
USECOMP = 3;
ExpList
defdirs;


% get example data from ones subject 
S = 1;
Experiment = 1;

ppd = 23;    % pixels per degree

%% ----------------------------------------------------------------------------
% parameters for filtering and data inclusion - We expect EMRO around 50 Hz
ARG_ear.hpfilter   = 'no';  
ARG_ear.hpfreq     = 10;
ARG_ear.lpfilter   = 'no';  
ARG_ear.lpfreq     = 50;

ARG_eog.hpfilter   = 'no';  
ARG_eog.hpfreq     = 20;
ARG_eog.lpfilter   = 'yes';  
ARG_eog.lpfreq     = 80;

ARG_event.exp = Experiment;

ARG_eye.Window = [-0.1,0.3]; % seconds
ARG_eye.Align = 'onset';
ARG_eye.exp = Experiment;


Midpoint = [960,540]; % screen center

%% --------------------------------------------------------------------------
% Load Event data
[EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath_log,ARG_event);


%%
% Load Eye Tracking data
% which experiment should be loaded
ExpPart = ARG_eye.exp;
if ~ischar(ExpPart)
    ExpPart = char(string(ExpPart));
end

filedir = dir([datapath_eye, '*.asc']);

ETdata = [];
for isub = S
    % get subject data
    Sname = split(SubList{S}.name,'_');
    Sname = ['FB' Sname{2} ExpPart];
    subdir = contains({filedir.name},Sname);
    subdir = filedir(subdir);
    for l = 1:length(subdir)
        
        fname = [datapath_eye subdir(l).name]

        [ET,Eye,Vel,Acc] = eegck_readEDF_v4(fname);
        [Sacc{l},TimingLog{l}] = find_saccades(ARG_eye,ET,Eye,Vel,Acc);
        
    end
    % concatenate all blocks
    Sacc = ft_appenddata([],Sacc{:});
    TimingLog = cat(1,TimingLog{:});
end


%% ---------------------------------------------------------------
% now we extract the Ear data locked to saccade onset / offset
% convert to matrix

% first convert condition labels into conditional direction labels and
% append log 
condtransform = [3 4 1 2];
condition = EventLog{S}.EventLog(:,2);
condition = condition-1; % to sum later
condition(condition>4) = condition(condition>4) - 4; % both angles are same conditions
prevcond = [0; condition(1:end-1)];
prevcond(prevcond>0) = condtransform(prevcond(prevcond>0)); 
condition = sum([condition prevcond],2) + 1;

Analysis_log = EventLog{S};
Analysis_log.TimingLog = TimingLog;
Analysis_log.Condition = condition;



%%
% analyze log data
ntrial = length(EventLog{S}.EventLog);

% first, get the good saccades, defined by distance
min_distance = 2; % below 3 degrees
max_distance = 16; % above 16 degrees
StartPos = EventLog{S}.SaccPos(:,[1,2]);
EndPos =  EventLog{S}.SaccPos(:,[3,4]);

% augment gaze vectors if any of the positions are not valid
validfix = [StartPos(:,1) == 0];
StartPos(validfix,:) = repmat([960, 540],sum(validfix),1);
validtag = [EndPos(:,1) == 0];
EndPos(validtag,:) = repmat([960, 540],sum(validtag),1);

% get saccade vectors
gazeVectors = (EndPos - StartPos)';
gazeVmag = sqrt(sum(gazeVectors.^2)) ./ ppd;


% compute latency and duration for saccades
Log_Latency = diff(EventLog{S}.SaccTime(:,[1,2]),[],2);
Log_Duration = diff(EventLog{S}.SaccTime(:,[2,4]),[],2);

% reject faulty trials
bad_dist = (gazeVmag < min_distance | gazeVmag > max_distance);
bad_lat = (Log_Latency > 0.4 | Log_Latency < 0);
bad_dur = (Log_Duration > 0.15 | Log_Duration < 0);
goodSacc = true(ntrial,1);
goodSacc(bad_dist) = false;
goodSacc(bad_lat) = false;
goodSacc(bad_dur) = false;


% clean out falty trials that are too short
Log_Latency = Log_Latency(goodSacc); 
Log_Duration = Log_Duration(goodSacc);

% plot time distribution
figure
subplot(211);
histogram(Log_Latency,100);
subplot(212);
histogram(Log_Duration,100);

title('log data latency and duration');

fprintf('\n\n');
fprintf(' - all saccades Log file data - \n');
fprintf('median latency:    %.3f ms \n',1000*median(Log_Latency));
fprintf('median duration:   %.3f ms \n',1000*median(Log_Duration));


% plot position and saccade vector
figure
plot(StartPos(goodSacc,1),StartPos(goodSacc,2),'r+');
hold on
plot(EndPos(goodSacc,1),EndPos(goodSacc,2),'bo');
plot([StartPos(goodSacc,1) EndPos(goodSacc,1)]', [StartPos(goodSacc,2) EndPos(goodSacc,2)]','k-');



%%
% now try something similar for the saccades

% define Sacc_latency and Sacc_duration based on ET data
Sacc_latency = TimingLog(:,3);
Sacc_duration = diff(TimingLog(:,[1,2]),[],2);

figure
subplot(211)
histogram(Sacc_latency,ntrial);

subplot(212)
histogram(Sacc_duration,ntrial);

title('edf data latency and duration');

fprintf('\n\n');
fprintf(' - all saccades EDF data - \n');
fprintf('median latency:    %.3f ms \n',median(Sacc_latency,1,'omitnan'));
fprintf('median duration:   %.3f ms \n',median(Sacc_duration,1,'omitnan'));

fprintf('\n\n');
fprintf(' - difference between Log and EDF data - \n');
fprintf('latency:    %.3f ms \n',1000*median(Log_Latency,1,'omitnan') - median(Sacc_latency,1,'omitnan'));
fprintf('duration:   %.3f ms \n',1000*median(Log_Duration,1,'omitnan') - median(Sacc_duration,1,'omitnan'));



% now check how the saccades from both files correspond
Lat_diff = 1000*Log_Latency - Sacc_latency(goodSacc)';
Dur_diff = 1000*Log_Duration - Sacc_duration(goodSacc)';

figure
subplot(211);
histogram(Lat_diff,200);
%xlim([0,30]);
subplot(212);
histogram(Dur_diff,200);
%xlim([0,30]);

title('trial-wise difference between Log and edf data');




%%
% check if saccade onset if at equal latencies from trial onset with both
% measurement techniques (log files and ETdata)
ETTrialOn = TimingLog(:,3);
EventTrialOn = diff(EventLog{S}.SaccTime(:,[3,4]),[],2).*1000;

% now get latencies and subtract from respective trial onset

%ETTrialOn = ETTrialOn - Sacc_latency';
%EventTrialOn = EventTrialOn - diff(EventLog{2}(:,[1,2]),[],2) .* 1000;


% plot histogram of the trial latencies 
figure
subplot(311)
histogram(ETTrialOn,200);
xlim([0,500]);
title('ET data trial latencies');

subplot(312)
histogram(EventTrialOn,200);
xlim([0,500]);
title('Log data trial latencies');

subplot(313)
histogram(EventTrialOn(goodSacc),50);
xlim([0,500]);
title('Log data trial latencies of good trial only');


% just for safety
t2 = diff(EventLog{S}.SaccTime(:,[3,4]),[],2).*1000;
%t_fix = tmp.SaccadeTimings(:,1);
% -> extremely small difference

%%
% check change in position from ETdata around ETdata saccade onset

% take a trial which is a good one in Log file
itrial = find(goodSacc);
itrial = itrial(2);

% time frame to look at in samples
tframe = [-0,30]; 
tx = [tframe(1):1:tframe(2)];
n = length(tx);
% interpolate color with respect to time
R = linspace(1,0,n)';
G = linspace(0,1,n)';
B = linspace(1,0,n)';
cmap = [R G B];

% from saccade detection
sacc_start = EventLog{S}.SaccPos(itrial,[1,2]);
log_start = EventLog{S}.SaccPos(itrial,[1,2]) - Midpoint;

% from ETdata pos
% get saccade onset time
time0 = TimingLog(itrial,1);

% saccade positions
pos0 = Sacc.trial{itrial}([2,3], time0 + tx)' - Midpoint;

% plot the position along a saccade
figure
hold on
iter = 1;
for iter = 1:length(tx)
    % plot a line between one point and the next
    if iter < length(tx)
        pos = [pos0(iter,:); pos0(iter+1,:)];
        plot(pos(:,1),pos(:,2),'-','Color',cmap(iter,:));
    end
    plot(pos0(iter,1),pos0(iter,2),'*','Color',cmap(iter,:));
    drawnow
    pause(0.2);
end

plot(sacc_start(1),sacc_start(2),'o','Color','b');
plot(log_start(1),log_start(2),'o','Color','r');

axis([-700 700 -700 700])


