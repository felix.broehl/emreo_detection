function [Saccades,TimingLog] = load_eyedata_blocks(S,SubList,datapath_eye,ARG_eye)
% [Saccades,TimingLog,Velocity,Acceleration] = load_eyedata_blocks(S,SubList,datapath,ARG_eye)
%
% load all blocks of eardata for any participant S
% this function appends all loaded data blocks, so make sure you only load
% blocks of the same experiment with each function call!
%

% Load Eye Tracking data
% which experiment should be loaded
ExpPart = ARG_eye.exp;
if ~ischar(ExpPart)
    ExpPart = char(string(ExpPart));
end

filedir = dir([datapath_eye, '*.asc']);
nblocks = 4; % we expect 4 blocks


% get subject data
Sname = split(SubList(S).name,'_');
Sname = ['FB' Sname{2} ExpPart];
subdir = contains({filedir.name},Sname);
subdir = filedir(subdir);
% remember missing data
missingflag = false(nblocks,1);
for l = 1:nblocks

    fname = sprintf([datapath_eye Sname 'B%d.asc'],l)
    try
        % this function can resample 
        %[ET,Eye] = eegck_readEDF_fb(fname,ARG_eye.ppd,ARG_eye.resamplefs);
        
        
        % new function, can not resample
        [ET,Eye] = ckeye_readEDF_v5(fname,ARG_eye.ppd);
        
        % resample eye data
        if ET.fsample ~= ARG_eye.resamplefs
            
            % correct TrialInfo for new sample rate
            Eye.TrialInfo(:,[7,8]) = Eye.TrialInfo(:,[7,8]) * (ARG_eye.resamplefs / ET.fsample);
            Eye.sacc(:,[10,11]) = Eye.sacc(:,[10,11]) * (ARG_eye.resamplefs / ET.fsample);
            
            % resample ET data
            cfg_rs = [];
            cfg_rs.resamplefs = ARG_eye.resamplefs;
            ET = ft_resampledata(cfg_rs, ET);
            
        end
        
        % find biggest saccade and determine onset and offset
        [Sacc{l},Time{l}] = find_saccades(ARG_eye,ET,Eye);
        
        
    catch
        missingflag(l) = true;
        warning('one part of eye tracking data is missing!');
        continue
    end

end

% check if data is incomplete
if sum(missingflag) == nblocks
    error('Could not load any data for this experiment');
end

% if some files are missing substitute them with empty ft structure
emptystruc = Sacc{find(~missingflag,1)};
emptystruc.trial = repmat({nan(size(emptystruc.trial{1}))}, [1,length(emptystruc.trial)]);
Sacc(missingflag) = repmat({emptystruc}, [1,sum(missingflag)]);
% same for TimingLog
emptytime = nan(size(Time{find(~missingflag,1)}));
Time(missingflag) = repmat({emptytime}, [1,sum(missingflag)]);

% we deal with missing trials within the data later


% concatenate all blocks
Saccades = ft_appenddata([],Sacc{:});
TimingLog = cat(1,Time{:});


end
