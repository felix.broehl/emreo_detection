function Ear_data = get_eardata(FILENAME,ARG)


%----------------------------------------------------------
% params for epoching
trialdef.prestim = ARG.prestim; % Trial definition
trialdef.poststim = ARG.poststim; % sec
Trig_range = [1:ARG.Ntrials]; % triggers to look for (trials). Paradigm specific


%------------------------------------------------------
% define trials
fprintf('\n Processing %s \n',FILENAME);
cfg = [];
cfg.dataset     = FILENAME;
event = ft_read_event(cfg.dataset);
hdr   = ft_read_header(cfg.dataset);

eventS = ft_filter_event(event, 'type','STATUS');
val = []; t_onset = []; t_onset_sound = [];
c=1;
for k=1:length(eventS)
    if sum(eventS(k).value==Trig_range) && (eventS(k).sample>500)
        % skip erroneous trials with duplicate value (found in one participant)
        if sum(eventS(k).value == val)
            continue
        end
        val(c) = eventS(k).value;
        t_onset(c) = eventS(k).sample;
        c = c + 1;
    end
end

%-------------------------------------------------------------------------------
% create fieldtrip trial structure - make sure to onset the onset
trl =[];
for t=1:length(t_onset)
    ts =t_onset(t); % use precise stim onset
    begsample     = ts + trialdef.prestim*hdr.Fs;
    endsample     = ts + trialdef.poststim*hdr.Fs - 1;
    offset        = trialdef.prestim*hdr.Fs;
    trl(t,:) = round([begsample endsample offset val(t)]);
end
Good_trls = find(trl(:,2)<hdr.nSamples );
trl = trl(Good_trls,:);

%-------------------------------------------------------------------------------
% data processing and loading
cfg = [];
cfg.dataset     = FILENAME;
cfg.trl = trl;
cfg.demean     = 'no';
cfg.detrend = 'no';
cfg.polyremoval   = 'no';
cfg.lpfilter   = ARG.lpfilter ;                             % apply lowpass filter
cfg.lpfiltdir = 'twopass' ;   % apply highpass filter
cfg.lpfreq     = ARG.lpfreq;

cfg.hpfilter   = ARG.hpfilter;  
cfg.hpfreq     = ARG.hpfreq;
cfg.hpfiltord = 6;
cfg.hpfiltdir = 'twopass';    % apply highpass filter
cfg.reref         = 'no';                      % referencing
cfg.continuous = 'yes';
data = ft_preprocessing(cfg);


%-------------------------------------------------------------------------------
% select relevant channel and convert to matrix
cfg=[];
cfg.channel = 'Erg1';
Ear_data = ft_selectdata(cfg,data);


