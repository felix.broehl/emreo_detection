function [Sacc,TimingLog] = find_saccades(ARG,data_eye,Eye)
%
% Sacc = find_saccades(ARG,data_eye,Eye)
%
% within each trial, find biggest saccade event
%
% find_saccades can return saccade onset and offsets that are not inside of
% the window specified with ARG. These have to be removed afterwards if any
% of those exist or the window has to be wide enough so there are
% practically none outside.
%
% Parameter:
%       ARG
%       data_eye
%       Eye
%       Vel
%       Acc
%
% Returns
%
%   return eye data aligned to onset
%   time points of onset and offset for each trial
%   
%   in all ouputs the data is align accrosing to the time window to the
%   onset of the saccade in each trial
%
%   TimingLog columns:
%   saccade onset
%   saccade offset
%   saccade onset  distance to DISPLAY_ON command in matlab script
%   saccade offset distance to DISPLAY_ON command in matlab script
%


% default criteria for saccades window
ARG = checkarg(ARG,'Window',[-0.5,1.0]);

% - not implemented yet! - 
ARG = checkarg(ARG,'Align','onset');


ntrl = size(Eye.TrialInfo,1);
ndims = size(data_eye.label,2);
eventwin = ARG.Window .* data_eye.fsample;

% output variables
Sacc = data_eye;
Sacc.trial = cell(1,ntrl); 
Sacc.time = repmat({[eventwin(1):eventwin(2)-1] ./ data_eye.fsample},[1,ntrl]);
TimingLog = nan(ntrl,4); % start and end time with respect to Sacc

X = data_eye.trial{1};
for t = 1:ntrl
    % allocate empty trial
    Sacc.trial{t} = nan(ndims,diff(eventwin));
    % window to extract events from paradigm
    on  = Eye.TrialInfo(t,7); % DISPLAY ON 
    off = Eye.TrialInfo(t,8); % DISPLAY OFF 
    % find saccades in the Eye structure
    jj = find( Eye.sacc(:,10)>on & Eye.sacc(:,11)<off );
    
    % no saccade detected - ignore
    if isempty(jj)
        continue;
    end
    
    % find largest saccade by distance
    pos0 = Eye.sacc(jj,4:5);
    pos1 = Eye.sacc(jj,6:7);
    pos = pos1 - pos0;
    amp = sqrt(sum(pos.^2,2));
    [~,o] = max(amp);
    jj = jj(o);
    
    % get time vector, aligned to onset 
    j = [Eye.sacc(jj,10)+eventwin(1) Eye.sacc(jj,10)+eventwin(2)-1];
    Sacc.trial{t} = X(:,[j(1):j(2)]);
    
    % find start, end in respect to trial def, and onset in respect to
    % DISPLAY ON in experiment
    % duration is the difference between first two columns
    duration = diff(Eye.sacc(jj,[10,11]));
    windist = abs(eventwin(1)) + [0 duration];
    displayOn_to_onset = Eye.sacc(jj,10) - on;
    displayOn_to_offset = displayOn_to_onset + duration;
    TimingLog(t,:) = [windist displayOn_to_onset displayOn_to_offset];
    
end

end
