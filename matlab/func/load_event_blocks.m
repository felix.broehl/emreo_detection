function [EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath,ARG_event)
%
% [EventLog,ARG_Matlab] = load_event_blocks(S,SubList,datapath)
%
% returns {eventlog, SaccTime, SaccPos}
%
% load all blocks of eardata for any participant S
% this function appends all loaded data blocks, so make sure you only load
% blocks of the same experiment with each function call!
%

% which experiment should be loaded
ExpPart = ARG_event.exp;
if ~ischar(ExpPart)
    ExpPart = char(string(ExpPart));
end

filedir = dir([datapath, '*.mat']);
nblocks = 4; % we expect 4 blocks

% Load matlab log
% get subject data
Sname = [ExpPart '_' SubList(S).name '_B']; % exclude the SaccParam
subdir = contains({filedir.name},Sname);
subdir = filedir(subdir);
% remember missing data
missingflag = false(nblocks,1);
for l = 1:nblocks
    % find name for file in that experiment
    fname = ['EyeMov_' ExpPart '_'  SubList(S).name '_B' num2str(l)];
    fileflag = contains({subdir.name},fname);
    % get newest entry (by timestamp in name)
    fnameind = find(fileflag,1,'last');
    
    % try to load data, otherwise mark as missing
    try
        fname = [datapath subdir(fnameind).name];
        tmp = load(fname);
        arg_Matlab{l} = tmp.ARG;
        tmp = removefields(tmp,'ARG');
        tmp.Sequence = arg_Matlab{l}.Sequence;
        events{l} = tmp;
    catch
        missingflag(l) = true;
        warning('one part of log data is missing!');
        continue
    end
end

% --------------------------------------------------------
% check if data is incomplete
if sum(missingflag) == nblocks
    error('Could not load any data for this experiment');
end

% --------------------------------------------------------
% substitute missing data with NaN trials (removed when cleaning trials)
emptystruc = events{find(~missingflag,1)};
% array of same size but filled with NaNs
eventfields = fieldnames(emptystruc);
for ifield = 1:length(eventfields)
    emptystruc.(eventfields{ifield}) = nan(size(emptystruc.(eventfields{ifield})));
end
events(missingflag) = {emptystruc};

% --------------------------------------------------------
% take all events and concatenate fields
% assumes same fieldnames in events cell
events = cell2mat(events);
eventfields = fieldnames(events);
for ifield = 1:length(eventfields)
    % assumes they are all row vectors
    EventLog.(eventfields{ifield}) = cell2mat({events(:).(eventfields{ifield})}');
end
ARG_Matlab = arg_Matlab{find(~missingflag,1)};


end
