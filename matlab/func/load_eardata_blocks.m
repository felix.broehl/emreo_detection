function Ear_data = load_eardata_blocks(S,SubList,datapath,ARG_ear)
% Ear_data = load_eardata_blocks(S,SubList,datapath,ARG_ear)
%
% load all blocks of eardata for any participant S
% this function appends all loaded data blocks, so make sure you only load
% blocks of the same experiment with each function call!
%

% which experiment should be loaded
ExpPart = ARG_ear.exp;
if ~ischar(ExpPart)
    ExpPart = char(string(ExpPart));
end

filedir = dir([datapath, '*.bdf']);
nblocks = 4; % we expect 4 blocks

% Load Ear data
iter = 1;
% get subject data
Sname = [SubList(S).name '_' ExpPart];
subdir = contains({filedir.name},Sname);
subdir = filedir(subdir);
% remeber missing data
missingflag = false(nblocks,1);
for l = 1:length(subdir)
    fname = [SubList(S).name '_' ExpPart '_B' num2str(l)]
    fileflag = contains({subdir.name},fname);
    % get newest entry (by timestamp in name)
    fnameind = find(fileflag,1,'last');
    
    % try to load data, otherwise mark as missing
    try
        fname = [datapath subdir(fnameind).name];
        data{l} = get_eardata(fname,ARG_ear);
        fsample{l} = data{l}.fsample;
        % z score withn each trial
        if ARG_ear.zscore 
            data{l}.trial = cellfun(@(x) (x-mean(x(:))) ./ std(x(:)), data{l}.trial,'UniformOutput',false);
        end
    catch
        missingflag(l) = true;
        warning('one part of ear data is missing!');
        continue
    end
end 

% check if data is incomplete
if sum(missingflag) == nblocks
    error('Could not load any data for this experiment');
end

% substitue missing data with NaN trials (removed when cleaning trials)
emptystruc = data{find(~missingflag,1)};
% array of same size but filled with NaNs
emptystruc.trial = repmat({nan(size(emptystruc.trial{1}))}, [1,length(emptystruc.trial)]);
data(missingflag) = repmat({emptystruc}, [1,sum(missingflag)]);

% concatenate blocks
Ear_data = ft_appenddata([],data{:});

% check for consistent sample rate
if length(unique([fsample{:}])) > 1
    error('inconsistent sample rate!');
end
Ear_data.fsample = fsample{1};



end
