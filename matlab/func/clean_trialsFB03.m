function [Analysis_log,Ear_data,Eye_data,Stats] = clean_trialsFB03(cfg,Analysis_log,Ear_data,Eye_data)
%
% [Analysis_log,Ear_data,Eye_data,Stats] = clean_trials(Analysis_log,Ear_data,Eye_data)
%
% clean trials based on fixed parameters
% designed for FB03 experiment data
%
% exclusion criteria could also be stated with the cfg structure
%

% set parameters from global parameters
globalP = FB03_AnalysisParameters;

% define trial exclusion criteria
RT_limit = globalP.RT_limit; % 0.5
Ear_SD = globalP.Ear_SD; % 2.5
Ear_SD2 = globalP.Ear_SD2; % 3.0
Fix_SD = globalP.Fix_SD; % 3.0
Pos_Max = globalP.Pos_Max; % 16 degrees

% screen center
centerpoint = globalP.centerpoint ./ globalP.ppd;

% set default parameters
cfg = checkarg(cfg,'fsample',1000);
cfg = checkarg(cfg,'window',[-0.15 0.2]);
cfg = checkarg(cfg,'TFR',false);
cfg = checkarg(cfg,'TFR_window',[-0.6 0.8]);

% convert time windows to ms
cfg.window = cfg.window .* 1000; 
cfg.TFR_window = cfg.TFR_window .* 1000;

% get number of trials to start with
ntrial = length(Ear_data.trial);

% put ear data into matrix
Ear_data2 = sq(eegck_trials2mat(Ear_data));


% get time vector
tax = Ear_data.time{1} * 1000; % cfg.fsample; 
rate = median(diff(tax));
win = [round(cfg.window(1)/rate):round(cfg.window(2)/rate)];
tax_ear = win*rate;
% find time zero in vector
[~,o] = min(abs(tax));

% tfr time vector
win_tfr = [round(cfg.TFR_window(1)/rate):round(cfg.TFR_window(2)/rate)];
tax_tfr = win_tfr*rate;
% find time zero in this vector as well
[~,o_tfr] = min(abs(tax_tfr));

% allocate variable
ALL_EAR_ONSET =  nan(ntrial,length(win));
ALL_EAR_OFFSET = nan(ntrial,length(win));
ALL_EAR_TFR = nan(ntrial,length(win_tfr));

% go through all trials and find onset in ear data
for t = 2:ntrial
    onset = Analysis_log.TimingLog(t,3);
    offset = Analysis_log.TimingLog(t,4); % using this alignes ear data to saccade offset
    % skip trial if reaction time of saccade is too slow
    if onset > RT_limit*cfg.fsample
        continue
    end
    % this also filters out missing ET data
    % onset bigger than 50 ms and smaller than 300 ms from data matrix end
    if ~isnan(onset) && (onset>0.05*cfg.fsample) && (onset < (size(Ear_data2,2)-0.3*cfg.fsample))
        % shift trigger onset relative to saccade onset
        
        % slice ear data
        ALL_EAR_ONSET(t,:) = Ear_data2(t,win + o + onset);
        % slice data for TFR matrix
        if cfg.TFR
            try
                % get data from matrix, sometimes failes because win_tfr is
                % much larger than win
                ALL_EAR_TFR(t,:) = Ear_data2(t,win_tfr + o + onset);
            catch
                % if does not work, skip this trial, will be excluded
                % anyways!
                continue
            end
        end
    end
    % same for aligning with the offset
    if ~isnan(offset) && (offset>0.05*cfg.fsample) && (offset < (size(Ear_data2,2)-0.3*cfg.fsample))
        ALL_EAR_OFFSET(t,:) = Ear_data2(t,win + o + offset);
    end
end


% ----------------------------------------------------------------------
% EAR DATA
% find bad trials and remove them 

% determine good trials, based on Saccade RT
bad_rt = (Analysis_log.TimingLog(:,3) > RT_limit*cfg.fsample);

% bad trial SD
max_amp = Ear_SD * std(ALL_EAR_ONSET(:),'omitnan');
trial_sd = std(ALL_EAR_ONSET,[],2);
bad_sd = (trial_sd > max_amp);

% bad signal SD in at least three samples
trial_sample_sd = (ALL_EAR_ONSET-mean(ALL_EAR_ONSET(:)))./std(ALL_EAR_ONSET(:));
bad_sd2 = (sum(abs(trial_sample_sd) > Ear_SD2,2) > 3); % less than 3 samples with SD > 6

% find trials with NaN values in ear data
bad_nan = isnan(ALL_EAR_ONSET(:,1));

% find missing trials in EventLog (in subject 09) or ET data
bad_seq = (Analysis_log.Condition == 0 | isnan(Analysis_log.Condition));


% ------------------------------------------
% EYE DATA
% bad saccades based on bad fixation or bad position during saccades
% tranform condition label to distinguish intended saccade end position
% based on Gruters paper, ipsilateral is negative, contralateral positive
degrees = [ 6 -6 12 -12]; % index of entries correspond to condition labels
SaccPosCondition = Analysis_log.Condition;
SaccPosCondition(SaccPosCondition > 8) = SaccPosCondition(SaccPosCondition > 8) - 8;
SaccPosCondition(SaccPosCondition > 4) = SaccPosCondition(SaccPosCondition > 4) - 4;
Analysis_log.SaccPosCondition = SaccPosCondition;

% define time window for expected saccade
tax_eye = [Eye_data.Window(1):1/Ear_data.fsample:Eye_data.Window(2)-1/Ear_data.fsample] .* 1000;
eye_bound = [-20 50]; % window in ms where we want to exclude outlier trials
eye_win = ( tax_eye > eye_bound(1) & tax_eye < eye_bound(2) );

% convert trial data into matrix
Sacc_trial = cellfun(@(x) reshape(x,[1,1,size(x)]), Eye_data.Sacc.trial, 'UniformOutput',false);
Sacc_trial = sq(cell2mat(Sacc_trial));


% skip bad trials, find amplitude of all other saccades
onset_offset = Analysis_log.TimingLog(:,[1,2]);
nantime = any(isnan(onset_offset),2);
onset_offset(nantime,:) = nanmean(onset_offset(:,1)); % fill with index at time zero
onset_offset(bad_rt,:) = nanmean(onset_offset(:,1));
% skip trial where saccades lie outside of the window
ndtime = size(Sacc_trial,3);
bad_win = onset_offset(:,2) > ndtime;
onset_offset(bad_win,:) = nanmean(onset_offset(:,1));

% only consider x and y movement dimension
Sacc_trial = Sacc_trial(:,[2,3],:);
% get through trials and find start and end position
for itrial = 1:ntrial
    Pos0(itrial,:) = Sacc_trial(itrial,:,onset_offset(itrial,1));
    Pos1(itrial,:) = Sacc_trial(itrial,:,onset_offset(itrial,2));
end

% find bad starting position
mFix = nanmean(Pos0(:,1)); 
sFix = std(Pos0(:,1),'omitnan');
FixationBoundary = [ mFix - Fix_SD * sFix, mFix + Fix_SD * sFix ];
bad_fix = (Pos0(:,1) < FixationBoundary(1) | Pos0(:,1) > FixationBoundary(2));

% find trials with excessive positions during saccades
for itrial = 1:ntrial
    ma = max(abs(sq(Sacc_trial(itrial,:,eye_win))),[],2);
    MaxPos(itrial,:) = ma - centerpoint';
end
% find trials with positions outside 16 degrees
bad_pos = any(MaxPos > Pos_Max,2);

% exclude any trial where NaN are on saccade boundaries, regression on
% saccade pos won't work otherwise
bad_eye = any( isnan([Pos0 Pos1]),2 );


% --------------------------------------------------------
% flag bad trials
good_trials = true(1,ntrial);
good_trials(1) = false;  
good_trials(bad_rt) = false;
good_trials(bad_sd) = false;
good_trials(bad_sd2) = false;
good_trials(bad_nan) = false;
good_trials(bad_seq) = false;
good_trials(bad_pos) = false;
good_trials(bad_fix) = false;
good_trials(bad_win) = false;
good_trials(bad_eye) = false;

Ngood_trials = sum(good_trials);


% --------------------------------------------------------
% clean data 
% select good trials in EAR data
ALL_EAR_ONSET = ALL_EAR_ONSET(good_trials,:);
ALL_EAR_OFFSET = ALL_EAR_OFFSET(good_trials,:);
ALL_EAR_TFR = ALL_EAR_TFR(good_trials,:);

% subtract trial-wise mean 
ALL_EAR_ONSET = ALL_EAR_ONSET - mean(ALL_EAR_ONSET,2);
ALL_EAR_OFFSET = ALL_EAR_OFFSET - mean(ALL_EAR_OFFSET,2);
ALL_EAR_TFR = ALL_EAR_TFR - mean(ALL_EAR_TFR,2);


% save to ouput variable
clear Ear_data
Ear_data.ALL_EAR_ONSET = ALL_EAR_ONSET;
Ear_data.ALL_EAR_OFFSET = ALL_EAR_OFFSET;
Ear_data.tax_ear = tax_ear;
% output tfr data if desired
if cfg.TFR
    Ear_data.ALL_EAR_TFR = ALL_EAR_TFR;
    Ear_data.tax_tfr = tax_tfr;
end

% clean trials in all fields
afnames = fieldnames(Analysis_log);
for ii = 1:length(afnames)
    Analysis_log.(afnames{ii}) = Analysis_log.(afnames{ii})(good_trials,:);
end
% add unaltered degrees description for condition labels
Analysis_log.Degrees = degrees;

% clean ET data
Eye_data.Sacc.trial = Eye_data.Sacc.trial(good_trials);
Eye_data.Sacc.time = Eye_data.Sacc.time(good_trials);
% clean TimingLog
Eye_data.TimingLog = Eye_data.TimingLog(good_trials,:);


% --------------------------------------------------------
% save indices of removed trials 
Stats.bad_rt = bad_rt';
Stats.bad_sd = bad_sd';
Stats.bad_sd2 = bad_sd2';
Stats.bad_nan = bad_nan';
Stats.bad_seq = bad_seq';
Stats.bad_pos = bad_pos';
Stats.bad_fix = bad_fix';
Stats.bad_win = bad_win';
Stats.bad_eye = bad_eye';
%Stats.bad_vel = bad_vel';
%Stats.bad_acc = bad_acc';
Stats.ntrial = ntrial;
Stats.good_trials = good_trials;
Stats.Ngood_trials = Ngood_trials;


end
