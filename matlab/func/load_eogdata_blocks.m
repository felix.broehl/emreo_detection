function EOG_data = load_eogdata_blocks(S,SubList,datapath,ARG_eog)
% EOG_data = load_eogdata_blocks(S,SubList,datapath,ARG_eog)
%
% load all blocks of eardata for any participant S
% this function appends all loaded data blocks, so make sure you only load
% blocks of the same experiment with each function call!
%

% which experiment should be loaded
ExpPart = ARG_eog.exp;
if ~ischar(ExpPart)
    ExpPart = char(string(ExpPart));
end

filedir = dir([datapath, '*.bdf']);

% Load EOG data
iter = 1;
% get subject data
Sname = [SubList(S).name '_' ExpPart];
subdir = contains({filedir.name},Sname);
subdir = filedir(subdir);
for l = 1:length(subdir)
    fname = [datapath subdir(l).name]
    data = get_eogdata(fname,ARG_eog);
    if iter==1
        eog_data = data;
    else
        eog_data = ft_appenddata([],eog_data,data);
    end
    iter = iter + 1;
end
EOG_data = eog_data;


end
