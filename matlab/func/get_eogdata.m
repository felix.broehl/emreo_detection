function Eog_data = get_eogdata(FILENAME,ARG)


%----------------------------------------------------------
% params
trialdef.prestim = -0.2; % Trial definition
trialdef.poststim = 1.2; % sec
Trig_range = [1:255]; % triggers to look for (trials). Paradimg specific


%------------------------------------------------------
% define trials
fprintf('\n Processing %s \n',FILENAME);
cfg = [];
cfg.dataset     = FILENAME;
event = ft_read_event(cfg.dataset);
hdr   = ft_read_header(cfg.dataset);

eventS = ft_filter_event(event, 'type','STATUS');
val =[]; t_onset =[];t_onset_sound=[];
c=1;
for k=1:length(eventS)
  if sum(eventS(k).value==Trig_range) && (eventS(k).sample>500)
    val(c) = eventS(k).value;
    t_onset(c) = eventS(k).sample;
    c=c+1;
  end
end

%-------------------------------------------------------------------------------
% create fieldtrip trial structure - make sure to onset the onset
trl =[];
for t=1:length(t_onset)
  ts =t_onset(t); % use precise stim onset
  begsample     = ts + trialdef.prestim*hdr.Fs;
  endsample     = ts + trialdef.poststim*hdr.Fs - 1;
  offset        = trialdef.prestim*hdr.Fs;
  trl(t,:) = round([begsample endsample offset val(t)]);
end
Good_trls = find(trl(:,2)<hdr.nSamples );
trl = trl(Good_trls,:);

%-------------------------------------------------------------------------------
% data processing and loading
cfg = [];
cfg.dataset     = FILENAME;
cfg.trl = trl;
cfg.demean     = 'no';
cfg.detrend = 'no';
cfg.polyremoval   = 'no';
cfg.lpfilter   = ARG.lpfilter;                              % apply lowpass filter
cfg.lpfiltdir = 'twopass';    % apply highpass filter
cfg.lpfreq     = ARG.lpfreq;

cfg.hpfilter   = ARG.hpfilter;  
cfg.hpfreq     = ARG.hpfreq;
cfg.hpfiltord = 4;
cfg.hpfiltdir = 'twopass';    % apply highpass filter

cfg.reref         = 'no';                      % referencing
cfg.continuous = 'yes';
data = ft_preprocessing(cfg);


%-------------------------------------------------------------------------------
% select relevant channel and convert to matrix
cfg=[];
cfg.channel = 'EXG*';
Eog_data = ft_selectdata(cfg,data);


% VEOG as difference between (EX3 - EX1) and (EX4-EX2)
% HEOG as difference EX3-EX4
for t=1:length(Eog_data.trial)
  veog = (Eog_data.trial{t}(3,:)- Eog_data.trial{t}(1,:))+(Eog_data.trial{t}(4,:)- Eog_data.trial{t}(2,:));
  veog = veog/2;
  heog = (Eog_data.trial{t}(3,:)- Eog_data.trial{t}(4,:));
  %  Eog_data.trial{t} = [ck_sig2z(veog);ck_sig2z(heog);;ck_sig2z(Eog_data.trial{t}(1,:));ck_sig2z(Eog_data.trial{t}(2,:));ck_sig2z(Eog_data.trial{t}(3,:));ck_sig2z(Eog_data.trial{t}(4,:))];
  Eog_data.trial{t} = [(veog);(heog);(Eog_data.trial{t}(1,:));(Eog_data.trial{t}(2,:));(Eog_data.trial{t}(3,:));(Eog_data.trial{t}(4,:))];
end
Eog_data.label ={'VEOG','HEOG','EXG1' 'EXG2' 'EXG3' 'EXG4'};
