function History = Analyze_SummaryDataset(goodsubs)
%
% load excel table and return stats about participants
%

% insert path
USECOMP = 3;
defdirs;

% set paramters
exclude = [1:8]; % exclude first 9 participants - they were for testing only

% load excel data
tablepath = 'C:\Users\fbroehl\Documents\FB03\';
tablename = 'participants_FB03.xlsx';
filename = [tablepath tablename];

% load and modify table
sub_table = readtable(filename);
sub_table(exclude,:) = [];

% recorded participants
History.Nrecorded = size(sub_table,1);
History.Ngoodsubs = length(goodsubs);
% number excluded, based on experiment 
excluded = table2array(sub_table(:,5));
excluded = ~cellfun(@isempty, excluded);
History.excluded = find(excluded);
History.Nexcluded = sum(excluded);

% Age
Age = table2array(sub_table(:,3));
History.meanAge = mean(Age);
History.stdAge = std(Age);

% Sex
Sex = table2array(sub_table(:,4));
sexclass = 'mwd';
for isex = 1:length(sexclass)
    Nsexclass(isex) = sum(ismember(Sex,sexclass(isex)));
end
History.Nsexclass = Nsexclass;
History.sexclass = sexclass;

% print history
fprintf(repmat('\n',1,4));
fprintf('Summary of dataset: \n');
disp(History);


% load GroupStats data and create table with n excluded trials
filename = sprintf([finalfigurepath 'GroupStats_FB03.mat']);
GroupStats = load(filename,'Ratio');
fprintf(repmat('\n',1,4));
fprintf('N trials excluded by participants: \n');
disp(GroupStats.Ratio.SubCriterion);
fprintf('N trials excluded Mean and Std: \n');
disp(GroupStats.Ratio.AggregateCriterion);
fprintf('\n');
fprintf('on average we excluded %.2f +- %.2f percent of trials \n',...
    mean(GroupStats.Ratio.Ngood_trials)*100,std(GroupStats.Ratio.Ngood_trials)*100);



end
